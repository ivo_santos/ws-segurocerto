ALTER TABLE `ws_segurocerto`.`cotacoes`
  ADD COLUMN `origem` VARCHAR(255) NULL AFTER `ip`,
  ADD COLUMN `parametros_url` VARCHAR(255) NULL AFTER `origem`;
