ALTER TABLE `ws_segurocerto`.`cotacoes`
  ADD COLUMN `data_validade` DATE NULL AFTER `situacao`;

ALTER TABLE `ws_segurocerto`.`vendas`
  CHANGE `data_cadastro` `data_cadastro` DATETIME NULL  AFTER `viu_voucher_em`,
  CHANGE `data_modificacao` `data_modificacao` DATETIME NULL  AFTER `data_cadastro`;


ALTER TABLE `ws_segurocerto`.`vendas`
  DROP COLUMN `viu_voucher`,
  CHANGE `salt` `salt` VARCHAR(20) CHARSET latin1 COLLATE latin1_swedish_ci NULL  AFTER `tipo`,
  CHANGE `braco_id` `braco_id` INT(11) DEFAULT 0  NULL  AFTER `cupom_id`,
  CHANGE `funcionario_id` `funcionario_id` INT(11) NULL  AFTER `forma_pagamento_id`,
  CHANGE `data_pagamento` `data_pagamento` DATE NULL  AFTER `viu_voucher_em`;


