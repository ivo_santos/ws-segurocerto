<?php

class MY_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    //Métodos Mágicos
    public function __call($method, $arguments) {
        $prefix = substr($method, 0, 3);
        $attr = strtolower(substr($method, 3));

        if ($prefix == "get") {
            return $this->$attr;
        } else if ($prefix == "set") {
            $this->$attr = reset($arguments);
            return $this;
        }
    }

}
