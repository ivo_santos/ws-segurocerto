<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

#### DESTINOS
$route['destinos/listar'] = "administracao/destinos/listarTodos";
$route['destinos/listar/(:any)'] = "administracao/destinos/listarUm/$1";

#### DESTINOS GRUPOS
$route['grupos/listar'] = "administracao/Destinos_grupos/listarTodos";
$route['grupos/listar/(:any)'] = "administracao/Destinos_grupos/listarUm/$1";
$route['grupos/adicionar'] = "administracao/Destinos_grupos/adicionar";
$route['grupos/alterar'] = "administracao/Destinos_grupos/alterar";
$route['grupos/remover/(:any)'] = "administracao/Destinos_grupos/remover/$1";

#### COTAÇÃO
$route['cotacao/adicionar'] = "cotacao/cotacoes/adicionar";
$route['cotacao/alterar'] = "cotacao/cotacoes/alterar";
$route['cotacao/remover/(:any)'] = "cotacao/cotacoes/remover/$1";
$route['cotacao/listar/(:any)'] = "cotacao/cotacoes/listarUm/$1";
$route['cotacao/gerar/(:any)'] = "cotacao/cotacoes/gerar/$1";
$route['cotacao/listar-todas-nao-venda'] = "cotacao/cotacoes/listarTodosNaoVenda";

#### COTAÇÃO POR EMAIL
$route['cotacao/enviar-para-meu-email'] = "cotacao/cotacoes/enviarParaMeuEmail";
$route['cotacao/enviar-para-amigo'] = "cotacao/cotacoes/enviarParaAmigo";
$route['cotacao/por-email/(:any)'] = "cotacao/cotacoes/listarUmCotacaoPorEmail/$1";

$route['cotacao/historico/adicionar'] = "cotacao/cotacoes_historicos/adicionar";
$route['cotacao/historico/alterar'] = "cotacao/cotacoes_historicos/alterar";
$route['cotacao/historico/listar-por-cotacao-id/(:any)'] = "cotacao/cotacoes_historicos/listarTodosPorCotacaoId/$1";
$route['cotacao/historico/listar/(:any)'] = "cotacao/cotacoes_historicos/listarUm/$1";

#### PLANOS
$route['planos/listar/(:any)'] = "cotacao/planos/listarUm/$1";

#### CLIENTES
$route['clientes/listar'] = "administracao/clientes/listarTodos";
$route['clientes/listar/(:any)'] = "administracao/clientes/listarUm/$1";
$route['clientes/adicionar'] = "administracao/clientes/adicionar";
$route['clientes/alterar'] = "administracao/clientes/alterar";
$route['clientes/remover/(:any)'] = "administracao/clientes/remover/$1";
$route['clientes/por-documento'] = "administracao/clientes/listarUmPorDocumento";
$route['clientes/por-venda-id/(:any)'] = "administracao/clientes/listarTodosPorVendaId/$1";
$route['clientes/com-vouchers-por-venda-id/(:any)'] = "administracao/clientes/listarTodosComVouchersPorVendaId/$1";

#### BOLETOS
$route['boletos/listar/(:any)'] = "vendas/boletos/listarUm/$1";
$route['boletos/ver/(:any)'] = "vendas/boletos/ver/$1";

#### VENDAS
$route['vendas/resumo/(:any)/(:num)'] = "vendas/resumos/gerar/$1/$2";
$route['vendas/pre-venda'] = "vendas/vendas/preVenda";
$route['vendas/alterar'] = "vendas/vendas/alterar";
$route['vendas/pre-venda/atualizar-cotacao/(:any)'] = "vendas/vendas/atualizarCotacao/$1";
$route['vendas/listar'] = "vendas/vendas/listarTodos";
$route['vendas/listar/(:any)'] = "vendas/vendas/listarUm/$1";
$route['vendas/setar-pagamento'] = "vendas/vendas/setarPagamento";
$route['vendas/salvar-clientes-em-uma-venda'] = "vendas/vendas/salvarClientesEmUmaVenda";
$route['vendas/remover-cliente-em-uma-venda'] = "vendas/vendas/removerClienteEmUmaVenda";
$route['vendas/cancelar'] = "vendas/vendas/cancelar";
$route['vendas/por-credenciais'] = "vendas/vendas/listarUmPorCredenciais";
$route['vendas/processar-por-boleto'] = "vendas/boletos/vendaPorBoleto";
$route['vendas/processar-por-deposito'] = "vendas/depositos/vendaPorDeposito";
$route['vendas/processar-por-cartao'] = "vendas/cartoes/vendaPorCartao";
$route['vendas/pre-processar-por-debito'] = "vendas/debitos/preProcessar";
$route['vendas/processar-por-debito/(:any)'] = "vendas/debitos/processar/$1";
$route['vendas/pixel/(:any)'] = "vendas/pixels/visualizou/$1";

$route['vendas/por-cotacao-id/(:any)'] = "vendas/vendas/listarUmPorCotacaoId/$1";

#### CUPONS
$route['cupons/listar'] = "vendas/Cupons/listarTodos";
$route['cupons/listar/(:any)'] = "vendas/Cupons/listarUm/$1";
$route['cupons/adicionar'] = "vendas/Cupons/adicionar";
$route['cupons/alterar'] = "vendas/Cupons/alterar";
$route['cupons/remover/(:any)'] = "vendas/Cupons/remover/$1";

$route['cupons/listar-por-codigo/(:any)'] = "vendas/Cupons/listarUmPorCodigo/$1";


$route['cupons/aplicar-cupom'] = "vendas/Cupons/aplicarCupom";
$route['cupons/ignorar-cupom'] = "vendas/Cupons/ignorarCupom";

#### VOUCHER
$route['voucher/online/(:any)/(:any)'] = "vouchers/vouchers/online/$1/$2";
$route['voucher/download/(:any)/(:any)'] = "vouchers/vouchers/download/$1/$2";

#### ENDEREÇOS
$route['enderecos/listar-um-robo-correios/(:any)'] = "endereco/enderecos/roboCorreios/$1";
$route['enderecos/listar-um-por-venda-id/(:any)'] = "endereco/enderecos/listarUmPorVendaId/$1";

#### EMPRESA
$route['empresas/listar/(:any)'] = "administracao/empresas/listarUm/$1";

#### METAS MENSAIS
$route['metas/listar'] = "vendas/metas_mensais/listarTodos";
$route['metas/listar/(:any)'] = "vendas/metas_mensais/listarUm/$1";
$route['metas/adicionar'] = "vendas/metas_mensais/adicionar";
$route['metas/alterar'] = "vendas/metas_mensais/alterar";
$route['metas/remover/(:any)'] = "vendas/metas_mensais/remover/$1";

#### BRAÇOS
$route['bracos/listar'] = "vendas/bracos/listarTodos";
$route['bracos/listar/(:any)'] = "vendas/bracos/listarUm/$1";
$route['bracos/adicionar'] = "vendas/bracos/adicionar";
$route['bracos/alterar'] = "vendas/bracos/alterar";
$route['bracos/remover/(:any)'] = "vendas/bracos/remover/$1";

####  CMS
$route['cms/listar'] = "cms/cms/listarTodos";
$route['cms/listar/(:any)'] = "cms/cms/listarUm/$1";
$route['cms/adicionar'] = "cms/cms/adicionar";
$route['cms/alterar'] = "cms/cms/alterar";
$route['cms/remover/(:any)'] = "cms/cms/remover/$1";
$route['cms/listar-por-url/(:any)'] = "cms/cms/listarUmPorUrl/$1";

####  CMS EMAILS
$route['emails/listar'] = "cms/Cms_emails/listarTodos";
$route['emails/listar/(:any)'] = "cms/Cms_emails/listarUm/$1";
$route['emails/adicionar'] = "cms/Cms_emails/adicionar";
$route['emails/alterar'] = "cms/Cms_emails/alterar";
$route['emails/remover/(:any)'] = "cms/Cms_emails/remover/$1";

## CMS ENVIO
$route['emails/enviar/para-cotacao'] = "cms/Cms_emails/enviarParaCotacao";
$route['emails/enviar/para-venda'] = "cms/Cms_emails/enviarParaVenda";

## RASTREIO DE ABERTURA DE EMAIL ENVIADO PELO ERP.
$route['emails/abertura/(:any)'] = "cms/Cms_emails/abertura/$1";

#### FAIXAS DE IDADE
$route['faixas-idade/listar'] = "administracao/faixas_idade/listarTodos";
$route['faixas-idade/listar/(:any)'] = "administracao/faixas_idade/listarUm/$1";

#### TIPOS DE VIAGEM
$route['tipos-viagem/listar'] = "administracao/tipos_viagem/listarTodos";
$route['tipos-viagem/listar/(:any)'] = "administracao/tipos_viagem/listarUm/$1";

#### MOTIVOS DE VIAGEM
$route['motivos-viagem/listar'] = "administracao/motivos_viagem/listarTodos";
$route['motivos-viagem/listar/(:any)'] = "administracao/motivos_viagem/listarUm/$1";

#### CRON
$route['crons/listar'] = "crons/crons/listarTodos";
$route['crons/emissao-de-vouchers'] = "crons/crons/emitirVouchers";
$route['crons/envio-de-email-com-vouchers'] = "crons/crons/enviarEmailComVouchers";
$route['crons/cancelar-vendas-nao-pagas'] = "crons/crons/cancelarVendasPendentes";
$route['crons/gerar-cache-cotacoes'] = "cotacao/caches/fazerAtualizacao";


/**
 * TODO remover essa url pois o ela mudou de nome.
 */
#### CACHE
$route['cache/fazer-atualizacao'] = "cotacao/caches/fazerAtualizacao";

#### TESTES
$route['sulamerica'] = "testes/sulamericas/index";