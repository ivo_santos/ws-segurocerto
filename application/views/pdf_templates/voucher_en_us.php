<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link rel='stylesheet' type='text/css' href='<?php echo $base_url . 'assets/css_voucher/voucher.css'; ?>'/>
</head>
<body>
<div style="width: 100%; background: #fff;" id="voucher">
	<?php if (isset($online)) { ?>
		<div style="width: 100%;" class="mensagem-download">
			This is your online english voucher, to download it click
			<a href="<?php echo $base_url . 'voucher/download/' . $lang . '/' . $voucher_id_print; ?>" target="_blank" title="Print">here</a>
			<style>
				#voucher {
					margin: 0 auto;
				}

				.mensagem-download {
					font-family: "Montserrat";
					font-size: 15px;
					font-weight: normal;
					background: #d9edf7;
					border: 1px solid #bce8f1;
					padding: 10px;
					color: #31708f;
					margin-bottom: 15px;
				}

				.mensagem-download a {
					font-size: 16px;
					color: #31708f;
					font-weight: bold;
					line-height: 20px;
				}
			</style>
		</div>
	<?php } ?>
	<div style="width: 100%;">
		<h1>Travel insurance certificate</h1>
	</div>
	<div style="width: 100%;">
		<table class="table table-12" border="0">
			<tr>
				<td class="col-4" style="text-align: left;">
					<img width="200" src="<?php echo $base_url . 'assets/css_voucher/img/segurocerto.png'; ?>" alt="Seguro Certo"/>
				</td>
				<td class="col-4 bloco-voucher-id">
					<h3><span class="h3-destaque">Policy number:</span> <br/><?php echo $bilhete_id; ?></h3>
				</td>
				<td class="col-4" style="text-align: right; padding-right: 15px;">
					<img width="150" src="<?php echo $base_url . 'assets/css_voucher/img/sulamerica.png'; ?>" alt="Sulamérica"/>
				</td>
			</tr>
		</table>
	</div>

	<!--informações do passageiro-->
	<div style="width: 100%; margin-bottom: 10px; " id="passageiro-info">
		<table class="table table-12" border="0" style="margin-bottom: 0px;">
			<tr>
				<td class="col-4" style="text-align: left;">
					<table class="table table-12" border="0">
						<tr>
							<td class="col-12">
								Name: <?php echo $nome; ?>
							</td>
						</tr>
						<tr>
							<td class="col-12">
								Document: <?php echo $documento; ?>
							</td>
						</tr>
						<tr>
							<td class="col-12">
								Date of birth: <?php echo $nascimento; ?>
							</td>
						</tr>
						<tr>
							<td class="col-12">
								E-mail: <?php echo $email; ?>
							</td>
						</tr>
						<tr>
							<td class="col-12">
								Date of issue: <?php echo $data_emissao; ?>
							</td>
						</tr>
						<tr>
							<td class="col-12">
								Lucky number: <?php echo $numero_sorte; ?>
							</td>
						</tr>
					</table>
				</td>
				<td class="col-4" style="text-align: center;">
					<table class="table table-12" border="0">
						<tr>
							<td class="col-12">
								Contracted Plan: <?php echo $plano_nome; ?>
							</td>
						</tr>
						<tr>
							<td class="col-12">
								Total Award: R$ <?php echo $valor_premio; ?>
							</td>
						</tr>
						<tr>
							<td class="col-12">
								Effective beginning: <?php echo $inicio_vigencia; ?>
							</td>
						</tr>
						<tr>
							<td class="col-12">
								End of Term: <?php echo $final_vigencia; ?>
							</td>
						</tr>
						<tr>
							<td class="col-12">
								Origin: BRASIL
							</td>
						</tr>
						<tr>
							<td class="col-12">
								Destiny: <?php echo $destino; ?>
							</td>
						</tr>
					</table>
				</td>

				<td class="col-4" style="text-align: center;">
					<table class="table table-12" border="0">
						<tr>
							<td class="col-12">
								Sul América Seguros de Pessoas e Previdência S.A.
							</td>
						</tr>
						<tr>
							<td class="col-12">
								CNPJ: 01.704.513/0001-46 / SUSEP code 06220
							</td>
						</tr>
						<tr>
							<td class="col-12">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td class="col-12">
								FAN VIAGENS E TURISMO LTDA
							</td>
						</tr>
						<tr>
							<td class="col-12">
								CNPJ: 13.225.630/0001-06
							</td>
						</tr>
						<tr>
							<td class="col-12">
								SEGUROCERTO ID: <?php echo $venda_id; ?>
							</td>
						</tr>
					</table>
				</td>

			</tr>
		</table>
	</div>
	<!--informações do passageiro-->

	<!--informações do contato -->

	<div style="width: 100%;" id="contato">
		<div style="width: 100%;">
			<h4>IN CASE OF EMERGENCY, YOU MUST CALL TO OUR CALL CENTER</h4>
		</div>
		<table class="table table-12 table-bordered" style="margin-bottom: 15px;">
			<tbody>
			<tr>
				<td class="last-td">
					<p class="info-sac">

						Service Center (to trigger insurance or request services): International travel - <span class="tel">+55 11 4126 7463</span> | National travel - <span class="tel">3003 7798</span> (capitals
						And metropolitan areas) and <span class="tel">0800 770 7798</span> (other locations) • Central Services: <span class="tel">3003 9807</span> (capitals and metropolitan areas) | <span class="tel">0800 726 9807</span>
						(Other locations) - Business hours: from Monday to Friday, from 8:00 am to 6:30 pm (except national holidays) • SAC: <span class="tel">0800 725 1894</span> | SAC - Service
						To people with special needs (hearing and speaking): <span class="tel">0800 702 2242</span> • Ombudsman: <span class="tel">0800 725 3374</span>. Website: www.sulamérica.com.br
					</p>
				</td>
			</tr>
			</tbody>
		</table>
	</div>

	<!--informações do contato -->


	<!--coberturas-->
	<div id="coberturas" style="width: 100%;">
		<div style="width: 100%;">
			<h3>Coverage</h3>
		</div>
		<div style="width: 100%">
			<table class="table table-12">
				<thead>
				<tr>
					<td><h4>Services</h4></td>
					<td><h4>Limits</h4></td>
				</tr>
				</thead>
				<tbody>
				<?php if (!empty($coberturas)) { ?>
					<?php foreach ($coberturas as $chave => $cobertura) { ?>
						<tr class="<?php echo (($chave % 2) == 0) ? '' : 'destaque'; ?>">
							<td class="servico">
								<span><?php echo $cobertura->nome; ?></span>
							</td>
							<td class="limite">
								<span><?php echo $cobertura->moeda_limite . ' ' . $cobertura->limite; ?></span>
							</td>
						</tr>
					<?php } ?>
				<?php } ?>
				</tbody>
			</table>
		</div>
	</div>

	<!--coberturas-->

</div>
</body>
</html>