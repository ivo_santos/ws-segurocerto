<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>Apólice de seguro viagem</title>

	<style type="text/css">
		@font-face {
			font-family: "lintel";
			font-weight: 400;
			font-style: normal;
			src: url('/assets/css_voucher/fonts/lintel/Lintel-Regular.ttf') format("truetype");
		}

		body, html {
			margin: 5px 5px;
			background: #fff;
			font-family: lintel;
		}

		#voucher {
			width: 100%;
			max-width: 900px;
		}

		table {
			width: 100%;
		}

		table tr {
			line-height: 10px;
		}

		table tr td {
			margin: 0;
		}

		table tr td p {
			margin: 2px 0 2px 5px;
		}
	</style>
</head>
<body>
<div style="width: 100%; background: #fff;" id="voucher">

	<?php if (isset($online)) { ?>
		<div style="width: 100%;" class="mensagem-download">
			Essa é a seu apólice online em português para fazer o download, clique em
			<a href="<?php echo $base_url . 'voucher/download/' . $lang . '/' . $voucher_id_print; ?>" target="_blank" title="Imprimir">aqui</a>.
			<style>
				#voucher {
					margin: 0 auto;
				}

				.mensagem-download {
					font-family: Arial;
					font-size: 15px;
					background: #d9edf7;
					border: 1px solid #bce8f1;
					padding: 10px;
					color: #31708f;
					margin-bottom: 10px;
					overflow: hidden;
					max-width: 880px;
				}

				.mensagem-download a {
					font-size: 16px;
					color: #31708f;
					font-weight: bold;
					line-height: 20px;
				}
			</style>
		</div>
	<?php } ?>

	<div style="width: 100%;">
		<table border="0" align="center" cellpadding="1" cellspacing="0" bgcolor="#ffffff" style="color: #1d2e6c; font-weight: normal !important; font-size: 11px; line-height: 10px;">
			<tbody>
			<tr>
				<td align="center"><strong style="font-size: 13px;">APÓLICE INDIVIDUAL DE SEGUROS DE PESSOAS</strong>
				</td>
			</tr>
			<tr>
				<td align="center" valign="middle">
					<strong style="font-size: 13px;">SULAMERICA VIAGEM INDIVIDUAL - PROCESSO SUSEP N° 15414.901176/2015-81</strong><br>
					<div>
						<div></div>
					</div>
				</td>
			</tr>
			<tr>
				<td align="center" valign="middle"><strong>DADOS DO SEGURO</strong></td>
			</tr>
			<tr>
				<td>
					<table border="0" cellspacing="0" cellpadding="2" style="background: #e3e1e1;">
						<tbody>
						<tr>
							<td style="">N° DA APÓLICE: <?php echo $bilhete_id; ?></td>
							<td style="">DATA DE EMISSÃO DA APÓLICE: <?php echo $data_emissao; ?></td>
							<td style="">PERÍODO DE VIGÊNCIA DO SEGURO:</td>
						</tr>
						<tr>
							<td style="">N° DA PROPOSTA: <?php echo $numero_proposta; ?></td>
							<td style="">DATA DE CONTRATAÇÃO: <?php echo $data_emissao; ?></td>
							<td style=""><?php echo $inicio_vigencia; ?> até <?php echo $final_vigencia; ?></td>
						</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td align="center" valign="middle" style="font-size:11px; text-align: left;">
					<table border="0" cellspacing="0" cellpadding="0">
						<tbody>
						<tr>
							<td>
								<table border="0" cellspacing="0" cellpadding="2" style="background: #e3e1e1; margin-top: 3px;">
									<tbody>
									<tr>
										<td style="border-right: 1px solid #1d2e6c;">SEGURO</td>
										<td style="">GRUPO/RAMO: Pessoas</td>
									</tr>
									<tr>
										<td style="border-right: 1px solid #1d2e6c;">VIAGEM</td>
										<td style="">CÓDIGO DO RAMO: 69</td>
									</tr>
									</tbody>
								</table>
							</td>
							<td>
								<table border="0" cellspacing="0" cellpadding="2" style="background: #e3e1e1; margin-top: 3px; border-left: 3px solid #FFF">
									<tbody>
									<tr>
										<td style="border-right: 1px solid #1d2e6c; border-left: 2px solid  #e3e1e1;">DADOS DA</td>
										<td style="">ORIGEM DA VIAGEM:</td>
										<td style=""><?php echo $origem; ?></td>
										<td style="">PERÍODO DA VIAGEM:</td>
									</tr>
									<tr>
										<td style="border-right: 1px solid #1d2e6c; border-left: 2px solid  #e3e1e1;">VIAGEM</td>
										<td style="">DESTINO DA VIAGEM:</td>
										<td style=""><?php echo $destino; ?></td>
										<td style=""><?php echo $inicio_vigencia; ?> até <?php echo $final_vigencia; ?></td>
									</tr>
									</tbody>
								</table>
							</td>
						</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td align="center" valign="middle" style="font-size: 11px; text-align: left;">Independentemente do prazo de vigência da apólice, deverão ser respeitados os limites de idade especificados por cobertura, na Condição Geral, quando houver.</td>
			</tr>
			<tr>
				<td>
					<table border="0" cellspacing="0" cellpadding="5" style="background: #e3e1e1;">
						<tbody>
						<tr>
							<td align="center" valign="middle" style=""><strong>DADOS DO SEGURADO</strong></td>
						</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table border="0" cellspacing="0" cellpadding="4">
						<tbody>
						<tr>
							<td bgcolor="#feecdf"><strong>Nome do Segurado</strong><br>
								<?php echo $nome; ?>
							</td>
							<td bgcolor="#feecdf"><strong>CPF</strong><br>
								<?php echo $documento; ?>
							</td>
							<td bgcolor="#feecdf"><strong>Data de Nascimento</strong><br>
								<?php echo $nascimento; ?>
							</td>
						</tr>
						</tbody>
					</table>
					<table border="0" cellspacing="0" cellpadding="4">
						<tbody>
						<tr>
							<td bgcolor="#feecdf"><strong>N° do Passaporte</strong><br> -</td>
							<td bgcolor="#feecdf"><strong>Pais de Expedição</strong><br> -</td>
							<td bgcolor="#feecdf"><strong>Endereço</strong><br>
								<?php echo $endereco['logradouro']; ?>, <?php echo $endereco['numero']; ?> <?php echo $endereco['complemento']; ?> <?php echo $endereco['cidade']; ?>/<?php echo $endereco['estado']; ?>
							</td>
						</tr>
						</tbody>
					</table>
					<table border="0" cellspacing="0" cellpadding="5" style="background: #e3e1e1; margin-top: 5px;">
						<tbody>
						<tr>
							<td align="center" valign="middle" style=""><strong>COBERTURAS</strong></td>
						</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table border="0" cellspacing="0" cellpadding="2" style="background: #feecdf; line-height: 15px;">
						<tbody>
						<tr>
							<td bgcolor="#feecdf"><strong>Coberturas</strong><br></td>
							<td bgcolor="#feecdf"><strong>Vigência da cobertura</strong><br></td>
							<td bgcolor="#feecdf"><strong>Capitais Segurados<br>
								</strong>
							</td>
							<td bgcolor="#feecdf"><strong>Franquia</strong></td>
							<td bgcolor="#feecdf"><strong>Carência</strong></td>
							<td bgcolor="#feecdf"><strong>Valor do Prêmio</strong></td>
						</tr>
						<?php if (!empty($coberturas)) { ?>
							<?php foreach ($coberturas as $cobertura) { ?>
								<tr>
									<td bgcolor="#feecdf">
										<p><?php echo $cobertura['nome']; ?></p>
									</td>
									<td bgcolor="#feecdf"><?php echo $inicio_vigencia; ?> - <?php echo $final_vigencia; ?></td>
									<td bgcolor="#feecdf"><?php echo $cobertura['moeda_limite']; ?> <?php echo $cobertura['limite']; ?></td>
									<td bgcolor="#feecdf">-</td>
									<td bgcolor="#feecdf">-</td>
									<td bgcolor="#feecdf">R$ <?php echo $cobertura['premio']; ?></td>
								</tr>
							<?php } ?>
						<?php } ?>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td align="center" valign="middle" style="font-size: 11px; text-align: left;">As regras estabelecidas para cada cobertura estão devidamente definidas nas Condições Gerais desta apólice.</td>
			</tr>
			<tr>
				<td align="center" valign="middle">
					<table border="0" cellspacing="0" cellpadding="5" style="background: #e3e1e1;">
						<tbody>
						<tr>
							<td align="center" valign="middle" style=""><strong>ASSISTÊNCIAS</strong></td>
						</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td align="center" valign="middle">
					<table border="0" cellspacing="0" cellpadding="4">
						<tbody>
						<tr>
							<td bgcolor="#feecdf" style="line-height: 14px;">
								<?php if (!empty($assistencias)) {
									foreach ($assistencias as $assistencia) {
										echo $assistencia['nome'] . '<br>';
									}
								}
								?>
							</td>
						</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table border="0" cellspacing="0" cellpadding="5" style="background: #e3e1e1;">
						<tbody>
						<tr>
							<td align="center" valign="middle" style=""><strong>BENEFICIÁRIOS</strong></td>
						</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table border="0" cellspacing="0" cellpadding="4" style="background: #feecdf;">
						<tbody>
						<tr>
							<td width="168" bgcolor="#feecdf">
								<strong>Nome dos Beneficiários</strong>
							</td>
							<td width="137" bgcolor="#feecdf">
								<strong>Grau de Parentesco</strong>
							</td>
							<td width="132" bgcolor="#feecdf">
								<strong>Data de Nascimento</strong>
							</td>
							<td width="79" bgcolor="#feecdf">
								<strong>Participação %</strong>
							</td>
						</tr>
						<tr>
							<td bgcolor="#feecdf">-</td>
							<td bgcolor="#feecdf">-</td>
							<td bgcolor="#feecdf">-</td>
							<td bgcolor="#feecdf">-</td>
						</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td style="font-size:11px;">
					<strong>IMPORTANTE:</strong> Não havendo indicação de beneficiários, a indenização será paga conforme legislação vigente.
				</td>
			</tr>
			<tr>
				<td>
					<table border="0" cellspacing="0" cellpadding="5" style="background: #e3e1e1;">
						<tbody>
						<tr>
							<td align="center" valign="middle" style=""><strong>PRÊMIO - PAGAMENTO</strong></td>
						</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table border="0" cellspacing="0" cellpadding="4" bgcolor="#feecdf">
						<tbody>
						<tr>
							<td>
								<strong>Prêmio Liquido Total</strong>
								<br>
								-
							</td>
							<td>
								<strong>IOF</strong>
								<br>
								R$ <?php echo $iof; ?>
							</td>
							<td>
								<strong>Forma de Pagamento</strong>
								<br>
								<?php echo $forma_pagamento; ?>
							</td>
							<td>
								<strong>Periodicidade de Pagamento</strong>
								<br>
								-
							</td>
						</tr>
						</tbody>
					</table>
					<table border="0" cellspacing="0" cellpadding="4" bgcolor="#feecdf" style="margin-top: 2px;">
						<tbody>
						<tr>
							<td>
								<strong>Quantidade de Parcelas</strong>
								<br>
								-
							</td>
							<td>
								<strong>Valor da Parcela</strong>
								<br>
								-
							</td>
							<td>
								<strong>Prêmio Total</strong><br>
								R$ <?php echo $valor_premio; ?>
							</td>
						</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td align="center" valign="middle" style="font-size: 11px; text-align: left;">ATENÇÃO: Este seguro poderá ser cancelado no prazo de 7 (sete) dias, a contar da data de sua contratação, tendo os valores pagos devolvidos integralmente.</td>
			</tr>
			<tr>
				<td align="center" valign="middle">
					<table border="0" cellspacing="0" cellpadding="5" style="background: #e3e1e1;">
						<tbody>
						<tr>
							<td align="center" valign="middle" style=""><strong>DADOS DO CORRETOR</strong></td>
						</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td align="center" valign="middle">
					<table border="0" cellspacing="0" cellpadding="4">
						<tbody>
						<tr>
							<td bgcolor="#feecdf"><strong>Nome do Corretor Líder</strong><br>
								SHARE 1234010
							</td>
							<td bgcolor="#feecdf"><strong>CNPJ / CPF</strong><br>
								00.595.872/0001-40
							</td>
							<td bgcolor="#feecdf"><strong>Telefone</strong><br>
								11 - 87558788
							</td>
							<td bgcolor="#feecdf"><strong>Registro SUSEP</strong><br>
								00001020328419
							</td>
						</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table border="0" cellspacing="0" cellpadding="4">
						<tbody>
						<tr>
							<td bgcolor="#feecdf"><strong>Unidade Produtiva / Sucursal / Inspetoria</strong><br>
								60 - SAO PAULO
							</td>
							<td bgcolor="#feecdf"><strong>Telefone</strong><br>
								11 - 37795000
							</td>
							<td bgcolor="#feecdf"><strong>EV</strong><br>
								2846845
							</td>
							<td bgcolor="#feecdf"><strong>EA</strong><br>
								47320
							</td>
							<td bgcolor="#feecdf"><strong>AA</strong><br>
								25547
							</td>
							<td bgcolor="#feecdf"><strong>AV</strong><br>
								0
							</td>
						</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td style="line-height: 11px; font-size: 9px;"> Sul América Seguros de Pessoas e Previdência S.A., a seguir denominada Seguradora, mediante o recebimento do Prêmio, a fim de garantir o interesse legítimo do Segurado, obriga-se a conceder osbenefícios previstos em Contrato, nas Condições Gerais e Particulares integrantes desta apólice, respeitando sempre as disposições legais aplicáveis. Este seguro é por prazo determinado, tendo a Seguradoraa faculdade de não renovar a apólice na data de vencimento, sem devolução dos prêmios pagos nos termos da apólice. SUSEP – Superintendência de Seguros Privados – Autarquia Federal responsável pelafiscalização, normatização e controle dos mercados de seguro, previdência complementar aberta, capitalização, resseguro e corretagem de seguros. As condições contratuais desse produto protocolizadas pelasociedade junto à SUSEP, poderão ser consultadas no endereço eletrônico www.susep.gov. br, de acordo com o número de processo constante da apólice. SUSEP Central de Atendimento ao Consumidor:0800 021 8484. Em atendimento à Lei 12.741/12, informamos que incidem as alíquotas de 0,65% de PIS/Pasep e de 4% de COFINS sobre os prêmios de seguros, deduzidos do estabelecido em legislaçãoespecífica. Central de Atendimento (para acionar o seguro ou solicitar serviços): Viagens internacionais - +55 11 4126 7463 | Viagens nacionais – 3003 7798 (capitais e regiões metropolitanas) e 0800 770 7798(demais localidades) • Central de Serviços: 3003 9807 (capitais e regiões metropolitanas) | 0800 726 9807 (demais localidades) – Horário de atendimento: de 2a a 6a feira, das 8h às 18h30 (exceto feriadosnacionais) • SAC: 0800 725 1894 | SAC - Atendimento a portadores de necessidades especiais (audição e fala): 0800 702 2242 • Ouvidoria: 0800 725 3374. Site: www.sulamérica.com.br.</td>
			</tr>
			</tbody>
		</table>
		<table border="0" align="center" cellpadding="10" cellspacing="0" bgcolor="#FFF" style="font-size: 9px;">
			<tbody>
			<tr>
				<td>
					<img src="<?php echo $base_url . 'assets/css_voucher/img/assinatura.jpg'; ?>" width="109" height="48" alt=""/>
					<br>
					<strong>Fabiano Lima </strong>
					<br>
					Diretor Técnico Vida e Previdência
				</td>
				<td>Sul América Seguros de Pessoas e Previdência S.A.<br>
					Rua Beatriz Larragoiti Lucas, 121 - Cidade Nova, Rio de Janeiro - RJ - 20211-903<br>
					CNPJ 01.704.513/0001-46
				</td>
				<td align="center" valign="middle">
					<p>
						<img src="<?php echo $base_url . 'assets/css_voucher/img/logo.jpg'; ?>" width="141" height="43" alt=""/>
						<br>
					</p>
				</td>
			</tr>
			</tbody>
		</table>

	</div>
</div>
</body>
</html>
