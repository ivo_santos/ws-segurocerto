<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

	<title></title>
	<style type="text/css">
		body{
			background: #f7f7f7;
		}
		#content td {
			font-family: Helvetica, Arial, sans-serif;
			font-size: 15px;
			line-height: 24px;
			color: #4d4f53;
		}

		#content h2 {
			/*text-transform: uppercase;*/
			color: #8a8a87;
			font-style: normal;
			font-weight: normal;
			font-size: 17px;
			line-height: 24px;
			margin-bottom: 15px;
			margin-top: 0px;
		}

		#content h3 {
			font-family: Helvetica, Arial, sans-serif;
			font-size: 25px;
			line-height: 34px;
			font-weight: bold;
			/*color: #3faacc;*/
			color: #072c73;
			margin-top: 0px;
			margin-bottom: 10px;
			text-align: left;
		}

		#content p a {
			font-weight: bold;
			color: #2066b1;
			text-decoration: underline;
		}

		#content p {
			padding-right: 0px;
			padding-left: 0px;
		}

		.getting-started p {
			font-size: 15px;
			line-height: 20px;
			margin-right: 0px;
			padding-right: 0px;
		}

		.getting-started {
			padding-left: 0;
			padding-right: 0;
		}
		.tabela-conteudo p{
			margin-top:25px;
			margin-bottom:20px;
			text-align:left;
			font-size: 15px;
			color: #4d4f53;
			line-height: 24px;
		}

	</style>
</head>
<body id="archivebody" style="margin:0px;padding:0px">
<table style="width:100%;margin:0px;padding:0px" id="container" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
		<td align="center" style="width:100%; background-color:#f7f7f7">

			<table border="0" align="center" cellpadding="0" cellspacing="0" id="header" style="width:100%; max-width:650px;margin-top:0px;margin-bottom:0px; background: #FFF; border: 1px solid #DDD;">
				<tbody>
				<tr>
					<td>
						<a class="brand" href="https://segurocerto.com" title="">
							<img src="<?php echo $base_url . 'assets/imagens/logos/segurocerto.png'; ?>"  width="236"/>
						</a>
					</td>
				</tr>
				</tbody>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" align="center" id="content" style="width:100%; max-width:650px; margin-top:0px; border: 1px solid #DDD; border-top: 0;">
				<tbody>
				<tr>
					<td align="center" style="background-color:#FFFFFF; border-right: 20px solid #FFF; border-left: 20px solid #FFF;">
						<table cellpadding="0" cellspacing="0" border="0" align="left" style="width: 100%;">
							<tbody>
							<tr>
								<td>
									<table border="0" align="center" cellpadding="0" cellspacing="0" class="responsive-table tabela-conteudo" style=" width:100%;">
										<tbody>
										<tr>
											<td colspan="4">
												<p>
													<?php echo $conteudo; ?>
												</p>
											</td>
										</tr>
										</tbody>
									</table>
								</td>
							</tr>
							</tbody>
						</table>

						<table border="0" align="center" cellpadding="0" cellspacing="0" style="width:100%; margin-top: 15px;">
							<tbody>
							<tr>
								<td align="center">
									<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-top:10px">
										<tbody>
										<tr>
											<td style="padding-top:0px; padding-right:0px" class="getting-started" valign="top">
												<h3 style="color: #f47521; margin-bottom: 0; font-size: 20px;">Atendimento</h3>
												<p style="padding-left:0px; margin-top: 0; text-align:left; font-size: 15px; font-family: Helvetica, Arial, sans-serif; color: #4d4f53; line-height: 24px;">
													Em caso de dúvidas, você pode entrar em contato com a nossa
													central de atendimento
													<br> <span style="color: #f47521;"><strong>0800 591 5107</strong></span> de segunda a sábado das 09:00 às 21:00
													ou atráves do email
													<a style="color: #f47521; text-decoration: none;" href="malito:contato@tripguard.com.br" title="Atendimento">contato@tripguard.com.br</a>.
													<br>
													<br>
												</p>
											</td>
										</tr>
										</tbody>
									</table>
								</td>
							</tr>
							</tbody>
						</table>
					</td>
				</tr>
				</tbody>
			</table>
		</td>
	</tr>
	</tbody>
</table>
</body>
</html>