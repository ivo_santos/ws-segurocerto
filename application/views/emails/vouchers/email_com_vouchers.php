<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <title>Contratação Seguro Viagem</title>
        <style type="text/css">
            #content td {
                font-family: Helvetica, Arial, sans-serif;
                font-size: 15px;
                line-height: 24px;
                color: #4d4f53;
            }

            #content h2 {
                /*text-transform: uppercase;*/
                color: #8a8a87;
                font-style: normal;
                font-weight: normal;
                font-size: 17px;
                line-height: 24px;
                margin-bottom: 15px;
                margin-top: 0px;
            }

            #content h3 {
                font-family: Helvetica, Arial, sans-serif;
                font-size: 25px;
                line-height: 34px;
                font-weight: bold;
                /*color: #3faacc;*/
                color: #072c73;
                margin-top: 0px;
                margin-bottom: 10px;
                text-align: left;
            }

            #content p a {
                font-weight: bold;
                color: #2066b1;
                text-decoration: underline;
            }

            #content p {
                padding-right: 0px;
                padding-left: 0px;
            }

            #footer {
                font-family: Helvetica, Arial, sans-serif;
            }

            #footer a {
                color: #ffffff;
                text-decoration: underline;
            }

            .getting-started p {
                font-size: 15px;
                line-height: 20px;
                margin-right: 0px;
                padding-right: 0px;
            }

            .getting-started {
                /* [disabled]padding-bottom: 0px; */
                padding-left: 15px;
                padding-right: 0px;
            }

            .info-sulamerica{
                width: 100%;
                margin-top: 10px;
            }

            .info-sulamerica td{
                margin: 0;
            }

            .info-sulamerica h4{
                font-size: 15px;
                text-transform: uppercase;
                color: #f47521;
                margin: 5px 5px 5px;
            }

            .info-sulamerica h5{
                font-weight: bold;
                font-size: 14px;
                color: #777;
                margin: 0 5px 0;
            }

            .info-sulamerica p{
                margin: 0 5px;
                font-size: 13px;
            }

            .info-sulamerica h4 span{
                font-size: 13px;
            }

        </style>
    </head>
    <body id="archivebody" style="margin:0px;padding:0px">
        <table style="width:100%;margin:0px;padding:0px" id="container" cellpadding="0" cellspacing="0">
            <tbody>
                <tr>
                    <td align="center" style="width:100%; background-color:#f7f7f7">

                        <table border="0" align="center" cellpadding="0" cellspacing="0" id="header" style="width:100%; max-width:650px;margin-top:0px;margin-bottom:0px; background: #FFF; border: 1px solid #DDD;">
                            <tbody>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <a class="brand" href="https://segurocerto.com" title="" style="margin-left: 20px;">
                                            <img src="<?php echo $base_url . 'vendas/pixel/' . $venda_id; ?>" width="1" height="1" style="display: none;"/>
                                            <img src="<?php echo $base_url . 'assets/imagens/logos/segurocerto.png'; ?>" width="236"/>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>

                        <table cellpadding="0" cellspacing="0" border="0" align="center" id="content" style="width:100%; max-width:650px; margin-top:0px; border: 1px solid #DDD; border-top: 0; border-bottom: 0;">
                            <tbody>
                                <tr>
                                    <td align="center" style="background-color:#FFFFFF; border-right: 20px solid #FFF; border-left: 20px solid #FFF; ">
                                        <table cellpadding="0" cellspacing="0" border="0" align="center" style="border-bottom: 1px solid #d5d6d2;">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <h2 style="text-align:left; font-family: Helvetica, Arial, sans-serif; color: #8a8a87; font-size: 18px; font-style: normal; font-weight: normal; line-height: 24px; margin-bottom: 15px; margin-top: 20px;">Olá, <?php echo $nome; ?>.</h2>
                                                        <h3 style="color: #072c73; font-family: Helvetica,Arial,sans-serif; font-size: 25px; font-weight: bold; line-height: 34px; margin-bottom: 10px; margin-top: 0; text-align: left;">
                                                            Agora você já pode viajar com toda tranquilidade!
                                                        </h3>
                                                        <p style="margin-top:0px; text-align:left; font-size: 15px; font-family: Helvetica, Arial, sans-serif; color: #4d4f53; line-height: 24px;">
                                                            Em anexo está a apólice de
                                                            <span style="font-weight: bold;"><?php echo $cliente['nome']; ?></span>,
                                                            esse documento deve ser apresentado ao chegar no seu destino.
                                                            <br />
                                                        </p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <table cellpadding="0" cellspacing="0" border="0" align="center" style="border-bottom: 1px solid #d5d6d2;">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <p style="">
                                                            Com o SulAmérica Viagem, você ainda concorre a 
                                                            R$ 5.000,00* no último sábado do mês subsequente a sua viagem. Abaixo está o seu número da sorte:
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="background:#f47521; ">
                                                        <p style="color: #FFF; font-size: 25px; font-weight: bold; margin: 10px 0; text-align: center;" ><?php echo $numero_sorte; ?></p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p style="">
                                                            Os sorteios são realizados pela extração da loteria federal. Caso seja sorteado, a SulAmérica entrará em contato e lhe dará a boa notícia.
                                                        </p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <table border="0" cellpadding="0" cellspacing="0" class="info-sulamerica" style="margin-bottom: 15px;">
                                            <tbody>
                                                <tr>
                                                    <td colspan="2">
                                                        <h3 style="color: #f47521; margin-bottom: 0; font-size: 20px;">SulAmérica</h3>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 50%;" valign="top">
                                                        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" >
                                                            <tbody>
                                                                <tr>
                                                                    <td style="background: #f47521;">
                                                                        <h4 style="color: #FFF;">
                                                                            Centrais de atendimento
                                                                            <br />
                                                                            <span>(pré e pós viagem)</span>
                                                                        </h4>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <h5>Central de Serviços</h5>
                                                                        <p>
                                                                            3003 9807
                                                                            <br />
                                                                            (capitais e regiões metropolitanas) 
                                                                        </p>
                                                                        <p>
                                                                            0800 726 9807 (demais regiões)
                                                                            <br />
                                                                            Atendimento de 2ª a 6ª, das 8h às 18h30.
                                                                        </p>

                                                                        <h5>SAC</h5>
                                                                        <p>
                                                                            0800 725 1894
                                                                        </p>

                                                                        <h5>SAC - Portadores de necessidades especiais</h5>
                                                                        <p>
                                                                            0800 702 2242
                                                                        </p>

                                                                        <h5>Ouvidoria</h5>
                                                                        <p>
                                                                            0800 725 3374
                                                                        </p>

                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                    <td style="width: 50%;" valign="top">
                                                        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" >
                                                            <tbody>
                                                                <tr>
                                                                    <td style="background:  #072c73;">
                                                                        <h4 style="color: #FFF;">
                                                                            Central de atendimento 24H
                                                                            <br />
                                                                            <span>durante a viagem</span>
                                                                        </h4>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <h5>Viagens Nacionais</h5>
                                                                        <p>
                                                                            3003 7798
                                                                            <br />
                                                                            (capitais e regiões metropolitanas)
                                                                        </p>
                                                                        <p>
                                                                            0800 770 7798 (demais regiões)
                                                                        </p>

                                                                        <h5>Viagens Internacionais</h5>
                                                                        <p>
                                                                            +55 11 4126 7463
                                                                        </p>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        
                                         <table cellpadding="0" cellspacing="0" border="0" align="center" style="border-top: 1px solid #d5d6d2; margin-top: 5px;">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <h3 style="color: #072c73; margin: 0; font-size: 11px; text-transform: uppercase;">Nota legal:</h3>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p style="margin: 0; font-size: 10px; color: #777; line-height: 15px;">
                                                            SulAmérica Viagem Individual – Processo SUSEP: 15414.901176/2015-81. (*) Títulos de Capitalização da modalidade incentivo emitidos por SulAmérica Capitalização S.A, CNPJ 03.558.096/0001-04 e Processo SUSEP 15414.901092/2013-85. Prêmio no valor total de R$ 10.000,00 (dez mil reais), sendo R$ 5.000,00 (cinco mil reais) para o contemplado (segurado) e R$ 5.000,00 (cinco mil reais) para o corretor vendedor (ou loja) bruto de IR (25%), conforme legislação vigente. Ouvidoria SulAmérica 0800 725 3374.
                                                        </p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table border="0" align="center" cellpadding="0" cellspacing="0" style="width:100%; max-width:650px; border: 1px solid #DDD; border-top: 0; margin-top: 0px;">
                            <tbody>
                                <tr>
                                    <td id="footer" align="center" style="background-color:#fff;">
                                        <p style="color: #777;">
                                            &nbsp;
                                        </p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <p>&nbsp; </p>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>