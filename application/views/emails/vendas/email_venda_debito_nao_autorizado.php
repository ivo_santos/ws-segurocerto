<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>Cartão não autorizado | Seguro Viagem</title>
    <style type="text/css">
        #content td {
            font-family: Helvetica, Arial, sans-serif;
            font-size: 15px;
            line-height: 24px;
            color: #4d4f53;
        }

        #content h2 {
            /*text-transform: uppercase;*/
            color: #8a8a87;
            font-style: normal;
            font-weight: normal;
            font-size: 17px;
            line-height: 24px;
            margin-bottom: 15px;
            margin-top: 0px;
        }

        #content h3 {
            font-family: Helvetica, Arial, sans-serif;
            font-size: 25px;
            line-height: 34px;
            font-weight: bold;
            /*color: #3faacc;*/
            color: #072c73;
            margin-top: 0px;
            margin-bottom: 10px;
            text-align: left;
        }

        #content p a {
            font-weight: bold;
            color: #2066b1;
            text-decoration: underline;
        }

        #content p {
            padding-right: 0px;
            padding-left: 0px;
        }

        #footer {
            font-family: Helvetica, Arial, sans-serif;
        }

        #footer a {
            color: #ffffff;
            text-decoration: underline;
        }

        .getting-started p {
            font-size: 15px;
            line-height: 20px;
            margin-right: 0px;
            padding-right: 0px;
        }

        .getting-started {
            /* [disabled]padding-bottom: 0px; */
            padding-left: 15px;
            padding-right: 0px;
        }
    </style>
</head>
<body id="archivebody" style="margin:0px;padding:0px">
<table style="width:100%;margin:0px;padding:0px" id="container" cellpadding="0" cellspacing="0">
    <tbody>
    <tr>
        <td align="center" style="width:100%; background-color:#f7f7f7">

            <table border="0" align="center" cellpadding="0" cellspacing="0" id="header" style="width:100%; max-width:650px;margin-top:0px;margin-bottom:0px; background: #FFF; border: 1px solid #DDD;; border: 1px solid #DDD;">
                <tbody>
                <tr>
                    <td style="background-color:#fff;">&nbsp;</td>
                    <td style="background-color:#fff;">&nbsp;</td>
                </tr>
                <tr>
                    <td style="background-color: #fff;" colspan="2">
                        <a class="brand" href="https://segurocerto.com" title="" style="margin-left: 20px;">
                            <img src="<?php echo $base_url . 'assets/imagens/logos/segurocerto.png'; ?>" width="236"/>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#fff;">&nbsp;</td>
                    <td style="background-color:#fff;">&nbsp;</td>
                </tr>
                </tbody>
            </table>

            <table cellpadding="0" cellspacing="0" border="0" align="center" id="content" style="width:100%; max-width:650px; margin-top:0px; border: 1px solid #DDD; border-top: 0; border-bottom: 0;">
                <tbody>
                <tr>
                    <td align="center" style="background-color:#FFFFFF; border-right: 20px solid #FFF; border-left: 20px solid #FFF;">
                        <table cellpadding="0" cellspacing="0" border="0" align="center">
                            <tbody>
                            <tr>
                                <td>
                                    <h2 style="text-align:left; font-family: Helvetica, Arial, sans-serif; color: #8a8a87; font-size: 18px; font-style: normal; font-weight: normal; line-height: 24px; margin-bottom: 15px; margin-top: 20px;">Olá, <?php echo $nome; ?>.</h2>
                                    <h3 style="color: #f47521; font-family: Helvetica,Arial,sans-serif; font-size: 25px; font-weight: bold; line-height: 34px; margin-bottom: 10px; margin-top: 0; text-align: left;">
                                        Infelizmente sua contratação não foi autorizada!
                                    </h3>
                                    <p style="margin-top:0px; text-align:left; font-size: 15px; font-family: Helvetica, Arial, sans-serif; color: #4d4f53; line-height: 24px;">
                                        Informamos que a sua contratação de seguro viagem, não foi autorizada pela administradora do seu cartão de crédito. Você pode tentar um novo pagamento atráves do botão abaixo.
                                    </p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table border="0" align="center" cellpadding="0" cellspacing="0" class="responsive-table" style="margin-top:20px; margin-bottom:40px">
                            <tbody>
                            <tr>
                                <td bgcolor="#072c73" style="border-left-style: none; border-right-style: none; border-top-style: none; border-bottom-style: none; padding: 12px 18px 12px 18px;" align="center">
                                    <a href="<?php echo $url_nova_tentativa; ?>" target="_blank" style="font-size: 18px; font-family: Helvetica, Arial, sans-serif; font-weight: bold; color: #ffffff; text-decoration: none;">
                                        EFETUE UM NOVO PAGAMENTO
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table cellpadding="0" cellspacing="0" border="0" align="center">
                            <tbody>
                            <tr>
                                <td>
                                    <p style="margin-top:0px; text-align:left; font-size: 15px; font-family: Helvetica, Arial, sans-serif; color: #4d4f53; line-height: 24px;">
                                        <strong>Atenção</strong>:<br/> Essa contratação e seu valor ficará disponível até
                                        <span style="font-weight: bold;"><?php echo $data_expiracao; ?></span>,
                                        e a emissão dos bilhetes está vinculada a confirmação do pagamento.
                                        Se não houver a confirmação essa contratação será cancelada.
                                    </p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table border="0" align="center" cellpadding="0" cellspacing="0" style="width:100%; border-top: 1px solid #d5d6d2; margin-top: 15px;">
                            <tbody>
                            <tr>
                                <td align="center">
                                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-top:10px">
                                        <tbody>
                                        <tr>
                                            <td style="padding-top:0px; padding-right:0px" class="getting-started" valign="top">
                                                <h3 style="color: #f47521; margin-bottom: 0; font-size: 20px;">Atendimento</h3>
                                                <p style="padding-left:0px; margin-top: 0; text-align:left; font-size: 15px; font-family: Helvetica, Arial, sans-serif; color: #4d4f53; line-height: 24px;">
                                                    Em caso de dúvidas, você pode entrar em contato com a nossa
                                                    central de atendimento
                                                    <span style="color: #f47521;"><?php echo $empresa['telefone']; ?></span>  de segunda a sábado das 09:00 às 21:00
                                                    ou atráves do email
                                                    <a style="color: #f47521; text-decoration: none;" href="malito:contato@tripguard.com.br" title="contato">contato@tripguard.com.br</a>.
                                                    <br/>
                                                    <br/>
                                                    Equipe,
                                                    <br/>
                                                    TripGuard
                                                </p>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>

            <table border="0" align="center" cellpadding="0" cellspacing="0" style="width:100%; max-width:650px; border: 1px solid #DDD; border-top: 0;">
                <tbody>
                <tr>
                    <td id="footer" align="center" style="background-color:#fff;">
                        <p style="color: #777;">
                            &nbsp;
                        </p>
                    </td>
                </tr>
                </tbody>
            </table>
            <p>&nbsp; </p>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>