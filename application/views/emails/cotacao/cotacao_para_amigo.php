<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

	<title>Cotação | Seguro Certo</title>
	<style type="text/css">
		#content td {
			font-family: Helvetica, Arial, sans-serif;
			font-size: 15px;
			line-height: 24px;
			color: #4d4f53;
		}

		#content h2 {
			/*text-transform: uppercase;*/
			color: #8a8a87;
			font-style: normal;
			font-weight: normal;
			font-size: 17px;
			line-height: 24px;
			margin-bottom: 15px;
			margin-top: 0px;
		}

		#content h3 {
			font-family: Helvetica, Arial, sans-serif;
			font-size: 25px;
			line-height: 34px;
			font-weight: bold;
			/*color: #3faacc;*/
			color: #072c73;
			margin-top: 0px;
			margin-bottom: 10px;
			text-align: left;
		}

		#content p a {
			font-weight: bold;
			color: #2066b1;
			text-decoration: underline;
		}

		#content p {
			padding-right: 0px;
			padding-left: 0px;
		}

		#footer {
			font-family: Helvetica, Arial, sans-serif;
		}

		#footer a {
			color: #ffffff;
			text-decoration: underline;
		}

		.getting-started p {
			font-size: 15px;
			line-height: 20px;
			margin-right: 0px;
			padding-right: 0px;
		}

		.getting-started {
			/* [disabled]padding-bottom: 0px; */
			padding-left: 15px;
			padding-right: 0px;
		}
	</style>
</head>
<body id="archivebody" style="margin:0px;padding:0px">
<table style="width:100%;margin:0px;padding:0px" id="container" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
		<td align="center" style="width:100%; background-color:#f7f7f7">

			<table border="0" align="center" cellpadding="0" cellspacing="0" id="header" style="width:100%; max-width:650px;margin-top:0px;margin-bottom:0px; background: #FFF; border: 1px solid #DDD;">
				<tbody>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">
						<a class="brand" href="https://segurocerto.com" title="" style="margin-left: 20px;">
							<img src="<?php echo $base_url . 'assets/imagens/logos/segurocerto.png'; ?>" width="236"/>
						</a>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				</tbody>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" align="center" id="content" style="width:100%; max-width:650px; margin-top:0px; border: 1px solid #DDD; border-top: 0; border-bottom: 0;">
				<tbody>
				<tr>
					<td align="center" style="background-color:#FFFFFF; border-right: 20px solid #FFF; border-left: 20px solid #FFF;">
						<table cellpadding="0" cellspacing="0" border="0" align="center">
							<tbody>
							<tr>
								<td>
									<h2 style="text-align:left; font-family: Helvetica, Arial, sans-serif; color: #8a8a87; font-size: 18px; font-style: normal; font-weight: normal; line-height: 24px; margin-bottom: 15px; margin-top: 20px;">Olá, <?php echo $nome_amigo; ?>.</h2>
									<h3 style="color: #072c73; font-family: Helvetica,Arial,sans-serif; font-size: 25px; font-weight: bold; line-height: 34px; margin-bottom: 10px; margin-top: 0; text-align: left;">
										Seu amigo <?php echo $nome; ?>, solicitou o envio dessa cotação de seguro viagem para seu email.
									</h3>
								</td>
							</tr>
							<tr>
								<td>
									<table border="0" align="center" cellpadding="0" cellspacing="0" class="responsive-table" style="margin: 5px 0 10px; width:100%;">
										<tbody>
										<tr>
											<td>
												<p style="font-size: 15px; color: #072c73; margin: 0; ">
													Destino
												</p>
											</td>
											<td>
												<p style="font-size: 15px; color: #072c73; margin: 0; ">
													Data ida
												</p>
											</td>
											<td>
												<p style="font-size: 15px; color: #072c73; margin: 0; ">
													Data volta
												</p>
											</td>
											<td>
												<p style="font-size: 15px; color: #072c73; margin: 0; ">
													Duração
												</p>
											</td>
										</tr>
										<tr>
											<td>
												<p style="font-size: 15px; color: #f47521;margin: 0; ">
													<?php echo $cotacao->destino_grupo; ?>
												</p>
											</td>
											<td>
												<p style="font-size: 15px; color: #f47521;margin: 0; ">
													<?php echo formata_data($cotacao->data_inicio); ?>
												</p>
											</td>
											<td>
												<p style="font-size: 15px; color: #f47521;margin: 0; ">
													<?php echo formata_data($cotacao->data_final); ?>
												</p>
											</td>
											<td>
												<p style="font-size: 15px; color: #f47521;margin: 0; ">
													<?php $dias = diferenca_datas($cotacao->data_inicio, $cotacao->data_final, '-', 'd'); ?>
													(<?php echo ($dias > 1) ? $dias . ' dias' : $dias . ' dia'; ?>)
												</p>
											</td>
										</tr>
										<tr>
											<td colspan="4">
												<p style="margin-top:5px; text-align:left; font-size: 15px; font-family: Helvetica, Arial, sans-serif; color: #4d4f53; line-height: 24px;">
													Atenção:<br/>
													Aproveite para contratar seu seguro viagem agora!
													<br/>
													Os valores dessa cotação ficarão disponíveis até:
													<span style="font-weight: bold;"><?php echo $data_expiracao; ?></span>, não perca!
												</p>
											</td>
										</tr>
										</tbody>
									</table>
								</td>
							</tr>
							</tbody>
						</table>

						<?php if (!empty($planos)) { ?>
							<?php foreach ($planos as $chave => $plano) { ?>
								<table border="0" align="center" cellpadding="0" cellspacing="0" class="responsive-table" style="width: 100%; margin-top:10px; border: 1px solid #DDD;">
									<tbody>
									<tr>
										<td style="width: 35%; border-left-style: none; border-right-style: none; border-top-style: none; border-bottom-style: none;" align="center">
											<table border="0" align="center" cellpadding="0" cellspacing="0" class="responsive-table" style="margin:0;">
												<tbody>
												<tr>
													<td style="border-left-style: none; border-right-style: none; border-top-style: none; border-bottom-style: none;" align="center">
														<img src="<?php echo $base_url . 'assets/imagens/logos/sulamerica.png'; ?>" style="width:70%; margin: 10px 0 15px;"/>
													</td>
												</tr>
												</tbody>
											</table>
										</td>
										<td style="width: 65%; border-left-style: none; border-right-style: none; border-top-style: none; border-bottom-style: none; ">
											<table border="0" align="center" cellpadding="0" cellspacing="0" class="responsive-table" style="margin: 10px 0 15px; width:100%;">
												<tbody>
												<tr>
													<td colspan="2" style="border-left-style: none; border-right-style: none; border-top-style: none; border-bottom-style: none;" align="left">
														<h3 style="line-height: 20px; margin: 5px 0 10px; color: #f47521; font-size: 15px; text-transform: uppercase;">
															<?php echo $plano->nome; ?>
														</h3>
													</td>
												</tr>
												<tr>
													<td style="border-left-style: none; border-right-style: none; border-top-style: none; border-bottom-style: none;" align="left">
														<p style="margin: 0; text-align: left; margin-bottom: 5px; font-size: 15px;">
															0 até 64 anos
															<br/>
															por
															<span style="font-weight: bold; color: #777; font-size: 22px;">
                                                                R$ <?php echo number_format($plano->cotacao->valor, 2, ',', '.'); ?>
                                                            </span>
															<br/>
															<strong style="color: #f47521;">*</strong> preço por pessoa
														</p>
													</td>
													<td style="border-left-style: none; border-right-style: none; border-top-style: none; border-bottom-style: none;" align="left">
														<p style="margin: 0; text-align: left; margin-bottom: 5px; font-size: 15px;">
															65 até 90 anos
															<br/>
															por
															<span style="font-weight: bold; color: #777; font-size: 22px;">
                                                                R$ <?php echo number_format($plano->cotacao->valor_idoso, 2, ',', '.'); ?>
                                                            </span>
															<br/>
															<strong style="color: #f47521;">*</strong> preço por pessoa
														</p>
													</td>
												</tr>
												</tbody>
											</table>
										</td>
									</tr>
									</tbody>
								</table>
							<?php } ?>
						<?php } ?>

						<table border="0" align="center" cellpadding="0" cellspacing="0" class="responsive-table" style="margin-top:30px; margin-bottom:20px">
							<tbody>
							<tr>
								<td bgcolor="#5cb85c" style="border-left-style: none; border-right-style: none; border-top-style: none; border-bottom-style: none; padding: 12px 18px 12px 18px;" align="center">
									<a href="<?php echo $url_cotacao_por_email; ?>" target="_blank" style="font-size: 18px; font-family: Helvetica, Arial, sans-serif; font-weight: bold; color: #ffffff; text-decoration: none; margin: 0 20px;">
										COMPARE AS COBERTURAS
									</a>
								</td>
							</tr>
							</tbody>
						</table>

						<table border="0" align="center" cellpadding="0" cellspacing="0" style="width:100%; margin-top: 15px; border-top: 1px solid #d5d6d2;">
							<tbody>
							<tr>
								<td align="center">
									<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-top:10px">
										<tbody>
										<tr>
											<td style="padding-top:0px; padding-right:0px" class="getting-started" valign="top">
												<h3 style="color: #f47521; margin-bottom: 0; font-size: 20px;">Atendimento</h3>
												<p style="padding-left:0px; margin-top: 0; text-align:left; font-size: 15px; font-family: Helvetica, Arial, sans-serif; color: #4d4f53; line-height: 24px;">
													Em caso de dúvidas, você pode entrar em contato com a nossa
													central de atendimento
													<span style="color: #f47521;"><?php echo $empresa['telefone']; ?></span> de segunda a sábado das 09:00 às 21:00
													ou atráves do email
													<a style="color: #f47521; text-decoration: none;" href="malito:contato@tripguard.com.br" title="Atendimento">contato@tripguard.com.br</a>.
													<br/>
													<br/>
													Equipe,
													<br/>
													TripGuard
												</p>
											</td>
										</tr>
										</tbody>
									</table>
								</td>
							</tr>
							</tbody>
						</table>
					</td>
				</tr>
				</tbody>
			</table>

			<table border="0" align="center" cellpadding="0" cellspacing="0" style="width:100%; max-width:650px; border: 1px solid #DDD; border-top: 0;">
				<tbody>
				<tr>
					<td id="footer" align="center" style="background-color:#fff;">
						<p style="color: #777;">
							&nbsp;
						</p>
					</td>
				</tr>
				</tbody>
			</table>
			<p>&nbsp; </p>
		</td>
	</tr>
	</tbody>
</table>
</body>
</html>