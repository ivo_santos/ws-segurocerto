<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Vouchers extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('vouchers/voucher');
    }

    public function online($lang, $id) {
        $this->rest->libGet(function() use($lang, $id) {
            $data = $this->voucher->online($lang, $id);
            retornarDados($data);
        }, function() use($lang, $id) {
            return (($lang == "pt_BR" || $lang == "en_US") && !empty($id)) ? true : false;
        });
    }

    /**
     * Fazer download de um voucher
     * @param string $lang
     * @param string $id
     */
    public function download($lang, $id) {
        $this->rest->libGet(function() use($lang, $id) {
            $data = $this->voucher->download($lang, $id);
            retornarDados($data);
        }, function() use($lang, $id) {
            return (($lang == "pt_BR" || $lang == "en_US") && !empty($id)) ? true : false;
        });
    }

}
