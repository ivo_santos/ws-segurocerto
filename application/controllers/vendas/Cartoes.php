<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cartoes extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('vendas/cartao');
    }

    /**
     * Venda por cartão de crédito
     */
    public function vendaPorCartao() {
        $data = Formato::requisicaoPost();
        $this->rest->libPost(function() use($data) {
            $data = $this->cartao->vendaPorCartao($data);
            retornarDados($data);
        });
    }

}
