<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Boletos extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('vendas/boleto');
    }

    /**
     * Listar um boleto
     * @param type $id
     */
    public function listarUm($id) {
        $this->rest->libGet(function() use($id) {
            $data = $this->boleto->listarUm($id);
            retornarDados($data);
        });
    }

    /**
     * Venda por boleto
     */
    public function vendaPorBoleto() {
        $data = Formato::requisicaoPost();
        $this->rest->libPost(function() use($data) {
            $data = $this->boleto->vendaPorBoleto($data);
            retornarDados($data);
        });
    }
    
     /**
     * Ver boleto por hash do boleto_id
     * @param string $id
     */
    public function ver($id) {
        $this->rest->libGet(function() use($id) {
            $this->boleto->ver($id);
        });
    }

}
