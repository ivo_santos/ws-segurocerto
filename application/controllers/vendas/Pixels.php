<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pixels extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('vendas/pixel');
    }

    /**
     * Visualizou um email com os vouchers
     * @param $venda_id
     */
    public function visualizou($venda_id) {
        $this->rest->libGet(function() use($venda_id) {
            $this->pixel->visualizou($venda_id);
        });
    }
}
