<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Debitos extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('vendas/debito');
    }

    /**
     * Pre processar venda via debito para gerar o link de autenticacao cielo
     */
    public function preProcessar() {
        $data = Formato::requisicaoPost();
        $this->rest->libPost(function() use($data) {
            $data = $this->debito->preProcessar($data);
            retornarDados($data);
        });
    }

    /**
     * Processa a venda de acordo com o status da autenticacao
     * @param string $tid
     */
    public function processar($tid) {
        $this->rest->libGet(function() use($tid) {
            $data = $this->debito->processar($tid);
            retornarDados($data);
        });
    }

}
