<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Bracos extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('vendas/braco');
    }

    /**
     * Adicionar um pelo hash do id
     */
    public function adicionar()
    {
        $data = Formato::requisicaoPost();
        $this->rest->libPost(function () use ($data) {
            $data = $this->braco->adicionar($data);
            retornarDados($data);
        });
    }

    /**
     * Alterar um pelo hash do id
     */
    public function alterar()
    {
        $data = Formato::requisicaoPut();
        $this->rest->libPut(function () use ($data) {
            $data = $this->braco->alterar($data);
            retornarDados($data);
        });
    }

    /**
     * Remover um pelo hash do id
     * @param $id
     */
    public function remover($id)
    {
        $this->rest->libDelete(function () use ($id) {
            $data = $this->braco->remover($id);
            retornarDados($data);
        });
    }

    /**
     * Listar todos
     */
    public function listarTodos()
    {
        $this->rest->libGet(function () {
            $filtros = $this->input->get();
            $data = $this->braco->listarTodos($filtros);
            retornarDados($data);
        });
    }

    /**
     * Listar um pelo hash do id
     * @param $id
     */
    public function listarUm($id)
    {
        $this->rest->libGet(function () use ($id) {
            $data = $this->braco->listarUm($id);
            retornarDados($data);
        });
    }

}
