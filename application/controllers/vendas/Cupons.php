<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cupons extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('vendas/cupom');
    }

    /**
     * Listar todos os cupons
     */
    public function listarTodos() {
        $this->rest->libGet(function() {
            $filtros = $this->input->get();
            $data = $this->cupom->listarTodos($filtros);
            retornarDados($data);
        });
    }

    /**
     * Listar um pelo hash do id
     * @param string $id
     */
    public function listarUm($id) {
        $this->rest->libGet(function() use($id) {
            $data = $this->cupom->listarUm($id);
            retornarDados($data);
        });
    }

    /**
     * Adicionar um cupom
     */
    public function adicionar() {
        $data = Formato::requisicaoPost();
        $this->rest->libPost(function() use($data) {
            $data = $this->cupom->adicionar($data);
            retornarDados($data);
        });
    }

    /**
     * Alterar dados de um cupom pelo hash do id
     */
    public function alterar() {
        $data = Formato::requisicaoPut();
        $this->rest->libPut(function() use($data) {
            $data = $this->cupom->alterar($data);
            retornarDados($data);
        });
    }

    /**
     * Remover um cupom pelo hash do id
     * @param string $id
     */
    public function remover($id) {
        $this->rest->libDelete(function() use($id) {
            $data = $this->cupom->remover($id);
            retornarDados($data);
        });
    }

	/**
	 * Listar um pelo codigo
	 * @param string $id
	 */
	public function listarUmPorCodigo($id) {
		$this->rest->libGet(function() use($id) {
			$data = $this->cupom->listarUmPorCodigo($id);
			retornarDados($data);
		});
	}

    /**
     * Aplicar cupom a uma venda
     */
    public function aplicarCupom() {
        $data = Formato::requisicaoPost();
        $this->rest->libPost(function() use($data) {
            $data = $this->cupom->aplicarCupom($data);
            retornarDados($data);
        });
    }

    /**
     * Remover um cupom de uma venda
     */
    public function ignorarCupom() {
        $data = Formato::requisicaoPost();
        $this->rest->libPost(function() use($data) {
            $data = $this->cupom->ignorarCupom($data);
            retornarDados($data);
        });
    }

}
