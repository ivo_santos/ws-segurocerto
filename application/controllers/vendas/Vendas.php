<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Vendas extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('vendas/venda');
    }

    /**
     * Listar todas as vendas
     */
    public function listarTodos() {
        $this->rest->libGet(function() {
            $filtros = $this->input->get();
            $data = $this->venda->listarTodos($filtros);
            retornarDados($data);
        });
    }

    /**
     * Listar uma venda pelo hash do id
     * @param string $id
     */
    public function listarUm($id) {
        $this->rest->libGet(function() use($id) {
            $data = $this->venda->listarUm($id);
            retornarDados($data);
        });
    }

    /**
     * Busca a venda pelas credenciais
     */
    public function listarUmPorCredenciais() {
        $data = Formato::requisicaoPost();
        $this->rest->libPost(function() use($data) {
            $data = $this->venda->listarUmPorCredenciais($data);
            retornarDados($data);
        });
    }

    /**
     * Busca a venda por hash da cotacao_id
     */
    public function listarUmPorCotacaoId($cotacao_id) {
        $this->rest->libGet(function() use($cotacao_id) {
            $data = $this->venda->listarUmPorCotacaoId($cotacao_id);
            retornarDados($data);
        });
    }

    /**
     * Cancelar uma venda
     */
    public function cancelar() {
        $data = Formato::requisicaoPost();
        $this->rest->libPost(function() use($data) {
            $data = $this->venda->cancelar($data);
            retornarDados($data);
        });
    }

    /**
     * Aprova o pagamento de uma venda manualmente
     */
    public function setarPagamento() {
        $data = Formato::requisicaoPost();
        $this->rest->libPost(function() use($data) {
            $data = $this->venda->setarPagamento($data);
            retornarDados($data);
        });
    }

    /**
     *  Salva os clientes de uma venda e atualiza o valor total
     */
    public function salvarClientesEmUmaVenda() {
        $data = Formato::requisicaoPost();
        $this->rest->libPost(function() use($data) {
            $data = $this->venda->salvarClientesEmUmaVenda($data);
            retornarDados($data);
        });
    }

    /**
     *  Remover um cliente de uma venda e atualiza o valor total
     */
    public function removerClienteEmUmaVenda() {
        $data = Formato::requisicaoPost();
        $this->rest->libPost(function() use($data) {
            $data = $this->venda->removerClienteEmUmaVenda($data);
            retornarDados($data);
        });
    }

    /**
     * Adiciona uma venda com o tipo = 2 pré-venda
     */
    public function preVenda() {
        $data = Formato::requisicaoPost();
        $this->rest->libPost(function() use($data) {
            $data = $this->venda->adicionar($data);
            retornarDados($data);
        });
    }

    /**
     * Atualiza o valor de uma venda caso tenha passado das 23:59:59
	 * e envia os dados para continuar a compra.
     */
    public function atualizarCotacao($venda_id) {
		$this->rest->libGet(function() use($venda_id) {
			$data = $this->venda->atualizarCotacao($venda_id);
			retornarDados($data);
		});
    }

	public function alterar() {
		$data = Formato::requisicaoPut();
		$this->rest->libPut(function() use($data) {
			$data = $this->venda->alterar($data);
			retornarDados($data);
		});
	}

}
