<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Depositos extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('vendas/deposito');
    }

    /**
     * Venda via deposito
     */
    public function vendaPorDeposito() {
        $data = Formato::requisicaoPost();
        $this->rest->libPost(function() use($data) {
            $data = $this->deposito->vendaPorDeposito($data);
            retornarDados($data);
        });
    }

}
