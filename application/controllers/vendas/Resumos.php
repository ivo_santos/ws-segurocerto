<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Resumos extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('vendas/resumo');
    }
    
    /**
     * Busca um resumo da venda
     * @param string $venda_id
     * @param int $etapa
     */
    public function gerar($venda_id, $etapa) {
        $this->rest->libGet(function () use ($venda_id, $etapa) {
            $data = $this->resumo->gerar($venda_id, $etapa);
            retornarDados($data);
        });
    }
}
