<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cms_emails extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('cms/cms_email');
    }

    /**
     * Adicionar
     */
    public function adicionar()
    {
        $data = Formato::requisicaoPost();
        $this->rest->libPost(function () use ($data) {
            $data = $this->cms_email->adicionar($data);
            retornarDados($data);
        });
    }

    /**
     * Alterar um pelo hash do id
     */
    public function alterar()
    {
        $data = Formato::requisicaoPut();
        $this->rest->libPut(function () use ($data) {
            $data = $this->cms_email->alterar($data);
            retornarDados($data);
        });
    }

    /**
     * Remover um pelo hash do id
     */
    public function remover($id)
    {
        $this->rest->libDelete(function () use ($id) {
            $data = $this->cms_email->remover($id);
            retornarDados($data);
        });
    }

    /**
     * Listar todos
     */
    public function listarTodos()
    {
        $this->rest->libGet(function () {
            $filtros = $this->input->get();
            $data = $this->cms_email->listarTodos($filtros);
            retornarDados($data);
        });
    }

    /**
     * Listar um pelo hash do id
     * @param $id
     */
    public function listarUm($id)
    {
        $this->rest->libGet(function () use ($id) {
            $data = $this->cms_email->listarUm($id);
            retornarDados($data);
        });
    }



	public function enviarParaCotacao()
	{
		$data = Formato::requisicaoPost();
		$this->rest->libPost(function () use ($data) {
			$data = $this->cms_email->enviarParaCotacao($data);
			retornarDados($data);
		});
	}

	public function enviarParaVenda()
	{
		$data = Formato::requisicaoPost();
		$this->rest->libPost(function () use ($data) {
			$data = $this->cms_email->enviarParaVenda($data);
			retornarDados($data);
		});
	}

	/**
	 * Notifica abertura de um email e altera o historico do envio para visualizado.
	 */
	public function abertura($id)
	{
		$this->rest->libGet(function () use ($id) {
			$data = $this->cms_email->abertura($id);
			retornarDados($data);
		});
	}
}
