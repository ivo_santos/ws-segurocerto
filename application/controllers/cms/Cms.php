<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cms extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('cms/lib_cms');
    }

    /**
     * Adicionar
     */
    public function adicionar()
    {
        $data = Formato::requisicaoPost();
        $this->rest->libPost(function () use ($data) {
            $data = $this->lib_cms->adicionar($data);
            retornarDados($data);
        });
    }

    /**
     * Alterar um pelo hash do id
     */
    public function alterar()
    {
        $data = Formato::requisicaoPut();
        $this->rest->libPut(function () use ($data) {
            $data = $this->lib_cms->alterar($data);
            retornarDados($data);
        });
    }

    /**
     * Remover um pelo hash do id
     */
    public function remover($id)
    {
        $this->rest->libDelete(function () use ($id) {
            $data = $this->lib_cms->remover($id);
            retornarDados($data);
        });
    }

    /**
     * Listar todos
     */
    public function listarTodos()
    {
        $this->rest->libGet(function () {
            $filtros = $this->input->get();
            $data = $this->lib_cms->listarTodos($filtros);
            retornarDados($data);
        });
    }

    /**
     * Listar um pelo hash do id
     * @param $id
     */
    public function listarUm($id)
    {
        $this->rest->libGet(function () use ($id) {
            $data = $this->lib_cms->listarUm($id);
            retornarDados($data);
        });
    }

    /**
     * Listar um pela url amigavel
     * @param $url_amigavel
     */
    public function listarUmPorUrl($url_amigavel)
    {
        $this->rest->libGet(function () use ($url_amigavel) {
            $data = $this->lib_cms->listarUmPorUrl($url_amigavel);
            retornarDados($data);
        });
    }
}
