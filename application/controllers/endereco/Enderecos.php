<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Enderecos extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('endereco/endereco');
    }

    /**
     * Listar um endereço do robo correios por cep.
     * @param string $cep
     */
    public function roboCorreios($cep) {
        $this->rest->libGet(function() use($cep) {
            $data = $this->endereco->roboCorreios($cep);
            retornarDados($data);
        });
    }

    /**
     * Listar um endereço de uma venda por hash venda_id
     * @param string $venda_id
     */
    public function listarUmPorVendaId($venda_id) {
        $this->rest->libGet(function() use($venda_id) {
            $data = $this->endereco->listarUmPorVendaId($venda_id);
            retornarDados($data);
        });
    }

}
