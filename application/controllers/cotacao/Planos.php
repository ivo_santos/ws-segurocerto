<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Planos extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('cotacao/plano');
    }

    /**
     * Listar um plano pelo id
     */
    public function listarUm($id) {
        $this->rest->libGet(function() use($id) {
            $data = $this->plano->listarUm($id);
            retornarDados($data);
        });
    }

    /**
     * Listar todos os planos
     */
    public function todosPlanos() {
        $this->rest->libGet(function() {
            $data = $this->plano->todosPlanos();
            retornarDados($data);
        });
    }

}
