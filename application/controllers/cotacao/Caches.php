<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Caches extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('administracao/cache_cotacao');
    }

    /**
     * FAZ A ATUALIZAÇÃO DE UM CACHE DE PREÇO
     */
    public function fazerAtualizacao() {
        $this->rest->libGet(function(){
            $data = $this->cache_cotacao->fazerAtualizacao();
            retornarDados($data);
        });
    }

}
