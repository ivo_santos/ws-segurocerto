<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cotacoes_historicos extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('cotacao/cotacao_historicos');
    }

	/**
	 * Listar todos historicos por hash do id da cotacao
	 * @param $cotacao_id
	 */
    public function listarTodosPorCotacaoId($cotacao_id) {
        $this->rest->libGet(function() use($cotacao_id){
            $data = $this->cotacao_historicos->listarTodosPorCotacaoId($cotacao_id);
            retornarDados($data);
        });
    }

    /**
     * Listar um historico de cotacao por hash do id da cotacao
     * @param string $id
     */
    public function listarUm($id) {
        $this->rest->libGet(function() use($id) {
            $data = $this->cotacao_historicos->listarUm($id);
            retornarDados($data);
        });
    }

	/**
	 * Adiciona um historico de cotacao
	 */
    public function adicionar() {
        $data = Formato::requisicaoPost();
        $this->rest->libPost(function() use($data) {
            $data = $this->cotacao_historicos->adicionar($data);
            retornarDados($data);
        });
    }

	/**
	 * Altera um historico de cotação por hash do id
	 */
	public function alterar() {
		$data = Formato::requisicaoPut();
		$this->rest->libPut(function() use($data) {
			$data = $this->cotacao_historicos->alterar($data);
			retornarDados($data);
		});
	}

}
