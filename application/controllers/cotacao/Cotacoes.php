<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cotacoes extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('cotacao/cotacao');
    }

    /**
     * Adicionar os dados de uma cotação e retorna o hash do id
     */
    public function adicionar() {
        $data = Formato::requisicaoPost();
        $this->rest->libPost(function() use($data) {
            $data = $this->cotacao->adicionar($data);
            retornarDados($data);
        });
    }

    /**
     * altera os dados de uma cotação e retorna o hash do id
     */
    public function alterar() {
        $data = Formato::requisicaoPut();
        $this->rest->libPut(function() use($data) {
            $data = $this->cotacao->alterar($data);
            retornarDados($data);
        });
    }

	/**
	 * Remover um por hash do id
	 * @param string $id
	 */
	public function remover($id) {
		$this->rest->libDelete(function() use($id) {
			$data = $this->cotacao->remover($id);
			retornarDados($data);
		});
	}

    /**
     * Busca a cotação
     */
    public function gerar($id) {
        $this->rest->libGet(function() use($id) {
            $data = $this->cotacao->gerar($id);
            retornarDados($data);
        });
    }

    /**
     * Busca uma cotação realizada por um cliente gravada no banco
     */
    public function listarUm($id) {
        $this->rest->libGet(function() use($id) {
            $data = $this->cotacao->listarUm($id);
            retornarDados($data);
        });
    }

    /**
     * Busca registro de cotação por hash do id
     * @param string $id
     */
    public function listarUmCotacaoPorEmail($id) {
        $this->rest->libGet(function() use($id) {
            $data = $this->cotacao->listarUmCotacaoPorEmail($id);
            retornarDados($data);
        });
    }

    /**
     * Faz a cotação e envia para o email solicitado
     */
    public function enviarParaMeuEmail() {
        $data = Formato::requisicaoPost();
        $this->rest->libPost(function() use($data) {
            $data = $this->cotacao->enviarParaMeuEmail($data);
            retornarDados($data);
        });
    }
    
     /**
     * Faz a cotação e envia para um amigo
     */
    public function enviarParaAmigo() {
        $data = Formato::requisicaoPost();
        $this->rest->libPost(function() use($data) {
            $data = $this->cotacao->enviarParaAmigo($data);
            retornarDados($data);
        });
    }

	/**
	 * Listar todas as cotações não venda
	 */
	public function listarTodosNaoVenda() {
		$this->rest->libGet(function() {
			$filtros = $this->input->get();
			$data = $this->cotacao->listarTodosNaoVenda($filtros);
			retornarDados($data);
		});
	}
}
