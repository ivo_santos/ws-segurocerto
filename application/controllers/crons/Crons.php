<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Crons extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('crons/cron');
    }

	/**
	 * Listar todas as vendas
	 */
	public function listarTodos() {
		$this->rest->libGet(function() {
			$filtros = $this->input->get();
			$data = $this->cron->listarTodos($filtros);
			retornarDados($data);
		});
	}


    /**
     * Emite os vouchers na seguradora
     */
    public function emitirVouchers() {
        $this->rest->libGet(function () {
            $data = $this->cron->emitirVouchers();
            retornarDados($data);
        });
    }

    /**
     * Envia emails com vouchers
     */
    public function enviarEmailComVouchers() {
        $this->rest->libGet(function () {
            $data = $this->cron->enviarEmailComVouchers();
            retornarDados($data);
        });
    }

	/**
	 * Cancela vendas não pagas
	 */
	public function cancelarVendasPendentes() {
		$this->rest->libGet(function () {
			$data = $this->cron->cancelarVendasPendentes();
			retornarDados($data);
		});
	}
}
