<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Clientes extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('administracao/cliente');
    }

    /**
     * Adicionar
     * @param array $data
     */
    public function adicionar($data) {
        $data = Formato::requisicaoPost();
        $this->rest->libPost(function() use($data) {
            $data = $this->cliente->adicionar($data);
            retornarDados($data);
        });
    }

    /**
     * Alterar um por hash do id
     * @param array $data
     */
    public function alterar($data) {
        $data = Formato::requisicaoPut();
        $this->rest->libPut(function() use($data) {
            $data = $this->cliente->alterar($data);
            retornarDados($data);
        });
    }

    /**
     * Remover um por hash do id
     * @param string $id
     */
    public function remover($id) {
        $this->rest->libDelete(function() use($id) {
            $data = $this->cliente->remover($id);
            retornarDados($data);
        });
    }

    /**
     * Listar todos
     */
    public function listarTodos() {
        $this->rest->libGet(function() {
            $filtros = $this->input->get();
            $data = $this->cliente->listarTodos($filtros);
            retornarDados($data);
        });
    }

    /**
     * Listar um por hash id
     * @param string $id
     */
    public function listarUm($id) {
        $this->rest->libGet(function() use($id) {
            $data = $this->cliente->listarUm($id);
            retornarDados($data);
        });
    }

    /**
     * Buscar um cliente por documento
     * @param string $documento
     */
    public function listarUmPorDocumento() {
        $data = Formato::requisicaoPost();
        $this->rest->libPost(function() use($data) {
            $data = $this->cliente->listarUmPorDocumento($data['documento']);
            retornarDados($data);
        });
    }

    /**
     * Busca os clientes de uma venda por hash do id da venda
     * @param string $id
     */
    public function listarTodosPorVendaId($id) {
        $this->rest->libGet(function() use($id) {
            $data = $this->cliente->listarTodosPorVendaId($id);
            retornarDados($data);
        });
    }
    
    /**
     * Listar todos com vouchers por hash do venda_id
     * @param string $venda_id
     */
    public function listarTodosComVouchersPorVendaId($venda_id) {
        $this->rest->libGet(function() use($venda_id) {
            $data = $this->cliente->listarClientesComVouchersPorVendaId($venda_id);
            retornarDados($data);
        });
    }

}
