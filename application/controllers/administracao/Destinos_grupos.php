<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Destinos_grupos extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('administracao/destino_grupo');
    }

	/**
	 * Adicionar
	 * @param array $data
	 */
	public function adicionar() {
		$data = Formato::requisicaoPost();
		$this->rest->libPost(function() use($data) {
			$data = $this->destino_grupo->adicionar($data);
			retornarDados($data);
		});
	}

	/**
	 * Alterar um por hash do id
	 * @param array $data
	 */
	public function alterar() {
		$data = Formato::requisicaoPut();
		$this->rest->libPut(function() use($data) {
			$data = $this->destino_grupo->alterar($data);
			retornarDados($data);
		});
	}

	/**
	 * Remover um por hash do id
	 * @param string $id
	 */
	public function remover($id) {
		$this->rest->libDelete(function() use($id) {
			$data = $this->destino_grupo->remover($id);
			retornarDados($data);
		});
	}

	/**
	 * Listar todos
	 */
	public function listarTodos() {
		$this->rest->libGet(function() {
			$filtros = $this->input->get();
			$data = $this->destino_grupo->listarTodos($filtros);
			retornarDados($data);
		});
	}

	/**
	 * Listar um por hash id
	 * @param string $id
	 */
	public function listarUm($id) {
		$this->rest->libGet(function() use($id) {
			$data = $this->destino_grupo->listarUm($id);
			retornarDados($data);
		});
	}

}
