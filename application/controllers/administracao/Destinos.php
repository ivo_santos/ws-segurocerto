<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Destinos extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('administracao/destino');
    }

    /**
     * Listar um destino por id
     */
    public function listarUm($id) {
        $this->rest->libGet(function() use($id) {
            $data = $this->destino->listarUm($id);
            retornarDados($data);
        });
    }

    /**
     * Busca os destinos disponíveis
     */
    public function listarTodos() {
        $this->rest->libGet(function() {
            $data = $this->destino->listarTodos();
            retornarDados($data);
        });
    }

}
