<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Empresas extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('administracao/empresa');
    }

    /**
     * Listar uma por hash do id
     * @param $id
     */
    public function listarUm($id) {
        $this->rest->libGet(function() use($id) {
            $data = $this->empresa->listarUm($id);
            retornarDados($data);
        });
    }
}
