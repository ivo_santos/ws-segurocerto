<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tipos_viagem extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('administracao/tipo_viagem');
    }

    /**
     * Listar um tipo por id
     */
    public function listarUm($id) {
        $this->rest->libGet(function() use($id) {
            $data = $this->tipo_viagem->listarUm($id);
            retornarDados($data);
        });
    }

    /**
     * Busca os tipos disponíveis
     */
    public function listarTodos() {
        $this->rest->libGet(function() {
            $data = $this->tipo_viagem->listarTodos();
            retornarDados($data);
        });
    }

}
