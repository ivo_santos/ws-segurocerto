<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Motivos_viagem extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('administracao/motivo_viagem');
    }

    /**
     * Listar um motivo por id
     */
    public function listarUm($id) {
        $this->rest->libGet(function() use($id) {
            $data = $this->motivo_viagem->listarUm($id);
            retornarDados($data);
        });
    }

    /**
     * Busca os motivos disponíveis
     */
    public function listarTodos() {
        $this->rest->libGet(function() {
            $data = $this->motivo_viagem->listarTodos();
            retornarDados($data);
        });
    }

}
