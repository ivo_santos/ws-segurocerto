<?php

function tratarDefault($chave, $data, $default = null) {
    return array_key_exists($chave, $data) ? $data[$chave] : $default;
}

function extrairIdadeFromNascimento($data) {
    list($dia, $mes, $ano) = explode('/', $data);
    $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
    $nascimento = mktime(0, 0, 0, $mes, $dia, $ano);
    $idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);
    return $idade;
}

function tratarMoeda($valor) {
    return (float) str_replace(',', '.', str_replace('.', '', $valor));
}

function gerarChave($id) {
    return sha1(md5($id));
}

function encriptar($chave) {
    return sha1(md5($chave) . sha1(KEY_ENCRIPTION));
}

function gerarSenha($senha, $salt) {
    // senha e salt
    //SELECT SHA1(CONCAT(MD5('102030'), SHA1('155f06b22')))
    return sha1(md5($senha) . sha1($salt));
}

function f($data, $matar = true) {
    echo "<pre>";
    print_r($data);
    echo "</pre>";

    if ($matar) {
        die();
    }
}

function estados($get = 0) {
    $estados = array(
        "AC" => "Acre",
        "AL" => "Alagoas",
        "AM" => "Amazonas",
        "AP" => "Amapá",
        "BA" => "Bahia",
        "CE" => "Ceará",
        "DF" => "Distrito Federal",
        "ES" => "Espírito Santo",
        "GO" => "Goiás",
        "MA" => "Maranhão",
        "MT" => "Mato Grosso",
        "MS" => "Mato Grosso do Sul",
        "MG" => "Minas Gerais",
        "PA" => "Pará",
        "PB" => "Paraíba",
        "PR" => "Paraná",
        "PE" => "Pernambuco",
        "PI" => "Piauí",
        "RJ" => "Rio de Janeiro",
        "RN" => "Rio Grande do Norte",
        "RO" => "Rondônia",
        "RS" => "Rio Grande do Sul",
        "RR" => "Roraima",
        "SC" => "Santa Catarina",
        "SE" => "Sergipe",
        "SP" => "São Paulo",
        "TO" => "Tocantins"
    );
    $i = 1;
    foreach ($estados as $chave => $valor) {
        $estados2[$i]['id'] = $i;
        $estados2[$i]['uf'] = $chave;
        $estados2[$i]['nome'] = $valor;
        if ($get == $i) {
            return $estados2[$i];
            break;
        }
        $i++;
    }
    return $estados2;
}

function formata_data($data) {
    if (!empty($data)) {
        $data_real = explode("-", $data);
        $data = $data_real[2] . "/" . $data_real[1] . "/" . $data_real[0];
    }
    return $data;
}

function formata_data_com_horario($data, $hora = "nao") {
    if (!empty($data)) {
        $data = explode(" ", $data);
        $horario = $data['1'];
        $data_real = explode("-", $data['0']);

        if ($hora == "nao") {
            $data = $data_real[2] . "/" . $data_real[1] . "/" . $data_real[0];
        } else {
            $data = $data_real[2] . "/" . $data_real[1] . "/" . $data_real[0] . " " . $horario;
        }
    }
    return $data;
}

function desformata_data($data, $hora = false, $separador = "-") {
    $d = explode(' ', $data);
    $datas = explode("/", $d[0]);
    if ($hora && !empty($d[1])) {
        return $datas[2] . $separador . $datas[1] . $separador . $datas[0] . " " . $d[1];
    } else {
        return $datas[2] . $separador . $datas[1] . $separador . $datas[0];
    }
}

function diferenca_datas($d1, $d2, $sep = '-', $type = '') {
    $d1 = explode("-", date_format(date_create(str_replace($sep, "-", $d1)), "Y-m-d"));
    $d2 = explode("-", date_format(date_create(str_replace($sep, "-", $d2)), "Y-m-d"));

    switch ($type) {
        case 'Y':
            $X = 31536000;
            break;
        case 'm':
            $X = 2592000;
            break;
        case 'd':
            $X = 86400;
            break;
        case 'H':
            $X = 3600;
            break;
        case 'i':
            $X = 60;
            break;
        default:
            $X = 1;
            break;
    }

    return floor((mktime(0, 0, 0, $d2[1], $d2[2], $d2[0]) - mktime(0, 0, 0, $d1[1], $d1[2], $d1[0])) / $X) + 1;
}

function remove_caracteres($texto, $lower = true) {
    $characteres = array(
        'Š' => 'S', 'š' => 's', 'Ð' => 'Dj', 'Ž' => 'Z', 'ž' => 'z', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A',
        'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I',
        'Ï' => 'I', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U',
        'Û' => 'U', 'Ü' => 'U', 'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a',
        'å' => 'a', 'æ' => 'a', 'ç' => 'c', 'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i',
        'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o', 'ù' => 'u',
        'ú' => 'u', 'û' => 'u', 'ý' => 'y', 'ý' => 'y', 'þ' => 'b', 'ÿ' => 'y', 'ƒ' => 'f'
    );

    $retorno = strtr($texto, $characteres);

    if ($lower) {
        return strtolower($retorno);
    } else {
        return $retorno;
    }
}

function retornarDados($array, $debug = false, $tipo = 'json') {
    if ($debug){
        echo "<pre>";
        print_r($array);
        echo "</pre>";
        echo "<br />";
        echo "<br />";
        die('... debugando...');
    } else if ($tipo == 'json') {
        echo json_encode($array, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        die();
    } else if ($tipo == 'xml') {
        
    }
}


function calcularMenorParcela($valor, $valor_minimo, $maximo_parcela) {
    for ($maximo_parcela; $maximo_parcela >= 1; $maximo_parcela--) {
        if (($valor / $maximo_parcela) >= $valor_minimo) {
            return array('parcelas' => $maximo_parcela, 'valor' => ($valor / $maximo_parcela));
        }
    }
    return array('parcelas' => 1, 'valor' => $valor);
}

function calcularParcelamento($valor, $valor_minimo, $maximo_parcela) {
    $parcelamento = ['parcela' => [], 'valor' => []];

    for ($i = 1; $i <= $maximo_parcela; $i++) {
        if (($valor / $i) >= $valor_minimo) {
            $parcelamento['parcela'][] = $i;
            $parcelamento['valor'][] = ($valor / $i);
        }
    }

    return $parcelamento;
}


function array_sort($array, $on, $order=SORT_ASC)
{
	$new_array = array();
	$sortable_array = array();

	if (count($array) > 0) {
		foreach ($array as $k => $v) {
			if (is_array($v)) {
				foreach ($v as $k2 => $v2) {
					if ($k2 == $on) {
						$sortable_array[$k] = $v2;
					}
				}
			} else {
				$sortable_array[$k] = $v;
			}
		}

		switch ($order) {
			case SORT_ASC:
				asort($sortable_array);
				break;
			case SORT_DESC:
				arsort($sortable_array);
				break;
		}

		foreach ($sortable_array as $k => $v) {
			$new_array[$k] = $array[$k];
		}
	}

	return $new_array;
}

