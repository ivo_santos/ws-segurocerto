<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

use Dompdf\Dompdf;
use Dompdf\Options;

function pdf($html, $filename = null, $paper = "a4", $orientation = "portrait") {
    require_once("dompdf/autoload.inc.php");

    $dompdf = new Dompdf();

    $options = new Options();
    $options->setIsRemoteEnabled(true);
//    $options->setIsHtml5ParserEnabled(true);
//    $options->setIsFontSubsettingEnabled(true);
//    $options->setDebugCss(true);
    $dompdf->setOptions($options);

    $dompdf->loadHtml(($html));
    $dompdf->setPaper($paper, $orientation);
    
    $dompdf->render();

    if ($filename == null) {
        $filename = date("Y-m-d_his") . '_impressao.pdf';
    }

    return $dompdf->output();
}
