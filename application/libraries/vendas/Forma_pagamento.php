<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Forma_pagamento
{

    private $CI;

    function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->model('vendas/formas_pagamento_model');
    }

    /**
     * Lista forma de pagamento
     *
     * @param int $id
     * @return object
     */
    function listarUm($id)
    {
        return $this->CI->formas_pagamento_model
            ->setId($id)
            ->listarUm();
    }
}
