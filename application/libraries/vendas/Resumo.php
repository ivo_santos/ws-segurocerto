<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Resumo
{

    private $CI;

    function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->library('vendas/venda');
        $this->CI->load->library('vendas/forma_pagamento');
        $this->CI->load->library('cotacao/plano');
        $this->CI->load->library('cotacao/cotacao');
        $this->CI->load->library('vendas/cupom');
		$this->CI->load->library('administracao/destino_grupo');
        $this->CI->load->library('administracao/cliente');
    }

    /**
     * Constroi um resumo sobre a venda de acordo com a etapa.
     * @param $venda_id
     * @param $etapa
     * @return array
     */
    function gerar($venda_id, $etapa)
    {
        $venda = $this->CI->venda->listarUm($venda_id);
        $cotacao = $this->CI->cotacao->listarUm(gerarChave($venda->cotacao_id));
        $plano = $this->CI->plano->listarUm($venda->plano_id);

		$grupo = $this->CI->destino_grupo->listarUm(gerarChave($cotacao->destino_grupo_id));
        $viajantes = $this->CI->cliente->listarTodosPorVendaId($venda_id);
        $cupom = $this->CI->cupom->listarUm(gerarChave($venda->cupom_id));

        $resumo = [];
        $resumo['etapa'] = $etapa;


        if ($etapa >= 1) {
            $resumo['cotacao'] = [
                'destino_grupo' => $grupo->nome,
                'data_inicio' => $cotacao->data_inicio,
                'data_final' => $cotacao->data_final,
                'dias' => diferenca_datas($cotacao->data_inicio, $cotacao->data_final, '-', 'd'),
            ];
        }

        if ($etapa >= 2) {
            $resumo['plano_escolhido'] = [
                'plano' => $plano->nome,
                'valor' => $venda->valor,
                'valor_idoso' => $venda->valor_idoso,
                'limite_idade' => (!empty($plano->idade_max_acrescimo))? $plano->idade_max_acrescimo:$plano->idade_max
            ];
        }

        if ($etapa >= 3) {
            $resumo['viajantes'] = [
                'qtd_viajantes' => $cotacao->num_passageiros,
                'qtd_viajantes_maiores' => $cotacao->num_passageiros_maiores,
                'viajantes' => $viajantes
            ];
        }

        if ($etapa >= 4) {
            if ($venda->forma_pagamento_id == 1) {
                $this->CI->load->library('vendas/boleto');
                $boleto = $this->CI->boleto->listarUltimoPorVendaId($venda_id);

                $resumo['forma_pagamento'] = [
                    'forma' => $this->CI->forma_pagamento->listarUm(1)->forma_pagamento,
                    'forma_id' => 1,
                    'valor' => $boleto->valor,
                    'data_vencimento' => $boleto->data_vencimento,
                    'porcentagem_desconto' => (!empty($cupom) && $cupom->desconto > DESCONTO_BOLETO) ? $cupom->desconto : DESCONTO_BOLETO,
                    'desconto_por' => (!empty($cupom) && $cupom->desconto > DESCONTO_BOLETO) ? 'através do cupom - ' . $cupom->codigo : 'desconto por forma de pagamento escolhida',
                ];
            } else if ($venda->forma_pagamento_id == 2) {
                $resumo['forma_pagamento'] = [
                    'forma' => $this->CI->forma_pagamento->listarUm(2)->forma_pagamento,
                    'forma_id' => 2,
                    'valor_parcela' => ($venda->valor_total - $venda->valor_desconto) / $venda->parcelas,
                    'parcelas' => $venda->parcelas,
                    'porcentagem_desconto' => (!empty($cupom)) ? $cupom->desconto : '',
                    'desconto_por' => (!empty($cupom)) ? 'através do cupom - ' . $cupom->codigo : '',
                ];
            } else if ($venda->forma_pagamento_id == 3) {
                $resumo['forma_pagamento'] = [
                    'forma' => $this->CI->forma_pagamento->listarUm(3)->forma_pagamento,
                    'forma_id' => 3,
                    'valor' => ($venda->valor_total - $venda->valor_desconto),
                    'porcentagem_desconto' => (!empty($cupom)) ? $cupom->desconto : '',
                    'desconto_por' => (!empty($cupom)) ? 'através do cupom - ' . $cupom->codigo : '',
                ];
            } else if ($venda->forma_pagamento_id == 4) {
                $data_cadastro = explode(' ', $venda->data_cadastro);
                $data_vencimento = (date('Y-m-d', strtotime($data_cadastro[0] . ' + 1 days')));

                $resumo['forma_pagamento'] = [
                    'forma' => $this->CI->forma_pagamento->listarUm(4)->forma_pagamento,
                    'forma_id' => 4,
                    'valor' => $venda->valor_total - $venda->valor_desconto,
                    'data_vencimento' => $data_vencimento,
                    'porcentagem_desconto' => (!empty($cupom) && $cupom->desconto > DESCONTO_DEPOSITO) ? $cupom->desconto : DESCONTO_DEPOSITO,
                    'desconto_por' => (!empty($cupom) && $cupom->desconto > DESCONTO_DEPOSITO) ? 'através do cupom - ' . $cupom->codigo : 'desconto por forma de pagamento escolhida',
                ];
            }
        }

        return $resumo;
    }

}
