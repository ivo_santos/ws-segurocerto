<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Venda
{

	private $CI;
	private $model;

	function __construct()
	{
		$this->CI = &get_instance();
		$this->CI->load->model('vendas/vendas_model');
		$this->model = $this->CI->vendas_model;
	}

	/**
	 * Adiciona uma venda com o tipo = 2 pré-venda
	 * @param type $data
	 * @return int id inserido
	 */
	function adicionar($data)
	{
		$this->CI->load->library('cotacao/cotacao');
		$cotacao = $this->CI->cotacao->listarUm(gerarChave($data['cotacao_id']));

		$data = array_merge($data, [
			'nome' => $cotacao->nome,
			'email' => $cotacao->email,
			'telefone' => $cotacao->telefone,
			'tipo' => 2,
			'valor' => round($data['valor'], 2),
			'valor_idoso' => round($data['valor_idoso'], 2),
			'salt' => strtolower(substr(md5(uniqid(rand(), true)), 0, 6)),
			'data_cadastro' => date("Y-m-d H:i:s"),
			'data_modificacao' => date("Y-m-d H:i:s"),
		]);

		return $this->model->hydrate($data)->adicionar();
	}

	/**
	 * Altera uma venda.
	 * @param array $data
	 */
	function alterar($data)
	{
		$data['id'] = $data['venda_id'];
		$data['data_modificacao'] = date("Y-m-d H:i:s");
		$venda = (array)$this->listarUm($data['id']);

		## CONCLUI HISTORICOS PENDENTES
		if (!empty($data['status']) && $data['status'] == 5) {
			$this->CI->load->library('cotacao/cotacao_historicos');
			$this->CI->cotacao_historicos->marcarHistoricosComoConcluidos($venda['cotacao_id']);
		}

		return $this->model->hydrate(array_merge($venda, $data))->alterar();
	}

	/**
	 * Lista venda por hash id
	 * @param string $id
	 * @return object
	 */
	function listarUm($id)
	{
		return $this->CI->vendas_model->setId($id)->listarUm();
	}

	/**
	 * Lista venda por hash cotacao_id
	 * @param string $cotacao_id
	 * @return object
	 */
	function listarUmPorCotacaoId($cotacao_id)
	{
		return $this->CI->vendas_model->setCotacao_id($cotacao_id)->listarUmPorCotacaoId();
	}

	private function filtrosListarTodos($filtros)
	{
		$model = $this->CI->vendas_model;

		if (!empty($filtros['tipo_pesquisa'])) {
			$termo = !empty($filtros['termo']) ? trim($filtros['termo']) : null;

			if ($filtros['tipo_pesquisa'] == "v.id") {
				$model->setId($termo);

			} else if ($filtros['tipo_pesquisa'] == "v.documento") {
				$model->setDocumento($termo);

			} else if ($filtros['tipo_pesquisa'] == "v.nome") {
				$model->setNome($termo);

			} else if ($filtros['tipo_pesquisa'] == "c.email") {
				$model->setEmail($termo);

			} else if (substr($filtros['tipo_pesquisa'], 0, 8) == "v.status") {
				$status = explode("=", $filtros['tipo_pesquisa']);
				$model->setStatus(!empty($status[1]) ? $status[1] : null);

			} else if ($filtros['tipo_pesquisa'] == "historico_agendado_hoje" || $filtros['tipo_pesquisa'] == "historico_agendado_atrasado") {
				$this->CI->load->library('cotacao/cotacao_historicos');

				if ($filtros['tipo_pesquisa'] == "historico_agendado_hoje") {
					$historicos = $this->CI->cotacao_historicos->listarTodosAgendamentosParaHoje();
				} else {
					$historicos = $this->CI->cotacao_historicos->listarTodosAgendamentosAtrasados();
				}

				$ids = [0];
				if (!empty($historicos)) {
					foreach ($historicos as $historico) {
						$ids[] = $historico->cotacao_id;
					}
				}

				$model->setCotacoes_id($ids);

			}

			$model->setData_inicio(null)->setData_final(null);

		} else {
			$model->setData_inicio(!empty($filtros['data_inicio']) ? $filtros['data_inicio'] . ' 00:00:00' : date('Y-m-d') . ' 00:00:00')
				->setData_final(!empty($filtros['data_final']) ? $filtros['data_final'] . ' 23:59:59' : date('Y-m-d') . ' 23:59:59');
		}

		$model->setLimit(!empty($filtros['limit']) ? $filtros['limit'] : 20)->setOffset(!empty($filtros['inicio']) ? $filtros['inicio'] : 0);
	}


	/**
	 * Lista vendas de acordo com o filtro
	 *
	 * @param array $filtros
	 * @return object
	 */
	function listarTodos($filtros)
	{
		$this->filtrosListarTodos($filtros);

		$result['result'] = $this->CI->vendas_model->listarTodos();
		$result['total'] = $this->CI->vendas_model->total();


		if (!empty($result['result'])) {
			$this->CI->load->library('vouchers/voucher');
			$this->CI->load->library('administracao/cliente');

			foreach ($result['result'] as $venda) {
				$venda->viajantes = $this->CI->cliente->listarTodosPorVendaId(gerarChave($venda->id));

				if (!empty($venda->viajantes)) {
					foreach ($venda->viajantes as $viajante) {
						$voucher = $this->CI->voucher->listarUmPorClienteId(gerarChave($viajante->id));

						if (!empty($voucher)) {
							$viajante->voucher_id = $voucher->id;
							$viajante->seguradora_numero_apolice = $voucher->seguradora_numero_apolice;
							$viajante->seguradora_numero_sorte = $voucher->seguradora_numero_sorte;
							$viajante->seguradora_numero_compra = $voucher->seguradora_numero_compra;
						}
					}
				}
			}
		}

		return $result;
	}

	/**
	 * Salvar clientes em uma venda
	 * @param array $data
	 * @return array
	 */
	function salvarClientesEmUmaVenda($data)
	{
		$this->CI->load->library('administracao/cliente');
		$this->CI->load->library('cotacao/cotacao');
		$this->CI->load->library('endereco/endereco');

		$venda = $this->listarUm($data['venda_id']);

		$endereco = (array)$this->CI->endereco->listarUmPorVendaId(gerarChave($venda->id));

		if (!empty($endereco)) {
			$this->CI->endereco->alterar(array_merge($endereco, $data));
		} else {
			$this->CI->endereco->adicionar(array_merge($data, ['venda_id' => $venda->id]));
		}

		foreach ($data['cliente_documento'] as $chave => $documento) {
			if (empty($data['cliente_id'][$chave])) {
				$this->CI->cliente->adicionar([
					'nome' => $data['cliente_nome'][$chave],
					'documento' => $documento,
					'nascimento' => $data['cliente_nascimento'][$chave],
					'sexo' => $data['cliente_sexo'][$chave],
					'venda_id' => $venda->id
				]);
			} else {
				$this->CI->cliente->alterar([
					'id' => $data['cliente_id'][$chave],
					'nome' => $data['cliente_nome'][$chave],
					'documento' => $documento,
					'nascimento' => $data['cliente_nascimento'][$chave],
					'sexo' => $data['cliente_sexo'][$chave],
				]);
			}
		}

		// Altera a venda
		$this->alterar([
			'venda_id' => gerarChave($venda->id),
			'nome' => $data['nome'],
			'documento' => $data['documento'],
			'telefone' => $data['telefone'],
			'nascimento' => desformata_data($data['nascimento']),
			'email' => $data['email'],
			'valor_total' => $this->valorTotal(gerarChave($venda->id)),
		]);

		// Alterar a cotacao
		$cotacao = $this->CI->cotacao->listarUm(gerarChave($venda->cotacao_id));
		$total_clientes = $this->CI->cliente->totalClientesTotalClientesIdosos(gerarChave($venda->id));
		$this->CI->cotacao->alterar([
			'cotacao_id' => gerarChave($cotacao->id),
			'num_passageiros' => $total_clientes['total_clientes'],
			'num_passageiros_maiores' => $total_clientes['total_clientes_idosos'],
		]);

		return ['success' => true, 'mensagem' => 'Clientes associados a uma venda'];
	}

	/**
	 * Remover um cliente de uma venda por hash de cliente_id e atualiza o valor total da venda
	 * @param array $data
	 * @return array
	 */
	function removerClienteEmUmaVenda($data)
	{
		$this->CI->load->library('administracao/cliente');
		$this->CI->load->library('cotacao/cotacao');
		$this->CI->cliente->remover($data['cliente_id']);

		$this->alterar([
			'venda_id' => $data['venda_id'],
			'valor_total' => $this->valorTotal($data['venda_id']),
		]);

		$venda = $this->listarUm($data['venda_id']);

		// Alterar a cotacao
		$cotacao = $this->CI->cotacao->listarUm(gerarChave($venda->cotacao_id));
		$total_clientes = $this->CI->cliente->totalClientesTotalClientesIdosos(gerarChave($venda->id));
		$this->CI->cotacao->alterar([
			'cotacao_id' => gerarChave($cotacao->id),
			'num_passageiros' => $total_clientes['total_clientes'],
			'num_passageiros_maiores' => $total_clientes['total_clientes_idosos'],
		]);

		return ['success' => true, 'mensagem' => 'Cliente removido de uma venda'];
	}

	/**
	 * Calcula o valor total de acordo com as idades dos viajantes e com a idade máxima do plano
	 * @param string $venda_id
	 * @return float
	 */
	public function valorTotal($venda_id)
	{
		$this->CI->load->library('cotacao/plano');
		$this->CI->load->library('administracao/cliente');

		$venda = $this->listarUm($venda_id);
		$clientes = $this->CI->cliente->listarTodosPorVendaId($venda_id);
		$plano = $this->CI->plano->listarUm($venda->plano_id);

		$valor = 0;

		foreach ($clientes as $cliente) {
			$idade_limite = !empty($plano->idade_max_acrescimo) ? $plano->idade_max_acrescimo : $plano->idade_max;
			$idade = extrairIdadeFromNascimento(formata_data($cliente->nascimento));

			if ($idade <= $idade_limite) {
				if (!empty($plano->idade_max_acrescimo) && $idade <= $plano->idade_max_acrescimo && $idade >= $plano->idade_min_acrescimo) {
					$valor += $venda->valor_idoso;
				} else if ($idade <= $plano->idade_max) {
					$valor += $venda->valor;
				}
			}
		}

		return round($valor, 2);
	}

	/**
	 * Alterar a venda para paga
	 * @param type $data
	 * @return type
	 */
	function setarPagamento($data)
	{
		$this->CI->load->library('vendas/boleto');
		$this->CI->load->library('emails/emails');
		$this->CI->load->library('administracao/empresa');

		$this->alterar([
			'venda_id' => $data['venda_id'],
			'funcionario_id' => $data['funcionario_id'],
			'status' => 1,
			'aprovacao_manual' => 1,
			'comentario' => $data['comentario'],
			'data_pagamento' => desformata_data($data['data_pagamento']),
		]);

		## VERIFICA SE A VENDA É VIA BOLETO.
		$venda = $this->listarUm($data['venda_id']);
		if ($venda->forma_pagamento_id == 1) {

			// CANCELA TODOS OS BOLETOS PARA POSTERIOR APROVAÇÃO SOMENTE DO ULTIMO
			$this->CI->boleto->cancelarTodosPorVendaId($data['venda_id']);

			// APROVA O ULTIMO BOLETO
			$this->CI->boleto->aprovarPorVendaId([
				'venda_id' => $data['venda_id'],
				'funcionario_id' => $data['funcionario_id'],
				'data_pagamento' => $data['data_pagamento']
			]);
		}

		## BUSCA EMPRESA
		$empresa = $this->CI->empresa->listarUm(gerarChave(1));

		## ENVIA EMAIL DE VENDA APROVADA ERP
		$nome = explode(" ", $venda->nome);
		$this->CI->emails->emailVendaAprovadaERP(array(
			'nome' => $nome[0],
			'emails' => array($nome[0] => $venda->email),
			'codigo' => $venda->id,
			'senha' => $venda->salt,
			'empresa' => (array)$empresa,
		));

		## COLOCA A VENDA NA CRON
		$this->CI->load->library('crons/cron');
		$this->CI->cron->adicionar(gerarChave($venda->id), '2', 'confirmação manual aprovada!');

		return $data['venda_id'];
	}

	/**
	 * Busca a venda pelas credenciais
	 * @param array $data
	 * @return object
	 */
	function listarUmPorCredenciais($data)
	{
		return $this->CI->vendas_model
			->setEmail($data['email'])
			->setSalt(gerarChave(strtolower($data['senha'])))
			->setTipo(1)
			->listarUmPorCredenciais();
	}

	/**
	 * Cancela a venda os vouchers
	 *
	 * @param array $data
	 * @return object
	 */
	function cancelar($data)
	{
		$this->CI->load->library('crons/cron');
		$this->CI->load->library('administracao/cliente');
		$this->CI->load->library('seguradora/seguradora_cancelamento');
		$this->CI->load->library('cielo/cielo_cancelamento');
		$this->CI->load->library('cotacao/cotacao');
		$this->CI->load->library('administracao/empresa');

		## BUSCA ESSA VENDA
		$venda = $this->listarUm($data['venda_id']);
		$cotacao = $this->CI->cotacao->listarUm(gerarChave($venda->cotacao_id));

		## TEMPO PARA CANCELAR ATÉ 48 HORAS ANTES
		## REGRA VALIDA CASO NÃO SEJA CANCELAMENTO CRON
		$flag_cancel = date('Y-m-d', strtotime(date('Y-m-d') . ' + 1 days')) <= $cotacao->data_inicio;
		if (!$flag_cancel && !isset($data['cron'])) {
			return ['success' => false, 'message' => 'Prazo de cancelamento menor do que 24 horas.'];
		}

		## REMOVE DA CRON CASO O REGISTRO AINDA ESTEJA LÁ
		$this->CI->cron->removerPorVendaId($data['venda_id']);

		## BUSCA TODOS OS VOUCHERS DESSA VENDA
		$vouchers = $this->CI->cliente->listarClientesComVouchersPorVendaId($data['venda_id']);

		$result = [];
		if (!empty($vouchers)) {
			foreach ($vouchers as $voucher) {
				$retorno = $this->CI->seguradora_cancelamento->cancelar([
					'numero_apolice' => $voucher->seguradora_numero_apolice,
					'motivo' => 'solicitado pelo cliente',
				]);

				if (isset($retorno->codigoRetorno) && $retorno->codigoRetorno->codigo == "0") {
					$result[] = ['voucher_id' => $voucher->voucher_id, 'message' => 'cancelado com sucesso!'];
				} else {
					$result[] = ['voucher_id' => $voucher->voucher_id, 'message' => 'erro ao cancelar o voucher'];
				}
			}
		}

		## CANCELA CIELO CASO SEJA DEBITO OU CREDITO
		if ($venda->forma_pagamento_id == 2 || $venda->forma_pagamento_id == 3) {
			$this->CI->cielo_cancelamento->cancelar($venda->tid);
		}

		## MUDAR STATUS DA VENDA PARA CANCELADO
		$this->alterar([
			'venda_id' => $data['venda_id'],
			'funcionario_id' => $data['funcionario_id'],
			'status' => 3,
			'comentario' => $data['comentario']
		]);

		## BUSCA EMPRESA
		$empresa = $this->CI->empresa->listarUm(gerarChave(1));

		## ENVIA EMAIL DE VENDA CANCELADA, CASO NÃO SEJA CRON
		if (!isset($data['cron'])) {
			$nome = explode(" ", $venda->nome);
			$this->CI->emails->emailVendaCancelada([
				'nome' => $nome[0],
				'emails' => array($nome[0] => $venda->email),
				'codigo' => $venda->id,
				'empresa' => (array)$empresa,
			]);
		}

		$result['success'] = true;

		return $result;
	}

	/**
	 * TODO REMOVER ESSA FUNÇÃO QUANDO IMPLEMENTADO O METODO DE COTACAO POR DESTINO GRUPO
	 * @return mixed
	 */
	function listarUmParaCancelar()
	{
		return $this->CI->vendas_model->listarUmParaCancelar();
	}

	/**
	 * Atualiza o valor da cotação caso tenha passado do dia que ela foi feita.
	 * @param $venda_id
	 * @return array
	 */
	function atualizarCotacao($venda_id)
	{
		$this->CI->load->library('cotacao/cotacao');
		$venda = $this->listarUm($venda_id);

		if (!empty($venda) && $venda->tipo == 2) {
			$cotacao = $this->CI->cotacao->listarUm(gerarChave($venda->cotacao_id));
			if ($cotacao->data_inicio > date('Y-m-d')) {
				if ($cotacao->data_validade < date('Y-m-d')) {
					// atualiza a data de cadastro da cotação
					$cotacao_gerada = $this->CI->cotacao->gerar(gerarChave($cotacao->id));

					$flag_atualizou = false;
					if ($cotacao_gerada['success'] && !empty($cotacao_gerada)) {
						foreach ($cotacao_gerada['planos_valores'] as $plano) {
							if ($plano['plano_id'] == $venda->plano_id) {
								// atualiza a cotação
								$this->CI->cotacao->alterar([
									'cotacao_id' => gerarChave($cotacao->id),
									'data_validade' => date('Y-m-d')
								]);

								// atualiza os valores da venda
								$this->alterar([
									'venda_id' => gerarChave($venda->id),
									'coberturas_info' => $plano['coberturas_info'],
									'servicos_info' => $plano['servicos_info'],
									'valor' => $plano['valor'],
									'valor_idoso' => $plano['valor_idoso'],
								]);

								$flag_atualizou = true;
								break;
							}
						}
					}

					if ($flag_atualizou) {
						$retorno = ['success' => true, 'message' => 'Cotação atualizada.', 'cotacao_id' => $cotacao->id, 'venda_id' => $venda->id];
					} else {
						$retorno = ['success' => false, 'message' => 'Ocorreu algum erro ao atualizar.'];
					}
				} else {
					$retorno = ['success' => true, 'message' => 'Cotação atual.', 'cotacao_id' => $cotacao->id, 'venda_id' => $venda->id];
				}
			} else {
				$retorno = ['success' => false, 'message' => 'Cotação com data de início passada!'];
			}
		} else {
			$retorno = ['success' => false, 'message' => 'Apenas pré vendas podem atualizar o valor da cotação.'];
		}

		return $retorno;
	}

}
