<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Debito {

    private $CI;

    function __construct() {
        $this->CI = &get_instance();
    }

    /**
     * Venda via debito pré processar
     *
     * @param array $dados
     * @return object
     */
    function preProcessar($dados) {
        $this->CI->load->library('vendas/venda');
        $this->CI->load->library('cielo/cielo_debito');
        $this->CI->load->library('vendas/cupom');
        $this->CI->load->library('cotacao/cotacao');

        ## BUSCA ESSA VENDA
        $venda = $this->CI->venda->listarUm($dados['venda_id']);
        $cotacao = $this->CI->cotacao->listarUm(gerarChave($venda->cotacao_id));

        // CALCULA O DESCONTO CASO TENHA ALGUM CUPOM APLICADO
        if (!empty($venda->cupom_id)) {
            $cupom = $this->CI->cupom->listarUm(gerarChave($venda->cupom_id));
            if (!empty($cupom)) {
                $valor_desconto = ($cupom->desconto * $venda->valor_total) / 100;
            } else {
                $valor_desconto = 0;
            }
        } else {
            $valor_desconto = 0;
        }

        // formatar em 2 casas decimais
        $valor_desconto = number_format((float) $valor_desconto, 2, '.', '');

        ## ALTERA O TIPO DE PRE-VENDA PARA VENDA
        $this->CI->venda->alterar([
            'venda_id' => $dados['venda_id'],
            'forma_pagamento_id' => '3',
            'ip' => $dados['ip'],
            'valor_desconto' => $valor_desconto,
            'tipo' => '1',
            'status' => '2',
            'bandeira' => $dados['bandeira'],
            'parcelas' => 1
        ]);

        ## PRÉ PROCESSA A COMPRA POR DEBITO E GERA O LINK DA CIELO PARA VALIDAR NO BANCO EMISSOR.
        $transacao = $this->CI->cielo_debito->preProcessar([
            'venda_id' => $venda->id,
            'valor_total' => $venda->valor_total - $valor_desconto,
            'parcelas' => 1,
            'cartao' => [
                'numero' => $dados['numero_cartao'],
                'validade' => $dados['validade'],
                'bandeira' => $dados['bandeira'],
                'cvv' => $dados['codigo'],
                'nome_portador' => $dados['nome_portador']
            ]
        ]);

        if ($transacao['success'] && $transacao['tid']) {
            $this->CI->venda->alterar([
                'venda_id' => gerarChave($venda->id),
                'tid' => $transacao['tid']
            ]);
        }

        return $transacao;
    }

    /**
     * Venda via débito processar
     * @param $venda_id
     * @return object
     */
    function processar($venda_id) {
        $this->CI->load->library('vendas/venda');
        $this->CI->load->library('cielo/cielo_debito');
        $this->CI->load->library('emails/emails');
        $this->CI->load->library('administracao/empresa');
        $this->CI->load->library('cotacao/plano');
        $this->CI->load->library('cotacao/cotacao');

        $venda = $this->CI->venda->listarUm($venda_id);
        $cotacao = $this->CI->cotacao->listarUm(gerarChave($venda->cotacao_id));

        $return = $this->CI->cielo_debito->processar($venda->tid);
        $return['venda_id'] = $venda_id;

        ## BUSCA EMPRESA
        $empresa = $this->CI->empresa->listarUm(gerarChave(1));

        ## STATUS = 6 = APROVADO E CAPTURADO
        if ($return['status'] == 6) {

            ## ENVIA EMAIL DE NOVA VENDA
            $plano = $this->CI->plano->listarUm($venda->plano_id);
            $this->CI->emails->emailNovaVenda(array(
                'venda_id' => $venda->id,
                'cliente' => $venda->nome,
                'valor' => number_format($venda->valor_total - $venda->valor_desconto, 2, ',', '.'),
                'data_inicio' => formata_data($cotacao->data_inicio),
                'data_final' => formata_data($cotacao->data_final),
                'forma' => 'DÉBITO',
                'braco' => 'Seguro Certo',
                'plano' => $plano->nome,
            ));

            ## SETA PAGAMENTO
            $this->CI->venda->alterar([
                'venda_id' => $venda_id,
                'funcionario_id' => 0,
                'status' => 1,
                'comentario' => 'Venda paga via débito',
                'data_pagamento' => date('Y-m-d H:i:s')
            ]);

            ## ENVIA EMAIL DE VENDA AUTORIZADA
            $nome = explode(" ", $venda->nome);
            $this->CI->emails->emailVendaViaDebito(array(
                'nome' => $nome[0],
                'emails' => array($nome[0] => $venda->email),
                'codigo' => $venda->id,
                'senha' => $venda->salt,
                'empresa' => (array) $empresa,
            ));
        } else {
            ## ENVIA EMAIL DE VENDA NÃO AUTORIZADA
            $nome = explode(" ", $venda->nome);
            $this->CI->emails->emailVendaDebitoNaoAutorizado(array(
                'nome' => $nome[0],
                'url_nova_tentativa' => getenv('FRONTEND_URL') . 'transacao/gerar-nova-tentativa/' . gerarChave($venda->id),
                'data_expiracao' => formata_data_com_horario($venda->data_cadastro) . ' 23:59:59',
                'emails' => array($nome[0] => $venda->email),
                'empresa' => (array) $empresa,
            ));
        }
        return $return;
    }

}
