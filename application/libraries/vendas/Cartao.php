<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cartao {

    private $CI;

    function __construct() {
        $this->CI = &get_instance();
    }

    /**
     * Venda via cartão
     *
     * @param array $dados
     * @return object
     */
    function vendaPorCartao($dados) {
        $this->CI->load->library('vendas/cupom');
        $this->CI->load->library('crons/cron');
        $this->CI->load->library('vendas/venda');
        $this->CI->load->library('cotacao/plano');
        $this->CI->load->library('cotacao/cotacao');
        $this->CI->load->library('cielo/cielo_cartao');
        $this->CI->load->library('emails/emails');
        $this->CI->load->library('administracao/empresa');

        ## BUSCA ESSA VENDA
        $venda = $this->CI->venda->listarUm($dados['venda_id']);
        $cotacao = $this->CI->cotacao->listarUm(gerarChave($venda->cotacao_id));

        // CALCULA O DESCONTO CASO TENHA ALGUM CUPOM APLICADO
        if (!empty($venda->cupom_id)) {
            $cupom = $this->CI->cupom->listarUm(gerarChave($venda->cupom_id));
            if (!empty($cupom)) {
                $valor_desconto = ($cupom->desconto * $venda->valor_total) / 100;
            } else {
                $valor_desconto = 0;
            }
        } else {
            $valor_desconto = 0;
        }

        // formatar em 2 casas decimais
        $valor_desconto = number_format((float) $valor_desconto, 2, '.', '');

        ## ALTERA O TIPO DE PRE-VENDA PARA VENDA
        $this->CI->venda->alterar([
            'venda_id' => $dados['venda_id'],
            'forma_pagamento_id' => '2',
            'ip' => $dados['ip'],
            'valor_desconto' => $valor_desconto,
            'tipo' => '1',
            'status' => '2',
            'bandeira' => $dados['bandeira'],
            'parcelas' => (!empty($dados['parcelas'])) ? $dados['parcelas'] : 1,
        ]);

        /**
         * PROCESSA NA CIELO
         */
        $transacao = $this->CI->cielo_cartao->processar([
            'venda_id' => $venda->id,
            'valor_total' => $venda->valor_total - $valor_desconto,
            'parcelas' => (!empty($dados['parcelas'])) ? $dados['parcelas'] : 1,
            'cartao' => [
                'numero' => $dados['numero_cartao'],
                'validade' => $dados['validade'],
                'bandeira' => $dados['bandeira'],
                'cvv' => $dados['codigo'],
                'nome_portador' => $dados['nome_portador']
            ]
        ]);
        $transacao['venda_id'] = gerarChave($venda->id);

        ## BUSCA EMPRESA
        $empresa = $this->CI->empresa->listarUm(gerarChave(1));

        ## STATUS = 6 = APROVADO E CAPTURADO
        if ($transacao['status'] == 6) {

            if ($transacao['success'] && $transacao['tid']) {
                $this->CI->venda->alterar([
                    'venda_id' => gerarChave($venda->id),
                    'tid' => $transacao['tid']
                ]);
            }

            ## SETA PAGAMENTO VIA CARTÃO DE CRÉDITO
            $this->CI->venda->alterar([
                'venda_id' => gerarChave($venda->id),
                'funcionario_id' => 0,
                'status' => 1,
                'comentario' => 'Venda paga via cartão',
                'data_pagamento' => date('Y-m-d H:i:s'),
            ]);

            ## COLOCA A VENDA NA CRON
            $this->CI->cron->adicionar(gerarChave($venda->id));

            ## ENVIA EMAIL DE NOVA VENDA
            $plano = $this->CI->plano->listarUm($venda->plano_id);
            $this->CI->emails->emailNovaVenda(array(
                'venda_id' => $venda->id,
                'cliente' => $venda->nome,
                'valor' => number_format($venda->valor_total - $valor_desconto, 2, ',', '.'),
                'data_inicio' => formata_data($cotacao->data_inicio),
                'data_final' => formata_data($cotacao->data_final),
                'forma' => 'CARTÃO DE CRÉDITO',
                'braco' => 'Seguro Certo',
                'plano' => $plano->nome,
            ));

            ## ENVIA EMAIL DE VENDA AUTORIZADA
            $nome = explode(" ", $venda->nome);
            $this->CI->emails->emailVendaViaCartao(array(
                'nome' => $nome[0],
                'emails' => array($nome[0] => $venda->email),
                'codigo' => $venda->id,
                'senha' => $venda->salt,
                'empresa' => (array) $empresa,
            ));
        } else {
            ## ENVIA EMAIL DE VENDA NÃO AUTORIZADA
            $nome = explode(" ", $venda->nome);
            $this->CI->emails->emailVendaCartaoNaoAutorizado(array(
                'nome' => $nome[0],
                'url_nova_tentativa' => getenv('FRONTEND_URL') . 'transacao/gerar-nova-tentativa/' . gerarChave($venda->id),
                'data_expiracao' => formata_data_com_horario($venda->data_cadastro) . ' 23:59:59',
                'emails' => array($nome[0] => $venda->email),
                'empresa' => (array) $empresa,
            ));
        }

        return $transacao;
    }

}
