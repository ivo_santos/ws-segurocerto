<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Braco
{
	private $CI;

	function __construct()
	{
		$this->CI = &get_instance();
		$this->CI->load->model('vendas/bracos_model');
	}

	function adicionar($data)
	{
		$imagem = $this->salvarImagem($data['imagem'], null);

		return $this->CI->bracos_model
			->setNome($data['nome'])
			->setDescricao($data['descricao'])
			->setImagem($imagem)
			->adicionar();
	}

	function alterar($data)
	{
		$imagem = $this->salvarImagem($data['imagem'], $data['imagem_atual']);

		return $this->CI->bracos_model
			->setId($data['id'])
			->setNome($data['nome'])
			->setDescricao($data['descricao'])
			->setImagem($imagem)
			->alterar();
	}

	private function salvarImagem($imagem, $padrao){
		$nome = $padrao;
		if(!empty($imagem)) {
			$imagem = base64_decode(str_replace('@', '', $imagem));

			$nome = md5(date('Y-m-d H:i:s')) . ".jpg";
			$caminho = realpath('./') . "/assets/imagens/bracos/";
			file_put_contents($caminho . $nome, $imagem);
		}

		return $nome;
	}

	function listarTodos($filtros = array('limit' => '20', 'inicio' => 0))
	{
		$result['result'] = $this->CI->bracos_model->setLimit($filtros['limit'])->setOffset($filtros['inicio'])->listarTodos();
		$result['total'] = $this->CI->bracos_model->total();
		return $result;
	}

	function listarUm($id)
	{
		return $this->CI->bracos_model->setId($id)->listarUm();
	}

	function remover($id)
	{
		$caminho = realpath('./') . "/assets/imagens/bracos/";
		$braco = $this->listarUm($id);
		unlink($caminho . $braco->imagem);

		return $this->CI->bracos_model->setId($id)->remover();
	}

}
