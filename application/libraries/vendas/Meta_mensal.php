<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Meta_mensal
{

    private $CI;

    function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->model('vendas/metas_mensais_model');
    }

    /**
     * Adicionar um pelo hash do id
     */
    function adicionar($data)
    {
        return $this->CI->metas_mensais_model
            ->setMes($data['mes'])
            ->setAno($data['ano'])
            ->setValor(tratarMoeda($data['valor']))
            ->adicionar();
    }

    /**
     * Alterar um pelo hash do id
     */
    function alterar($data)
    {
        return $this->CI->metas_mensais_model
            ->setId($data['meta_id'])
            ->setMes($data['mes'])
            ->setAno($data['ano'])
            ->setValor(tratarMoeda($data['valor']))
            ->alterar();
    }

    /**
     * Listar todos
     */
    function listarTodos($filtros = array('limit' => '2', 'inicio' => 0))
    {
        $result['total'] = $this->CI->metas_mensais_model->totalMetasMensais();
        $result['result'] = $this->CI->metas_mensais_model
            ->setLimit($filtros['limit'])
            ->setOffset($filtros['inicio'])
            ->total();
        return $result;
    }

    /**
     * Listar um pelo hash do id
     * @param $id
     */
    function listarUm($id)
    {
        return $this->CI->metas_mensais_model->setId($id)->listarUm();
    }

    /**
     * Remover um pelo hash do id
     * @param $id
     */
    function remover($id)
    {
        return $this->CI->metas_mensais_model
            ->setId($id)
            ->remover();
    }

}
