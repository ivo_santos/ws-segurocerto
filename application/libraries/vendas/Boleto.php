<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Boleto {

    private $CI;

    function __construct() {
        $this->CI = &get_instance();
        $this->CI->load->model('vendas/boletos_model');
    }

    /**
     * Venda via boleto
     * @param array $data
     * @return object
     */
    function vendaPorBoleto($data) {
        $this->CI->load->library('vendas/venda');
		$this->CI->load->library('crons/cron');
		$this->CI->load->library('vendas/cupom');
        $this->CI->load->library('cotacao/cotacao');
        $this->CI->load->library('cotacao/plano');
        $this->CI->load->library('administracao/empresa');
        $this->CI->load->library('emails/emails');

        ## BUSCA ESSA VENDA
        $venda = $this->CI->venda->listarUm($data['venda_id']);
        $cotacao = $this->CI->cotacao->listarUm(gerarChave($venda->cotacao_id));

        // CALCULA O DESCONTO CASO TENHA ALGUM CUPOM APLICADO
        if (!empty($venda->cupom_id)) {
            $cupom = $this->CI->cupom->listarUm(gerarChave($venda->cupom_id));
            if (!empty($cupom) && $cupom->desconto > DESCONTO_BOLETO) {
                $valor_desconto = ($cupom->desconto * $venda->valor_total) / 100;
            } else {
                $valor_desconto = (DESCONTO_BOLETO * $venda->valor_total) / 100;
                $this->CI->cupom->ignorarCupom(['venda_id' => $data['venda_id']]);
            }
        } else {
            $valor_desconto = (DESCONTO_BOLETO * $venda->valor_total) / 100;
        }

        // formatar em 2 casas decimais
        $valor_desconto = number_format((float) $valor_desconto, 2, '.', '');

        ## ALTERA O TIPO DE PRE-VENDA PARA VENDA
        $this->CI->venda->alterar([
            'venda_id' => $data['venda_id'],
            'forma_pagamento_id' => '1',
            'ip' => $data['ip'],
            'valor_desconto' => $valor_desconto,
            'tipo' => '1',
            'status' => '4',
        ]);

        ## BUSCA ESSA VENDA
        $venda = $this->CI->venda->listarUm($data['venda_id']);

        ## ENVIA EMAIL DE NOVA VENDA
        $plano = $this->CI->plano->listarUm($venda->plano_id);
        $this->CI->emails->emailNovaVenda(array(
            'venda_id' => $venda->id,
            'cliente' => $venda->nome,
            'valor' => number_format($venda->valor_total - $valor_desconto, 2, ',', '.'),
            'data_inicio' => formata_data($cotacao->data_inicio),
            'data_final' => formata_data($cotacao->data_final),
            'forma' => 'Boleto',
            'braco' => 'Seguro Certo',
            'plano' => $plano->nome
        ));
        ## / ENVIA EMAIL DE NOVA VENDA
        ## ADICIONA BOLETO DA VENDA
        // DESCONTO DE BOLETO É ADICIONADO DENTRO DESSA FUNÇÃO
        $boleto_id = $this->adicionar($venda);

        ## LISTA UM BOLETO
        $boleto = $this->listarUm(gerarChave($boleto_id));

        ## BUSCA EMPRESA
        $empresa = $this->CI->empresa->listarUm(gerarChave(1));

        ## ENVIA EMAIL COM BOLETO
        $nome = explode(" ", $venda->nome);
        $this->CI->emails->emailVendaViaBoleto([
            'url_boleto' => $this->CI->config->base_url('boletos/ver/' . gerarChave($boleto_id)),
            'nome' => $nome[0],
            'codigo' => $venda->id,
            'senha' => $venda->salt,
            'vencimento_boleto' => formata_data($boleto->data_vencimento),
            'emails' => array($nome[0] => $venda->email),
            'empresa' => (array) $empresa,
        ]);
        ## / ENVIA EMAIL COM BOLETO

        return ['success' => true, 'boleto_id' => gerarChave($boleto_id)];
    }

    /**
     * Adiciona um boleto para venda
     * 
     * @param array $data
     * @return object
     */
    function adicionar($data) {
        return $this->CI->boletos_model
                        ->setVenda_id($data->id)
                        ->setNome($data->nome)
                        ->setEmail($data->email)
                        ->setCampo_sacado('')
                        ->setDescricao('')
                        ->setVencimento(date('Y-m-d', strtotime(date('Y-m-d') . ' + 1 days')))
                        ->setValor($data->valor_total - $data->valor_desconto)
                        ->setStatus(2)
                        ->adicionar();
    }

    /**
     * Aprova último boleto
     * @param array $data
     */
    function aprovarPorVendaId($data) {
        ## MUDAR STATUS DO BOLETO
        $boleto = $this->listarUltimoPorVendaId($data['venda_id']);

        ## APROVA O ULTIMO BOLETO
        $this->CI->boletos_model
                ->setId(gerarChave($boleto->id))
                ->setFuncionario_id($data['funcionario_id'])
                ->setStatus(1)
                ->setData_pagamento(desformata_data($data['data_pagamento']))
                ->aprovar();
    }

    /**
     * Cancela todos os boletos de uma venda por hash do venda_id
     * @param string $venda_id
     */
    function cancelarTodosPorVendaId($venda_id) {
        $this->CI->boletos_model->setVenda_id($venda_id)->cancelarTodosPorVendaId();
    }

    /**
     * Busca um boleto por hash do venda_id
     * @param string $venda_id
     * @return object
     */
    function listarUltimoPorVendaId($venda_id) {
        return $this->CI->boletos_model->setVenda_id($venda_id)->listarUltimoPorVendaId();
    }

    /**
     * Busca um boleto por hash do id
     * @param string $id
     * @return object
     */
    function listarUm($id) {
        return $this->CI->boletos_model
                        ->setId($id)
                        ->listarUm();
    }

    /**
     * Visualizar boleto por hash do id do boleto
     * @param string $id
     */
    function ver($id) {
        $this->CI->load->helper('boletophp_bradesco');
        $this->CI->load->library('administracao/cliente');
        $this->CI->load->library('vendas/venda');
        $this->CI->load->library('administracao/empresa');

        if (!empty($id)) {

            $boleto = $this->listarUm($id);

            if($boleto->status != 2){
                die('Boleto indisponível ...');
            }

            ## BUSCA ESSA VENDA
            $venda = $this->CI->venda->listarUm(gerarChave($boleto->venda_id));

            $clientes = $this->CI->cliente->listarTodosPorVendaId(gerarChave($boleto->venda_id));

            $dadosboleto["path_imagens"] = $this->CI->config->base_url() . "assets/imagens/boletophp/";
            if (!empty($boleto)) {

                $id_cobranca = $venda->id;
                $nome_cliente = $venda->nome;
//                $campo_livre_sacado = "CPF: " . $clientes[0]->documento;
                $campo_livre_sacado = "";

                $empresa = $this->CI->empresa->listarUm(gerarChave($boleto->empresa_id));

                // Data de vencimento
                list($ano, $mes, $dia) = explode("-", $boleto->data_vencimento);
                $mktime_vencimento = mktime(0, 0, 0, $mes, $dia, $ano);

                // DADOS DO BOLETO PARA O SEU CLIENTE
                $dias_de_prazo_para_pagamento = 1;
                $taxa_boleto = 0;
                $data_venc = date("d/m/Y", $mktime_vencimento); //date("d/m/Y", time() + ($dias_de_prazo_para_pagamento * 86400));  // Prazo de X dias OU informe data: "13/04/2006";
                $valor_cobrado = $boleto->valor; //"643,12"; // Valor - REGRA: Sem pontos na milhar e tanto faz com "." ou "," ou com 1 ou 2 ou sem casa decimal
                $valor_cobrado = str_replace(",", ".", $valor_cobrado);
                $valor_boleto = number_format($valor_cobrado, 2, ',', '');

                $dadosboleto["nosso_numero"] = $venda->id;  // Nosso numero sem o DV - REGRA: Mï¿½ximo de 11 caracteres!
                $dadosboleto["numero_documento"] = $dadosboleto["nosso_numero"]; // Num do pedido ou do documento = Nosso numero
                $dadosboleto["data_vencimento"] = $data_venc; // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
                $dadosboleto["data_documento"] = date("d/m/Y"); // Data de emissï¿½o do Boleto
                $dadosboleto["data_processamento"] = date("d/m/Y"); // Data de processamento do boleto (opcional)
                $dadosboleto["valor_boleto"] = $valor_boleto;  // Valor do Boleto - REGRA: Com vï¿½rgula e sempre com duas casas depois da virgula

                // DADOS DO SEU CLIENTE
                $dadosboleto["sacado"] = $nome_cliente; //"Isadora Feitoza de Carvalho";
                $dadosboleto["endereco1"] = nl2br($campo_livre_sacado); //$campo_livre1_boleto; //"CPF: " . $obj_cliente['cpf']; //390.697.758-75"; //"Endereï¿½o do seu Cliente";
                $dadosboleto["endereco2"] = ""; //$campo_livre2_boleto; //"Guarulhos - SP"; //Cidade - Estado -  CEP: 00000-000";

                // INFORMACOES PARA O CLIENTE
                $dadosboleto["demonstrativo1"] = nl2br($boleto->descricao);
                $dadosboleto["demonstrativo2"] = ""; //"<br>Taxa bancária - R$ " . number_format($taxa_boleto, 2, ',', ''); //"Mensalidade referente a nonon nonooon nononon<br>Taxa bancï¿½ria - R$ ".number_format($taxa_boleto, 2, ',', '');
                $dadosboleto["demonstrativo3"] = ""; //BoletoPhp - http://www.boletophp.com.br";
                $dadosboleto["instrucoes1"] = "** Homebrasil soluções para internet é a empresa responsável pela gestão dos boletos do Seguro Certo **";
                $dadosboleto["instrucoes2"] = "- Não receber após o vencimento";
                $dadosboleto["instrucoes3"] = "- Em caso de dúvidas entre em contato conosco: 0800 591 5107 ou atráves do email contato@tripguard.com.br";
                $dadosboleto["instrucoes4"] = "";

                // DADOS OPCIONAIS DE ACORDO COM O BANCO OU CLIENTE
                $dadosboleto["quantidade"] = "001";
                $dadosboleto["valor_unitario"] = $valor_boleto;
                $dadosboleto["aceite"] = "";
                $dadosboleto["especie"] = "R$";
                $dadosboleto["especie_doc"] = "DS";


                // ---------------------- DADOS FIXOS DE CONFIGURAï¿½ï¿½O DO SEU BOLETO --------------- //
                // DADOS DA SUA CONTA - Bradesco
                $dadosboleto["agencia"] = "0200"; // Num da agencia, sem digito
                $dadosboleto["agencia_dv"] = "3"; // Digito do Num da agencia
//                $dadosboleto["conta"] = "0107584";  // Num da conta, sem digito
                $dadosboleto["conta"] = "086197";  // Num da conta, sem digito
//                $dadosboleto["conta_dv"] = "5";  // Digito do Num da conta
                $dadosboleto["conta_dv"] = "9";  // Digito do Num da conta

//                $dadosboleto["conta_cedente"] = "0107584"; //"0403005"; // ContaCedente do Cliente, sem digito (Somente Nï¿½meros)
                $dadosboleto["conta_cedente"] = "086197"; //"0403005"; // ContaCedente do Cliente, sem digito (Somente Nï¿½meros)
//                $dadosboleto["conta_cedente_dv"] = "5"; //2"; // Digito da ContaCedente do Cliente
                $dadosboleto["conta_cedente_dv"] = "9"; //2"; // Digito da ContaCedente do Cliente
                $dadosboleto["carteira"] = "25"; //06";  // Cï¿½digo da Carteira: pode ser 06 ou 03

                $dadosboleto["identificacao"] = 'Seguro Certo';
                $dadosboleto["imagem_braco"] = $dadosboleto["path_imagens"] . "seguroviagem.png";

                $dadosboleto["cpf_cnpj"] = $empresa->cnpj; //"13.197.933/0001-62";
                $dadosboleto["endereco"] = $empresa->endereco . ", " . $empresa->numero . " - " . $empresa->complemento; //"Av. Brigadeiro Luis Antonio, 290 - 11 andar cj 116";
                $dadosboleto["cidade_uf"] = $empresa->cidade . "/" . $empresa->estado; //São Paulo / SP";
//                $dadosboleto["cedente"] = $empresa->empresa; //"Trip Guard AGENCIA DE VIAGEM E TURISMO LTDA ";
                $dadosboleto["cedente"] = "SEGURO CERTO - SEGURO VIAGEM SULAMÉRICA"; 

                gera_boleto($dadosboleto);
                die;
            }
        }
    }

}
