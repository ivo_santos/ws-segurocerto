<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pixel {

    private $CI;

    function __construct() {
        $this->CI = &get_instance();
        $this->CI->load->library('vendas/venda');
    }

    /**
     * Alterar a venda para vouchers visualizados
     * @param $venda_id
     */
    function visualizou($venda_id) {
        $this->CI->venda->alterar([
            'venda_id' => $venda_id,
            'viu_voucher_em' => date('Y-m-d H:i:s'),
        ]);
    }

}
