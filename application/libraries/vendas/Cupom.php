<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cupom {

    private $CI;

    function __construct() {
        $this->CI = &get_instance();
        $this->CI->load->model('vendas/cupom_model');
    }

    /**
     * add a coupon
     * @param $data
     * @return mixed
     */
    function adicionar($data) {
        return $this->CI->cupom_model
                        ->setNome($data['nome'])
                        ->setCodigo(str_replace(' ', '', strtolower($data['codigo'])))
                        ->setData_inicio(desformata_data($data['data_inicio']))
                        ->setData_final(desformata_data($data['data_final']))
                        ->setQuantidade($data['quantidade'])
                        ->setDesconto((int) $data['desconto'])
                        ->setStatus($data['status'])
                        ->setFuncionario_id($data['funcionario_id'])
                        ->adicionar();
    }

    /**
     * update a coupon
     * @param $data
     * @return mixed
     */
    function alterar($data) {
        return $this->CI->cupom_model
                        ->setId($data['id'])
                        ->setNome($data['nome'])
                        ->setCodigo(str_replace(' ', '', strtolower($data['codigo'])))
                        ->setData_inicio(desformata_data($data['data_inicio']))
                        ->setData_final(desformata_data($data['data_final']))
                        ->setQuantidade($data['quantidade'])
                        ->setDesconto((int) $data['desconto'])
                        ->setStatus($data['status'])
                        ->setFuncionario_id($data['funcionario_id'])
                        ->alterar();
    }

    /**
     * list all register for filter limit and offset
     * @param array $filtros
     * @return mixed
     */
    function listarTodos($filtros = ['limit' => '2', 'inicio' => 0]) {
        $result['total'] = $this->CI->cupom_model->total();

        $result['result'] = $this->CI->cupom_model
                ->setLimit($filtros['limit'])
                ->setOffset($filtros['inicio'])
                ->listarTodos();
        return $result;
    }

    /**
     * find one by hash id
     * @param $id
     * @return mixed
     */
    function listarUm($id) {
        return $this->CI->cupom_model->setId($id)->listarUm();
    }

    /**
     * remove a coupon by hash id
     * @param $id
     * @return mixed
     */
    function remover($id) {
        return $this->CI->cupom_model->setId($id)->remover();
    }

    /**
     * find a coupon by code
     * @param $code
     * @return mixed
     */
    function listarUmPorCodigo($code) {
        return $this->CI->cupom_model->setCodigo(str_replace(' ', '', strtolower($code)))->listarUmPorCodigo();
    }

    /**
     * update a coupon
     * @param $data
     * @return mixed
     */
    function atualizarQuantidadeUsado($data) {
        return $this->CI->cupom_model
                        ->setId($data['id'])
                        ->setQuantidade_usado($data['quantidade_usado'])
                        ->atualizarQuantidadeUsado();
    }

    function aplicarCupom($data) {
        $this->CI->load->library('vendas/venda');

        $cupom = $this->listarUmPorCodigo($data['codigo']);
        $venda = $this->CI->venda->listarUm($data['venda_id']);

        if (!empty($cupom) && !empty($venda)) {
            if ($cupom->id != $venda->cupom_id) {
                $this->CI->venda->alterar([
                    'venda_id' => $data['venda_id'],
                    'cupom_id' => $cupom->id
                ]);

                $this->atualizarQuantidadeUsado([
                    'id' => gerarChave($cupom->id),
                    'quantidade_usado' => ((int) $cupom->quantidade_usado + 1)
                ]);

                return [
                    'success' => true,
                    'message' => 'Cupom aplicado com sucesso!',
                    'cupom' => $cupom
                ];
            } else {
                return [
                    'success' => true,
                    'message' => 'Esse cupom já foi aplicado!',
                    'cupom' => $cupom
                ];
            }
        } else {
            return [
                'success' => false,
                'message' => 'Cupom inválido!'
            ];
        }
    }

    function ignorarCupom($data) {
        $this->CI->load->library('vendas/venda');
        $venda = $this->CI->venda->listarUm($data['venda_id']);
        $cupom = $this->listarUm(gerarChave($venda->cupom_id));

        if (!empty($cupom) && !empty($venda)) {
            $this->CI->venda->alterar([
                'venda_id' => $data['venda_id'],
                'cupom_id' => null
            ]);

            $this->atualizarQuantidadeUsado([
                'id' => gerarChave($cupom->id),
                'quantidade_usado' => ((int) $cupom->quantidade_usado - 1)
            ]);

            return [
                'success' => true,
                'message' => 'Cupom removido com sucesso!',
            ];
        } else {
            return [
                'success' => false,
                'message' => 'Ocorreu algum erro!'
            ];
        }
    }

}
