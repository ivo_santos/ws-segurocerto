<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Deposito {

    private $CI;

    function __construct() {
        $this->CI = &get_instance();
    }

    /**
     * Venda via depósito
     * @param array $data
     * @return object
     */
    function vendaPorDeposito($data) {
        $this->CI->load->library('emails/emails');
		$this->CI->load->library('crons/cron');
		$this->CI->load->library('administracao/empresa');
        $this->CI->load->library('cotacao/plano');
        $this->CI->load->library('cotacao/cotacao');
        $this->CI->load->library('vendas/venda');
        $this->CI->load->library('vendas/cupom');

        ## BUSCA ESSA VENDA
        $venda = $this->CI->venda->listarUm($data['venda_id']);
        $cotacao = $this->CI->cotacao->listarUm(gerarChave($venda->cotacao_id));

        // CALCULA O DESCONTO CASO TENHA ALGUM CUPOM APLICADO
        if (!empty($venda->cupom_id)) {
            $cupom = $this->CI->cupom->listarUm(gerarChave($venda->cupom_id));
            if (!empty($cupom) && $cupom->desconto > DESCONTO_DEPOSITO) {
                $valor_desconto = ($cupom->desconto * $venda->valor_total) / 100;
            } else {
                $valor_desconto = (DESCONTO_DEPOSITO * $venda->valor_total) / 100;
                $this->CI->cupom->ignorarCupom(['venda_id' => $data['venda_id']]);
            }
        } else {
            $valor_desconto = (DESCONTO_DEPOSITO * $venda->valor_total) / 100;
        }
        
        // formatar em 2 casas decimais
        $valor_desconto = number_format((float) $valor_desconto, 2, '.', '');

        ## ALTERA O TIPO DE PRE-VENDA PARA VENDA
        $this->CI->venda->alterar([
            'venda_id' => $data['venda_id'],
            'forma_pagamento_id' => '4',
            'ip' => $data['ip'],
            'valor_desconto' => $valor_desconto,
            'tipo' => '1',
			'status' => '4',
		]);

        ## ENVIA EMAIL DE NOVA VENDA
        $plano = $this->CI->plano->listarUm($venda->plano_id);
        $this->CI->emails->emailNovaVenda(array(
            'venda_id' => $venda->id,
            'cliente' => $venda->nome,
            'valor' => number_format($venda->valor_total - $valor_desconto, 2, ',', '.'),
            'data_inicio' => formata_data($cotacao->data_inicio),
            'data_final' => formata_data($cotacao->data_final),
            'forma' => 'Depósito',
            'braco' => 'Seguro Certo',
            'plano' => $plano->nome
        ));

        ## BUSCA EMPRESA
        $empresa = $this->CI->empresa->listarUm(gerarChave(1));

        ## ENVIA EMAIL DEPÓSITO
        $nome = explode(" ", $venda->nome);
        $data_cadastro = explode(' ', $venda->data_cadastro);
        $this->CI->emails->emailVendaViaDeposito(array(
            'nome' => $nome[0],
            'codigo' => $venda->id,
            'senha' => $venda->salt,
            'valor_pagamento' => number_format($venda->valor_total - $valor_desconto, 2, ',', '.'),
            'data_inicio' => formata_data(date('Y-m-d', strtotime($data_cadastro[0] . ' + 1 days'))),
            'emails' => array($nome[0] => $venda->email),
            'empresa' => (array) $empresa,
        ));
        ## / ENVIA EMAIL DEPÓSITO

        return ['success' => true, 'venda_id' => gerarChave($venda->id)];
    }

}
