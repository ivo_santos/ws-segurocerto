<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Endereco {

    private $CI;

    function __construct() {
        $this->CI = &get_instance();
        $this->CI->load->model('endereco/enderecos_model');
    }

    /**
     * Adicionar endereço
     * @param array $data
     * @return int id
     */
    function adicionar($data) {
        return $this->CI->enderecos_model->hydrate($data)->adicionar();
    }

    /**
     * Alterar endereco
     * @param array $data
     * @return void
     */
    function alterar($data) {
        return $this->CI->enderecos_model->hydrate($data)->alterar();
    }
    
    /**
     * Listar um endereço por hash do venda_id
     * @param string $venda_id
     * @return type
     */
    function listarUmPorVendaId($venda_id) {
        return $this->CI->enderecos_model->setVenda_id($venda_id)->listarUmPorVendaId();
    }
    
    
    /**
     * Robo correios, busca cep dos correios
     * @param string $cep
     * @return object
     */
    function roboCorreios($cep) {
        $consulta = $this->CI->enderecos_model->setCep(str_replace('-', '', $cep))->getCacheCorreios();

        if (!empty($consulta)) {
            $endereco = unserialize($consulta->dados_correios);
            return $endereco;
        } else {

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, ROBO_CORREIOS . "cep.php?cep=" . $cep);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $dados = json_decode(curl_exec($ch));
            curl_close($ch);


            if (!empty($dados)) {
                $this->CI->enderecos_model
                        ->setCep(str_replace('-', '', $cep))
                        ->setDados_correios(serialize($dados))
                        ->inserirCacheCorreios();
            }

            return $dados;
        }
    }

}
