<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

use Tritoq\Payment\Cielo\AnaliseRisco;
use Tritoq\Payment\Cielo\Cartao;
use Tritoq\Payment\Cielo\CieloService;
use Tritoq\Payment\Cielo\Loja;
use Tritoq\Payment\Cielo\Pedido;
use Tritoq\Payment\Cielo\Portador;
use Tritoq\Payment\Cielo\Transacao;
use Tritoq\Payment\Cielo\AnaliseRisco\Modelo\ClienteAnaliseRiscoTest;
use Tritoq\Payment\Cielo\AnaliseRisco\PedidoAnaliseRisco;

class Cielo_cartao {

    private $CI;

    function __construct() {
        $this->CI = &get_instance();

        $this->CI->load->model('cielo/cielo_pagamentos_model');
        $this->CI->load->model('cielo/log_cielo_model');
    }

    public function processar($post) {
        $cartao = $post['cartao'];
        $cartao['numero'] = str_replace(' ', '', $cartao['numero']);

        $validade = explode('/', str_replace(' ', '', $cartao['validade']));
        $ano = $validade[1];
        $ano = strlen($ano) === 2 ? '20' . $ano : $ano;
        $mes = strlen($validade[0]) === 2 ? $validade[0] : '0' . $validade[0];

        $status = -1;
        $hoje = new DateTime();
        $id_venda = @$post['venda_id'];

        try {
            $loja = new Loja();

            if (getenv('CIELO_AMBIENTE') == "PRODUCAO") {
                $loja->setNomeLoja('Seguro Certo')
                        ->setAmbiente(Loja::AMBIENTE_PRODUCAO)
                        ->setChave(CIELO_CHAVE)
                        ->setNumeroLoja(CIELO_NUMERO);
            } else {
                $loja->setNomeLoja('Seguro Certo')
                        ->setAmbiente(Loja::AMBIENTE_TESTE)
                        ->setChave(Loja::LOJA_CHAVE_AMBIENTE_TESTE)
                        ->setNumeroLoja(Loja::LOJA_NUMERO_AMBIENTE_TESTE);
                if (getenv('CIELO_TESTE_APROVACAO') == "SIM") {
                    $post['valor_total'] = str_replace(',', '', str_replace('.', '', substr($post['valor_total'], 0, -2) . '00'));
                }
            }

			$cartao['bandeira'] = ($cartao['bandeira'] == "maestro") ? 'mastercard' : $cartao['bandeira'];

			$creditCard = new Cartao();
            $creditCard->setNumero($cartao['numero'])
                    ->setCodigoSegurancaCartao($cartao['cvv'])
                    ->setBandeira($cartao['bandeira'])
                    ->setNomePortador($cartao['nome_portador'])
                    ->setValidade($ano . $mes);

            $transacao = new Transacao();
            $transacao->setAutorizar(Transacao::AUTORIZAR_SEM_AUTENTICACAO)->setCapturar(Transacao::CAPTURA_NAO)->setParcelas($post['parcelas'])->setProduto(($post['parcelas'] == 1) ? Transacao::PRODUTO_CREDITO_AVISTA : Transacao::PRODUTO_PARCELADO_LOJA);

            $valorTotal = str_replace(',', '', str_replace('.', '', number_format($post['valor_total'], 2, ',', '.')));

            $pedido = new Pedido();
            $pedido->setDataHora(new \DateTime())
				->setDescricao('Seguro Certo')
				->setIdioma(Pedido::IDIOMA_PORTUGUES)
				->setNumero(substr($id_venda . "SeguroCerto", 0, 20))
				->setValor($valorTotal);

            $service = new CieloService(array(
                'portador' => new Portador(),
                'loja' => $loja,
                'cartao' => $creditCard,
                'transacao' => $transacao,
                'pedido' => $pedido
            ));

//            $service->setSsl(realpath('/ssl/VERISI~1.CRT'));

            $service->doTransacao(false, false);

            $requisicoes = $transacao->getRequisicoes(Transacao::REQUISICAO_TIPO_TRANSACAO);

            if (!empty($requisicoes)) {
                $req = $requisicoes[0]->getXmlRequisicao()->asXML();

                /**
                 * REMOVE DADOS DO CARTÃO DO XML QUE VAMOS SALVAR 
                 * CASO A CHAVE DE CAPTURA ESTEJA SIM
                 */
                if (getenv('CIELO_CAPTURA') == "SIM") {
                    if (strpos($req, '<codigo-seguranca>') !== false) {
                        $req = substr($req, 0, strpos($req, '<dados-portador><numero>')) .
                                substr($req, strpos($req, '</codigo-seguranca>') + strlen('</codigo-seguranca>'));
                    }
                }

                // ERRO CASO EXISTA O CAMPO CÓDIGO DE ERRO
				$retorno = json_decode(json_encode((array)$requisicoes[0]->getXmlRetorno()), TRUE);
				if(isset($retorno['codigo'])){
					return [
						'success' => false,
						'message' => $retorno['mensagem'],
						'status' => 4
					];
				}

				$ret = $requisicoes[0]->getXmlRetorno()->asXML();
				$this->CI->cielo_pagamentos_model->registrar(array(
                    'request' => $req,
                    'response' => $ret,
                    'origem' => getenv('CIELO_AMBIENTE'),
                    'operacao' => 'transacao',
                    'data_pagamento' => $hoje->format('Y-m-d H:i:s')
                ));
            }

            $tid = (array) $transacao->getTid();
            $tid = $tid[0];

            $status = (array) $transacao->getStatus();
            $status = $status[0];

            // AUTORIZADO
            if ($status == 4) {

                if (getenv('CIELO_CAPTURA') != "SIM") {
                    return [
                        'success' => true,
                        'status' => '',
                        'request' => $req,
                        'response' => $ret,
                    ];
                    die();
                }

                $service->doCaptura();
                $requisicoes = $transacao->getRequisicoes(Transacao::REQUISICAO_TIPO_CAPTURA);

                $status = (array) $transacao->getStatus();
                $status = $status[0];

                if ($status == 6) {
                    $req = $requisicoes[0]->getXmlRequisicao()->asXML();
                    $ret = $requisicoes[0]->getXmlRetorno()->asXML();

                    $this->CI->cielo_pagamentos_model->registrar(array(
                        'request' => $req,
                        'response' => $ret,
                        'origem' => getenv('CIELO_AMBIENTE'),
                        'operacao' => 'transacao-captura',
                        'data_pagamento' => $hoje->format('Y-m-d H:i:s')
                    ));

                    return [
                        'success' => true,
                        'tid' => $tid,
                        'message' => "Transação capturada com sucesso!",
                        'status' => $status
                    ];
                } else {
                    return [
                        'success' => false,
                        'tid' => $tid,
                        'message' => 'Transaçao não capturada!',
                        'status' => $status
                    ];
                }
            } else {
                return [
                    'success' => false,
                    'tid' => $tid,
                    'message' => 'Transação não autorizada!',
                    'status' => $status
                ];
            }
        } catch (\Exception $ex) {
            $this->CI->log_cielo_model->registrar(array(
                'msg' => $ex->getMessage(),
                'id_venda' => $id_venda,
                'origem' => getenv('CIELO_AMBIENTE')
            ));

            return [
                'success' => false,
                'message' => $ex->getMessage(),
                'status' => $status
            ];
        }
    }

}
