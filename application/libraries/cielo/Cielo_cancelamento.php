<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

use Tritoq\Payment\Cielo\CieloService;
use Tritoq\Payment\Cielo\Loja;
use Tritoq\Payment\Cielo\Transacao;

class Cielo_cancelamento {

    private $CI;

    function __construct() {
        $this->CI = &get_instance();
        $this->CI->load->model('cielo/cielo_pagamentos_model');
    }

    public function cancelar($tid) {

        $hoje = new DateTime();
        $loja = new Loja();

        if (getenv('CIELO_AMBIENTE') == "PRODUCAO") {
            $loja->setNomeLoja('TripGuard')
                    ->setAmbiente(Loja::AMBIENTE_PRODUCAO)
                    ->setChave(CIELO_CHAVE)
                    ->setNumeroLoja(CIELO_NUMERO);
        } else {
            $loja->setNomeLoja('TripGuard')
                    ->setAmbiente(Loja::AMBIENTE_TESTE)
                    ->setChave(Loja::LOJA_CHAVE_AMBIENTE_TESTE)
                    ->setNumeroLoja(Loja::LOJA_NUMERO_AMBIENTE_TESTE);
        }

        $transacao = new Transacao();
        $transacao->setTid($tid);
        $service = new CieloService(array(
            'loja' => $loja,
            'transacao' => $transacao,
        ));
        $service->setSsl(realpath('/ssl/VERISI~1.CRT'));
        $service->doCancela();

        $requisicoes = $transacao->getRequisicoes(Transacao::REQUISICAO_TIPO_CANCELA);
        
        $status = (array) $transacao->getStatus();
        $status = $status[0];

        $req = $requisicoes[0]->getXmlRequisicao()->asXML();
        $ret = $requisicoes[0]->getXmlRetorno()->asXML();

        $this->CI->cielo_pagamentos_model->registrar([
            'request' => $req,
            'response' => $ret,
            'origem' => getenv('CIELO_AMBIENTE'),
            'operacao' => 'cancelamento',
            'data_pagamento' => $hoje->format('Y-m-d H:i:s')
        ]);

        if ($status == Transacao::STATUS_CANCELADA) {
            return [
                'success' => true,
                'tid' => $tid,
                'message' => 'Transação cancelada com sucesso',
                'status' => $status
            ];
        } else {
            return [
                'success' => false,
                'tid' => $tid,
                'message' => 'Erro no cancelamento',
                'status' => $status
            ];
        }
    }

}
