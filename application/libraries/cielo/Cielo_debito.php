<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

use Tritoq\Payment\Cielo\AnaliseRisco;
use Tritoq\Payment\Cielo\Cartao;
use Tritoq\Payment\Cielo\CieloService;
use Tritoq\Payment\Cielo\Loja;
use Tritoq\Payment\Cielo\Pedido;
use Tritoq\Payment\Cielo\Portador;
use Tritoq\Payment\Cielo\Transacao;
use Tritoq\Payment\Cielo\AnaliseRisco\Modelo\ClienteAnaliseRiscoTest;
use Tritoq\Payment\Cielo\AnaliseRisco\PedidoAnaliseRisco;

class Cielo_debito {

    private $CI;

    function __construct() {
        $this->CI = &get_instance();
        $this->CI->load->model('cielo/cielo_pagamentos_model');
        $this->CI->load->model('cielo/log_cielo_model');
    }

    /**
     * Faz o pré processamento da transação onde gera a url de autenticação.
     *
     * @param array $post
     * @return array
     */
    public function preProcessar($post) {

        $cartao = $post['cartao'];
        $cartao['numero'] = str_replace(' ', '', $cartao['numero']);

        $validade = explode('/', str_replace(' ', '', $cartao['validade']));
        $ano = $validade[1];
        $ano = strlen($ano) === 2 ? '20' . $ano : $ano;
        $mes = strlen($validade[0]) === 2 ? $validade[0] : '0' . $validade[0];

        $hoje = new DateTime();
        $id_venda = @$post['venda_id'];

        if ($cartao['bandeira'] == "maestro") {
            $cartao['bandeira'] = "mastercard";
        }

        try {
            $loja = new Loja();

            if (getenv('CIELO_AMBIENTE') == "PRODUCAO") {
                $loja->setNomeLoja('Seguro Certo')
                        ->setAmbiente(Loja::AMBIENTE_PRODUCAO)
                        ->setChave(CIELO_CHAVE)
                        ->setUrlRetorno(getenv('FRONTEND_URL') . 'pagar/debito-retorno-autenticacao/' . gerarChave($id_venda))
                        ->setNumeroLoja(CIELO_NUMERO);
            } else {
                $loja->setNomeLoja('Seguro Certo')
                        ->setAmbiente(Loja::AMBIENTE_TESTE)
                        ->setChave(Loja::LOJA_CHAVE_AMBIENTE_TESTE)
                        ->setUrlRetorno(getenv('FRONTEND_URL') . 'pagar/debito-retorno-autenticacao/' . gerarChave($id_venda))
                        ->setNumeroLoja(Loja::LOJA_NUMERO_AMBIENTE_TESTE);
            }

            $card = new Cartao();
            $card->setNumero($cartao['numero'])->setCodigoSegurancaCartao($cartao['cvv'])->setBandeira($cartao['bandeira'])->setNomePortador($cartao['nome_portador'])->setValidade($ano . $mes);

            $transacao = new Transacao();
            $transacao->setAutorizar(Transacao::AUTORIZAR_SOMENTE_AUTENTICADA)->setCapturar(Transacao::CAPTURA_NAO)->setParcelas(1)->setProduto(Transacao::PRODUTO_DEBITO);

            $valorTotal = str_replace(',', '', str_replace('.', '', number_format($post['valor_total'], 2, ',', '.')));

            $pedido = new Pedido();
            $pedido->setDataHora(new \DateTime())
				->setDescricao('Seguro Certo')
				->setIdioma(Pedido::IDIOMA_PORTUGUES)
				->setNumero(substr($id_venda . "SeguroCerto", 0, 20))
				->setValor($valorTotal);

            $service = new CieloService([
                'portador' => new Portador(),
                'loja' => $loja,
                'cartao' => $card,
                'transacao' => $transacao,
                'pedido' => $pedido
            ]);

//            $service->setSsl(realpath('/ssl/VERISI~1.CRT'));

            $service->doTransacao(false, false);

            $requisicoes = $transacao->getRequisicoes(Transacao::REQUISICAO_TIPO_TRANSACAO);

            if (!empty($requisicoes)) {
                $req = $requisicoes[0]->getXmlRequisicao()->asXML();

                /**
                 * REMOVE DADOS DO CARTÃO DO XML QUE VAMOS SALVAR 
                 * CASO A CHAVE DE CAPTURA ESTEJA SIM
                 */
                if (getenv('CIELO_CAPTURA') == "SIM") {
                    if (strpos($req, '<codigo-seguranca>') !== false) {
                        $req = substr($req, 0, strpos($req, '<dados-portador><numero>')) . substr($req, strpos($req, '</codigo-seguranca>') + strlen('</codigo-seguranca>'));
                    }
                }

                $ret = $requisicoes[0]->getXmlRetorno()->asXMl();

                $this->CI->cielo_pagamentos_model->registrar([
                    'request' => $req,
                    'response' => $ret,
                    'origem' => getenv('CIELO_AMBIENTE'),
                    'operacao' => 'requisicao-transacao',
                    'data_pagamento' => $hoje->format('Y-m-d H:i:s')
                ]);
            }


            $tid = (array) $transacao->getTid();
            if (!empty($tid)) {
                $url = (array) $transacao->getUrlAutenticacao();
                return [
                    'success' => true,
                    'tid' => $tid[0],
                    'url_autenticacao' => $url[0],
                    'message' => 'Transaçao pre processada com sucesso!'
                ];
            } else {
                // SALVA MENSAGEM DE ERRO NO LOG NO BANCO
                $this->CI->log_cielo_model->registrar(['msg' => 'Erro ao pre-processar cartao de debito', 'id_venda' => $id_venda, 'origem' => getenv('CIELO_AMBIENTE')]);

                return [
                    'success' => false,
                    'message' => 'Parece que ocorreu algum erro ao tentar processar o seu cartao de debito.',
                ];
            }
        } catch (\Exception $ex) {
            $this->CI->log_cielo_model->registrar(array(
                'msg' => $ex->getMessage(),
                'id_venda' => $id_venda,
                'origem' => getenv('CIELO_AMBIENTE')
            ));

            return [
                'success' => false,
                'message' => $ex->getMessage(),
            ];
        }
    }

    /**
     * Processa a segunda etapa do débito em caso de autenticado
     *
     * @param string $tid hash retornado pela cielo no preProcessar
     * @return array
     */
    public function processar($tid) {

        $hoje = new DateTime();
        $loja = new Loja();

        if (getenv('CIELO_AMBIENTE') == "PRODUCAO") {
            $loja->setNomeLoja('TripGuard')
                    ->setAmbiente(Loja::AMBIENTE_PRODUCAO)
                    ->setChave(CIELO_CHAVE)
                    ->setNumeroLoja(CIELO_NUMERO);
        } else {
            $loja->setNomeLoja('TripGuard')
                    ->setAmbiente(Loja::AMBIENTE_TESTE)
                    ->setChave(Loja::LOJA_CHAVE_AMBIENTE_TESTE)
                    ->setNumeroLoja(Loja::LOJA_NUMERO_AMBIENTE_TESTE);
        }

        $transacao = new Transacao();
        $transacao->setTid($tid);
        $service = new CieloService(array(
            'loja' => $loja,
            'transacao' => $transacao,
        ));
        $service->setSsl(realpath('/ssl/VERISI~1.CRT'));
        $service->doConsulta();

        $requisicoes = $transacao->getRequisicoes(Transacao::REQUISICAO_TIPO_CONSULTA);

        $status = (array) $transacao->getStatus();
        $status = $status[0];

        $req = $requisicoes[0]->getXmlRequisicao()->asXML();
        $ret = $requisicoes[0]->getXmlRetorno()->asXML();

        $this->CI->cielo_pagamentos_model->registrar([
            'request' => $req,
            'response' => $ret,
            'origem' => getenv('CIELO_AMBIENTE'),
            'operacao' => 'transacao',
            'data_pagamento' => $hoje->format('Y-m-d H:i:s')
        ]);

        // AUTORIZADO
        if ($status == 4) {

            if (getenv('CIELO_CAPTURA') != "SIM") {
                return [
                    'success' => true,
                    'message' => 'Transaçao autorizada mas não capturada.',
                    'status' => '',
                    'request' => $req,
                    'response' => $ret,
                ];
                die();
            }

            $service->doCaptura();
            $status = $transacao->getStatus();
            $requisicoes = $transacao->getRequisicoes(Transacao::REQUISICAO_TIPO_CAPTURA);

            if ($status == 6) {
                $req = $requisicoes[0]->getXmlRequisicao()->asXML();
                $ret = $requisicoes[0]->getXmlRetorno()->asXML();

                $this->CI->cielo_pagamentos_model->registrar([
                    'request' => $req,
                    'response' => $ret,
                    'origem' => getenv('CIELO_AMBIENTE'),
                    'operacao' => 'transacao-captura',
                    'data_pagamento' => $hoje->format('Y-m-d H:i:s')
                ]);

                return [
                    'success' => true,
                    'tid' => $tid,
                    'message' => "Transação capturada com sucesso!",
                    'status' => $status
                ];
            } else {
                return [
                    'success' => false,
                    'tid' => $tid,
                    'message' => 'Transaçao não capturada.',
                    'status' => $status
                ];
            }
        } else {
            return [
                'success' => true,
                'tid' => $tid,
                'message' => 'Transaçao não autorizada!',
                'status' => $status
            ];
        }
    }

}
