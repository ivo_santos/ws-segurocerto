<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

use Tritoq\Payment\Cielo\AnaliseRisco;
use Tritoq\Payment\Cielo\Cartao;
use Tritoq\Payment\Cielo\CieloService;
use Tritoq\Payment\Cielo\Loja;
use Tritoq\Payment\Cielo\Pedido;
use Tritoq\Payment\Cielo\Portador;
use Tritoq\Payment\Cielo\Transacao;
use Tritoq\Payment\Cielo\AnaliseRisco\Modelo\ClienteAnaliseRiscoTest;
use Tritoq\Payment\Cielo\AnaliseRisco\PedidoAnaliseRisco;

class Cielo
{

    private $CI;

    function __construct()
    {
        $this->CI = &get_instance();
    }

    public function efetuar($post)
    {
        //f($post, false);

        $this->CI->load->model('cielo/cielo_pagamentos_model');
        $this->CI->load->model('cielo/log_cielo_model');

        $cartao = $post['cartao'];
        $cartao['numero'] = str_replace(' ', '', $cartao['numero']);

        $validade = explode('/', str_replace(' ', '', $cartao['validade']));
        $ano = $validade[1];
        $ano = strlen($ano) === 2 ? '20' . $ano : $ano;
        $mes = strlen($validade[0]) === 2 ? $validade[0] : '0' . $validade[0];

        $hash = '';
        $status = -1;
        $hoje = new DateTime();
        $id_venda = @$post['venda_id'];

        $mensagem = "";

        try {
            $loja = new Loja();

            if (MEU_AMBIENTE == "PRODUCAO") {
                $loja->setNomeLoja(' ')
                    ->setAmbiente(Loja::AMBIENTE_PRODUCAO)
                    ->setChave(CIELO_CHAVE)
                    ->setNumeroLoja(CIELO_NUMERO);
            } else {
                $loja->setNomeLoja('Seguro Viagem')
                    ->setAmbiente(Loja::AMBIENTE_TESTE)
                    ->setChave(Loja::LOJA_CHAVE_AMBIENTE_TESTE)
                    ->setNumeroLoja(Loja::LOJA_NUMERO_AMBIENTE_TESTE);

                //CONFIGURA PARA NO AMBIENTE DE TESTES A TRANSACAO POSSA SER APROVADA
                // NECESSARIO 00 NO FINAL DO VALOR

                if ($cartao['numero'] == "4551870000000183" || isset($post['testando']) && $post['testando']) {
                    $post['valor_total'] = str_replace(',', '', str_replace('.', '', substr($post['valor_total'], 0, -2) . '00'));
                }
            }

            $creditCard = new Cartao();
            $creditCard
                ->setNumero($cartao['numero'])
                ->setCodigoSegurancaCartao($cartao['cvv'])
                ->setBandeira($cartao['bandeira'])
                ->setNomePortador($cartao['nome_portador'])
                ->setValidade($ano . $mes);

            $transacao = new Transacao();
            $transacao
                ->setAutorizar(Transacao::AUTORIZAR_SEM_AUTENTICACAO)
                ->setCapturar(Transacao::CAPTURA_NAO)
                ->setParcelas($post['parcelas'])
                ->setProduto(($post['parcelas'] == 1) ? Transacao::PRODUTO_CREDITO_AVISTA : Transacao::PRODUTO_PARCELADO_LOJA);

            $valorTotal = str_replace('.', '', $post['valor_total']);

            $pedido = new Pedido();
            $pedido->setDataHora(new \DateTime())
                ->setDescricao('Compra de Seguro')
                ->setIdioma(Pedido::IDIOMA_PORTUGUES)
                ->setNumero(!empty($id_venda) ? $id_venda : '123456')
                ->setValor($valorTotal);

            $service = new CieloService(array(
                'portador' => new Portador(),
                'loja' => $loja,
                'cartao' => $creditCard,
                'transacao' => $transacao,
                'pedido' => $pedido
            ));

            $service->setSsl(realpath('/ssl/VERISI~1.CRT'));

            $service->doTransacao(false, false);

            $requisicoes = $transacao->getRequisicoes(Transacao::REQUISICAO_TIPO_TRANSACAO);

            if (!empty($requisicoes)) {
                $req = $requisicoes[0]->getXmlRequisicao()->asXML();

//                if (strpos($req, '<codigo-seguranca>') !== false) {
//                    $req = substr($req, 0, strpos($req, '<dados-portador><numero>')) .
//                        substr($req, strpos($req, '</codigo-seguranca>') + strlen('</codigo-seguranca>'));
//                }

                $ret = $requisicoes[0]->getXmlRetorno()->asXML();

                $this->CI->cielo_pagamentos_model->registrar(array(
                    'request' => $req,
                    'response' => $ret,
                    'origem' => MEU_AMBIENTE,
                    'operacao' => 'transacao-do',
                    'data_pagamento' => $hoje->format('Y-m-d H:i:s')
                ));
            }

            $hash = (string)$transacao->getTid();
            $status = $transacao->getStatus();
            $requisicoes = $transacao->getRequisicoes(Transacao::REQUISICAO_TIPO_TRANSACAO);

            //f($requisicoes, false);

            if (!empty($requisicoes)) {
                $req = $requisicoes[0]->getXmlRequisicao()->asXML();

                if (strpos($req, '<codigo-seguranca>') !== false) {
                    $req = substr($req, 0, strpos($req, '<dados-portador><numero>')) . substr($req, strpos($req, '</codigo-seguranca>') + strlen('</codigo-seguranca>'));
                }

                $ret = $requisicoes[0]->getXmlRetorno()->asXML();

                $this->CI->cielo_pagamentos_model->registrar(array(
                    'request' => $req,
                    'response' => $ret,
                    'origem' => MEU_AMBIENTE,
                    'operacao' => 'transacao-efetuar',
                    'data_pagamento' => $hoje->format('Y-m-d H:i:s')
                ));
            }

            if ($status == 4) {
                $service->doCaptura();
                $status = $transacao->getStatus();
                $requisicoes = $transacao->getRequisicoes(Transacao::REQUISICAO_TIPO_CAPTURA);

                if (!empty($requisicoes)) {
                    $req = $requisicoes[0]->getXmlRequisicao()->asXML();
                    $ret = $requisicoes[0]->getXmlRetorno()->asXML();

                    $this->CI->cielo_pagamentos_model->registrar(array(
                        'request' => $req,
                        'response' => $ret,
                        'origem' => MEU_AMBIENTE,
                        'operacao' => 'transacao-captura',
                        'data_pagamento' => $hoje->format('Y-m-d H:i:s')
                    ));

                    $mensagem = "Transação capturada com sucesso!";
                }
            }
        } catch (\Exception $ex) {
            $mensagem = $ex->getMessage();

            $this->CI->log_cielo_model->registrar(array(
                'msg' => $ex->getMessage(),
                'id_venda' => $id_venda,
                'origem' => MEU_AMBIENTE
            ));
        }

        $status = (array)$status;
        return array(
            'hash' => $hash,
            'status' => $status[0],
            'mensagem' => $mensagem
        );
    }

    public function consultar($tid)
    {
        $loja = new Loja();

        if (MEU_AMBIENTE == "PRODUCAO") {
            $loja->setNomeLoja('TripGuard')
                ->setAmbiente(Loja::AMBIENTE_PRODUCAO)
                ->setChave(CIELO_CHAVE)
                ->setNumeroLoja(CIELO_NUMERO);
        } else {
            $loja->setNomeLoja('TripGuard')
                ->setAmbiente(Loja::AMBIENTE_TESTE)
                ->setChave(Loja::LOJA_CHAVE_AMBIENTE_TESTE)
                ->setNumeroLoja(Loja::LOJA_NUMERO_AMBIENTE_TESTE);
        }

        $transacao = new Transacao();
        $transacao->setTid($tid);
        $service = new CieloService(array(
            'loja' => $loja,
            'transacao' => $transacao,
        ));
        $service->setSsl(realpath('/ssl/VERISI~1.CRT'));
        $service->doConsulta();

        $requisicoes = $transacao->getRequisicoes(Transacao::REQUISICAO_TIPO_CONSULTA);

    //    echo ($requisicoes[0]->getXmlRequisicao()->asXML());
        echo ($requisicoes[0]->getXmlRetorno()->asXML());
        die;
        
        return array(
            'hash' => $tid,
            'status' => $transacao->getStatus()
        );
    }

    public function debitoPreProcessar($post)
    {

        $this->CI->load->model('cielo/cielo_pagamentos_model');
        $this->CI->load->model('cielo/log_cielo_model');

        $cartao = $post['cartao'];
        $cartao['numero'] = str_replace(' ', '', $cartao['numero']);

        $validade = explode('/', str_replace(' ', '', $cartao['validade']));
        $ano = $validade[1];
        $ano = strlen($ano) === 2 ? '20' . $ano : $ano;
        $mes = strlen($validade[0]) === 2 ? $validade[0] : '0' . $validade[0];

        $hoje = new DateTime();
        $id_venda = @$post['venda_id'];
        
        if($cartao['bandeira'] == "maestro"){
            $cartao['bandeira'] = "mastercard";
        }
        
        try {
            $loja = new Loja();

            if (MEU_AMBIENTE == "PRODUCAO") {
                $loja->setNomeLoja('TripGuard')
                    ->setAmbiente(Loja::AMBIENTE_PRODUCAO)
                    ->setChave(CIELO_CHAVE)
                    ->setUrlRetorno(getenv('FRONTEND_URL') . 'pagar/debito-retorno-autenticacao/' . gerarChave($id_venda))
                    ->setNumeroLoja(CIELO_NUMERO);
            } else {
                $loja->setNomeLoja('TripGuard')
                    ->setAmbiente(Loja::AMBIENTE_TESTE)
                    ->setChave(Loja::LOJA_CHAVE_AMBIENTE_TESTE)
                    ->setUrlRetorno(getenv('FRONTEND_URL') . 'pagar/debito-retorno-autenticacao/' . gerarChave($id_venda))
                    ->setNumeroLoja(Loja::LOJA_NUMERO_AMBIENTE_TESTE);
            }

            $card = new Cartao();
            $card->setNumero($cartao['numero'])->setCodigoSegurancaCartao($cartao['cvv'])->setBandeira($cartao['bandeira'])->setNomePortador($cartao['nome_portador'])->setValidade($ano . $mes);

//            $card
//                ->setNumero('5453010000066167')
//                ->setCodigoSegurancaCartao('123')
//                ->setBandeira('mastercard')
//                ->setNomePortador('ivo santos')
//                ->setValidade('201805');

            $transacao = new Transacao();
            $transacao->setAutorizar(Transacao::AUTORIZAR_SOMENTE_AUTENTICADA)->setCapturar(Transacao::CAPTURA_NAO)->setParcelas(1)->setProduto(Transacao::PRODUTO_DEBITO);
    
            $valorTotal = str_replace(',', '', str_replace('.', '', number_format($post['valor_total'], 2, ',', '.')));
    
            $pedido = new Pedido();
            $pedido->setDataHora(new \DateTime())->setDescricao('SEGURO VIAGEM')->setIdioma(Pedido::IDIOMA_PORTUGUES)->setNumero(!empty($id_venda) ? $id_venda : '123456')->setValor($valorTotal);

            $service = new CieloService([
                'portador' => new Portador(),
                'loja' => $loja,
                'cartao' => $card,
                'transacao' => $transacao,
                'pedido' => $pedido
            ]);

            $service->setSsl(realpath('/ssl/VERISI~1.CRT'));

            $service->doTransacao(false, false);

            $requisicoes = $transacao->getRequisicoes(Transacao::REQUISICAO_TIPO_TRANSACAO);

            if (!empty($requisicoes)) {
                $req = $requisicoes[0]->getXmlRequisicao()->asXML();

//                if (strpos($req, '<codigo-seguranca>') !== false) {
//                    $req = substr($req, 0, strpos($req, '<dados-portador><numero>')) . substr($req, strpos($req, '</codigo-seguranca>') + strlen('</codigo-seguranca>'));
//                }

                $ret = $requisicoes[0]->getXmlRetorno()->asXMl();

                $this->CI->cielo_pagamentos_model->registrar([
                    'request' => $req,
                    'response' => $ret,
                    'origem' => MEU_AMBIENTE,
                    'operacao' => 'requisicao-transacao',
                    'data_pagamento' => $hoje->format('Y-m-d H:i:s')
                ]);
            }


            $tid = (array)$transacao->getTid();
            if (!empty($tid)) {
                $url = (array)$transacao->getUrlAutenticacao();
                return [
                    'success' => true,
                    'tid' => $tid[0],
                    'url_autenticacao' => $url[0],
                    'message' => 'Transaçao pre processada com sucesso!'
                ];
            } else {
                // SALVA MENSAGEM DE ERRO NO LOG NO BANCO
                $this->CI->log_cielo_model->registrar(['msg' => 'Erro ao pre-processar cartao de debito', 'id_venda' => $id_venda, 'origem' => MEU_AMBIENTE]);

                return [
                    'success' => false,
                    'message' => 'Parece que ocorreu algum erro ao tentar processar o seu cartao de debito.',
                ];
            }

        } catch (\Exception $ex) {
            $this->CI->log_cielo_model->registrar(array(
                'msg' => $ex->getMessage(),
                'id_venda' => $id_venda,
                'origem' => MEU_AMBIENTE
            ));

            return [
                'success' => false,
                'message' => $ex->getMessage(),
            ];
        }

    }
    
    public function debitoProcessar($tid)
    {
    
        $this->CI->load->model('cielo/cielo_pagamentos_model');
        $this->CI->load->model('cielo/log_cielo_model');
        
        $hoje = new DateTime();
        
        $loja = new Loja();
        
        if (MEU_AMBIENTE == "PRODUCAO") {
            $loja->setNomeLoja('TripGuard')
                ->setAmbiente(Loja::AMBIENTE_PRODUCAO)
                ->setChave(CIELO_CHAVE)
                ->setNumeroLoja(CIELO_NUMERO);
        } else {
            $loja->setNomeLoja('TripGuard')
                ->setAmbiente(Loja::AMBIENTE_TESTE)
                ->setChave(Loja::LOJA_CHAVE_AMBIENTE_TESTE)
                ->setNumeroLoja(Loja::LOJA_NUMERO_AMBIENTE_TESTE);
        }
        
        $transacao = new Transacao();
        $transacao->setTid($tid);
        $service = new CieloService(array(
            'loja' => $loja,
            'transacao' => $transacao,
        ));
        $service->setSsl(realpath('/ssl/VERISI~1.CRT'));
        $service->doConsulta();
        
        $requisicoes = $transacao->getRequisicoes(Transacao::REQUISICAO_TIPO_CONSULTA);
    
        $status = (array)$transacao->getStatus();
        $status = $status[0];
        
        $req = $requisicoes[0]->getXmlRequisicao()->asXML();
        $ret = $requisicoes[0]->getXmlRetorno()->asXML();
        
        $this->CI->cielo_pagamentos_model->registrar([
            'request' => $req,
            'response' => $ret,
            'origem' => MEU_AMBIENTE,
            'operacao' => 'transacao',
            'data_pagamento' => $hoje->format('Y-m-d H:i:s')
        ]);
        
        // AUTORIZADO
        if ($status == 4) {
            
            f($status);
            $service->doCaptura();
            $status = $transacao->getStatus();
            $requisicoes = $transacao->getRequisicoes(Transacao::REQUISICAO_TIPO_CAPTURA);
    
            if ($status == 6) {
                $req = $requisicoes[0]->getXmlRequisicao()->asXML();
                $ret = $requisicoes[0]->getXmlRetorno()->asXML();
            
                $this->CI->cielo_pagamentos_model->registrar([
                    'request' => $req,
                    'response' => $ret,
                    'origem' => MEU_AMBIENTE,
                    'operacao' => 'transacao-captura',
                    'data_pagamento' => $hoje->format('Y-m-d H:i:s')
                ]);
    
                return [
                    'tid' => $tid,
                    'message' => "Transação capturada com sucesso!",
                    'status' => $status
                ];
            }else{
                return [
                    'tid' => $tid,
                    'message' => 'Transaçao nao capturada.',
                    'status' => $status
                ];
            }
        }else{
            return [
                'tid' => $tid,
                'message' => 'Transaçao nao autorizada!',
                'status' => $status
            ];
        }
    }


}
