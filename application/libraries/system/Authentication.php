<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Authentication
{

    private $CI;
    private $urls_exceptions = array(
        'boletos/ver',
        'crons/emissao-de-vouchers',
        'crons/envio-de-email-com-vouchers',
        'crons/cancelar-vendas-nao-pagas',
        'voucher/online',
        'voucher/download',
        'vendas/pixel',
        'emails/abertura',
    );

    function __construct()
    {
        $this->CI = &get_instance();
        $u = $this->CI->uri->segment(1) . "/" . $this->CI->uri->segment(2);
        if (!in_array($u, $this->urls_exceptions)) {
            $this->CI->load->model('system/Authentication_model');
            $this->validateAuthentication();
        }
    }

    private function validateAuthentication()
    {

        if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
            $user = $this->getWSUser();
            if (empty($user)) {
                $message = "Restrict Access";
                header("WWW-Authenticate: Basic realm='" . $message . "'");
                header("HTTP/1.1 401 unauthorized");
                $json['error'] = "user or password is wrong!";
                echo json_encode($json);
                exit();
            }
        } else {
            $message = "Restrict Access";
            header("WWW-Authenticate: Basic realm='" . $message . "'");
            header("HTTP/1.1 401 unauthorized");
            $json['error'] = "user or password is wrong!";
            echo json_encode($json);
            exit();
        }
    }

    private function getWSUser()
    {
        return $this->CI->Authentication_model
            ->setUser($_SERVER['PHP_AUTH_USER'])
            ->setPassword(sha1(md5($_SERVER['PHP_AUTH_PW'])))
            ->getWSUser();
    }

}
