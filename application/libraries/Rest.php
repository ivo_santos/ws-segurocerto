<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Rest {

    private $CI;

    function __construct() {
        $this->CI = &get_instance();
    }
    
    function libGet($acao, $validacao = null) {
        if ($this->validaGet()) {
            if (!is_string($validacao) && !is_array($validacao) && is_callable($validacao)) {
                if ($validacao()) {
                    $acao();
                    die();
                }
            } else {
                $acao();
                die();
            }
        }
    }

    function validaGet() {
        return $_SERVER['REQUEST_METHOD'] == "GET";
    }

    function libDelete($acao, $validacao = null) {
        if ($this->validaDelete()) {
            if (!is_string($validacao) && !is_array($validacao) && is_callable($validacao)) {
                if ($validacao()) {
                    $acao();
                    die();
                }
            } else {
                $acao();
                die();
            }
        }
    }

    function validaDelete() {
        return $_SERVER['REQUEST_METHOD'] == "DELETE";
    }

    function libPost($acao, $validacao = null) {
        if ($this->validaPost()) {
            if (!is_string($validacao) && !is_array($validacao) && is_callable($validacao)) {
                if ($validacao()) {
                    $acao();
                    die();
                }
            } else {
                $acao();
                die();
            }
        }
    }

    function validaPost() {
        return $_SERVER['REQUEST_METHOD'] == "POST";
    }

    function libPut($acao, $validacao = null) {
        if ($this->validaPut()) {
            if (!is_string($validacao) && !is_array($validacao) && is_callable($validacao)) {
                if ($validacao()) {
                    $acao();
                    die();
                }
            } else {
                $acao();
                die();
            }
        }
    }

    function validaPut() {
        return $_SERVER['REQUEST_METHOD'] == "PUT";
    }

}
