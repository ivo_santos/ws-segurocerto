<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Emails
{

	private $CI;

	function __construct()
	{
		$this->CI = &get_instance();
		$this->CI->load->library('emails/sendemail');
	}

	/**
	 * Template de nova venda para os administradores
	 *
	 * @param array $data
	 * @return boolean true se o email foi enviado
	 */
	function emailNovaVenda($data)
	{
		$config['assunto'] = "Nova venda";
		$config['fromName'] = "Segurocerto";
		$config['emails'] = array(getenv('EMAIL_COPIA_1'));
		$config['bcc'] = array(getenv('EMAIL_COPIA_2'), getenv('EMAIL_COPIA_3'));

		$template['venda_id'] = $data['venda_id'];
		$template['cliente'] = $data['cliente'];
		$template['data_inicio'] = $data['data_inicio'];
		$template['data_final'] = $data['data_final'];
		$template['valor'] = $data['valor'];
		$template['forma'] = $data['forma'];
		$template['plano'] = $data['plano'];
		$template['braco'] = 'Segurocerto';

		$template['base_url'] = $this->CI->config->base_url();

		$config['html'] = $this->CI->parser->parse("emails/vendas/info_nova_venda.php", $template, TRUE);
		return $this->CI->sendemail->libSendEmail($config);
	}

	/**
	 * Template de email com os vouchers
	 *
	 * @param array $data
	 * @return boolean true se o email foi enviado
	 */
	function emailVouchers($data)
	{
		$config['assunto'] = "Apólice de seguro viagem";
		$config['fromName'] = "Segurocerto";
		$config['emails'] = $data['emails'];
		$config['arquivos'] = $data['arquivos'];

		// cliente
		$template['cliente'] = $data['cliente'];
		$template['venda_id'] = $data['venda_id'];
		$template['nome'] = $data['nome'];
		$template['codigo'] = $data['codigo'];
		$template['senha'] = $data['senha'];
		$template['empresa'] = $data['empresa'];

		$template['numero_sorte'] = $data['numero_sorte'];

		$template['base_url'] = $this->CI->config->base_url();

		$config['html'] = $this->CI->parser->parse("emails/vouchers/email_com_vouchers.php", $template, TRUE);
		return $this->CI->sendemail->libSendEmail($config);
	}

	/**
	 * Template de venda via boleto
	 *
	 * @param array $data
	 * @return boolean true se o email foi enviado
	 */
	function emailVendaViaBoleto($data)
	{
		$config['assunto'] = "Falta pouco para concluir a sua contratação";
		$config['fromName'] = "Segurocerto";
		$config['emails'] = $data['emails'];

		$template['nome'] = $data['nome'];
		$template['codigo'] = $data['codigo'];
		$template['senha'] = $data['senha'];
		$template['empresa'] = $data['empresa'];

		$template['url_boleto'] = $data['url_boleto'];
		$template['vencimento_boleto'] = $data['vencimento_boleto'];
		$template['base_url'] = $this->CI->config->base_url();

		$config['html'] = $this->CI->parser->parse("emails/vendas/email_venda_boleto.php", $template, TRUE);
		return $this->CI->sendemail->libSendEmail($config);
	}

	/**
	 * Template de venda via depósito
	 *
	 * @param array $data
	 * @return boolean true se o email foi enviado
	 */
	function emailVendaViaDeposito($data)
	{
		$config['assunto'] = "Falta pouco para concluir a sua contratação";
		$config['fromName'] = "Segurocerto";
		$config['emails'] = $data['emails'];

		$template['nome'] = $data['nome'];
		$template['codigo'] = $data['codigo'];
		$template['senha'] = $data['senha'];
		$template['data_inicio'] = $data['data_inicio'];
		$template['valor_pagamento'] = $data['valor_pagamento'];
		$template['empresa'] = $data['empresa'];

		$template['base_url'] = $this->CI->config->base_url();

		$config['html'] = $this->CI->parser->parse("emails/vendas/email_venda_deposito.php", $template, TRUE);
		return $this->CI->sendemail->libSendEmail($config);
	}

	/**
	 * Template de venda via cartão de crédito
	 *
	 * @param array $data
	 * @return boolean true se o email foi enviado
	 */
	function emailVendaViaCartao($data)
	{
		$config['assunto'] = "Contratação realizada com sucesso";
		$config['fromName'] = "Segurocerto";
		$config['emails'] = $data['emails'];

		$template['nome'] = $data['nome'];
		$template['codigo'] = $data['codigo'];
		$template['senha'] = $data['senha'];
		$template['empresa'] = $data['empresa'];

		$template['base_url'] = $this->CI->config->base_url();

		$config['html'] = $this->CI->parser->parse("emails/vendas/email_venda_cartao.php", $template, TRUE);
		return $this->CI->sendemail->libSendEmail($config);
	}

	/**
	 * Template de cartão não autorizado
	 *
	 * @param array $data
	 * @return boolean true se o email foi enviado
	 */
	function emailVendaCartaoNaoAutorizado($data)
	{
		$config['assunto'] = "Transação não autorizada";
		$config['fromName'] = "Segurocerto";
		$config['emails'] = $data['emails'];

		$template['nome'] = $data['nome'];
		$template['url_nova_tentativa'] = $data['url_nova_tentativa'];
		$template['data_expiracao'] = $data['data_expiracao'];
		$template['empresa'] = $data['empresa'];

		$template['base_url'] = $this->CI->config->base_url();

		$config['html'] = $this->CI->parser->parse("emails/vendas/email_venda_cartao_nao_autorizado.php", $template, TRUE);
		return $this->CI->sendemail->libSendEmail($config);
	}

	/**
	 * Template de venda via debito
	 * @param array $data
	 * @return boolean true se o email foi enviado
	 */
	function emailVendaViaDebito($data)
	{
		$config['assunto'] = "Contratação realizada com sucesso";
		$config['fromName'] = "Segurocerto";
		$config['emails'] = $data['emails'];

		$template['nome'] = $data['nome'];
		$template['codigo'] = $data['codigo'];
		$template['senha'] = $data['senha'];
		$template['empresa'] = $data['empresa'];

		$template['base_url'] = $this->CI->config->base_url();

		$config['html'] = $this->CI->parser->parse("emails/vendas/email_venda_debito.php", $template, TRUE);
		return $this->CI->sendemail->libSendEmail($config);
	}

	/**
	 * Template de debito não autorizado
	 * @param array $data
	 * @return boolean true se o email foi enviado
	 */
	function emailVendaDebitoNaoAutorizado($data)
	{
		$config['assunto'] = "Transação não autorizada";
		$config['fromName'] = "Segurocerto";
		$config['emails'] = $data['emails'];

		$template['nome'] = $data['nome'];
		$template['url_nova_tentativa'] = $data['url_nova_tentativa'];
		$template['data_expiracao'] = $data['data_expiracao'];
		$template['empresa'] = $data['empresa'];

		$template['base_url'] = $this->CI->config->base_url();

		$config['html'] = $this->CI->parser->parse("emails/vendas/email_venda_debito_nao_autorizado.php", $template, TRUE);
		return $this->CI->sendemail->libSendEmail($config);
	}


	/**
	 * Template de venda aprovada via ERP
	 * @param array $data
	 * @return boolean true se o email foi enviado
	 */
	function emailVendaAprovadaERP($data)
	{
		$config['assunto'] = "Contratação realizada com sucesso";
		$config['fromName'] = "Segurocerto";
		$config['emails'] = $data['emails'];

		$template['nome'] = $data['nome'];
		$template['codigo'] = $data['codigo'];
		$template['senha'] = $data['senha'];
		$template['empresa'] = $data['empresa'];

		$template['base_url'] = $this->CI->config->base_url();

		$config['html'] = $this->CI->parser->parse("emails/vendas/email_venda_erp_aprovada.php", $template, TRUE);
		return $this->CI->sendemail->libSendEmail($config);
	}

	/**
	 * Template de venda cancelada
	 * @param array $data
	 * @return boolean true se o email foi enviado
	 */
	function emailVendaCancelada($data)
	{
		$config['assunto'] = "Contratação cancelada";
		$config['fromName'] = "Segurocerto";
		$config['emails'] = $data['emails'];

		$template['nome'] = $data['nome'];
		$template['codigo'] = $data['codigo'];
		$template['empresa'] = $data['empresa'];

		$template['base_url'] = $this->CI->config->base_url();

		$config['html'] = $this->CI->parser->parse("emails/vendas/email_venda_cancelada.php", $template, TRUE);
		return $this->CI->sendemail->libSendEmail($config);
	}


	/**
	 * Template cotação para meu email
	 *
	 * @param array $data
	 * @return boolean true se o email foi enviado
	 */
	function emailCotacaoParaMeuEmail($data)
	{
		$config['assunto'] = "Cotação do seu Seguro Viagem";
		$config['fromName'] = "Segurocerto";
		$config['emails'] = $data['emails'];

		$template['nome'] = $data['nome'];
		$template['data_expiracao'] = $data['data_expiracao'];
		$template['empresa'] = $data['empresa'];

		$template['planos'] = $data['cotacao']['planos'];
		$template['cotacao'] = $data['cotacao']['cotacao'];

		$template['url_cotacao_por_email'] = $data['url_cotacao_por_email'];

		$template['base_url'] = $this->CI->config->base_url();

		$config['html'] = $this->CI->parser->parse("emails/cotacao/cotacao_para_meu_email.php", $template, TRUE);
		return $this->CI->sendemail->libSendEmail($config);
	}

	/**
	 * Template cotação para amigo
	 *
	 * @param array $data
	 * @return boolean true se o email foi enviado
	 */
	function emailCotacaoParaAmigo($data)
	{
		$a = explode(' ', $data['nome']);
		$config['assunto'] = $a[0] . " enviou essa cotação de seguro viagem";

		$config['fromName'] = "Segurocerto";
		$config['emails'] = $data['emails'];

		$template['nome'] = $data['nome'];
		$template['nome_amigo'] = $data['nome_amigo'];

		$template['cotacao'] = $data['cotacao'];
		$template['data_expiracao'] = $data['data_expiracao'];
		$template['empresa'] = $data['empresa'];

		$template['planos'] = $data['cotacao']['planos'];
		$template['cotacao'] = $data['cotacao']['cotacao'];

		$template['url_cotacao_por_email'] = $data['url_cotacao_por_email'];

		$template['base_url'] = $this->CI->config->base_url();

		$config['html'] = $this->CI->parser->parse("emails/cotacao/cotacao_para_amigo.php", $template, TRUE);
		return $this->CI->sendemail->libSendEmail($config);
	}

	function emailCms($data)
	{
		/**
		 * Rastrear abertura do email.
		 */
		$rastreio = "<img src='". $this->CI->config->base_url('emails/abertura/' . $data['historico_id'])."' width='1' height='1' style='display: none;'/>";

		$config['fromName'] = "Segurocerto";
		$config['emails'] = $data['emails'];
		$config['assunto'] = $data['assunto'];
		$config['html'] = $rastreio . $data['conteudo'];
//		f($config);
		return $this->CI->sendemail->libSendEmail($config);
	}

}
