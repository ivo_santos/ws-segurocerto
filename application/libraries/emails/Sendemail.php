<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class SendEmail {

    private $CI;

    function __construct() {
        $this->CI = &get_instance();
    }
    
    function libSendEmail($config = array()) {
        $path = APPPATH . 'third_party/phpmailer_normal.php';
        require_once($path);

        $mail = new PHPMailer();
        $mail->IsSmtp();
        $mail->Host = "smtp.sparkpostmail.com";
        $mail->Password = "9da3265a186e99b7f1672fb757fbbd16e626b8b7";
        $mail->Username = "SMTP_Injection";
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = 'tsl';
        $mail->Port = 587;
        $mail->CharSet = 'UTF-8';

        $mail->FromName = "=?UTF-8?B?".base64_encode($config['fromName'])."?=";
        $mail->From = "contato@segurocerto.com";
        $mail->Subject = "=?UTF-8?B?".base64_encode($config['assunto'])."?=";

        $mail->AddReplyTo('contato@tripguard.com.br', "=?UTF-8?B?".base64_encode('TripGuard')."?=");

        if (isset($config['arquivos'])) {
            foreach ($config['arquivos'] as $chave => $arquivo) {
                $mail->AddAttachment($arquivo, $chave);
            }
        }

        if (isset($config['bcc'])) {
            foreach ($config['bcc'] as $nome => $email) {
                $mail->AddBcc($email, "=?UTF-8?B?".base64_encode($nome)."?=");
            }
        }

        if (isset($config['cc'])) {
            foreach ($config['cc'] as $nome => $email) {
                $mail->AddCc($email, "=?UTF-8?B?".base64_encode($nome)."?=");
            }
        }

        foreach ($config['emails'] as $nome => $email) {
            $mail->AddAddress($email, "=?UTF-8?B?".base64_encode($nome)."?=");
        }
        
        $mail->IsHTML(true);
        $mail->Body = $config['html'];

        if ($mail->Send()) {
            return "success";
        } else {
            return "error: " . $mail->ErrorInfo . " " . print_r($mail, true);
        }
    }
}
