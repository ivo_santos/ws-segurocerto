<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

require_once('assinatura/SignedSoapClient.php');

class Ws_seguradora
{

	private $ws_url;

	function __construct()
	{
		ini_set("default_socket_timeout", "99999999");
		ini_set("soap.wsdl_cache_enabled", 1);

		if (getenv('SULAMERICA_AMBIENTE') == "PRODUCAO") {
            $this->ws_url = realpath('./'). "/application/libraries/seguradora/assinatura/wsdl.wsdl";
		} else {
			$this->ws_url = realpath('./') . "/application/libraries/seguradora/assinatura/wsdl-v.wsdl";
		}
	}

	public function processar($funcao, $parametros = [])
	{
		$inicio = microtime(true);
		try {

			$conexao_soap = new SignedSoapClient($this->ws_url, [
				"trace" => true,
				"stream_context" => stream_context_create([
					'ssl' => [
						'verify_peer' => false,
						'verify_peer_name' => false,
						'allow_self_signed' => true
					]
				]),
				'ssl' => [
					'cert' => realpath('./') . '/application/libraries/seguradora/assinatura/certificate.pfx',
					'certpasswd' => '102030',
				]
			]);

			$return = $conexao_soap->$funcao($parametros);

			if (getenv('GRAVAR_WS_LOG') == "TRUE") {
				$this->salvarLog($funcao, $conexao_soap);
			}

			return $return;
		} catch (SoapFault $e) {
			if (ini_get('default_socket_timeout') < (microtime(true) - $inicio)) {
				return "Erro de time out!";
			} else {
				return $e->getMessage();
			}
		}
	}

	/**
	 * Salva o xml de requisicao e de resposta
	 *
	 * @param $funcao
	 * @param $conexao_soap
	 */
	private function salvarLog($funcao, $conexao_soap){
		$dir = realpath('./assets/logs') . '/' . date('m-Y');
		if (!is_dir($dir)) {
			mkdir($dir);
		}
		chmod($dir, 0777);

		$outputxml = '/' . date('d-m-Y-H:i:s') . '-';

		## o xml do request está sendo feito na classe SignedSoapClient
//		$file_request = $dir . $outputxml . 'request-' . ($funcao).'.xml';
//		file_put_contents($file_request, $conexao_soap->__getLastRequest());
//		chmod($file_request, 0777);

		$file_response = $dir . $outputxml . 'response-' . ($funcao).'.xml';
		file_put_contents($file_response, $conexao_soap->__getLastResponse());
		chmod($file_response, 0777);
	}

}
