<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once (APPPATH . '/libraries/seguradora/Library_base.php');

class Formas_pagamento extends Library_base {

    /**
     * Lista as formas de pagamento no webservice da seguradora
     * @return object
     */
    public function listar() {
        $parametros = $this->tratarParametros();
        return $this->CI->ws_seguradora->processar('GetFormasPagamento', $parametros)->formasPagamento->formaPagamento;
    }

    private function tratarParametros() {
        return [
            'identificadorSistemaOrigem' => 'RE',
            'codigoRepresentante' => $this->codigo_representante,
            'dataInicio' => date('Y-m-d'),
            'quantidadeViajantes' => '1',
        ];
    }

}
