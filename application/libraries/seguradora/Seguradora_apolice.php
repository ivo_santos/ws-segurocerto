<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once (APPPATH . '/libraries/seguradora/Library_base.php');

class Seguradora_apolice extends Library_base {

    public function listar($dados) {
        $parametros = $this->tratarParametros($dados);
        return $this->CI->ws_seguradora->processar('ConsultarApolices', $parametros);
    }

    private function tratarParametros($dados) {
        return [
            'identificadorSistemaOrigem' => 'RE',
            'codigoRepresentante' => $this->codigo_representante,
            'numeroApolice' => $dados['numero_apolice'],
            'quantidadeResultados' => 1,
        ];
    }

}
