<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once (APPPATH . '/libraries/seguradora/Library_base.php');

class Seguradora_cancelamento extends Library_base {

    public function cancelar($dados) {
        $parametros = $this->tratarParametros($dados);
        return $this->CI->ws_seguradora->processar('CancelarApolice', $parametros);
    }

    private function tratarParametros($dados) {
        return [
            'identificadorSistemaOrigem' => 'RE',
            'codigoRepresentante' => $this->codigo_representante,
            'numeroApolice' => $dados['numero_apolice'],
            'dataSolicitacao' => (new DateTime('NOW'))->format('Y-m-d\TH:i:s'),
            'motivoSolicitacao' => $dados['motivo'],
        ];
    }

}
