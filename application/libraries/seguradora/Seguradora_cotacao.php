<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once(APPPATH . '/libraries/seguradora/Library_base.php');

class Seguradora_cotacao extends Library_base
{

    /**
     * Lista as formas de pagamento no webservice da seguradora
     * @return object
     */
    public function cotar($dados)
    {
        $parametros = $this->tratarParametros($dados);
        $retorno = $this->CI->ws_seguradora->processar('GetPlanos', $parametros);

        return $retorno;
    }

    private function tratarParametros($dados)
    {
        return [
            'identificadorSistemaOrigem' => 'RE',
            'codigoRepresentante' => $this->codigo_representante,
            'dataInicio' => $dados['data_inicio'],
            'dataFim' => $dados['data_final'],
            'idLocalidadeOrigem' => $dados['localidade_origem_id'],
            'idLocalidadeDestino' => $dados['localidade_destino_id'],
            'idTipo' => $dados['tipo_id'],
            'idMotivo' => $dados['motivo_id'],
            'viajantesFaixa' => [
                'viajanteFaixa' => [
                    ['idadeMinima' => '65', 'idadeMaxima' => '90', 'quantidadeViajantes' => '1'],
                    ['idadeMinima' => '0', 'idadeMaxima' => '64', 'quantidadeViajantes' => '1'],
                ]
            ]
        ];
    }

}
