<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once (APPPATH . '/libraries/seguradora/Library_base.php');

class Motivos_viagem extends Library_base {

    /**
     * Lista os motivos de viagem no webservice da seguradora
     * @return object
     */
    public function listar() {
        $parametros = $this->listarParametros();
        return $this->CI->ws_seguradora->processar('GetMotivosViagem', $parametros)->motivos->motivo;
    }

    private function listarParametros() {
        return [
            'identificadorSistemaOrigem' => 'RE',
            'codigoRepresentante' => $this->codigo_representante
        ];
    }

}
