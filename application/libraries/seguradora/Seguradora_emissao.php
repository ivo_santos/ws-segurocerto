<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

require_once(APPPATH . '/libraries/seguradora/Library_base.php');

class Seguradora_emissao extends Library_base
{

	public function emitir($dados)
	{
		$parametros = $this->tratarParametros($dados);
		$retorno = $this->CI->ws_seguradora->processar('Contratar', $parametros);
		return $retorno;
	}

	private function tratarParametros($dados)
	{
		return [
			'identificadorSistemaOrigem' => 'RE',
			'codigoRepresentante' => $this->codigo_representante,
			'inicioViagem' => $dados['data_inicio'],
			'fimViagem' => $dados['data_final'],
			'idLocalidadeOrigem' => $dados['localidade_origem_id'],
			'idLocalidadeDestino' => $dados['localidade_destino_id'],
			'idTipo' => $dados['tipo_id'],
			'idMotivo' => $dados['motivo_id'],
			'viajantes' => [
				'viajante' => $this->construirArrayViajantes($dados),
			],
			'responsavelFinanceiro' => [
				'nome' => $dados['nome'],
				'cpf' => str_replace('.', '', str_replace('-', '', $dados['documento'])),
				'email' => $dados['email'],
			],
			'formaPagamento' => [
				'tipo' => 'REP',
			],
		];
	}

	private function construirArrayViajantes($dados)
	{
		$viajantes = [];
		foreach ($dados['clientes'] as $chave => $cliente) {

			$idade = extrairIdadeFromNascimento(formata_data($cliente['nascimento']));

			if ($idade <= 64) {
				$coberturas = json_decode(json_encode($dados['coberturas_info']['cobertura']));
			} else {
				$coberturas = json_decode(json_encode($dados['coberturas_info']['cobertura_idoso']));
			}

			$servicos = json_decode(json_encode($dados['servicos_info']));

			$viajantes[$chave] = [
				'id' => $cliente['id'],
				'idViajantePrincipal' => $dados['clientes'][0]['id'],
				'nome' => $cliente['nome'],
				'cpf' => str_replace('.', '', str_replace('-', '', $cliente['documento'])),
				'dataNascimento' => $cliente['nascimento'],
				'profissao' => '109',
				'rendaMensalInformada' => false,
				'sexo' => ($cliente['sexo'] == 1) ? 'M' : 'F',
				'email' => $dados['email'],
				'telefone' => str_replace(' ', '', str_replace(')', '-', str_replace('(', '', str_replace('-', '', $dados['telefone'])))),
				'plano' => [
					'codigoPlano' => $dados['plano_id'],
					'coberturas' => [
						'cobertura' => $coberturas,
					],
					'servicos' => [
						'servico' => $servicos,
					],
				],
				'endereco' => [
					'cep' => str_replace('-', '', $dados['endereco']['cep']),
					'logradouro' => $dados['endereco']['logradouro'],
					'bairro' => $dados['endereco']['bairro'],
					'numero' => $dados['endereco']['numero'],
				],
			];

			// CASO O VIAJANTE SEJA MENOR DE IDADE, ELE DEVE INCLUIR UM RESPONSAVEL.
			$idade = extrairIdadeFromNascimento(formata_data($cliente['nascimento']));
			if ($idade < 18) {

				$viajantes[$chave]['responsavelLegal'] = [
					'nome' => $dados['nome'],
					'cpf' => str_replace('.', '', str_replace('-', '', $dados['documento'])),
					'email' => 'ivo@tripguard.com.br',
					'rendaMensalInformada' => false,
					'dataNascimento' => $dados['nascimento'],
					'endereco' => [
						'cep' => str_replace('-', '', $dados['endereco']['cep']),
						'logradouro' => $dados['endereco']['logradouro'],
						'bairro' => $dados['endereco']['bairro'],
						'numero' => $dados['endereco']['numero'],
					]
				];
			}

		}

		return $viajantes;
	}

}
