<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once('soap-wsse.php');

class MySoap extends SoapClient {

    function __doRequest($request, $location, $saction, $version, $one_way = NULL) {
        $doc = new DOMDocument('1.0');
        $doc->loadXML($request);

        $objWSSE = new WSSESoap($doc);

        /* add Timestamp with no expiration timestamp */
        // $objWSSE->addTimestamp();

        /* create new XMLSec Key using RSA SHA-1 and type is private key */
        $objKey = new XMLSecurityKey(XMLSecurityKey::RSA_SHA1, array('type' => 'private'));

        /* load the private key from file - last arg is bool if key in file (TRUE) or is string (FALSE) */
        $objKey->loadKey(PRIVATE_KEY_WS, TRUE);

        /* Sign the message - also signs appropraite WS-Security items */
        $objWSSE->signSoapDoc($objKey);

        /* Add certificate (BinarySecurityToken) to the message and attach pointer to Signature */
        $token = $objWSSE->addBinaryToken(file_get_contents(CERT_FILE_WS));
        $objWSSE->attachTokentoSig($token);

//        echo $objWSSE->saveXML();
        
          /* save xml request */
        if(getenv('GRAVAR_WS_LOG') == "TRUE") {
			$outputxml = '/' . date('d-m-Y-H:i:s') . '-request.xml';
			file_put_contents(realpath('./assets/logs/') . $outputxml, $objWSSE->saveXML());
			chmod(realpath('./assets/logs/') . $outputxml, 0777);
		}
        return parent::__doRequest($objWSSE->saveXML(), $location, $saction, $version);
    }

}
