<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

abstract class Library_base {

    protected $CI;
    protected $codigo_representante;

    function __construct() {
        $this->CI = &get_instance();
        $this->CI->load->library('seguradora/ws_seguradora');

        if (getenv('SULAMERICA_AMBIENTE') == "PRODUCAO") {
            $this->codigo_representante = getenv('SULAMERICA_PRODUCAO_CODIGO_REPRESENTANTE');
        } else {
            $this->codigo_representante = getenv('SULAMERICA_TESTES_CODIGO_REPRESENTANTE');
        }
    }

}
