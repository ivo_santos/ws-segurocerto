<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once (APPPATH . '/libraries/seguradora/Library_base.php');

class Tipos_viagem extends Library_base {

    /**
     * Lista os tipos de viagem no webservice da seguradora
     * @return object
     */
    public function listar() {
        return $this->CI->ws_seguradora->processar('GetTiposViagem')->tipos->tipo;
    }

}
