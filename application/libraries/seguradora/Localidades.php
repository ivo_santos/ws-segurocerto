<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once (APPPATH . '/libraries/seguradora/Library_base.php');

class Localidades extends Library_base {

    /**
     * Lista as faixas de idade no webservice da seguradora
     * @return object
     */
    public function listar($dados) {
        $parametros = $this->tratarParametros($dados);
        return $this->CI->ws_seguradora->processar('GetLocalidades', $parametros)->localidades->localidade;
    }

    private function tratarParametros($dados) {
        return [
            'identificadorSistemaOrigem' => 'RE',
            'codigoRepresentante' => $this->codigo_representante,
            'tipoLocalidade' => strtoupper($dados['tipo_localidade']), // O = origem / D = destino
            'categoriaViagem' => strtoupper($dados['categoria_viagem']), // I = internacional / N = nacional
        ];
    }

}
