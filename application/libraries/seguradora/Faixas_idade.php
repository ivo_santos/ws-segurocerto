<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once (APPPATH . '/libraries/seguradora/Library_base.php');

class Faixas_idade extends Library_base {

    /**
     * Lista as faixas de idade no webservice da seguradora
     * @return object
     */
    public function listar() {
        $parametros = $this->tratarParametros();
        return $this->CI->ws_seguradora->processar('GetFaixasIdade', $parametros)->faixasDeIdade->faixaIdade;
    }

    private function tratarParametros() {
        return [
            'identificadorSistemaOrigem' => 'RE',
            'codigoRepresentante' => $this->codigo_representante
        ];
    }

}
