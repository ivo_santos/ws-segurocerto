<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once (APPPATH . '/libraries/seguradora/Library_base.php');

class Seguradora_segunda_via extends Library_base {

    public function listar($dados) {
        $parametros = $this->tratarParametros($dados);
        f($parametros, false);
        return $this->CI->ws_seguradora->processar('GetSegundaVia', $parametros);
    }

    private function tratarParametros($dados) {
        return [
            'identificadorSistemaOrigem' => 'RE',
            'codigoRepresentante' => $this->codigo_representante,
            'numeroApolice' => $dados['numero_apolice'],
            'tipoKit' => $dados['tipo_kit'],
            'enviaEmail' => 1
        ];
    }

}
