<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Formato {

    private $CI;

    function __construct() {
        $this->CI = &get_instance();
    }

    function saidaDados($array, $tipo = 'json') {
        if (isset($_GET['formatar'])) {
            echo "<pre>";
            print_r($array);
            echo "</pre>";
            die('final');
        } else if ($tipo == 'json') {
            echo json_encode($array, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
            die();
        } else if ($tipo == 'xml') {
        }
    }

    static function requisicaoGet() {
        return $_GET;
    }

    static function requisicaoPost() {
        return $_POST;
    }

    static function requisicaoDelete() {
        parse_str(file_get_contents('php://input'), $_DELETE);
        return $_DELETE;
    }

    static function requisicaoPut() {
        parse_str(file_get_contents('php://input'), $_PUT);
        return $_PUT;
    }

}
