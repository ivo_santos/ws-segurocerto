<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Cotacao_historicos
{

	private $CI;
	private $model;

	function __construct()
	{
		$this->CI = &get_instance();
		$this->CI->load->model('cotacao/cotacoes_historicos_model');
		$this->model = $this->CI->cotacoes_historicos_model;
	}

	/**
	 * Adiciona um historico de cotação
	 * @param type $data
	 * @return array
	 */
	function adicionar($data)
	{
		$data = array_merge($data, [
			'data_retorno' => ((!empty($data['data_retorno']) && $data['status'] == 2) ? desformata_data($data['data_retorno']) : null),
			'data_cadastro' => date("Y-m-d H:i:s"),
			'data_modificacao' => date("Y-m-d H:i:s"),
		]);

		// ATUALIZA OS STATUS DOS HISTORICOS ANTERIORES PARA CONCLUIDOS
		if (!empty($data['data_retorno']) && (!empty($data['status']) && $data['status'] == 2)) {
			$this->marcarHistoricosComoConcluidos($data['cotacao_id']);
		}

		$id = $this->model->hydrate($data)->adicionar();
		return ['success' => true, 'id' => $id];
	}

	/**
	 * Altera um historico por hash do id do historico
	 * @param $data
	 * @return array
	 */
	function alterar($data)
	{
		$data['id'] = $data['historico_id'];
		$data['data_modificacao'] = date('Y-m-d H:i:s');
		$data['data_retorno'] = (!empty($data['data_retorno']) && (!empty($data['status']) && $data['status'] == 2)) ? desformata_data($data['data_retorno']) : null;

		// ATUALIZA OS STATUS DOS HISTORICOS ANTERIORES PARA CONCLUIDOS
		if (!empty($data['data_retorno']) && (!empty($data['status']) && $data['status'] == 2)) {
			$this->marcarHistoricosComoConcluidos($data['cotacao_id']);
		}

		$historico = (array)$this->listarUm($data['id']);
		$this->model->hydrate(array_merge($historico, $data))->alterar();
		return ['success' => true, 'id' => $historico['id']];
	}

	/**
	 * ATUALIZA OS STATUS DOS HISTORICOS ANTERIORES PARA CONCLUIDOS
	 * @param $cotacao_id
	 */
	function marcarHistoricosComoConcluidos($cotacao_id)
	{
		$this->model->setCotacao_id($cotacao_id)->marcarHistoricosComoConcluidos();
	}

	/**
	 * Lista historico por hash id
	 * @param string $id
	 * @return object
	 */
	function listarUm($id)
	{
		return $this->model->setId($id)->listarUm();
	}

	/**
	 * Lista todos historicos de uma cotação de acordo com o filtro e pelo hash do id da cotação
	 * @param $cotacao_id
	 * @param array $filtros
	 * @return mixed
	 */
	function listarTodosPorCotacaoId($cotacao_id, $filtros = ['limit' => 1000, 'inicio' => 0])
	{
		$result['result'] = $this->model
			->setCotacao_id($cotacao_id)
			->setLimit($filtros['limit'])
			->setOffset($filtros['inicio'])
			->listarTodosPorCotacaoId();

		$result['total'] = $this->model->totalPorCotacaoId();
		$result['success'] = true;

		return $result;
	}

	function listarTodosAgendamentosParaHoje(){
		return $this->model->listarTodosAgendamentosParaHoje();
	}

	function listarTodosAgendamentosAtrasados(){
		return $this->model->listarTodosAgendamentosAtrasados();
	}
}
