<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Plano
{

	private $CI;

	function __construct()
	{
		$this->CI = &get_instance();
		$this->CI->load->model('cotacao/planos_model');
		$this->CI->load->model('cotacao/coberturas_model');
	}

	/**
	 * listar um de um plano por id
	 * @param int $id
	 * @return object
	 */
	public function listarUm($id)
	{
		$result = $this->CI->planos_model->setId($id)->listarUm();

		if (!empty($result)) {
			$result->coberturas = $this->listarCoberturasPorPlanoId($id);
		}

		return $result;
	}

	/**
	 * Busca o plano por seguradora_plano_id
	 * @param int $seguradora_plano_id
	 * @return object
	 */
	public function listaUmPorSeguradoraPlanoId($seguradora_plano_id)
	{
		return $this->CI->planos_model->setSeguradora_plano_id($seguradora_plano_id)->listaUmPorSeguradoraPlanoId();
	}

	/**
	 * Busca coberturas por id do plano
	 *
	 * @param int $id
	 * @return object
	 */
	public function listarCoberturasPorPlanoId($id)
	{
		return $this->CI->coberturas_model->setPlano_id($id)->listarCoberturasPorPlanoId();
	}

	/**
	 * Busca coberturas por ids dos planos
	 *
	 * @param array $ids
	 * @return object
	 */
	public function listarCoberturasPorPlanosIds($ids)
	{
		return $this->CI->coberturas_model->setIds($ids)->listarCoberturasPorPlanosIds();
	}

	/**
	 * Busca cobertura especifica pelo id e pelo plano id
	 *
	 * @param  int $cobertura_id
	 * @param  int $plano_id
	 * @return string
	 */
	public function listarLimitePorCoberturaIdplanoId($cobertura_id, $plano_id)
	{
		return $this->CI->coberturas_model->setId($cobertura_id)->setPlano_id($plano_id)->listarLimitePorCoberturaIdplanoId();
	}

	/**
	 * Busca cobertura por código seguradora id
	 * @param int $id
	 * @return object
	 */
	public function listarCoberturaPorSeguradoraCodigoId($seguradora_codigo_id)
	{
		return $this->CI->coberturas_model->setSeguradora_codigo_id($seguradora_codigo_id)->listarUmPorSeguradoraCodigoId();
	}

	/**
	 * Busca cobertura pelo seguradora_codigo_id e plano_id com o limite
	 * @param int $seguradora_codigo_id
	 * @param int $plano_id
	 * @return object
	 */
	public function listarCoberturaPorSeguradoraCodigoIdComLimite($seguradora_codigo_id, $plano_id)
	{
		return $this->CI->coberturas_model->setSeguradora_codigo_id($seguradora_codigo_id)->setPlano_id($plano_id)->listarUmPorSeguradoraCodigoIdComLimite();
	}

}
