<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Cotacao
{

	private $CI;

	function __construct()
	{
		$this->CI = &get_instance();
		$this->CI->load->model('cotacao/cotacao_model');
	}

	public function adicionar($data)
	{
		$data = array_merge($data, [
			'data_inicio' => desformata_data($data['data_inicio']),
			'data_final' => desformata_data($data['data_final']),
			'data_validade' => date("Y-m-d"),
			'data_cadastro' => date("Y-m-d H:i:s"),
		]);

		$id = $this->CI->cotacao_model->hydrate($data)->adicionar();
		return ['success' => true, 'id' => $id];
	}

	public function alterar($data)
	{
		$data['id'] = $data['cotacao_id'];
		$cotacao = (array)$this->listarUm($data['id']);

		## CONCLUI HISTORICOS PENDENTES
		if(!empty($data['situacao']) && $data['situacao'] == 4){
			$this->CI->load->library('cotacao/cotacao_historicos');
			$this->CI->cotacao_historicos->marcarHistoricosComoConcluidos($cotacao['id']);
		}

		$this->CI->cotacao_model->hydrate(array_merge($cotacao, $data))->alterar();
		return ['success' => true, 'id' => $cotacao['id']];
	}

	public function remover($cotacao_id){
		$this->CI->load->library('vendas/venda');

		$cotacao = $this->listarUm($cotacao_id);
		$venda = $this->CI->venda->listarUmPorCotacaoId($cotacao_id);

		## Remove uma cotação caso ela tenha uma venda com status de pendente ou caso não tenha nenhuma venda associada.
		if((!empty($venda) && $venda->status == 2) || (empty($venda) && !empty($cotacao))){
			$this->CI->cotacao_model->setId($cotacao_id)->remover();
			return ['success' => true, 'message' => "Removido com sucesso!"];
		}

		return ['success' => false, 'message' => 'Não foi possível excluir!'];
	}

	/**
	 * Lista uma cotacao por hash do id
	 * @param string $id
	 * @return object
	 */
	public function listarUm($id)
	{
		return $this->CI->cotacao_model->setId($id)->listarUm();
	}

	public function gerar($id, $ignorar_cache = false)
	{
		$this->CI->load->library('administracao/destino_grupo');
		$this->CI->load->library('administracao/cache_cotacao');
		$this->CI->load->library('cotacao/plano');
		$this->CI->load->library('seguradora/seguradora_cotacao');

		$cotacao_gravada = $this->listarUm($id);

		if (!empty($cotacao_gravada) && $cotacao_gravada->data_inicio > date('Y-m-d')) {

			$destino_grupo = $this->CI->destino_grupo->listarUm(gerarChave($cotacao_gravada->destino_grupo_id));

			$cotacao_gravada->destino_grupo = $destino_grupo->nome;
			$cotacao_gravada->dias = diferenca_datas($cotacao_gravada->data_inicio, $cotacao_gravada->data_final, '-', 'd');

			/**
			 * CACHE DE COTAÇÃO PARA O GRUPO DE DESTINO
			 */
			$cache_cotacao = $this->CI->cache_cotacao->listarUmValidoParaDestinoGrupoId($cotacao_gravada->destino_grupo_id);

			if (!empty($cache_cotacao) && !$ignorar_cache) {
				$fator = $cotacao_gravada->dias;
				$retorno = unserialize($cache_cotacao->cotacao);
			} else {
				$this->CI->load->library('administracao/destino');
				$this->CI->load->library('administracao/motivo_viagem');
				$this->CI->load->library('administracao/tipo_viagem');

				$fator = 1;

				$destino = $this->CI->destino->listarUm($destino_grupo->destino_id);
				$motivo = $this->CI->motivo_viagem->listarUm($destino_grupo->motivo_id);
				$tipo = $this->CI->tipo_viagem->listarUm($destino_grupo->tipo_id);

				$retorno = $this->CI->seguradora_cotacao->cotar([
					'data_inicio' => $cotacao_gravada->data_inicio,
					'data_final' => $cotacao_gravada->data_final,
					'localidade_origem_id' => '253',
					'localidade_destino_id' => $destino->seguradora_destino_id,
					'tipo_id' => $tipo->seguradora_tipo_id,
					'motivo_id' => $motivo->seguradora_motivo_id
				]);

				// Erro ao cotar
				if ($retorno == "Service Unavailable" || $retorno == "Could not connect to host") {
					return ['success' => false, 'message' => 'Ws fora do ar.'];
				} else if ($retorno->codigoRetorno->codigo != 0) {
					return ['success' => false, 'message' => $retorno->codigoRetorno->descricao];
				}
			}

			/**
			 * TRATA O RETORNO E MONTA A COTAÇÃO
			 */
			if (!empty($retorno) && !empty($retorno->planos->plano)) {
				if (count($retorno->planos->plano) > 1) {
					$seguradora_cotacoes = $retorno->planos->plano;
				} else {
					$seguradora_cotacoes[] = $retorno->planos->plano;
				}

				$planos = [];
				$i = 0;

				foreach ($seguradora_cotacoes as $seguradora_cotacao) {
					$a = $this->CI->plano->listaUmPorSeguradoraPlanoId($seguradora_cotacao->codigo);

					if (!empty($a)) {
						// SEGUE OS DIAS PERMITIDOS PELO PLANO.
						if($cotacao_gravada->dias < $a->dias_min || $cotacao_gravada->dias > $a->dias_max){
							continue;
						}

						$planos[$i] = $a;
						$planos[$i]->cotacao = (object)[];

						$valor = 0;
						$valor_idoso = 0;
						$coberturas_info = [];

						foreach ($seguradora_cotacao->coberturas->cobertura as $cobertura) {
							if (count($cobertura->regrasCapital->regraCapital) > 1) {
								$regras_capitais = $cobertura->regrasCapital->regraCapital;
							} else {
								$regras_capitais[] = $cobertura->regrasCapital->regraCapital;
							}

							foreach ($regras_capitais as $chave => $regra) {
								if ($regra->idadeMinima == 0) {
									$valor_cobertura = ($regras_capitais[$chave]->premio);
									$valor = $valor + ($valor_cobertura * $fator);
									$coberturas_info['cobertura'][] = ['codigoCobertura' => $cobertura->codigo, 'capital' => $regras_capitais[$chave]->capital, 'premio' => $regras_capitais[$chave]->premio];
								} else {
									$valor_cobertura = ($regras_capitais[$chave]->premio);
									$valor_idoso = $valor_idoso + ($valor_cobertura * $fator);
									$coberturas_info['cobertura_idoso'][] = ['codigoCobertura' => $cobertura->codigo, 'capital' => $regras_capitais[$chave]->capital, 'premio' => $regras_capitais[$chave]->premio];
								}
							}
						}

						$servicos_info = [];

						if (!empty($seguradora_cotacao->servicos->servico)) {
							foreach ($seguradora_cotacao->servicos->servico as $servico) {
								$servicos_info[] = ['codigoServico' => $servico->codigo, 'custo' => $servico->custo];
							}
						}

						$planos[$i]->ordem = str_replace(',', '', str_replace('.', '', number_format($valor, 2, ',', '.')));
						$planos[$i]->cotacao->valor = $valor;
						$planos[$i]->cotacao->valor_idoso = $valor_idoso;

						// temporario no plano para mandar no planos_valores
						$planos[$i]->coberturas_info = serialize($coberturas_info);
						$planos[$i]->servicos_info = serialize($servicos_info);

						$i++;
					}
				}

				// transforma em array orderna e depois volta pra objeto
				$planos = array_sort(json_decode(json_encode($planos), true), 'ordem', SORT_ASC);
				$planos = json_decode(json_encode((object)$planos), FALSE);

				$planos_ids = [];
				$planos_nome = [];
				$planos_valores = [];

				$i = 0;
				foreach ($planos as $plano) {
					$planos_ordernados[] = $plano;
					$planos_ids[] = $plano->id;
					$planos_nome[$plano->id] = $plano->nome;

					$planos_valores[$i]['plano_id'] = $plano->id;
					$planos_valores[$i]['valor'] = $plano->cotacao->valor;
					$planos_valores[$i]['valor_idoso'] = $plano->cotacao->valor_idoso;

					$planos_valores[$i]['coberturas_info'] = $plano->coberturas_info;
					unset($plano->coberturas_info);

					$planos_valores[$i]['servicos_info'] = $plano->servicos_info;
					unset($plano->servicos_info);

					$i++;
				}

				$result['planos'] = $planos_ordernados;
				$result['planos_valores'] = $planos_valores;
				$result['comparar_coberturas'] = $this->agruparCoberturas($planos_ids, $planos_nome);
				$result['cotacao'] = $cotacao_gravada;
				$result['success'] = true;
				$result['message'] = 'cotação realizada com sucesso.';

				return $result;

			} else {
				return ['success' => false, 'message' => 'Erro ao fazer a cotação.'];
			}
		} else {
			return ['success' => false, 'message' => 'Cotação expirada!'];
		}

	}

	/**
	 * TODO depois de implementado o modulo de cotação por grupo de destino remover essa função.
	 */
	private function gerarComDestino($id)
	{
		$this->CI->load->library('administracao/destino');
		$this->CI->load->library('administracao/motivo_viagem');
		$this->CI->load->library('administracao/tipo_viagem');
		$this->CI->load->library('seguradora/seguradora_cotacao');
		$this->CI->load->library('cotacao/plano');
		$this->CI->load->library('cotacao/cache');

		$cotacao_gravada = (array)$this->listarUm($id);

		$cache = getenv('CACHE_COTACAO');
		$result = ['success' => false, 'message' => 'erro na cotação'];

		if (empty($cotacao_gravada) || $cotacao_gravada['data_inicio'] <= date('Y-m-d')) {
			return ['success' => false, 'message' => 'cotação expirada!'];
		}

		// DIAS
		$cotacao_gravada['dias'] = diferenca_datas($cotacao_gravada['data_inicio'], $cotacao_gravada['data_final'], '-', 'd');

		if (!empty($cotacao_gravada) && $cache == "TRUE") {
			$fator = $cotacao_gravada['dias'];

			// BUSCA PLANOS DISPONÍVEIS PARA O DESTINO
			$destino = $this->CI->destino->listarUm($cotacao_gravada['destino_id']);
			$motivo = $this->CI->motivo_viagem->listarUm($cotacao_gravada['motivo_id']);
			$tipo = $this->CI->tipo_viagem->listarUm($cotacao_gravada['tipo_id']);

			$cache = $this->CI->cache->listarUmCacheCotacao([
				'destino_codigo' => $destino->codigo,
				'tipo_id' => $tipo->seguradora_tipo_id,
				'motivo_id' => $motivo->seguradora_motivo_id,
			]);

			if (!empty($cache)) {
				$retorno = unserialize($cache->cotacao);
			}
		} else if (!empty($cotacao_gravada) && $cache == "FALSE") {
			$fator = 1;

			$destino = $this->CI->destino->listarUm($cotacao_gravada['destino_id']);
			$motivo = $this->CI->motivo_viagem->listarUm($cotacao_gravada['motivo_id']);
			$tipo = $this->CI->tipo_viagem->listarUm($cotacao_gravada['tipo_id']);

			$retorno = $this->CI->seguradora_cotacao->cotar([
				'data_inicio' => $cotacao_gravada['data_inicio'],
				'data_final' => $cotacao_gravada['data_final'],
				'localidade_origem_id' => '253',
				'localidade_destino_id' => $destino->seguradora_destino_id,
				'tipo_id' => $tipo->seguradora_tipo_id,
				'motivo_id' => $motivo->seguradora_motivo_id
			]);

//            f($retorno, false);
			// Erro ao cotar
			if ($retorno == "Service Unavailable") {
				return ['success' => false, 'message' => 'Ws fora do ar.'];
			}
		}

		// TRATA O RETORNO E MONTA A COTAÇÃO
		if (!empty($retorno)) {
			if (!empty($retorno->planos->plano)) {
				if (count($retorno->planos->plano) > 1) {
					$seguradora_cotacoes = $retorno->planos->plano;
				} else {
					$seguradora_cotacoes[] = $retorno->planos->plano;
				}

				$planos = [];
				$i = 0;

				foreach ($seguradora_cotacoes as $seguradora_cotacao) {
					$a = $this->CI->plano->listaUmPorSeguradoraPlanoId($seguradora_cotacao->codigo);
					if (!empty($a)) {
						$planos[$i] = $a;
						$planos[$i]->cotacao = (object)[];

						$valor = 0;
						$valor_idoso = 0;
						$coberturas_info = [];

						foreach ($seguradora_cotacao->coberturas->cobertura as $cobertura) {
							if (count($cobertura->regrasCapital->regraCapital) > 1) {
								$regras_capitais = $cobertura->regrasCapital->regraCapital;
							} else {
								$regras_capitais[] = $cobertura->regrasCapital->regraCapital;
							}

							foreach ($regras_capitais as $chave => $regra) {
								if ($regra->idadeMinima == 0) {
									$valor_cobertura = ($regras_capitais[$chave]->premio);
									$valor = $valor + ($valor_cobertura * $fator);
									$coberturas_info['cobertura'][] = ['codigoCobertura' => $cobertura->codigo, 'capital' => $regras_capitais[$chave]->capital, 'premio' => $regras_capitais[$chave]->premio];
								} else {
									$valor_cobertura = ($regras_capitais[$chave]->premio);
									$valor_idoso = $valor_idoso + ($valor_cobertura * $fator);
									$coberturas_info['cobertura_idoso'][] = ['codigoCobertura' => $cobertura->codigo, 'capital' => $regras_capitais[$chave]->capital, 'premio' => $regras_capitais[$chave]->premio];
								}
							}
						}

						$servicos_info = [];

						if (!empty($seguradora_cotacao->servicos->servico)) {
							foreach ($seguradora_cotacao->servicos->servico as $servico) {
								$servicos_info[] = ['codigoServico' => $servico->codigo, 'custo' => $servico->custo];
							}
						}

						$planos[$i]->ordem = str_replace(',', '', str_replace('.', '', number_format($valor, 2, ',', '.')));
						$planos[$i]->cotacao->valor = $valor;
						$planos[$i]->cotacao->valor_idoso = $valor_idoso;

						// temporario no plano para mandar no planos_valores
						$planos[$i]->coberturas_info = serialize($coberturas_info);
						$planos[$i]->servicos_info = serialize($servicos_info);

						$i++;
					}
				}

				// transforma em array orderna e depois volta pra objeto
				$planos = array_sort(json_decode(json_encode($planos), true), 'ordem', SORT_ASC);
				$planos = json_decode(json_encode((object)$planos), FALSE);

				$planos_ids = [];
				$planos_nome = [];
				$planos_valores = [];

				$i = 0;
				foreach ($planos as $plano) {
					$planos_ordernados[] = $plano;
					$planos_ids[] = $plano->id;
					$planos_nome[$plano->id] = $plano->nome;

					$planos_valores[$i]['plano_id'] = $plano->id;
					$planos_valores[$i]['valor'] = $plano->cotacao->valor;
					$planos_valores[$i]['valor_idoso'] = $plano->cotacao->valor_idoso;

					$planos_valores[$i]['coberturas_info'] = $plano->coberturas_info;
					unset($plano->coberturas_info);

					$planos_valores[$i]['servicos_info'] = $plano->servicos_info;
					unset($plano->servicos_info);

					$i++;
				}

				$result['planos'] = $planos_ordernados;
				$result['planos_valores'] = $planos_valores;
				$result['comparar_coberturas'] = $this->agruparCoberturas($planos_ids, $planos_nome);
				$result['cotacao'] = $cotacao_gravada;
				$result['success'] = true;
				$result['message'] = 'cotação realizada com sucesso.';
			}
		}

		return $result;
	}

	/**
	 * Agrupa as coberturas para a comparação
	 * @param array $ids
	 * @param array $planos_nomes
	 * @return array
	 */
	private function agruparCoberturas($ids, $planos_nomes)
	{
		$this->CI->load->library('cotacao/plano');

		$coberturas = $this->CI->plano->listarCoberturasPorPlanosIds($ids);

		foreach ($coberturas as $cobertura) {
			$cobertura->limites = array();
			foreach ($ids as $plano_id) {
				// BUSCAR LIMITE DO PLANO E DA COBERTURA ESPECIFICA
				$limite = $this->CI->plano->listarLimitePorCoberturaIdplanoId($cobertura->id, $plano_id);
				if (!empty($limite)) {
					$cobertura->limites[$plano_id] = array('nome_plano' => $planos_nomes[$plano_id], 'limite' => $limite->moeda_limite . ' ' . $limite->limite);
				} else {
					$cobertura->limites[$plano_id] = array('nome_plano' => $planos_nomes[$plano_id], 'limite' => '-');
				}
			}
		}

		return $coberturas;
	}

	/**
	 * Listar um cotação por hash do id
	 * @param string $id
	 */
	public function listarUmCotacaoPorEmail($id)
	{
		return $this->CI->cotacao_model->setId($id)->listarUmCotacaoPorEmail();
	}

	/**
	 * Faz a cotação e envia para o email solicitado
	 * @param array $data
	 * @return array
	 */
	public function enviarParaMeuEmail($data)
	{
		$this->CI->load->library('emails/emails');
		$this->CI->load->library('administracao/destino');
		$this->CI->load->library('administracao/motivo_viagem');
		$this->CI->load->library('administracao/tipo_viagem');
		$this->CI->load->library('administracao/empresa');

		$cotacao = $this->listarUm($data['cotacao_id']);

		$id = $this->CI->cotacao_model
			->setNome($data['nome'])
			->setEmail($data['email'])
			->setId($cotacao->id)
			->salvarCotacaoPorEmail();

		$cotacao_gerada = $this->gerar(gerarChave($cotacao->id));

		## BUSCA EMPRESA
		$empresa = $this->CI->empresa->listarUm(gerarChave(1));

		$data_expiracao = formata_data(date('Y-m-d')) . ' 23:59:59';

		## ENVIA EMAIL COM COTAÇÃO PARA MEU EMAIL
		$enviou = $this->CI->emails->emailCotacaoParaMeuEmail(array(
			'data_expiracao' => $data_expiracao,
			'url_cotacao_por_email' => getenv('FRONTEND_URL') . 'cotacao-por-email/' . gerarChave($id),
			'nome' => $data['nome'],
			'emails' => array($data['nome'] => $data['email']),
			'cotacao' => (array)$cotacao_gerada,
			'empresa' => (array)$empresa,
		));

		if ($enviou == "success") {
			return ['success' => true, 'message' => 'Email enviado com sucesso!', 'cotacao_id' => $cotacao->id];
		} else {
			return ['success' => false, 'message' => 'Ocorreu algum erro ao enviar.'];
		}
	}

	/**
	 * Faz a cotação e envia para um amigo
	 *
	 * @param array $data
	 *
	 */
	public function enviarParaAmigo($data)
	{
		$this->CI->load->library('emails/emails');
		$this->CI->load->library('administracao/destino');
		$this->CI->load->library('administracao/motivo_viagem');
		$this->CI->load->library('administracao/tipo_viagem');
		$this->CI->load->library('administracao/empresa');

		// ADICIONA O REGISTRO DESSA COTAÇÃO
		$cotacao = $this->listarUm($data['cotacao_id']);

		$id = $this->CI->cotacao_model
			->setNome($data['nome'])
			->setEmail($data['email'])
			->setNome_amigo($data['nome_amigo'])
			->setEmail_amigo($data['email_amigo'])
			->setId($cotacao->id)
			->salvarCotacaoPorEmail();

		$cotacao_gerada = $this->gerar(gerarChave($cotacao->id));

		## BUSCA EMPRESA
		$empresa = $this->CI->empresa->listarUm(gerarChave(1));

		$data_expiracao = formata_data(date('Y-m-d')) . ' 23:59:59';

		## ENVIA EMAIL COM COTAÇÃO PARA MEU AMIGO
		$enviou = $this->CI->emails->emailCotacaoParaAmigo(array(
			'data_expiracao' => $data_expiracao,
			'url_cotacao_por_email' => getenv('FRONTEND_URL') . 'cotacao-por-email/' . gerarChave($id),
			'nome' => $data['nome'],
			'nome_amigo' => $data['nome_amigo'],
			'emails' => array($data['nome_amigo'] => $data['email_amigo']),
			'cotacao' => (array)$cotacao_gerada,
			'empresa' => (array)$empresa,
		));

		if ($enviou == "success") {
			return ['success' => true, 'message' => 'Email enviado com sucesso!', 'cotacao_id' => $cotacao->id];
		} else {
			return ['success' => false, 'message' => 'Ocorreu algum erro ao enviar.'];
		}
	}

	/**
	 * Filtros para o listar todos  não venda.
	 * @param $filtros
	 */
	private function filtrosListarTodosNaoVenda($filtros)
	{
		$model = $this->CI->cotacao_model;

		if (!empty($filtros['tipo_pesquisa'])) {
			$termo = !empty($filtros['termo']) ? trim($filtros['termo']) : null;

			if ($filtros['tipo_pesquisa'] == "c.id") {
				$model->setId($termo);

			} else if ($filtros['tipo_pesquisa'] == "c.nome") {
				$model->setNome($termo);

			} else if ($filtros['tipo_pesquisa'] == "c.email") {
				$model->setEmail($termo);

			} else if (substr($filtros['tipo_pesquisa'], 0, 10) == "c.situacao") {
				$situacao = explode("=", $filtros['tipo_pesquisa']);
				$model->setSituacao(!empty($situacao[1]) ? $situacao[1] : null);

			} else if ($filtros['tipo_pesquisa'] == "historico_agendado_hoje" || $filtros['tipo_pesquisa'] == "historico_agendado_atrasado") {
				$this->CI->load->library('cotacao/cotacao_historicos');

				if($filtros['tipo_pesquisa'] == "historico_agendado_hoje"){
					$historicos = $this->CI->cotacao_historicos->listarTodosAgendamentosParaHoje();
				}else{
					$historicos = $this->CI->cotacao_historicos->listarTodosAgendamentosAtrasados();
				}

				$ids = [0];
				if(!empty($historicos)){
					foreach($historicos as $historico){
						$ids[] = $historico->cotacao_id;
					}
				}

				$model->setIds($ids);

			}

			$model->setData_inicio(null)->setData_final(null);

		} else {
			$model->setData_inicio(!empty($filtros['data_inicio']) ? $filtros['data_inicio'] . ' 00:00:00' : date('Y-m-d') . ' 00:00:00')
				->setData_final(!empty($filtros['data_final']) ? $filtros['data_final'] . ' 23:59:59' : date('Y-m-d') . ' 23:59:59');
		}

		$model->setLimit(!empty($filtros['limit']) ? $filtros['limit'] : 20)->setOffset(!empty($filtros['inicio']) ? $filtros['inicio'] : 0);
	}


	function listarTodosNaoVenda($filtros)
	{
		$this->filtrosListarTodosNaoVenda($filtros);
		$result['result'] = $this->CI->cotacao_model->listarTodosNaoVenda();
		$result['total'] = $this->CI->cotacao_model->totalNaoVenda();

		return $result;
	}

}
