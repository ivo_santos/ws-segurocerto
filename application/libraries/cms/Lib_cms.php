<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Lib_cms
{

	private $CI;

	function __construct()
	{
		$this->CI = &get_instance();
		$this->CI->load->helper('url');
		$this->CI->load->model('cms/cms_model');
	}

	/**
	 * Adiciona uma página
	 *
	 * @param array $data
	 * @return int
	 */
	function adicionar($data)
	{
		$imagem = $this->salvarImagem($data['imagem'], null, $data['titulo']);

		return $this->CI->cms_model
			->setTitulo($data['titulo'])
			->setDescricao($data['descricao'])
			->setH1($data['h1'])
			->setImagem($imagem)
			->setConteudo($data['conteudo'])
			->setUrl_amigavel(url_title((!empty($data['url_amigavel'])? $data['url_amigavel']: $data['titulo']) , '-', true))
			->adicionar();
	}

	/**
	 * Altera os dados da pagina pelo hash do id
	 *
	 * @param array $data
	 * @return void
	 */
	function alterar($data)
	{
		$imagem = $this->salvarImagem($data['imagem'], (!empty($data['imagem_atual'])? $data['imagem_atual'] : null), $data['titulo']);

		return $this->CI->cms_model
			->setId($data['pagina_id'])
			->setTitulo($data['titulo'])
			->setDescricao($data['descricao'])
			->setH1($data['h1'])
			->setImagem($imagem)
			->setConteudo($data['conteudo'])
			->setUrl_amigavel(url_title((!empty($data['url_amigavel'])? $data['url_amigavel']: $data['titulo']) , '-', true))
			->alterar();
	}

	/**
	 * Busca páginas de acordo com os filtros
	 *
	 * @return object
	 */
	function listarTodos($filtros)
	{
		$result['total'] = $this->CI->cms_model
			->setLimit($filtros['limit'])
			->setOffset($filtros['inicio'])
			->total();
		$result['result'] = $this->CI->cms_model->listarTodos();
		return $result;
	}

	/**
	 * Lista uma página pelo hash do id
	 *
	 * @param string $id
	 * @return object
	 */
	function listarUm($id)
	{
		return $this->CI->cms_model
			->setId($id)
			->listarUm();
	}

	/**
	 * Deleta uma página de acordo com o hash do id
	 *
	 * @param string $id
	 * @return void
	 */
	function remover($id)
	{

		$caminho = realpath('./') . "/assets/imagens/cms/";
		$cms = $this->listarUm($id);
		unlink($caminho . $cms->imagem);

		return $this->CI->cms_model->setId($id)->remover();
	}

	/**
	 * Lista uma página por url amigável
	 *
	 * @param string $url_amigavel
	 * @return object
	 */
	function listarUmPorUrl($url_amigavel)
	{
		return $this->CI->cms_model
			->setUrl_amigavel(url_title($url_amigavel, '-', true))
			->listarUmPorUrl();
	}

	private function salvarImagem($imagem, $padrao, $nome)
	{
		$arquivo = $padrao;

		if (!empty($imagem)) {
			$imagem = base64_decode(str_replace('@', '', $imagem));
			$arquivo = url_title($nome, '-', true) . ".jpg";
			$caminho = realpath('./') . "/assets/imagens/cms/";
			file_put_contents($caminho . $arquivo, $imagem);
			chmod($caminho . $arquivo, 0755);
		}

		return $arquivo;
	}

}
