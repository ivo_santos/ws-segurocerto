<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Cms_email
{

	private $CI;

	function __construct()
	{
		$this->CI = &get_instance();
		$this->CI->load->model('cms/emails_model');
	}

	/**
	 * Adiciona
	 *
	 * @param array $data
	 * @return int
	 */
	function adicionar($data)
	{
		return $this->CI->emails_model
			->setTitulo($data['titulo'])
			->setAssunto($data['assunto'])
			->setConteudo($data['conteudo'])
			->setTipo($data['tipo'])
			->adicionar();
	}

	/**
	 * Alterar
	 * @param array $data
	 * @return void
	 */
	function alterar($data)
	{

		return $this->CI->emails_model
			->setId($data['id'])
			->setTitulo($data['titulo'])
			->setAssunto($data['assunto'])
			->setConteudo($data['conteudo'])
			->setTipo($data['tipo'])
			->alterar();
	}

	/**
	 * Busca páginas de acordo com os filtros
	 *
	 * @return object
	 */
	function listarTodos($filtros)
	{
		$result['total'] = $this->CI->emails_model
			->setLimit($filtros['limit'])
			->setOffset($filtros['inicio'])
			->setTipo((!empty($filtros['tipo']))? $filtros['tipo']: null)
			->total();

		$result['result'] = $this->CI->emails_model->listarTodos();
		$result['success'] = true;
		return $result;
	}

	/**
	 * Lista uma página pelo hash do id
	 * @param string $id
	 * @return object
	 */
	function listarUm($id)
	{
		return $this->CI->emails_model->setId($id)->listarUm();
	}

	/**
	 * Deleta uma página de acordo com o hash do id
	 *
	 * @param string $id
	 * @return void
	 */
	function remover($id)
	{
		return $this->CI->emails_model->setId($id)->remover();
	}

	function enviarParaCotacao($data)
	{
		$this->CI->load->library('cotacao/cotacao');
		$this->CI->load->library('cotacao/cotacao_historicos');
		$this->CI->load->library('emails/emails');
		$this->CI->load->library('administracao/destino_grupo');

		$cotacao = $this->CI->cotacao->listarUm($data['cotacao_id']);

		## ALTERA A COTAÇÃO SEM ATENDIMENTO PARA PENDENTE.
		if($cotacao->situacao == 1){
			$this->CI->cotacao->alterar([
				'cotacao_id' => gerarChave($cotacao->id),
				'situacao' => 3
			]);
		}

		$grupo = $this->CI->destino_grupo->listarUm(gerarChave($cotacao->destino_grupo_id));
		$template = $this->listarUm(gerarChave($data['email_id']));

		$historico = $this->CI->cotacao_historicos->adicionar([
			'cotacao_id' => $cotacao->id,
			'titulo' => 'Email',
			'descricao' => 'Via ERP - título: '. $template->titulo,
		]);

		$primeiro = explode(' ', $cotacao->nome);
		$retorno = $this->CI->emails->emailCms([
			'historico_id' => gerarChave($historico['id']),
			'emails' => [$cotacao->nome => $cotacao->email],
			'assunto' => strtr($template->assunto, [
				'{$cotacao_hash_id}' => gerarChave($cotacao->id),
				'{$cotacao_id}' => $cotacao->id,
				'{$cotacao_data_inicio}' => formata_data($cotacao->data_inicio),
				'{$cotacao_data_final}' => formata_data($cotacao->data_final),
				'{$cotacao_nome}' => $cotacao->nome,
				'{$cotacao_primeiro_nome}' => $primeiro[0],
				'{$cotacao_destino_grupo_nome}' => $grupo->nome,
			]),
			'conteudo' => strtr($template->conteudo, [
				'{$cotacao_hash_id}' => gerarChave($cotacao->id),
				'{$cotacao_id}' => $cotacao->id,
				'{$cotacao_data_inicio}' => formata_data($cotacao->data_inicio),
				'{$cotacao_data_final}' => formata_data($cotacao->data_final),
				'{$cotacao_nome}' => $cotacao->nome,
				'{$cotacao_primeiro_nome}' => $primeiro[0],
				'{$cotacao_destino_grupo_nome}' => $grupo->nome,
			]),
		]);

		if ($retorno == "success") {
			return ['success' => true, 'mensagem' => 'Email enviado com sucesso.'];
		} else {
			return ['success' => false, 'mensagem' => 'Ocorreu algum erro ao enviar. ' . $retorno];
		}
	}

	function enviarParaVenda($data)
	{
		$this->CI->load->library('vendas/venda');
		$this->CI->load->library('cotacao/cotacao');
		$this->CI->load->library('cotacao/cotacao_historicos');
		$this->CI->load->library('emails/emails');
		$this->CI->load->library('administracao/destino_grupo');

		$venda = $this->CI->venda->listarUm($data['venda_id']);
		$cotacao = $this->CI->cotacao->listarUm(gerarChave($venda->cotacao_id));

		## ALTERA A COTAÇÃO SEM ATENDIMENTO PARA PENDENTE.
		if($cotacao->situacao == 1){
			$this->CI->cotacao->alterar([
				'cotacao_id' => gerarChave($cotacao->id),
				'situacao' => 3
			]);
		}

		$grupo = $this->CI->destino_grupo->listarUm(gerarChave($cotacao->destino_grupo_id));
		$template = $this->listarUm(gerarChave($data['email_id']));

		$historico = $this->CI->cotacao_historicos->adicionar([
			'cotacao_id' => $cotacao->id,
			'titulo' => 'Email',
			'descricao' => 'Via ERP - título: '. $template->titulo,
		]);

		$primeiro = explode(' ', $cotacao->nome);

		$retorno = $this->CI->emails->emailCms([
			'historico_id' => gerarChave($historico['id']),
			'emails' => [$venda->nome => $venda->email],
			'assunto' => strtr($template->assunto, [
				'{$venda_id}' => $venda->id,
				'{$venda_hash_id}' => gerarChave($venda->id),
				'{$cotacao_id}' => $cotacao->id,
				'{$cotacao_hash_id}' => gerarChave($cotacao->id),
				'{$cotacao_data_inicio}' => formata_data($cotacao->data_inicio),
				'{$cotacao_data_final}' => formata_data($cotacao->data_final),
				'{$cotacao_nome}' => $cotacao->nome,
				'{$cotacao_primeiro_nome}' => $primeiro[0],
				'{$cotacao_destino_grupo_nome}' => $grupo->nome,
			]),
			'conteudo' => strtr($template->conteudo, [
				'{$venda_id}' => $venda->id,
				'{$venda_hash_id}' => gerarChave($venda->id),
				'{$cotacao_id}' => $cotacao->id,
				'{$cotacao_hash_id}' => gerarChave($cotacao->id),
				'{$cotacao_data_inicio}' => formata_data($cotacao->data_inicio),
				'{$cotacao_data_final}' => formata_data($cotacao->data_final),
				'{$cotacao_nome}' => $cotacao->nome,
				'{$cotacao_primeiro_nome}' => $primeiro[0],
				'{$cotacao_destino_grupo_nome}' => $grupo->nome,
			]),
		]);

		if ($retorno == "success") {
			return ['success' => true, 'mensagem' => 'Email enviado com sucesso.'];
		} else {
			return ['success' => false, 'mensagem' => 'Ocorreu algum erro ao enviar. ' . $retorno];
		}
	}

	/**
	 * Notifica a abertura de um email e altera o historico do envio para visualizado.
	 *
	 * @param $historico_id
	 * @return array
	 */
	public function abertura($historico_id){
		$this->CI->load->library('cotacao/cotacao_historicos');

		$historico = $this->CI->cotacao_historicos->listarUm($historico_id);

		if(!empty($historico)){
			$this->CI->cotacao_historicos->alterar([
				'historico_id' => gerarChave($historico->id),
				'descricao' => str_replace(' << visualizado >>', '', $historico->descricao) . " << visualizado >>",
			]);

			return ['success' => true, 'message' => 'Email visualizado.'];
		}else{
			return ['success' => false, 'message' => 'Erro ao registrar visualizacao.'];
		}

	}
}
