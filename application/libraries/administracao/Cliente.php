<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cliente {

    private $CI;

    function __construct() {
        $this->CI = &get_instance();
        $this->CI->load->model('administracao/clientes_model');
    }

    /**
     * Busca cliente por hash do id
     * @param string $id
     * @return object
     */
    function listarUm($id) {
        return $this->CI->clientes_model->setId($id)->listarUm();
    }

    /**
     * Busca todos os clientes com base no filtro
     * 
     * @param array $filtros
     * @return array
     */
    function listarTodos($filtros) {
        $result['total'] = $this->CI->clientes_model->total();

        $result['result'] = $this->CI->clientes_model
                ->setLimit($filtros['limit'])->setOffset($filtros['inicio'])
                ->listarTodos();

        return $result;
    }

    /**
     * Adicionar um cliente
     * @param array $data
     * @return int
     */
    function adicionar($data) {
        return $this->CI->clientes_model
                        ->setNome($data['nome'])
                        ->setDocumento($data['documento'])
                        ->setNascimento(desformata_data($data['nascimento']))
                        ->setSexo($data['sexo'])
                        ->setVenda_id($data['venda_id'])
                        ->adicionar();
    }

    /**
     * Alterar o cliente por hash do id
     * @param array $data
     * @return void
     */
    function alterar($data) {
        return $this->CI->clientes_model
                        ->setId($data['id'])
                        ->setNome($data['nome'])
                        ->setDocumento($data['documento'])
                        ->setNascimento(desformata_data($data['nascimento']))
                        ->setSexo($data['sexo'])
                        ->alterar();
    }

    /**
     * Deletar o cliente pelo hash do id
     * @param string $id
     * @return void
     */
    function remover($id) {
        return $this->CI->clientes_model->setId($id)->remover();
    }

    /**
     * Listar todos os clientes de uma venda pelo hash do venda_id
     * @param string $venda_id
     * @return object
     */
    function listarTodosPorVendaId($venda_id) {
        return $this->CI->clientes_model->setVenda_id($venda_id)->listarTodosPorVendaId();
    }
    
    /**
     * Listar um cliente pelo documento
     * @param string $documento
     * @return object
     */
    function listarUmPorDocumento($documento) {
        return $this->CI->clientes_model
                        ->setDocumento($documento)
                        ->listarUmPorDocumento();
    }
    
    /**
     * Busca o voucher por hash do id da venda
     * 
     * @param string $venda_id
     * @return object
     */
    function listarClientesComVouchersPorVendaId($venda_id) {
        $this->CI->load->library('vendas/venda');
        $this->CI->load->library('vouchers/voucher');

        $vouchers = $this->CI->voucher->listarTodosPorVendaId($venda_id);
        $venda = $this->CI->venda->listarUm($venda_id);

        $clientes = [];
        if (!empty($vouchers)) {
            foreach ($vouchers as $chave => $voucher) {
                $cliente = $this->listarUm(gerarChave($voucher->cliente_id));
                // CLIENTES
                $clientes[$chave] = new stdClass();
                $clientes[$chave]->id = $cliente->id;
                $clientes[$chave]->nome = $cliente->nome;
                $clientes[$chave]->documento = $cliente->documento;
                $clientes[$chave]->nascimento = $cliente->nascimento;
                $clientes[$chave]->sexo = $cliente->sexo;
                $clientes[$chave]->voucher_id = $voucher->id;
                $clientes[$chave]->seguradora_numero_apolice = $voucher->seguradora_numero_apolice;
                $clientes[$chave]->seguradora_numero_sorte = $voucher->seguradora_numero_sorte;
                $clientes[$chave]->seguradora_numero_compra = $voucher->seguradora_numero_compra;
                $clientes[$chave]->status = $venda->status;
            }
        }

        return $clientes;
    }

    /**
     * Retornar quantos clientes no total e quantos idosos
     * @param $venda_id
     * @return array
     */
    public function totalClientesTotalClientesIdosos($venda_id)
    {
        $this->CI->load->library('vendas/venda');
        $this->CI->load->library('cotacao/plano');

        $venda = $this->CI->venda->listarUm($venda_id);
        $clientes = $this->listarTodosPorVendaId($venda_id);
        $plano = $this->CI->plano->listarUm($venda->plano_id);

        $total_clientes = count($clientes);
        $total_clientes_idosos = 0;

        foreach ($clientes as $cliente) {
            $idade_idoso = !empty($plano->idade_min_acrescimo) ? $plano->idade_min_acrescimo : $plano->idade_max;
            $idade = extrairIdadeFromNascimento(formata_data($cliente->nascimento));

            if ($idade >= $idade_idoso) {
                $total_clientes_idosos++;
            }
        }

        return ['total_clientes' => $total_clientes, 'total_clientes_idosos' => $total_clientes_idosos];
    }
}
