<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Cache_cotacao
{
	private $CI;
	private $model;

	function __construct()
	{
		$this->CI = &get_instance();
		$this->CI->load->model('administracao/cache_cotacoes_model');
		$this->model = $this->CI->cache_cotacoes_model;
	}

	/**
	 * TODO VAI PARA CLASSE CRON PRESTAR ATENÇÃO NOS MÉTODOS LOCAIS TIPO ADICIONAR/ALTERAR OU LISTAR
	 * TODO E CARREGAR ESSA CLASSE LÁ.
	 * @return array
	 */
	public function fazerAtualizacao()
	{
		$this->CI->load->library('administracao/destino_grupo');
		$this->CI->load->library('administracao/destino');
		$this->CI->load->library('administracao/motivo_viagem');
		$this->CI->load->library('administracao/tipo_viagem');
		$this->CI->load->library('seguradora/seguradora_cotacao');

		$grupos = $this->CI->destino_grupo->listarTodosComCache();

		foreach($grupos as $grupo){
			if(empty($grupo->cache_id)){
				$this->adicionar(['destino_grupo_id' => $grupo->id]);
			}
		}

		$cache = $this->listarUmVencido();
		if (!empty($cache)) {

			$grupo = $this->CI->destino_grupo->listarUm(gerarChave($cache->destino_grupo_id));
			$destino = $this->CI->destino->listarUm($grupo->destino_id);
			$motivo = $this->CI->motivo_viagem->listarUm($grupo->motivo_id);
			$tipo = $this->CI->tipo_viagem->listarUm($grupo->tipo_id);

			$result = $this->CI->seguradora_cotacao->cotar([
				'data_inicio' => date("Y-m-d", strtotime("+1 month", strtotime(date('Y-m-d')))),
				'data_final' => date("Y-m-d", strtotime("+1 month", strtotime(date('Y-m-d')))),
				'localidade_origem_id' => '253',
				'localidade_destino_id' => $destino->seguradora_destino_id,
				'tipo_id' => $tipo->seguradora_tipo_id,
				'motivo_id' => $motivo->seguradora_motivo_id,
			]);

			if ($result->codigoRetorno->codigo == 0) {
				$this->alterar(['id' => gerarChave($cache->id), 'cotacao' => $result]);
				return ['success' => true, 'message' => 'cache atualizado'];
			} else {
				return ['success' => false, 'message' => 'cache não atualizado'];
			}
		}else{
			return ['success' => false, 'message' => 'nada para atualizar'];
		}
	}

	function adicionar($data)
	{
		return $this->model->hydrate($data)->adicionar();
	}

	function alterar($data)
	{
		$this->model->hydrate(array_merge($data, [
			'cotacao' => serialize($data['cotacao']),
			'data_validade' => date("Y-m-d"),
		]))->alterar();

		return ['success' => true, 'message' => 'cache atualizado'];
	}

	function listarUmVencido()
	{
		return $this->model->listarUmVencido();
	}

	function listarUmValidoParaDestinoGrupoId($grupo_id)
	{
		return $this->model->setDestino_grupo_id($grupo_id)->listarUmValidoParaDestinoGrupoId();
	}
}
