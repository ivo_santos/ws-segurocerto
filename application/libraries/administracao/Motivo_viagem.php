<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Motivo_viagem {

    private $CI;

    function __construct() {
        $this->CI = &get_instance();
        $this->CI->load->model('administracao/motivos_viagem_model');
    }

    /**
     * Busca os motivos disponíveis
     * @return object
     */
    function listarTodos() {
        return $this->CI->motivos_viagem_model->listarTodos();
    }

    /**
     * Busca o motivo
     * @param int $id id do motivo
     * @return object
     */
    function listarUm($id) {
        return $this->CI->motivos_viagem_model->setId($id)->listarUm();
    }

    /**
     * Busca o motivo por código
     * @param int $seguradora_motivo_id motivo id
     * @return object
     */
    function listarUmPorSeguradoraMotivoId($seguradora_motivo_id) {
        return $this->CI->motivos_viagem_model->setSeguradora_motivo_id($seguradora_motivo_id)->listarUmPorSeguradoraMotivoId();
    }

}
