<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Empresa {

    private $CI;

    function __construct() {
        $this->CI = &get_instance();
        $this->CI->load->model('administracao/empresas_model');
    }

    /**
     * Listar uma empresa pelo hash do id
     * @param $id
     * @return void
     */
    function listarUm($id) {
        return $this->CI->empresas_model->setId($id)->listarUm();
    }
}
