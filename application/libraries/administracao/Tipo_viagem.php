<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tipo_viagem {

    private $CI;

    function __construct() {
        $this->CI = &get_instance();
        $this->CI->load->model('administracao/tipos_viagem_model');
    }

    /**
     * Busca os tipos disponíveis
     * @return object
     */
    function listarTodos() {
        return $this->CI->tipos_viagem_model->listarTodos();
    }

    /**
     * Busca o tipo
     * @param int $id id do tipo
     * @return object
     */
    function listarUm($id) {
        return $this->CI->tipos_viagem_model->setId($id)->listarUm();
    }

    /**
     * Busca o tipo por código
     * @param int $seguradora_tipo_id tipo id
     * @return object
     */
    function listarUmPorSeguradoraTipoId($seguradora_tipo_id) {
        return $this->CI->tipos_viagem_model->setSeguradora_tipo_id($seguradora_tipo_id)->listarUmPorSeguradoraTipoId();
    }

}
