<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Destino_grupo
{

	private $CI;
	private $model;

	function __construct()
	{
		$this->CI = &get_instance();
		$this->CI->load->model('administracao/destinos_grupos_model');
		$this->model = $this->CI->destinos_grupos_model;
	}

	function adicionar($data)
	{
		return $this->model->hydrate($data)->adicionar();
	}

	function alterar($data)
	{
		$data['id'] = $data['grupo_id'];
		$data['data_modificacao'] = date("Y-m-d H:i:s");
		$grupo = (array)$this->listarUm($data['id']);
		return $this->model->hydrate(array_merge($grupo, $data))->alterar();
	}

	function remover($id)
	{
		return $this->model->setId($id)->remover();
	}

	function listarUm($id)
	{
		return $this->model->setId($id)->listarUm();
	}

	function listarTodos()
	{
		return [
			'result' => $this->model->setLimit(100)->setOffset(0)->listarTodos(),
			'total' => $this->model->total()
		];
	}

	/**
	 * Retorna os grupos com o cache
	 * @return mixed
	 */
	function listarTodosComCache()
	{
		return $this->model->listarTodosComCache();
	}

	/**
	 * Retorna um grupo com o cache
	 * @param $id
	 * @return mixed
	 */
	function listarUmComCache($id)
	{
		return $this->model->setId($id)->listarUmComCache();
	}

}
