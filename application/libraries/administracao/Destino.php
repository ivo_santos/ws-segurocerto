<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Destino {

    private $CI;

    function __construct() {
        $this->CI = &get_instance();
        $this->CI->load->model('administracao/destinos_model');
    }

    /**
     * Busca os destinos disponíveis
     * 
     * @return object
     */
    function listarTodos() {
        return $this->CI->destinos_model->listarTodos();
    }

    /**
     * Busca o destino
     * 
     * @param int $destino_id id do destino
     * @return object
     */
    function listarUm($destino_id) {
        return $this->CI->destinos_model
                        ->setId($destino_id)
                        ->listarUm();
    }

    /**
     * Busca o destino por seguradora_destino_id
     * @param int $seguradora_destino_id código do estado
     * @return object
     */
    function listarUmPorSeguradoraDestinoId($seguradora_destino_id) {
        return $this->CI->destinos_model->setSeguradora_destino_id($seguradora_destino_id)->listarUmPorSeguradoraDestinoId();
    }

}
