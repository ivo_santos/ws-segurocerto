<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Cron
{

	private $CI;

	function __construct()
	{
		$this->CI = &get_instance();
		$this->CI->load->model('cron/cron_model');
		$this->CI->load->library('vendas/venda');
		$this->CI->load->library('administracao/cliente');
		$this->CI->load->library('endereco/endereco');
		$this->CI->load->library('administracao/destino');
		$this->CI->load->library('administracao/motivo_viagem');
		$this->CI->load->library('administracao/tipo_viagem');
		$this->CI->load->library('cotacao/plano');
		$this->CI->load->library('cotacao/cotacao');
		$this->CI->load->library('seguradora/seguradora_emissao');
		$this->CI->load->library('vouchers/voucher');
		$this->CI->load->library('emails/emails');
		$this->CI->load->library('cotacao/cotacao_historicos');
	}

	/**
	 * @param $filtros
	 * @return mixed
	 */
	function listarTodos($filtros)
	{
		$result['result'] = $this->CI->cron_model->setLimit($filtros['limit'])->setOffset($filtros['inicio'])->listarTodos();
		$result['total'] = $this->CI->cron_model->total();

		return $result;
	}


	/**
	 * Adicionar venda_id a cron
	 * @param string $venda_id
	 */
	function adicionar($venda_id, $status = 2, $comentario = '')
	{
		$venda = $this->CI->venda->listarUm($venda_id);
		$this->CI->cron_model->setVenda_id($venda->id)->setStatus($status)->setComentario($comentario)->adicionar();
	}

	/**
	 * Remove um registro da cron por hash venda_id
	 * @param string $venda_id
	 */
	function removerPorVendaId($venda_id)
	{
		$this->CI->cron_model->setVenda_id($venda_id)->removerPorVendaId();
	}


	/**
	 * TODO PENSAR EM UMA MANEIRA MELHOR DE COLOCAR ESSA EXTRAÇÃO DE DADOS.
	 * TODO TALVEZ QUEBRAR O MÉTODO DE COTAÇÃO E REUSAR.
	 * @param $cotacao_id
	 * @param $plano_id
	 * @return array
	 */
	private function extrarInfoCotacao($cotacao_id, $plano_id)
	{
		$info_geral = $this->CI->cotacao->gerar($cotacao_id, true);

		$info = [];
		if (!empty($info_geral['planos_valores'])) {
			foreach ($info_geral['planos_valores'] as $plano_valor) {
				if ($plano_valor['plano_id'] == $plano_id) {
					$info = [
						'coberturas_info' => $plano_valor['coberturas_info'],
						'servicos_info' => $plano_valor['servicos_info'],
					];
					break;
				}
			}
		}

		return $info;
	}

	/**
	 * Emitir os vouchers
	 */
	function emitirVouchers()
	{
		// BUSCA DADOS NA CRON
		$cron = $this->CI->cron_model->setStatus(2)->listarUm();

		if (!empty($cron)) {
			#### ALTERA CRON PARA STATUS RODANDO (4)
			$this->CI->cron_model->setStatus(4)->setComentario('rodando emissão de vouchers.')->setVenda_id($cron->venda_id)->alterar();
			// BUSCA DADOS DA VENDA
			$venda = $this->CI->venda->listarUm(gerarChave($cron->venda_id));

			// BUSCA DADOS DA COTACAO
			$cotacao = $this->CI->cotacao->listarUm(gerarChave($venda->cotacao_id));

			// EXTRAI DADOS DA COTAÇÃO REAL ATUALIZADA.
			$info = $this->extrarInfoCotacao(gerarChave($cotacao->id), $venda->plano_id);
			if(empty($info)){
				$this->CI->cron_model->setStatus(3)->setComentario('Erro ao buscar cotação atualizada')->setVenda_id($cron->venda_id)->alterar();
			}

			/**
			 * Atualiza a info de coberturas e serviços
			 */
			$this->CI->venda->alterar([
				'venda_id' => gerarChave($venda->id),
				'coberturas_info' => $info['coberturas_info'],
				'servicos_info' => $info['servicos_info'],
			]);

			// BUSCA DADOS DA COTACAO
			$endereco = $this->CI->endereco->listarUmPorVendaId(gerarChave($venda->id));
			// BUSCA DADOS DO CLIENTES
			$clientes = $this->CI->cliente->listarTodosPorVendaId(gerarChave($venda->id));
			// BUSCA DADOS DO DESTINO
			$destino = $this->CI->destino->listarUm($cotacao->destino_id);
			// BUSCA O MOTIVO
			$motivo = $this->CI->motivo_viagem->listarUm($cotacao->motivo_id);
			// BUSCA O TIPO
			$tipo = $this->CI->tipo_viagem->listarUm($cotacao->tipo_id);
			// BUSCA DADOS DO PLANO
			$plano = $this->CI->plano->listarUm($venda->plano_id);

			// PREPARA DADOS DA CHAMADA DE EMISSÃO NA SEGURADORA
			// (SEGUINDO O PADRÃO DAS CHAMADAS SEMPRE COM ARRAY ASSOCIATIVO)
			$data = [
				'nome' => $venda->nome,
				'email' => 'ivo@tripguard.com.br',
				'documento' => $venda->documento,
				'telefone' => $venda->telefone,
				'nascimento' => $venda->nascimento,
				'coberturas_info' => (array)unserialize($info['coberturas_info']),
				'servicos_info' => (array)unserialize($info['servicos_info']),
				'data_inicio' => $cotacao->data_inicio,
				'data_final' => $cotacao->data_final,
				'localidade_origem_id' => '253',
				'localidade_destino_id' => $destino->seguradora_destino_id,
				'tipo_id' => $tipo->seguradora_tipo_id,
				'motivo_id' => $motivo->seguradora_motivo_id,
				'plano_id' => $plano->seguradora_plano_id,
				'endereco' => (array)$endereco,
				'clientes' => json_decode(json_encode($clientes), true)
			];

			// CHAMADA SEGURADORA EMITIR
			$result = $this->CI->seguradora_emissao->emitir($data);

			if (isset($result->codigosRetorno->codigoRetorno->codigo) && $result->codigosRetorno->codigoRetorno->codigo == 0) {

				#### SALVA DADOS DOS VOUCHERS
				if (count($result->contratos->contrato) > 1) {
					$contratos = $result->contratos->contrato;
				} else {
					$contratos[] = $result->contratos->contrato;
				}

				// ADICIONA OS DADOS SOBRE O  VOUCHER
				foreach ($contratos as $contrato) {
					$this->CI->voucher->adicionar([
						'venda_id' => $venda->id,
						'cliente_id' => $contrato->idViajante,
						'seguradora_numero_apolice' => $contrato->numeroApolice,
						'seguradora_numero_sorte' => $contrato->numeroSorte,
						'seguradora_numero_compra' => $result->numeroCompra,
					]);
				}

				$this->CI->cotacao_historicos->adicionar([
					'cotacao_id' => $venda->cotacao_id,
					'titulo' => 'Venda',
					'descricao' => 'Apólice(s) gerada(s) com sucesso.',
					'funcionario_id' => 0,
				]);

				#### STATUS TRANSAÇÃO SEGURADORA (1)
				$this->CI->cron_model->setStatus(1)->setComentario('Apólice(s) gerada(s) com sucesso.')->setVenda_id($cron->venda_id)->alterar();
			} else {
				#### MENSAGEM DE ERRO / STATUS ERRO (3)
				$this->CI->cron_model->setStatus(3)->setComentario(serialize($result))->setVenda_id($cron->venda_id)->alterar();
			}

			f($result, false);
		} else {
			die('Nenhuma venda para gerar vouchers!');
		}
	}

	/**
	 * Envio de email com vouchers
	 */
	function enviarEmailComVouchers()
	{
		$this->CI->load->library('vouchers/voucher');
		$this->CI->load->helper('mpdf');
		$this->CI->load->library('administracao/empresa');

		// BUSCA DADOS NA CRON
		$cron = $this->CI->cron_model->setStatus(1)->listarUm();

		if (!empty($cron)) {
			#### ALTERA CRON PARA STATUS RODANDO (4)
			$this->CI->cron_model->setStatus(4)->setComentario('rodando envio de vouchers.')->setVenda_id($cron->venda_id)->alterar();

			// BUSCA DADOS DA VENDA
			$venda = $this->CI->venda->listarUm(gerarChave($cron->venda_id));

			if (in_array($venda->forma_pagamento_id, [1, 4]) && !$venda->aprovacao_manual) {
				$this->CI->cotacao_historicos->adicionar([
					'cotacao_id' => $venda->cotacao_id,
					'titulo' => 'Venda',
					'descricao' => 'Aguardando confirmação de pagamento para enviar apólice(s) para o email.',
					'funcionario_id' => 0,
				]);

				// REMOVE DA CRON
				$this->removerPorVendaId(gerarChave($venda->id));
				die('venda ainda não aprovada manualmente para ser enviada.');
			} else {

				$vouchers = $this->CI->voucher->listarTodosPorVendaId(gerarChave($cron->venda_id));

				// EMAIL QUE VAI RECEBER OS VOUCHERS
				## BUSCA EMPRESA
				$empresa = $this->CI->empresa->listarUm(gerarChave(1));
				$dados_email['venda_id'] = gerarChave($venda->id);
				$nome = explode(" ", $venda->nome);
				$dados_email['nome'] = $nome[0];
				$dados_email['emails'] = array($nome[0] => $venda->email);
				$dados_email['codigo'] = $venda->id;
				$dados_email['senha'] = $venda->salt;
				$dados_email['empresa'] = (array)$empresa;

				foreach ($vouchers as $voucher) {
					$data = $this->CI->voucher->gerarInfoApolice(gerarChave($voucher->id));

					// numero sorte
					$dados_email['numero_sorte'] = $voucher->seguradora_numero_sorte;

					$cliente = $this->CI->cliente->listarUm(gerarChave($voucher->cliente_id));
					$dados_email['cliente'] = (array)$cliente;

					## ASSUNTO DO EMAIL
					$nome_cliente = explode(" ", $cliente->nome);
					$dados_email['assunto'] = $nome_cliente[0] . ", Apólice de seguro viagem";

					// cria pasta temporaria em assets/anexos/
					$pasta = 'assets/anexos/' . $venda->id . '/';
					if (!file_exists($pasta)) {
						mkdir($pasta);
					}
					chmod($pasta, 0777);

					##
					#  VOUCHER PT_BR
					#
					$caminho_pdf = "pdf_templates/voucher_pt_br.php";
					$pdf = $this->CI->parser->parse($caminho_pdf, $data, true);
					$dados_email['arquivos']['apolice_' . $voucher->id . "-pt_br.pdf"] = $arquivo = $pasta . 'apolice_' . $voucher->id . "-pt_br.pdf";
					$stream = pdf($pdf);
					$fopen = fopen($arquivo, "w");
					fputs($fopen, $stream);
					fclose($fopen);

					##
					#  VOUCHER EN_US
					#
//				$data['sexo'] = ($data['sexo_id'] == 1) ? 'Male' : 'Female';
//				$caminho_pdf = "pdf_templates/voucher_en_us.php";
//				$pdf = $this->CI->parser->parse($caminho_pdf, $data, true);
//				$dados_email['arquivos']['apolice_' . $voucher->id . "-en_us.pdf"] = $arquivo = $pasta . 'apolice_' . $voucher->id . "-en_us.pdf";
//				$stream = pdf($pdf);
//				$fopen = fopen($arquivo, "w");
//				fputs($fopen, $stream);
//				fclose($fopen);

					// ANEXA GUIA DO SEGURADO E TAMBÉM O GUIA DE CAPITALIZAÇÃO
					$dados_email['arquivos']["guia-segurado.pdf"] = "assets/arquivos/guia-segurado.pdf";
					$dados_email['arquivos']["regulamento-capitalizacao.pdf"] = "assets/arquivos/regulamento-capitalizacao.pdf";

					// ENVIA EMAIL COM VOUCHERS
					$message = $this->CI->emails->emailVouchers($dados_email);
				}

				$this->CI->cotacao_historicos->adicionar([
					'cotacao_id' => $venda->cotacao_id,
					'titulo' => 'Venda',
					'descricao' => 'Enviou apólice(s) para o email',
					'funcionario_id' => 0,
				]);

				// REMOVE DA CRON
				$this->removerPorVendaId(gerarChave($venda->id));
				// REMOVE O DIRETÓRIO
				if (!empty($pasta) && file_exists($pasta)) {
					array_map('unlink', glob($pasta . "/*.*"));
					rmdir($pasta);
				}

				die('vai enviar email com vouchers');

			}
		} else {
			die('nenhum registro para gerar vouchers!');
		}
	}


	/**
	 * Busca uma venda que não foi paga até a data limite
	 * @return array
	 */
	function cancelarVendasPendentes()
	{
		$venda = $this->CI->venda->listarUmParaCancelar();

		if (!empty($venda)) {
			// CHAMA O CANCELAMENTO.
			$this->CI->venda->cancelar([
				'venda_id' => gerarChave($venda->id),
				'funcionario_id' => 0,
				'comentario' => 'Cancelada pela cron. motivo: falta de pagamento',
				'cron' => 1,
			]);

			$this->CI->cotacao_historicos->adicionar([
				'cotacao_id' => $venda->cotacao_id,
				'titulo' => 'Venda',
				'descricao' => 'Cancelada pela cron. motivo: falta de pagamento',
				'funcionario_id' => 0,
			]);

			return ['success' => true, 'mensagem' => "Venda {$venda->id} foi cancelada por falta de pagamento."];
		} else {
			return ['success' => false, 'mensagem' => "Nenhuma venda para cancelar."];
		}
	}
}
