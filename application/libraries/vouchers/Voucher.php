<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Voucher {

    private $CI;

    function __construct() {
        $this->CI = &get_instance();
        $this->CI->load->model('vouchers/vouchers_model');
    }

    /**
     * Adicionar um voucher
     * @param array $data
     * @return int
     */
    function adicionar($data) {
        return $this->CI->vouchers_model
                        ->setVenda_id($data['venda_id'])
                        ->setCliente_id($data['cliente_id'])
                        ->setSeguradora_numero_apolice($data['seguradora_numero_apolice'])
                        ->setSeguradora_numero_sorte($data['seguradora_numero_sorte'])
                        ->setSeguradora_numero_compra($data['seguradora_numero_compra'])
                        ->adicionar();
    }

    /**
     * Buscar os vouchers de uma venda por hash do id da venda
     * @param string $venda_id
     * @return object
     */
    function listarTodosPorVendaId($venda_id) {
        return $this->CI->vouchers_model->setVenda_id($venda_id)->listarTodosPorVendaId();
    }

    /**
     * Listar um voucher por hash do cliente_id
     * @param string $cliente_id
     * @return object
     */
    function listarUmPorClienteId($cliente_id) {
        return $this->CI->vouchers_model->setCliente_id($cliente_id)->listarUmPorClienteId();
    }

    /**
     * Listar um voucher pelo hash do id
     * @param string $id
     * @return object
     */
    function listarUm($id) {
        return $this->CI->vouchers_model->setId($id)->listarUm();
    }

    function gerarInfoApolice($voucher_id) {
        $this->CI->load->library('vendas/venda');
        $this->CI->load->library('administracao/cliente');
        $this->CI->load->library('endereco/endereco');
        $this->CI->load->library('administracao/destino');
        $this->CI->load->library('cotacao/plano');
        $this->CI->load->library('cotacao/cotacao');

        $data = array('base_url' => $this->CI->config->base_url());

        // VOUCHER
        $voucher = $this->listarUm($voucher_id);

        if (!empty($voucher)) {

            // BUSCA DADOS DA VENDA
            $venda = $this->CI->venda->listarUm(gerarChave($voucher->venda_id));

            if ($venda->status == 3) {
                die('Voucher indisponível.');
            }

            $cotacao = $this->CI->cotacao->listarUm(gerarChave($venda->cotacao_id));
            $destino = $this->CI->destino->listarUm($cotacao->destino_id);
            $endereco = $this->CI->endereco->listarUmPorVendaId(gerarChave($venda->id));

            ####
            # VARIAVEIS DO VOUCHER
            $data['inicio_vigencia'] = formata_data($cotacao->data_inicio);
            $data['final_vigencia'] = formata_data($cotacao->data_final);
            $data['data_emissao'] = formata_data($venda->data_pagamento);
            $data['destino'] = $destino->nome;
            $data['origem'] = $endereco->cidade;
            $data['endereco'] = (array) $endereco;

            $cliente = $this->CI->cliente->listarUm(gerarChave($voucher->cliente_id));

            // VALOR DO PREMIO DE ACORDO COM A IDADE DO PASSAGEIRO
            $idade = extrairIdadeFromNascimento(formata_data($cliente->nascimento));
            $valor_premio = ($idade <= 64) ? $venda->valor : $venda->valor_idoso;

            $data['valor_premio'] = number_format($valor_premio, 2, ',', '.');
            $data['iof'] = number_format((($valor_premio * 0.39) / 100), 2, ',', '.');

            if ($venda->forma_pagamento_id == 1) {
                $data['forma_pagamento'] = "Boleto";
            } else if ($venda->forma_pagamento_id == 2) {
                $data['forma_pagamento'] = "Cartão de crédito";
            } else if ($venda->forma_pagamento_id == 3) {
                $data['forma_pagamento'] = "Débito";
            } else {
                $data['forma_pagamento'] = "Depósito";
            }
            
            // TRATA AS COBERTURAS
            $data['coberturas'] = [];
            $venda_coberturas = unserialize($venda->coberturas_info);

            if (!empty($venda_coberturas['cobertura']) && $idade <= 64) {
                foreach ($venda_coberturas['cobertura'] as $chave => $venda_cobertura) {
                    $cobertura = $this->CI->plano->listarCoberturaPorSeguradoraCodigoIdComLimite($venda_cobertura['codigoCobertura'], $venda->plano_id);
                    if (!empty(!empty($cobertura))) {
                        $data['coberturas'][$chave]['nome'] = $cobertura->nome;
                        $data['coberturas'][$chave]['moeda_limite'] = $cobertura->moeda_limite;
                        $data['coberturas'][$chave]['limite'] = $cobertura->limite;
                        $data['coberturas'][$chave]['premio'] = number_format($venda_cobertura['premio'], 2, ',', '.');
                    }
                }
            } else if (!empty($venda_coberturas['cobertura_idoso'])) {
                foreach ($venda_coberturas['cobertura_idoso'] as $chave => $venda_cobertura) {
                    $cobertura = $this->CI->plano->listarCoberturaPorSeguradoraCodigoIdComLimite($venda_cobertura['codigoCobertura'], $venda->plano_id);
                    if (!empty(!empty($cobertura))) {
                        $data['coberturas'][$chave]['nome'] = $cobertura->nome;
                        $data['coberturas'][$chave]['moeda_limite'] = $cobertura->moeda_limite;
                        $data['coberturas'][$chave]['limite'] = $cobertura->limite;
                        $data['coberturas'][$chave]['premio'] = number_format($venda_cobertura['premio'], 2, ',', '.');
                    }
                }
            }
            // TRATA AS COBERTURAS
            // TRATA AS ASSISTENCIAS
            $data['assistencias'] = [];
            $venda_assistencias = unserialize($venda->servicos_info);
            if (!empty($venda_assistencias)) {
                foreach ($venda_assistencias as $chave => $venda_assistencia) {
                    $servico = $this->CI->plano->listarCoberturaPorSeguradoraCodigoId($venda_assistencia['codigoServico']);
                    if (!empty(!empty($servico))) {
                        $data['assistencias'][$chave]['nome'] = $servico->nome;
                    }
                }
            }
            // TRATA AS ASSISTENCIAS
            // DADOS DO VIAJANTE
            $data['voucher_id'] = $voucher->id;
            $data['numero_proposta'] = $voucher->seguradora_numero_compra;
            $data['bilhete_id'] = $voucher->seguradora_numero_apolice;
            $data['venda_id'] = $venda->id;
            $data['nome'] = $cliente->nome;
            $data['documento'] = $cliente->documento;
            $data['email'] = $venda->email;
            $data['sexo'] = ($cliente->sexo == 1) ? 'Masculino' : 'Feminino';
            $data['sexo_id'] = $cliente->sexo;
            $data['nascimento'] = formata_data($cliente->nascimento);

            return $data;
        } else {
            die('voucher não existe');
        }
    }

    /**
     * EXIBE UMA APÓLICE ONLINE
     * @param string $lang
     * @param string $voucher_id
     */
    function online($lang, $voucher_id) {
        $data = $this->gerarInfoApolice($voucher_id);
        $data['online'] = true;
        $data['lang'] = $lang;
        $data['voucher_id_print'] = $voucher_id;

        if ($lang == "pt_BR") {
            #  VOUCHER PT_BR
            echo $this->CI->parser->parse("pdf_templates/voucher_pt_br.php", $data, false);
        } else {
            die('indisponível');
            #  VOUCHER EN_US
            $data['sexo'] = ($data['sexo_id'] == 1) ? 'Male' : 'Female';
            echo $this->CI->parser->parse("pdf_templates/voucher_en_us.php", $data, false);
        }
        die();
    }

    /**
     * FAZ O DOWNLOAD DO PDF DA APOLICE
     * @param $lang
     * @param $voucher_id
     */
    function download($lang, $voucher_id) {
        $this->CI->load->helper('mpdf');

        $data = $this->gerarInfoApolice($voucher_id);

        if ($lang == "pt_BR") {
            #  VOUCHER PT_BR
            $pdf = $this->CI->parser->parse("pdf_templates/voucher_pt_br.php", $data, true);
            $file_name = 'apolice_' . $data['voucher_id'] . "-pt_br.pdf";
            file_put_contents('assets/temp/' . $file_name, pdf($pdf));
        } else {
            die('indisponível');
            #  VOUCHER EN_US
            $data['sexo'] = ($data['sexo_id'] == 1) ? 'Male' : 'Female';
            $pdf = $this->CI->parser->parse("pdf_templates/voucher_en_us.php", $data, true);
            $file_name = 'apolice_' . $data['voucher_id'] . "-en_us.pdf";
            file_put_contents('assets/temp/' . $file_name, pdf($pdf));
        }

        header('Content-type: application/pdf');
        header('Content-Disposition: attachment; filename="' . $file_name . '"');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize('assets/temp/' . $file_name));
        header('Accept-Ranges: bytes');
        readfile('assets/temp/' . $file_name);
        unlink('assets/temp/' . $file_name);
        exit();
    }

}
