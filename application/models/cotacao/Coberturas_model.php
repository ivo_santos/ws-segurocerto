<?php

class Coberturas_model extends MY_Model {

    protected $id;
    protected $ids;
    protected $plano_id;
    protected $seguradora_codigo_id;

    //Construtor
    function __construct() {
        parent::__construct();
    }

    public function adicionar($data) {
        $data = array(
            'nome' => $data['nome'],
            'obrigatorio' => $data['obrigatorio'],
            'seguradora_codigo_id' => $data['seguradora_codigo_id'],
        );
        $this->db->insert('coberturas', $data);
        return $this->db->insert_id();
    }

    public function adicionarLimite($data) {
        $data = array(
            'cobertura_id' => $data['cobertura_id'],
            'plano_id' => $data['plano_id'],
            'moeda_limite' => $data['moeda_limite'],
            'limite' => $data['limite'],
        );
        $this->db->insert('coberturas_limites', $data);
        return $this->db->insert_id();
    }

    public function listarUm($data) {
        $this->db->select('*');
        $this->db->from('coberturas_limites');
        $this->db->where('plano_id =', $data['plano_id']);
        $this->db->where('cobertura_id =', $data['cobertura_id']);
        $query = $this->db->get();
        return $query->row_object();
    }

    /** ok
     * Busca coberturas por seguradora_codigo_id
     * @return object
     */
    public function listarUmPorSeguradoraCodigoId() {
        $this->db->select('*');
        $this->db->from('coberturas');
        $this->db->where('seguradora_codigo_id =', $this->seguradora_codigo_id);
        $query = $this->db->get();
        return $query->row_object();
    }

	/**
	 * Busca cobertura pelo seguradora_codigo_id e plano_id com o limite
	 * @return object
	 */
	public function listarUmPorSeguradoraCodigoIdComLimite() {
		$this->db->select('c.nome, cl.moeda_limite, cl.limite');
		$this->db->from('coberturas_limites cl');
		$this->db->join('coberturas c', 'c.id = cl.cobertura_id', 'left');
		$this->db->where('seguradora_codigo_id =', $this->seguradora_codigo_id);
		$this->db->where('cl.plano_id =', $this->plano_id);
		$query = $this->db->get();
		return $query->row_object();
	}


    /**
     * Busca coberturas de um plano por id
     * @return object
     */
    public function listarCoberturasPorPlanoId() {
        $this->db->select('cl.*, c.nome, c.tipo');
        $this->db->from('coberturas_limites cl');
        $this->db->join('coberturas c', 'c.id = cl.cobertura_id', 'left');
        $this->db->where('plano_id =', $this->plano_id);
        $query = $this->db->get();
        return $query->result_object();
    }

    /**
     * Busca coberturas dos planos
     * @return object
     */
    public function listarCoberturasPorPlanosIds() {
        $this->db->select('c.id, c.nome, c.tipo');
        $this->db->from('coberturas_limites cl');
        $this->db->join('coberturas c', 'c.id = cl.cobertura_id', 'left');
        $this->db->group_by('c.id'); 
        $this->db->order_by('c.ordem', 'asc'); 
        $this->db->order_by('c.id', 'asc'); 
        $this->db->where_in('cl.plano_id', $this->ids);
        $query = $this->db->get();
        return $query->result_object();
    }

     /**
     * Busca cobertura especifica pelo id e pelo plano id
     * @return string
     */
    public function listarLimitePorCoberturaIdplanoId() {
        $this->db->select('cl.moeda_limite, cl.limite');
        $this->db->from('coberturas_limites cl');
        $this->db->where('cl.cobertura_id =', $this->id);
        $this->db->where('cl.plano_id =', $this->plano_id);
        $query = $this->db->get();
        return $query->row_object();
    }


}
