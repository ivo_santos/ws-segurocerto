<?php

class Planos_model extends MY_Model {

    protected $id;
    protected $seguradora_plano_id;
    protected $ids_planos = array();
    protected $idoso = false;
    protected $dias;
    protected $finalidade = false;

    /**
     * Detalhes de um plano por id
     * 
     * @param int $this->id id do plano
     * @return object
     */
    public function listarUm() {
        $this->db->select('*');
        $this->db->from('planos');
        $this->db->where('id =', $this->id);
        $query = $this->db->get();
        return $query->row_object();
    }


    /**
     * Busca o plano por id MAPFRE
     * 
     * @return object
     */
    public function listaUmPorSeguradoraPlanoId() {
        $this->db->select('*');
        $this->db->from('planos');
		$this->db->where('seguradora_plano_id =', $this->seguradora_plano_id);
		$this->db->where('status =', 1);
        $query = $this->db->get();
        return $query->row_object();
    }

}
