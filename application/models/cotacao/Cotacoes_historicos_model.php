<?php

class Cotacoes_historicos_model extends MY_Model {

    protected $id;
    protected $cotacao_id;
    protected $titulo;
    protected $descricao;
    protected $funcionario_id;
	protected $status;
	protected $data_retorno;
    protected $data_cadastro;
    protected $data_modificacao;
    protected $limit;
    protected $offset;

    function hydrate($data) {
        $this->id = tratarDefault('id', $data);
        $this->cotacao_id = tratarDefault('cotacao_id', $data);
        $this->titulo = tratarDefault('titulo', $data);
        $this->descricao = tratarDefault('descricao', $data);
        $this->funcionario_id = tratarDefault('funcionario_id', $data, 0);
        $this->status = tratarDefault('status', $data, 1);
        $this->data_retorno = tratarDefault('data_retorno', $data);
        $this->data_cadastro = tratarDefault('data_cadastro', $data);
        $this->data_modificacao = tratarDefault('data_modificacao', $data);
        return $this;
    }

    /**
     * Adiciona um historico
     * @return int id inserido
     */
    public function adicionar() {
        $data = array(
            'cotacao_id' => $this->cotacao_id,
            'titulo' => $this->titulo,
            'descricao' => $this->descricao,
            'funcionario_id' => $this->funcionario_id,
            'status' => $this->status,
            'data_retorno' => $this->data_retorno,
            'data_cadastro' => $this->data_cadastro,
            'data_modificacao' => $this->data_modificacao,
        );
        $this->db->insert('cotacoes_historicos', $data);
        return $this->db->insert_id();
    }

	/**
	 * Altera um historico
	 * @return mixed
	 */
	public function alterar() {
		$data = array(
			'cotacao_id' => $this->cotacao_id,
			'titulo' => $this->titulo,
			'descricao' => $this->descricao,
			'funcionario_id' => $this->funcionario_id,
			'status' => $this->status,
			'data_retorno' => $this->data_retorno,
			'data_modificacao' => $this->data_modificacao,
		);
		$this->db->where('sha1(md5(id)) =', $this->id);
		return $this->db->update('cotacoes_historicos', $data);
	}

	public function marcarHistoricosComoConcluidos() {
		$data = array(
			'status' => 1,
			'data_retorno' => null,
		);
		$this->db->where('cotacao_id =', $this->cotacao_id);
		return $this->db->update('cotacoes_historicos', $data);
	}

    /**
     * Busca um historico por hash do id
     * @param string $this->id
     * @return object
     */
    public function listarUm() {
        $this->db->select('*');
        $this->db->from('cotacoes_historicos');
        $this->db->where('sha1(md5(id)) =', $this->id);
        $query = $this->db->get();
        return $query->row_object();
    }

	/**
	 * Listar todos de uma cotação por hash do id da cotação
	 * @return mixed
	 */
	public function listarTodosPorCotacaoId() {
		$this->db->select('ch.*');
		$this->db->from('cotacoes_historicos ch');
		$this->db->where('sha1(md5(ch.cotacao_id)) =', $this->cotacao_id);
		$this->db->order_by("ch.id", "desc");
		$this->db->limit($this->limit, $this->offset);
		$query = $this->db->get();
		return $query->result_object();
	}

	/**
	 * Total de historicos por hash do id da venda
	 * @return mixed
	 */
    public function totalPorCotacaoId() {
        $this->db->select('count(*) as total');
        $this->db->from('cotacoes_historicos ch');
		$this->db->where('sha1(md5(ch.cotacao_id)) =', $this->cotacao_id);
		$query = $this->db->get();
        return $query->row_object()->total;
    }


	public function listarTodosAgendamentosParaHoje() {
		$this->db->select('ch.*');
		$this->db->from('cotacoes_historicos ch');
		$this->db->where('data_retorno =', date('Y-m-d'));
		$this->db->group_by('cotacao_id');
		$query = $this->db->get();
		return $query->result_object();
	}

	public function listarTodosAgendamentosAtrasados() {
		$this->db->select('ch.*');
		$this->db->from('cotacoes_historicos ch');
		$this->db->where('data_retorno <', date('Y-m-d'));
		$this->db->group_by('cotacao_id');
		$query = $this->db->get();
		return $query->result_object();
	}

}
