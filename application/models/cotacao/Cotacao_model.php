<?php

class Cotacao_model extends MY_Model
{

	protected $id;
	protected $destino_grupo_id;
	protected $destino_id;
	protected $data_inicio;
	protected $data_final;
	protected $num_passageiros;
	protected $num_passageiros_maiores;
	protected $motivo_id;
	protected $tipo_id;
	protected $nome;
	protected $email;
	protected $telefone;
	protected $nome_amigo;
	protected $email_amigo;
	protected $device;
	protected $ip;
	protected $origem;
	protected $parametros_url;
	protected $situacao;
	protected $ids;
	protected $data_validade;
	protected $data_cadastro;

	function hydrate($data)
	{
		$this->id = tratarDefault('id', $data);
		$this->destino_grupo_id = tratarDefault('destino_grupo_id', $data);
		$this->destino_id = tratarDefault('destino_id', $data);
		$this->data_inicio = tratarDefault('data_inicio', $data);
		$this->data_final = tratarDefault('data_final', $data);
		$this->num_passageiros = tratarDefault('num_passageiros', $data, 1);
		$this->num_passageiros_maiores = tratarDefault('num_passageiros_maiores', $data, 0);
		$this->motivo_id = tratarDefault('motivo_id', $data);
		$this->tipo_id = tratarDefault('tipo_id', $data);
		$this->nome = tratarDefault('nome', $data);
		$this->email = tratarDefault('email', $data);
		$this->telefone = tratarDefault('telefone', $data);
		$this->nome_amigo = tratarDefault('nome_amigo', $data);
		$this->email_amigo = tratarDefault('email_amigo', $data);
		$this->device = tratarDefault('device', $data);
		$this->ip = tratarDefault('ip', $data);
		$this->origem = tratarDefault('origem', $data);
		$this->parametros_url = tratarDefault('parametros_url', $data);
		$this->situacao = tratarDefault('situacao', $data, '1');
		$this->data_validade = tratarDefault('data_validade', $data);
		$this->data_cadastro = tratarDefault('data_cadastro', $data);
		return $this;
	}

	/**
	 * Salvar cotação
	 * @return int id inserido
	 */
	public function adicionar()
	{
		$data = array(
			'destino_grupo_id' => $this->destino_grupo_id,
			'destino_id' => $this->destino_id,
			'data_inicio' => $this->data_inicio,
			'data_final' => $this->data_final,
			'num_passageiros' => $this->num_passageiros,
			'num_passageiros_maiores' => $this->num_passageiros_maiores,
			'motivo_id' => $this->motivo_id,
			'tipo_id' => $this->tipo_id,
			'nome' => $this->nome,
			'email' => $this->email,
			'telefone' => $this->telefone,
			'device' => $this->device,
			'ip' => $this->ip,
			'origem' => $this->origem,
			'parametros_url' => $this->parametros_url,
			'situacao' => $this->situacao,
			'data_validade' => $this->data_validade,
			'data_cadastro' => $this->data_cadastro,
		);
		$this->db->insert('cotacoes', $data);
		return $this->db->insert_id();
	}

	public function alterar()
	{
		$data = array(
			'destino_grupo_id' => $this->destino_grupo_id,
			'destino_id' => $this->destino_id,
			'data_inicio' => $this->data_inicio,
			'data_final' => $this->data_final,
			'num_passageiros' => $this->num_passageiros,
			'num_passageiros_maiores' => $this->num_passageiros_maiores,
			'motivo_id' => $this->motivo_id,
			'tipo_id' => $this->tipo_id,
			'nome' => $this->nome,
			'email' => $this->email,
			'telefone' => $this->telefone,
			'device' => $this->device,
			'ip' => $this->ip,
			'origem' => $this->origem,
			'parametros_url' => $this->parametros_url,
			'situacao' => $this->situacao,
			'data_validade' => $this->data_validade,
		);
		$this->db->where('sha1(md5(id)) =', $this->id);
		return $this->db->update('cotacoes', $data);
	}

	/**
	 * Remove uma cotação por hash do id
	 * @return void
	 */
	public function remover()
	{
		return $this->db->delete('cotacoes', array('sha1(md5(id))' => $this->id));
	}

	public function listarUm()
	{
		$this->db->select('*, totalHistoricos(id) as total_historicos');
		$this->db->from('cotacoes');
		$this->db->where('sha1(md5(id)) =', $this->id);
		$query = $this->db->get();
		return $query->row_object();
	}

	public function salvarCotacaoPorEmail()
	{
		$data = array(
			'nome' => $this->nome,
			'email' => $this->email,
			'nome_amigo' => $this->nome_amigo,
			'email_amigo' => $this->email_amigo,
			'cotacao_id' => $this->id,
		);
		$this->db->insert('cotacao_por_email', $data);
		return $this->db->insert_id();
	}

	/**
	 * Busca registro de cotação por email via hash do id
	 * @param string $this ->id hash do id
	 * @return object
	 */
	public function listarUmCotacaoPorEmail()
	{
		$this->db->select('cpe.*, c.destino_grupo_id, c.destino_id,c.data_inicio, c.data_final, c.num_passageiros, c.num_passageiros_maiores, c.motivo_id, c.tipo_id, totalHistoricos(c.id) as total_historicos');
		$this->db->from('cotacao_por_email cpe');
		$this->db->join('cotacoes c', 'c.id = cpe.cotacao_id');
		$this->db->where('sha1(md5(cpe.id)) =', $this->id);
		$query = $this->db->get();
		return $query->row_object();
	}

	public function listarTodosNaoVenda()
	{
		$this->db->select('c.*, totalHistoricos(c.id) as total_historicos, historicoAgendadoHoje(c.id) as historico_agendado_hoje, historicoAgendadoAtrasado(c.id) as historico_agendado_atrasado, dg.nome as destino_grupo, d.nome as destino, tv.nome as tipo, mv.nome as motivo');
		$this->db->from('cotacoes c');
		$this->db->join('destinos d', 'd.id = c.destino_id', 'left');
		$this->db->join('destinos_grupos dg', 'dg.id = c.destino_grupo_id', 'left');
		$this->db->join('tipos_viagem tv', 'tv.id = c.tipo_id', 'left');
		$this->db->join('motivos_viagem mv', 'mv.id = c.motivo_id', 'left');
		$this->db->join('vendas v', 'c.id = v.cotacao_id', 'left');
		$this->db->where('v.id =', null);

		if (!empty($this->data_inicio) && !empty($this->data_final)) {
			$this->db->where('c.data_cadastro >=', $this->data_inicio);
			$this->db->where('c.data_cadastro <=', $this->data_final);
		}

		if (!empty($this->id)) {
			$this->db->where('c.id =', $this->id);
		}

		if (!empty($this->nome)) {
			$this->db->like('c.nome', $this->nome);
		}

		if (!empty($this->email)) {
			$this->db->like('c.email', $this->email);
		}

		if (!empty($this->situacao)) {
			$this->db->where('c.situacao =', $this->situacao);
		}

		$this->db->where_in('c.id', $this->ids);
		$this->db->order_by("c.id", "desc");
		$this->db->limit($this->limit, $this->offset);
		$query = $this->db->get();
		return $query->result_object();
	}

	public function totalNaoVenda()
	{
		$this->db->select('count(c.id) as total');
		$this->db->from('cotacoes c');
		$this->db->join('vendas v', 'c.id = v.cotacao_id', 'left');
		$this->db->where('v.id =', null);

		if (!empty($this->data_inicio) && !empty($this->data_final)) {
			$this->db->where('c.data_cadastro >=', $this->data_inicio);
			$this->db->where('c.data_cadastro <=', $this->data_final);
		}

		if (!empty($this->id)) {
			$this->db->where('c.id =', $this->id);
		}

		if (!empty($this->nome)) {
			$this->db->like('c.nome', $this->nome);
		}

		if (!empty($this->email)) {
			$this->db->like('c.email', $this->email);
		}

		if (!empty($this->situacao)) {
			$this->db->where('c.situacao =', $this->situacao);
		}

		if (!empty($this->ids)) {
			$this->db->where_in('c.id', $this->ids);
		}

		$query = $this->db->get();
		return $query->row_object()->total;
	}

}
