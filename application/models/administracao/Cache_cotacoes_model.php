<?php

class Cache_cotacoes_model extends MY_Model {

    protected $id;
    protected $destino_grupo_id;
    protected $cotacao;
    protected $data_validade;

	function hydrate($data) {
		$this->id = tratarDefault('id', $data);
		$this->destino_grupo_id = tratarDefault('destino_grupo_id', $data);
		$this->cotacao = tratarDefault('cotacao', $data);
		$this->data_validade = tratarDefault('data_validade', $data);
		return $this;
	}

	public function adicionar()
	{
		$data = array(
			'destino_grupo_id' => $this->destino_grupo_id,
			'cotacao' => null,
			'data_validade' => null,
			'data_atualizacao' => null
		);
		$this->db->insert('cache_cotacoes', $data);
		return $this->db->insert_id();
	}

    public function alterar() {
		$data = array(
			'cotacao' => $this->cotacao,
			'data_validade' => $this->data_validade,
			'data_atualizacao' => date('Y-m-d H:i:s')
		);
		$this->db->where('sha1(md5(id)) =', $this->id);
		return $this->db->update('cache_cotacoes', $data);
    }

	public function listarUmVencido() {
		$this->db->select('*');
		$this->db->from('cache_cotacoes');
		$this->db->where('data_validade <', date('Y-m-d'));
		$this->db->or_where('data_validade', null);
		$query = $this->db->get();
		return $query->row_object();
	}

	public function listarUmValidoParaDestinoGrupoId() {
		$this->db->select('*');
		$this->db->from('cache_cotacoes');
		$this->db->where('data_validade =', date('Y-m-d'));
		$this->db->where('destino_grupo_id =', $this->destino_grupo_id);
		$query = $this->db->get();
		return $query->row_object();
	}
}
