<?php

class Empresas_model extends MY_Model {

    protected $id;

    /**
     * Listar uma empresa pelo hash do id
     * @return mixed
     */
    public function listarUm() {
        $this->db->select('*');
        $this->db->from('empresas');
        $this->db->where('sha1(md5(id)) =', $this->id);
        $query = $this->db->get();
        return $query->row_object();
    }
}
