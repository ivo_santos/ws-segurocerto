<?php

class Destinos_model extends MY_Model {

    protected $id;
    protected $seguradora_destino_id;

    //Construtor
    function __construct() {
        parent::__construct();
    }
    
    /**
     * Busca o destino por id
     * @param int $this->id id do destino
     * @return object
     */
    public function listarUm() {
//        $this->db->select('d.*, dp.planos');
        $this->db->select('d.*');
        $this->db->from('destinos d');
//        $this->db->join('destinos_planos dp', 'dp.codigo_zona = d.codigo_zona', 'left');
        $this->db->where('d.id =', $this->id);
        $query = $this->db->get();
        return $query->row_object();
    }
    
    /**
     * Busca destinos disponíveis
     * @return object
     */
    public function listarTodos() {
//        $this->db->select('d.*, dp.planos');
        $this->db->select('d.*');
        $this->db->from('destinos d');
        //$this->db->join('destinos_planos dp', 'dp.codigo_zona = d.codigo_zona', 'left');
        $this->db->order_by("subdestino", "asc");
        $this->db->order_by("nome", "asc");
        $query = $this->db->get();
        return $query->result_object();
    }

    
    /**
     * Busca destino por código
     * @param int $this->seguradora_destino_id
     * @return object
     */
    public function listarUmPorSeguradoraDestinoId() {
        $this->db->select('d.*');
        $this->db->from('destinos d');
        $this->db->where('seguradora_destino_id =', $this->seguradora_destino_id);
        $query = $this->db->get();
        return $query->row_object();
    }

    
}
