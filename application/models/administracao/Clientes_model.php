<?php

class Clientes_model extends MY_Model {

    protected $id;
    protected $nome;
    protected $documento;
    protected $nascimento;
    protected $sexo;
    protected $venda_id;
    protected $limit;
    protected $offset;

    /**
     * Adiciona um cliente
     * @return int id inserido
     */
    public function adicionar() {
        $data = array(
            'nome' => $this->nome,
            'documento' => $this->documento,
            'nascimento' => $this->nascimento,
            'sexo' => $this->sexo,
            'venda_id' => $this->venda_id,
        );
        $this->db->insert('clientes', $data);
        return $this->db->insert_id();
    }

    /**
     * Alterar um cliente por hash do id
     * @return void
     */
    public function alterar() {
        $data = array(
            'nome' => $this->nome,
            'documento' => $this->documento,
            'nascimento' => $this->nascimento,
            'sexo' => $this->sexo,
        );
        $this->db->where('sha1(md5(id))', $this->id);
        return $this->db->update('clientes', $data);
    }

    /**
     * Listar o cliente por hash do id
     * @return object
     */
    public function listarUm() {
        return $this->db->select('*')->from('clientes')
                        ->where('sha1(md5(id)) =', $this->id)
                        ->get()->row_object();
    }

    /**
     * Listar todos clientes
     * @return object
     */
    public function listarTodos() {
        return $this->db->select('*')->from('clientes')
                        ->order_by("id", "desc")
                        ->limit($this->limit, $this->offset)
                        ->get()->result_object();
    }

    /**
     * Listar a quantidade de clientes
     * @return int
     */
    public function total() {
        return $this->db->select('count(*) as total')->from('clientes')
                        ->get()->row_object()->total;
    }

    /**
     * Remove o cliente por hash do id
     * @return void
     */
    public function remover() {
        return $this->db->delete('clientes', array('sha1(md5(id))' => $this->id));
    }

    /**
     * Listar clientes de pelo hash do venda_id
     * @return object
     */
    public function listarTodosPorVendaId() {
        return $this->db->select('*')->from('clientes')
                        ->where('sha1(md5(venda_id)) =', $this->venda_id)
                        ->order_by("id", "asc")
                        ->get()->result_object();
    }
    
    /**
     * Busca um cliente por documento
     * @return object
     */
    public function listarUmPorDocumento() {
        return $this->db->select('*')->from('clientes')
                ->where('documento =', $this->documento)
                ->order_by("nascimento", "desc")
                ->order_by("id", "desc")
                ->get()->row_object();
    }

}
