<?php

class Destinos_grupos_model extends MY_Model
{

	protected $id;
	protected $nome;
	protected $descricao;
	protected $destino_id;
	protected $motivo_id;
	protected $tipo_id;
	protected $data_cadastro;
	protected $data_modificacao;
	protected $ordem;
	protected $status;
	protected $limit;
	protected $offset;

	function hydrate($data)
	{
		$this->id = tratarDefault('id', $data);
		$this->nome = tratarDefault('nome', $data);
		$this->descricao = tratarDefault('descricao', $data);
		$this->destino_id = tratarDefault('destino_id', $data);
		$this->motivo_id = tratarDefault('motivo_id', $data);
		$this->tipo_id = tratarDefault('tipo_id', $data);
		$this->ordem = tratarDefault('ordem', $data, 10);
		$this->status = tratarDefault('status', $data);
		$this->data_cadastro = tratarDefault('data_cadastro', $data);
		$this->data_modificacao = tratarDefault('data_modificacao', $data);
		return $this;
	}

	public function adicionar()
	{
		$data = array(
			'nome' => $this->nome,
			'descricao' => $this->descricao,
			'destino_id' => $this->destino_id,
			'motivo_id' => $this->motivo_id,
			'tipo_id' => $this->tipo_id,
			'ordem' => $this->ordem,
			'status' => $this->status,
			'data_cadastro' => date('Y-m-d H:i:s'),
			'data_modificacao' => date('Y-m-d H:i:s'),
		);
		$this->db->insert('destinos_grupos', $data);
		return $this->db->insert_id();
	}

	public function alterar()
	{
		$data = array(
			'nome' => $this->nome,
			'descricao' => $this->descricao,
			'destino_id' => $this->destino_id,
			'motivo_id' => $this->motivo_id,
			'tipo_id' => $this->tipo_id,
			'ordem' => $this->ordem,
			'status' => $this->status,
			'data_cadastro' => $this->data_cadastro,
			'data_modificacao' => $this->data_modificacao,
		);
		$this->db->where('sha1(md5(id))', $this->id);
		return $this->db->update('destinos_grupos', $data);
	}

	public function listarUm()
	{
		return $this->db->select('*')->from('destinos_grupos')
			->where('sha1(md5(id)) =', $this->id)
			->get()->row_object();
	}

	public function listarTodos()
	{
		return $this->db->select('*')->from('destinos_grupos')
			->order_by("ordem", "asc")
			->limit($this->limit, $this->offset)
			->get()
			->result_object();
	}

	public function total()
	{
		return $this->db->select('count(*) as total')->from('destinos_grupos')
			->get()->row_object()->total;
	}

	public function remover()
	{
		return $this->db->delete('destinos_grupos', array('sha1(md5(id))' => $this->id));
	}



	/**
	 * Lista os grupos com o cache
	 * @return mixed
	 */
	public function listarUmComCache()
	{
		return $this->db->select('d.*, c.id as cache_id, c.cotacao, c.data_validade, c.data_atualizacao')
			->from('destinos_grupos d')
			->join('cache_cotacoes c', 'c.destino_grupo_id = d.id', 'left')
			->where('sha1(md5(id)) =', $this->id)
			->get()->row_object();
	}

	/**
	 * Lista os grupos com o cache
	 * @return mixed
	 */
	public function listarTodosComCache()
	{
		return $this->db->select('d.*, c.id as cache_id, c.cotacao, c.data_validade, c.data_atualizacao')
			->from('destinos_grupos d')
			->join('cache_cotacoes c', 'c.destino_grupo_id = d.id', 'left')
			->order_by("id", "desc")
			->get()->result_object();
	}

}
