<?php

class Motivos_viagem_model extends MY_Model {

    protected $id;
    protected $seguradora_motivo_id;

    /**
     * Busca o motivo por id
     * @return object
     */
    public function listarUm() {
        $this->db->select('m.*');
        $this->db->from('motivos_viagem m');
        $this->db->where('m.id =', $this->id);
        $query = $this->db->get();
        return $query->row_object();
    }
    
    /**
     * Busca motivos disponíveis
     * @return object
     */
    public function listarTodos() {
        $this->db->select('m.*');
        $this->db->from('motivos_viagem m');
        $query = $this->db->get();
        return $query->result_object();
    }

    /**
     * Busca motivo por código
     * @return object
     */
    public function listarUmPorSeguradoraMotivoId() {
        $this->db->select('m.*');
        $this->db->from('motivos_viagem m');
        $this->db->where('m.seguradora_motivo_id =', $this->seguradora_motivo_id);
        $query = $this->db->get();
        return $query->row_object();
    }
}
