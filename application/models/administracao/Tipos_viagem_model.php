<?php

class Tipos_viagem_model extends MY_Model {

    protected $id;
    protected $seguradora_tipo_id;

    /**
     * Busca o tipo por id
     * @return object
     */
    public function listarUm() {
        $this->db->select('t.*');
        $this->db->from('tipos_viagem t');
        $this->db->where('t.id =', $this->id);
        $query = $this->db->get();
        return $query->row_object();
    }
    
    /**
     * Busca tipos disponíveis
     * @return object
     */
    public function listarTodos() {
        $this->db->select('t.*');
        $this->db->from('tipos_viagem t');
        $query = $this->db->get();
        return $query->result_object();
    }

    /**
     * Busca tipo por código
     * @return object
     */
    public function listarUmPorSeguradoraTipoId() {
        $this->db->select('t.*');
        $this->db->from('tipos_viagem t');
        $this->db->where('t.seguradora_tipo_id =', $this->seguradora_tipo_id);
        $query = $this->db->get();
        return $query->row_object();
    }
}
