<?php

class Vouchers_model extends MY_Model {

    protected $id;
    protected $venda_id;
    protected $cliente_id;
    protected $seguradora_numero_apolice;
    protected $seguradora_numero_sorte;
    protected $seguradora_numero_compra;

    /**
     * Adicionar um voucher
     * @return int
     */
    public function adicionar() {
        $data = [
            'venda_id' => $this->venda_id,
            'cliente_id' => $this->cliente_id,
            'seguradora_numero_apolice' => $this->seguradora_numero_apolice,
            'seguradora_numero_sorte' => $this->seguradora_numero_sorte,
            'seguradora_numero_compra' => $this->seguradora_numero_compra,
            'data_cadastro' => date("Y-m-d H:i:s")
        ];
        $this->db->insert('vouchers', $data);
        return $this->db->insert_id();
    }

    /**
     * Listar um por hash do id
     * @return object
     */
    public function listarUm() {
        $this->db->select('*');
        $this->db->from('vouchers');
        $this->db->where('sha1(md5(id)) =', $this->id);
        $query = $this->db->get();
        return $query->row_object();
    }
    
    /**
     * Listar um voucher por hash do cliente_id
     * @return object
     */
    public function listarUmPorClienteId() {
        $this->db->select('*');
        $this->db->from('vouchers');
        $this->db->where('sha1(md5(cliente_id)) =', $this->cliente_id);
        $query = $this->db->get();
        return $query->row_object();
    }

    /**
     * Listar vouchers por hash do venda_id
     * @return object
     */
    public function listarTodosPorVendaId() {
        $this->db->select('*');
        $this->db->from('vouchers');
        $this->db->where('sha1(md5(venda_id)) =', $this->venda_id);
        $query = $this->db->get();
        return $query->result_object();
    }

}
