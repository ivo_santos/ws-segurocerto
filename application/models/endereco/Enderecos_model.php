<?php

class Enderecos_model extends MY_Model {

    protected $id;
    protected $venda_id;
    protected $cep;
    protected $logradouro;
    protected $numero;
    protected $complemento;
    protected $bairro;
    protected $cidade;
    protected $estado;
    protected $dados_correios;

    //Construtor
    function __construct() {
        parent::__construct();
    }

    function hydrate($data) {
        $this->id = tratarDefault('id', $data);
        $this->cep = tratarDefault('cep', $data);
        $this->logradouro = tratarDefault('logradouro', $data);
        $this->numero = tratarDefault('numero', $data);
        $this->complemento = tratarDefault('complemento', $data);
        $this->bairro = tratarDefault('bairro', $data);
        $this->cidade = tratarDefault('cidade', $data);
        $this->estado = tratarDefault('estado', $data);
        $this->venda_id = tratarDefault('venda_id', $data);
        return $this;
    }

    /**
     * Adicionar
     * @return int id
     */
    public function adicionar() {
        $data = array(
            "cep" => $this->cep,
            "logradouro" => $this->logradouro,
            "numero" => $this->numero,
            "complemento" => $this->complemento,
            "bairro" => $this->bairro,
            "cidade" => $this->cidade,
            "estado" => $this->estado,
            "venda_id" => $this->venda_id,
        );
        $this->db->insert('enderecos', $data);
        return $this->db->insert_id();
    }

    /**
     * Alterar
     * @return void
     */
    public function alterar() {
        $data = array(
            "cep" => $this->cep,
            "logradouro" => $this->logradouro,
            "numero" => $this->numero,
            "complemento" => $this->complemento,
            "bairro" => $this->bairro,
            "cidade" => $this->cidade,
            "estado" => $this->estado,
        );
        $this->db->where('id', $this->id);
        return $this->db->update('enderecos', $data);
    }

    /**
     * Listar um endereco pelo hash do venda_id
     * @return object
     */
    public function listarUmPorVendaId() {
        return $this->db->select('*')->from('enderecos')
                        ->where('sha1(md5(venda_id)) =', $this->venda_id)
                        ->get()->row_object();
    }
    
    
    /**
     * Adiciona cache correios
     * @return int
     */
    public function inserirCacheCorreios() {
        $data = array(
            "cep" => $this->cep,
            "dados_correios" => $this->dados_correios,
            "data_cadastro" => date('Y-m-d H:i:s')
        );
        $this->db->insert('cache_correios', $data);
        return $this->db->insert_id();
    }

    public function getCacheCorreios() {
        $this->db->select('*');
        $this->db->from('cache_correios');
        $this->db->where(array('cep' => $this->cep));
        $query = $this->db->get();
        return $query->row_object();
    }

}
