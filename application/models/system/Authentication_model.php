<?php

class Authentication_model extends MY_Model {

    protected $id;
    protected $user;
    protected $password;

    //Construtor
    function __construct() {
        parent::__construct();
    }

    public function getWSUser() {
        $query = $this->db->get_where('ws_users', array('user' => $this->user, 'password' => $this->password), 1, 0);
        return $query->row_object();
    }

}
