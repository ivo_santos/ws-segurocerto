<?php

class Log_emissao_model extends MY_Model {

    protected $id;
    protected $sent;
    protected $result;

    //Construtor
    function __construct() {
        parent::__construct();
    }

    public function salvar() {
        $data = array(
            'sent' => $this->sent,
            'result' => $this->result,
        );
        $this->db->insert('log_emissao', $data);
        return $this->db->insert_id();
    }

}
