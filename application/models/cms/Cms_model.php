<?php

class Cms_model extends MY_Model {

    protected $id;
    protected $titulo;
    protected $descricao;
    protected $h1;
    protected $imagem;
    protected $conteudo;
    protected $url_amigavel;
    protected $limit;
    protected $offset;

    /**
     * Adiciona uma página
     * 
     * @return int
     */
    public function adicionar() {
        $data = array(
            'titulo' => $this->titulo,
            'descricao' => $this->descricao,
            'h1' => $this->h1,
            'imagem' => $this->imagem,
            'conteudo' => $this->conteudo,
            'url_amigavel' => $this->url_amigavel,
            'data_cadastro' => date("Y-m-d H:i:s"),
            'data_modificacao' => date("Y-m-d H:i:s")
        );
        $this->db->insert('cms', $data);
        return $this->db->insert_id();
    }

    /**
     * Altera os dados da pagina pelo hash do id
     * 
     * @return void
     */
    public function alterar() {
        $data = array(
            'titulo' => $this->titulo,
            'descricao' => $this->descricao,
            'h1' => $this->h1,
			'imagem' => $this->imagem,
			'conteudo' => $this->conteudo,
            'url_amigavel' => $this->url_amigavel,
            'data_modificacao' => date("Y-m-d H:i:s")
        );
        $this->db->where('sha1(md5(id))', $this->id);
        return $this->db->update('cms', $data);
    }

    /**
     * Lista uma página pelo hash do id
     * 
     * @param string $this->id
     * @return object
     */
    public function listarUm() {
        $this->db->select('*');
        $this->db->from('cms');
        $this->db->where('sha1(md5(id)) =', $this->id);
        $query = $this->db->get();
        return $query->row_object();
    }

    /**
     * Busca páginas de acordo com os filtros
     * 
     * @return object
     */
    public function listarTodos() {
        $this->db->select('*');
        $this->db->from('cms');
        $this->db->order_by("id", "desc");
        $this->db->limit($this->limit, $this->offset);
        $query = $this->db->get();
        return $query->result_object();
    }

    /**
     * Busca a quantidade de páginas
     * 
     * @return int
     */
    public function total() {
        $this->db->select('count(*) as total');
        $this->db->from('cms');
        $query = $this->db->get();
        return $query->row_object()->total;
    }

    /**
     * Deleta uma página de acordo com o hash do id
     * 
     * @param string $this->id
     * @return void
     */
    public function remover() {
        return $this->db->delete('cms', array('sha1(md5(id))' => $this->id));
    }

    /**
     * Lista uma página por url amigável
     * @return object
     */
    public function listarUmPorUrl() {
        $this->db->select('*');
        $this->db->from('cms');
        $this->db->where('url_amigavel =', $this->url_amigavel);
        $query = $this->db->get();
        return $query->row_object();
    }

}
