<?php

class Emails_model extends MY_Model {

    protected $id;
    protected $titulo;
    protected $assunto;
    protected $conteudo;
	protected $tipo;
	protected $limit;
    protected $offset;

    /**
     * Adicionar
     * @return int
     */
    public function adicionar() {
        $data = array(
            'titulo' => $this->titulo,
            'assunto' => $this->assunto,
            'conteudo' => $this->conteudo,
            'tipo' => $this->tipo,
            'data_cadastro' => date("Y-m-d H:i:s"),
            'data_modificacao' => date("Y-m-d H:i:s")
        );
        $this->db->insert('emails', $data);
        return $this->db->insert_id();
    }

    /**
     * Altera os dados da pagina pelo hash do id
     * @return void
     */
    public function alterar() {
        $data = array(
			'titulo' => $this->titulo,
			'assunto' => $this->assunto,
            'conteudo' => $this->conteudo,
			'tipo' => $this->tipo,
			'data_modificacao' => date("Y-m-d H:i:s")
        );
        $this->db->where('sha1(md5(id))', $this->id);
        return $this->db->update('emails', $data);
    }

    /**
     * Lista uma página pelo hash do id
     * 
     * @param string $this->id
     * @return object
     */
    public function listarUm() {
        $this->db->select('*');
        $this->db->from('emails');
        $this->db->where('sha1(md5(id)) =', $this->id);
        $query = $this->db->get();
        return $query->row_object();
    }

    /**
     * Busca páginas de acordo com os filtros
     * 
     * @return object
     */
    public function listarTodos() {
        $this->db->select('*');
        $this->db->from('emails');

		if(!empty($this->tipo)){
			$this->db->where('tipo =', $this->tipo);
		}

        $this->db->order_by("id", "desc");
        $this->db->limit($this->limit, $this->offset);
        $query = $this->db->get();
        return $query->result_object();
    }

    /**
     * Busca a quantidade de páginas
     * 
     * @return int
     */
    public function total() {
        $this->db->select('count(*) as total');
        $this->db->from('emails');

		if(!empty($this->tipo)){
			$this->db->where('tipo =', $this->tipo);
		}

        $query = $this->db->get();
        return $query->row_object()->total;
    }

    /**
     * Deleta uma página de acordo com o hash do id
     * @param string $this->id
     * @return void
     */
    public function remover() {
        return $this->db->delete('emails', array('sha1(md5(id))' => $this->id));
    }
}
