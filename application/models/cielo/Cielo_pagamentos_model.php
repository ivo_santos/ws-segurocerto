<?php

class Cielo_pagamentos_model extends MY_Model {

    public function __construct() {
        parent::__construct();
    }

    public function registrar(array $pagamento) {
        try {
            $this->db->insert(
                    'xmls_cielo', $pagamento
            );

            return true;
        } catch (Exception $err) {
            return false;
        }
    }

}
