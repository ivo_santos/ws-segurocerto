<?php

class Log_cielo_model extends MY_Model {

    public function __construct() {
        parent::__construct();
    }

    public function registrar(array $log) {
        $hoje = new DateTime();

        $log = array_merge($log, array(
            'data_log' => $hoje->format('Y-m-d H:i:s')
        ));

        $this->db->insert('log_cielo', $log);
        return $this->db->insert_id();
    }

}
