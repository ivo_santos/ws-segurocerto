<?php

class Bracos_model extends MY_Model {

    protected $id;
    protected $nome;
    protected $descricao;
    protected $imagem;
    protected $limit;
    protected $offset;

    public function adicionar() {
        $data = array(
            'nome' => $this->nome,
            'descricao' => $this->descricao,
            'imagem' => $this->imagem,
            'data_cadastro' => date("Y-m-d H:i:s"),
            'data_modificacao' => date("Y-m-d H:i:s")
        );
        $this->db->insert('bracos', $data);
        return $this->db->insert_id();
    }

    public function alterar() {
        $data = array(
            'nome' => $this->nome,
            'descricao' => $this->descricao,
            'imagem' => $this->imagem,
			'data_modificacao' => date("Y-m-d H:i:s")
		);
        $this->db->where('sha1(md5(id))', $this->id);
        return $this->db->update('bracos', $data);
    }

    public function listarUm() {
        $this->db->select('*');
        $this->db->from('bracos');
        $this->db->where('sha1(md5(id)) =', $this->id);
        $query = $this->db->get();
        return $query->row_object();
    }

    public function listarTodos() {
        $this->db->select('b.*');
        $this->db->from('bracos b');
        $this->db->order_by("b.id", "desc");
        $this->db->limit($this->limit, $this->offset);
        $query = $this->db->get();
        return $query->result_object();
    }

    public function total() {
        $this->db->select('count(*) as total');
        $this->db->from('bracos');
        $query = $this->db->get();
        return $query->row_object()->total;
    }

    public function remover() {
        return $this->db->delete('bracos', array('sha1(md5(id))' => $this->id));
    }

}
