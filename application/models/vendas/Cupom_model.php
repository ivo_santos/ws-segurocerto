<?php

class Cupom_model extends MY_Model {

    protected $id;
    protected $nome;
    protected $codigo;
    protected $desconto;
    protected $status;
    protected $funcionario_id;
    protected $data_inicio;
    protected $data_final;
    protected $quantidade;
    protected $quantidade_usado;
    protected $limit;
    protected $offset;

    //Construtor
    function __construct() {
        parent::__construct();
    }

    /**
     * add a coupon
     * @return mixed
     */
    public function adicionar() {
        $data = array(
            'nome' => $this->nome,
            'codigo' => $this->codigo,
            'data_inicio' => $this->data_inicio,
            'data_final' => $this->data_final,
            'quantidade' => $this->quantidade,
            'desconto' => $this->desconto,
            'status' => $this->status,
            'funcionario_id' => $this->funcionario_id,
            'data_cadastro' => date("Y-m-d H:i:s")
        );
        $this->db->insert('cupons', $data);
        return $this->db->insert_id();
    }

    /**
     * update a coupon by hash id
     * @return mixed
     */
    public function alterar() {
        $data = array(
            'nome' => $this->nome,
            'codigo' => $this->codigo,
            'data_inicio' => $this->data_inicio,
            'data_final' => $this->data_final,
            'quantidade' => $this->quantidade,
            'desconto' => $this->desconto,
            'status' => $this->status,
        );
        $this->db->where('sha1(md5(id))', $this->id);
        return $this->db->update('cupons', $data);
    }

    /**
     * find one by hash id
     * @return mixed
     */
    public function listarUm() {
        $this->db->select('*');
        $this->db->from('cupons');
        $this->db->where('sha1(md5(id)) =', $this->id);
        $query = $this->db->get();
        return $query->row_object();
    }

    /**
     * list all with filter for limit and offset
     * @return mixed
     */
    public function listarTodos() {
        $this->db->select('*');
        $this->db->from('cupons');
        $this->db->order_by("id", "desc");
        $this->db->limit($this->limit, $this->offset);
        $query = $this->db->get();
        return $query->result_object();
    }

    /**
     * give quantidade of register on table
     * @return mixed
     */
    public function total() {
        $this->db->select('count(*) as total');
        $this->db->from('cupons');
        $query = $this->db->get();
        return $query->row_object()->total;
    }

    /**
     * remove a coupon by hash id
     * @return mixed
     */
    public function remover() {
        return $this->db->delete('cupons', array('sha1(md5(id))' => $this->id));
    }

    /**
     * find one coupon by codigo and valid between start date and end date.
     * @return mixed
     */
    public function listarUmPorCodigo() {
        $this->db->select('*');
        $this->db->from('cupons');
        $this->db->where('codigo', $this->codigo);
        $this->db->where('data_inicio <=', date('Y-m-d') . ' 00:00:00');
        $this->db->where('data_final >=', date('Y-m-d') . ' 23:59:59');
        $this->db->where('quantidade >', 'quantidade_usado');
        $this->db->where('status', 1);
        $query = $this->db->get();
        return $query->row_object();
    }

    /**
     * update quantidade used
     * @return mixed
     */
    public function atualizarQuantidadeUsado() {
        $data = [
            'quantidade_usado' => $this->quantidade_usado,
        ];
        $this->db->where('sha1(md5(id))', $this->id);
        return $this->db->update('cupons', $data);
    }

}
