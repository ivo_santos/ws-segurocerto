<?php

class Vendas_model extends MY_Model
{

	protected $id;
	protected $nome;
	protected $documento;
	protected $nascimento;
	protected $telefone;
	protected $email;
	protected $cotacao_id;
	protected $plano_id;
	protected $coberturas_info;
	protected $servicos_info;
	protected $tipo;
	protected $valor;
	protected $valor_idoso;
	protected $valor_desconto;
	protected $cupom_id;
	protected $forma_pagamento_id;
	protected $tid;
	protected $bandeira;
	protected $parcelas;
	protected $salt;
	protected $status;
	protected $aprovacao_manual;
	protected $funcionario_id;
	protected $comentario;
	protected $data_pagamento;
	protected $ip;
	protected $braco_id;
	protected $viu_voucher_em;
	protected $data_cadastro;
	protected $data_modificacao;

	protected $cotacoes_id;
	protected $limit;
	protected $offset;

	function hydrate($data)
	{
		$this->id = tratarDefault('id', $data);
		$this->nome = tratarDefault('nome', $data);
		$this->documento = tratarDefault('documento', $data);
		$this->nascimento = tratarDefault('nascimento', $data);
		$this->telefone = tratarDefault('telefone', $data);
		$this->email = tratarDefault('email', $data);
		$this->cotacao_id = tratarDefault('cotacao_id', $data);
		$this->plano_id = tratarDefault('plano_id', $data);
		$this->coberturas_info = tratarDefault('coberturas_info', $data);
		$this->servicos_info = tratarDefault('servicos_info', $data);
		$this->tipo = tratarDefault('tipo', $data);
		$this->valor = tratarDefault('valor', $data);
		$this->valor_idoso = tratarDefault('valor_idoso', $data);
		$this->valor_total = tratarDefault('valor_total', $data);
		$this->valor_desconto = tratarDefault('valor_desconto', $data);
		$this->cupom_id = tratarDefault('cupom_id', $data);
		$this->forma_pagamento_id = tratarDefault('forma_pagamento_id', $data);
		$this->tid = tratarDefault('tid', $data);
		$this->bandeira = tratarDefault('bandeira', $data);
		$this->parcelas = tratarDefault('parcelas', $data);
		$this->salt = tratarDefault('salt', $data);
		$this->status = tratarDefault('status', $data);
		$this->aprovacao_manual = tratarDefault('aprovacao_manual', $data);
		$this->funcionario_id = tratarDefault('funcionario_id', $data);
		$this->comentario = tratarDefault('comentario', $data);
		$this->data_pagamento = tratarDefault('data_pagamento', $data);
		$this->ip = tratarDefault('ip', $data);
		$this->braco_id = tratarDefault('braco_id', $data, null);
		$this->viu_voucher_em = tratarDefault('viu_voucher_em', $data);
		$this->data_cadastro = tratarDefault('data_cadastro', $data);
		$this->data_modificacao = tratarDefault('data_modificacao', $data);
		return $this;
	}

	/**
	 * Adiciona uma venda com o tipo = 2 pré-venda
	 * @return int id inserido
	 */
	public function adicionar()
	{
		$data = array(
			'nome' => $this->nome,
			'email' => $this->email,
			'telefone' => $this->telefone,
			'cotacao_id' => $this->cotacao_id,
			'plano_id' => $this->plano_id,
			'coberturas_info' => $this->coberturas_info,
			'servicos_info' => $this->servicos_info,
			'tipo' => $this->tipo,
			'valor' => $this->valor,
			'valor_idoso' => $this->valor_idoso,
			'salt' => $this->salt,
			'ip' => $this->ip,
			'braco_id' => $this->braco_id,
			'data_cadastro' => $this->data_cadastro,
			'data_modificacao' => $this->data_modificacao,
		);
		$this->db->insert('vendas', $data);
		return $this->db->insert_id();
	}

	/**
	 * Altera o tipo da venda
	 * @return void
	 */
	public function alterar()
	{
		$data = array(
			'nome' => $this->nome,
			'documento' => $this->documento,
			'nascimento' => $this->nascimento,
			'telefone' => $this->telefone,
			'email' => $this->email,
			'cotacao_id' => $this->cotacao_id,
			'plano_id' => $this->plano_id,
			'coberturas_info' => $this->coberturas_info,
			'servicos_info' => $this->servicos_info,
			'tipo' => $this->tipo,
			'valor' => $this->valor,
			'valor_idoso' => $this->valor_idoso,
			'valor_total' => $this->valor_total,
			'valor_desconto' => $this->valor_desconto,
			'salt' => $this->salt,
			'cupom_id' => $this->cupom_id,
			'forma_pagamento_id' => $this->forma_pagamento_id,
			'tid' => $this->tid,
			'bandeira' => $this->bandeira,
			'parcelas' => $this->parcelas,
			'funcionario_id' => $this->funcionario_id,
			'status' => $this->status,
			'aprovacao_manual' => $this->aprovacao_manual,
			'data_pagamento' => $this->data_pagamento,
			'comentario' => $this->comentario,
			'ip' => $this->ip,
			'braco_id' => $this->braco_id,
			'data_modificacao' => $this->data_modificacao,
			'viu_voucher_em' => $this->viu_voucher_em,
		);
		$this->db->where('sha1(md5(id)) =', $this->id);
		return $this->db->update('vendas', $data);
	}

	/**
	 * Busca venda por hash do id da venda
	 *
	 * @param string $this ->id
	 * @return object
	 */
	public function listarUm()
	{
		$this->db->select('*');
		$this->db->from('vendas');
		$this->db->where('sha1(md5(id)) =', $this->id);
		$query = $this->db->get();
		return $query->row_object();
	}

	/**
	 * Busca venda por hash do cotacao_id
	 *
	 * @param string $this ->cotacao_id
	 * @return object
	 */
	public function listarUmPorCotacaoId()
	{
		$this->db->select('*');
		$this->db->from('vendas');
		$this->db->where('sha1(md5(cotacao_id)) =', $this->cotacao_id);
		$query = $this->db->get();
		return $query->row_object();
	}

	public function listarTodos()
	{
		$this->db->select('v.*, cp.nome as cupom, c.destino_grupo_id, c.destino_id, c.data_inicio, c.data_final, c.num_passageiros, c.num_passageiros_maiores, c.motivo_id, c.tipo_id, c.device,  c.origem,  c.parametros_url, totalHistoricos(c.id) as total_historicos, historicoAgendadoHoje(c.id) as historico_agendado_hoje, historicoAgendadoAtrasado(c.id) as historico_agendado_atrasado, d.nome as destino,  dg.nome as destino_grupo, fp.forma_pagamento, p.nome as plano, b.imagem as braco_imagem, b.nome as braco_nome');
		$this->db->from('vendas v');
		$this->db->join('cotacoes c', 'c.id = v.cotacao_id');
		$this->db->join('cupons cp', 'cp.id = v.cupom_id', 'left');
		$this->db->join('destinos_grupos dg', 'dg.id = c.destino_grupo_id', 'left');
		$this->db->join('destinos d', 'd.id = c.destino_id', 'left');
		$this->db->join('planos p', 'p.id = v.plano_id', 'left');
		$this->db->join('bracos b', 'b.id = v.braco_id', 'left');
		$this->db->join('formas_pagamento fp', 'fp.id = v.forma_pagamento_id', 'left');

		if (!empty($this->data_inicio) && !empty($this->data_final)) {
			$this->db->where('v.data_cadastro >=', $this->data_inicio);
			$this->db->where('v.data_cadastro <=', $this->data_final);
		}

		if (!empty($this->id)) {
			$this->db->where('v.id =', $this->id);
		}

		if (!empty($this->documento)) {
			$this->db->like('v.documento', $this->documento);
		}

		if (!empty($this->nome)) {
			$this->db->like('v.nome', $this->nome);
		}

		if (!empty($this->email)) {
			$this->db->like('c.email', $this->email);
		}

		if (!empty($this->status)) {
			$this->db->where('v.status =', $this->status);
		}

		$this->db->where_in('cotacao_id', $this->cotacoes_id);
		$this->db->order_by("v.id", "desc");
		$this->db->limit($this->limit, $this->offset);
		$query = $this->db->get();

//		echo $this->db->last_query();
//		die;

		return $query->result_object();
	}

	public function total()
	{
		$this->db->select('count(*) as total');
		$this->db->from('vendas v');
		$this->db->join('cotacoes c', 'c.id = v.cotacao_id');

		if (!empty($this->data_inicio) && !empty($this->data_final)) {
			$this->db->where('v.data_cadastro >=', $this->data_inicio);
			$this->db->where('v.data_cadastro <=', $this->data_final);
		}

		if (!empty($this->id)) {
			$this->db->where('v.id =', $this->id);
		}

		if (!empty($this->documento)) {
			$this->db->where('v.documento =', $this->documento);
		}

		if (!empty($this->nome)) {
			$this->db->like('v.nome', $this->nome);
		}

		if (!empty($this->email)) {
			$this->db->like('c.email', $this->email);
		}

		if (!empty($this->status)) {
			$this->db->where('v.status =', $this->status);
		}

		$this->db->where_in('cotacao_id', $this->cotacoes_id);
		$query = $this->db->get();
		return $query->row_object()->total;
	}


	/**
	 * Busca a venda pelas credenciais
	 * @return object
	 */
	public function listarUmPorCredenciais()
	{
		$this->db->select('v.*, c.destino_id, c.data_inicio, c.data_final, c.num_passageiros,  c.num_passageiros_maiores, c.tipo_id,  c.motivo_id, c.device,  c.origem,  c.parametros_url, totalHistoricos(c.id) as total_historicos');
		$this->db->from('vendas v');
		$this->db->join('cotacoes c', 'c.id = v.cotacao_id');
		$this->db->where('v.email =', $this->email);
		$this->db->where('sha1(md5(v.salt)) =', $this->salt);
		$this->db->where('v.tipo =', $this->tipo);
		$query = $this->db->get();
		return $query->row_object();
	}


	/**
	 * TODO REMOVER ESSA FUNÇÃO QUANDO IMPLEMENTADO O CACHE PELO DESTINOS GRUPOS
	 * @return mixed
	 */
	public function listarUmParaCancelar()
	{
		$this->db->select('v.*');
		$this->db->from('vendas v');
		$this->db->where_in('status', [2, 4]);
		$this->db->where('tipo', 1);
		$this->db->where('DATE_ADD(v.data_cadastro, INTERVAL 1 DAY) <=', date('Y-m-d'));
		$query = $this->db->get();
		return $query->row_object();
	}

}
