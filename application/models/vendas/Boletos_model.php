<?php

class Boletos_model extends MY_Model {

    protected $id;
    protected $venda_id;
    protected $nome;
    protected $email;
    protected $campo_sacado;
    protected $descricao;
    protected $vencimento;
    protected $valor;
    protected $status; //1= pago 2 = aguardando pagamento 3=cancelado
    protected $data_pagamento;
    protected $funcionario_id;

    /**
     * Adiciona um boleto para venda
     * 
     * @return int id do boleto inserido
     */
    public function adicionar() {
        $data = array(
            'venda_id' => $this->venda_id,
            'cliente_nome' => $this->nome,
            'cliente_email' => $this->email,
            'campo_livre_sacado' => $this->campo_sacado,
            'descricao' => $this->descricao,
            'data_vencimento' => $this->vencimento,
            'valor' => $this->valor,
            'status' => $this->status,
            'data_cadastro' => date("Y-m-d H:i:s")
        );
        $this->db->insert('vendas_boletos', $data);
        return $this->db->insert_id();
    }

    /**
     * Busca um boleto por hash do id
     * @return object
     */
    public function listarUm() {
        $this->db->select('*');
        $this->db->from('vendas_boletos');
        $this->db->where('sha1(md5(id)) =', $this->id);
        $query = $this->db->get();
        return $query->row_object();
    }
    
    /**
     * Busca ultimo boleto de uma venda por id da venda
     * @return object
     */
    public function listarUltimoPorVendaId() {
        $this->db->select('*');
        $this->db->from('vendas_boletos');
        $this->db->where('sha1(md5(venda_id)) =', $this->venda_id);
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->row_object();
    }
    
    
    /**
     * Aprova status do boleto por hash do id do boleto
     * @return object
     */
    public function aprovar() {
        $data = array(
            'funcionario_id' => $this->funcionario_id,
            'status' => $this->status,
            'data_pagamento' => $this->data_pagamento
        );
        $this->db->where('sha1(md5(id))', $this->id);
        return $this->db->update('vendas_boletos', $data);
    }

    /**
     * Cancela todos boletos de uma venda por hash da venda_id
     * @return object
     */
    public function cancelarTodosPorVendaId() {
        $data = [
            'status' => 3
        ];
        $this->db->where('sha1(md5(venda_id)) =', $this->venda_id);
        return $this->db->update('vendas_boletos', $data);
    }
}
