<?php

class Metas_mensais_model extends MY_Model {

    protected $id;
    protected $mes;
    protected $valor;
    protected $ano;
    protected $limit;
    protected $offset;

    /**
     * Adicionar um pelo hash do id
     */
    public function adicionar() {
        $data = array(
            'mes' => $this->mes,
            'ano' => $this->ano,
            'valor' => $this->valor,
            'data_cadastro' => date("Y-m-d H:i:s")
        );
        $this->db->insert('metas_mensais', $data);
        return $this->db->insert_id();
    }

    /**
     * Alterar um pelo hash do id
     */
    public function alterar() {
        $data = array(
            'mes' => $this->mes,
            'ano' => $this->ano,
            'valor' => $this->valor
        );
        $this->db->where('sha1(md5(id))', $this->id);
        return $this->db->update('metas_mensais', $data);
    }

    /**
     * Listar um pelo hash do id
     * @return mixed
     */
    public function listarUm() {
        $this->db->select('*');
        $this->db->from('metas_mensais');
        $this->db->where('sha1(md5(id)) =', $this->id);
        $query = $this->db->get();
        return $query->row_object();
    }

    /**
     * Listar todos
     */
    public function listarTodos() {
        $this->db->select('mm.*, m.nome as nome_mes');
        $this->db->from('metas_mensais mm');
        $this->db->join('meses m', 'm.id = mm.mes');
        $this->db->order_by("mm.id", "desc");
        $this->db->limit($this->limit, $this->offset);
        $query = $this->db->get();
        return $query->result_object();
    }

    /**
     * Total de registros
     * @return mixed
     */
    public function total() {
        $this->db->select('count(*) as total');
        $this->db->from('metas_mensais');
        $query = $this->db->get();
        return $query->row_object()->total;
    }

    /**
     * Remover um pelo hash do id
     * @param $id
     */
    public function remover() {
        return $this->db->delete('metas_mensais', array('sha1(md5(id))' => $this->id));
    }

}
