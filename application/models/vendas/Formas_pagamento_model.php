<?php

class Formas_pagamento_model extends MY_Model {

    protected $id;
    protected $forma_pagamento;

    //Construtor
    function __construct() {
        parent::__construct();
    }

    /**
     * Busca forma pagamento por id
     * 
     * @param int $this->id
     * @return object
     */
    public function listarUm() {
        $this->db->select('*');
        $this->db->from('formas_pagamento');
        $this->db->where('id =', $this->id);
        $query = $this->db->get();
        return $query->row_object();
    }
}
