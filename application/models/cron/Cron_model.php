<?php

class Cron_model extends MY_Model
{

	protected $id;
	protected $venda_id;
	protected $cliente_id;
	protected $status;
	protected $comentario = '';
	protected $limit;
	protected $offset;

	/**
	 * Listar todos
	 * @return object
	 */
	public function listarTodos() {
		return $this->db->select('*')->from('vendas_cron')
			->order_by("id", "desc")
			->limit($this->limit, $this->offset)
			->get()->result_object();
	}

	/**
	 * Listar a quantidade de clientes
	 * @return int
	 */
	public function total() {
		return $this->db->select('count(*) as total')->from('vendas_cron')->get()->row_object()->total;
	}


	public function adicionar()
	{
		$data = array(
			'venda_id' => $this->venda_id,
			'status' => $this->status,
			'data_cadastro' => date('Y-m-d H:i:s'),
			'data_modificacao' => date('Y-m-d H:i:s'),
			'comentario' => $this->comentario
		);
		$this->db->insert('vendas_cron', $data);
		return $this->db->insert_id();
	}

	public function listarUm()
	{
		$this->db->select('*');
		$this->db->from('vendas_cron');
		$this->db->where('status =', $this->status);
		$this->db->order_by("id", "asc");
		$query = $this->db->get();
		return $query->row_object();
	}

	public function alterar()
	{
		$data = array(
			'status' => $this->status,
			'data_modificacao' => date('Y-m-d H:i:s'),
			'comentario' => $this->comentario
		);
		$this->db->where('venda_id', $this->venda_id);
		return $this->db->update('vendas_cron', $data);
	}

	public function removerPorVendaId()
	{
		return $this->db->delete('vendas_cron', array('sha1(md5(venda_id))' => $this->venda_id));
	}

}
