/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.7.16-0ubuntu0.16.10.1 : Database - ws_segurocerto
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`ws_segurocerto` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `ws_segurocerto`;

/*Table structure for table `cache_correios` */

DROP TABLE IF EXISTS `cache_correios`;

CREATE TABLE `cache_correios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cep` varchar(10) DEFAULT NULL,
  `dados_correios` varchar(255) DEFAULT NULL,
  `data_cadastro` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `cache_correios` */

insert  into `cache_correios`(`id`,`cep`,`dados_correios`,`data_cadastro`) values (4,'05422030','O:8:\"stdClass\":5:{s:3:\"rua\";s:25:\"Rua Cl&aacute;udio Soares\";s:6:\"bairro\";s:9:\"Pinheiros\";s:6:\"cidade\";s:16:\"S&atilde;o Paulo\";s:6:\"estado\";s:2:\"SP\";s:3:\"cep\";s:9:\"05422-030\";}','2016-03-16 15:14:49'),(3,'08452435','O:8:\"stdClass\":5:{s:3:\"rua\";s:40:\"Travessa Cl&aacute;udio Sanchez Albornoz\";s:6:\"bairro\";s:14:\"Jardim Lourdes\";s:6:\"cidade\";s:16:\"S&atilde;o Paulo\";s:6:\"estado\";s:2:\"SP\";s:3:\"cep\";s:9:\"08452-435\";}','2016-03-16 15:12:52'),(5,'08141000','O:8:\"stdClass\":5:{s:3:\"rua\";s:34:\"Rua Manuel &Aacute;lvares Pimentel\";s:6:\"bairro\";s:13:\"Jardim Miriam\";s:6:\"cidade\";s:16:\"S&atilde;o Paulo\";s:6:\"estado\";s:2:\"SP\";s:3:\"cep\";s:9:\"08141-000\";}','2016-03-17 13:38:21'),(6,'08140060','O:8:\"stdClass\":5:{s:3:\"rua\";s:41:\"Rua Ant&ocirc;nio Jo&atilde;o de Medeiros\";s:6:\"bairro\";s:14:\"Itaim Paulista\";s:6:\"cidade\";s:16:\"S&atilde;o Paulo\";s:6:\"estado\";s:2:\"SP\";s:3:\"cep\";s:9:\"08140-060\";}','2016-03-17 13:38:34'),(7,'08738510','O:8:\"stdClass\":5:{s:3:\"rua\";s:49:\"Rua Professor Jo&atilde;o Gualberto Mafra Machado\";s:6:\"bairro\";s:15:\"Vila Brasileira\";s:6:\"cidade\";s:15:\"Mogi das Cruzes\";s:6:\"estado\";s:2:\"SP\";s:3:\"cep\";s:9:\"08738-510\";}','2016-03-17 13:39:04'),(8,'01311300','O:8:\"stdClass\":5:{s:3:\"rua\";s:53:\"Avenida Paulista - de 1867 ao fim - lado &iacute;mpar\";s:6:\"bairro\";s:10:\"Bela Vista\";s:6:\"cidade\";s:16:\"S&atilde;o Paulo\";s:6:\"estado\";s:2:\"SP\";s:3:\"cep\";s:9:\"01311-300\";}','2016-03-17 13:40:03'),(9,'01311000','O:8:\"stdClass\":5:{s:3:\"rua\";s:53:\"Avenida Paulista - at&eacute; 609 - lado &iacute;mpar\";s:6:\"bairro\";s:10:\"Bela Vista\";s:6:\"cidade\";s:16:\"S&atilde;o Paulo\";s:6:\"estado\";s:2:\"SP\";s:3:\"cep\";s:9:\"01311-000\";}','2016-03-17 13:40:13'),(10,'01405000','O:8:\"stdClass\":5:{s:3:\"rua\";s:40:\"Rua Pamplona - at&eacute; 598 - lado par\";s:6:\"bairro\";s:15:\"Jardim Paulista\";s:6:\"cidade\";s:16:\"S&atilde;o Paulo\";s:6:\"estado\";s:2:\"SP\";s:3:\"cep\";s:9:\"01405-000\";}','2016-03-17 13:40:48'),(11,'20011030','O:8:\"stdClass\":5:{s:3:\"rua\";s:34:\"Rua da Quitanda - at&eacute; 77/78\";s:6:\"bairro\";s:6:\"Centro\";s:6:\"cidade\";s:14:\"Rio de Janeiro\";s:6:\"estado\";s:2:\"RJ\";s:3:\"cep\";s:9:\"20011-030\";}','2016-03-18 02:44:59'),(12,'30110013','O:8:\"stdClass\":5:{s:3:\"rua\";s:56:\"Avenida do Contorno - de 2563 a 3071 - lado &iacute;mpar\";s:6:\"bairro\";s:20:\"Santa Efig&ecirc;nia\";s:6:\"cidade\";s:14:\"Belo Horizonte\";s:6:\"estado\";s:2:\"MG\";s:3:\"cep\";s:9:\"30110-013\";}','2016-03-18 02:45:20'),(13,'01122030','O:8:\"stdClass\":5:{s:3:\"rua\";s:16:\"Rua Carmo Cintra\";s:6:\"bairro\";s:10:\"Bom Retiro\";s:6:\"cidade\";s:16:\"S&atilde;o Paulo\";s:6:\"estado\";s:2:\"SP\";s:3:\"cep\";s:9:\"01122-030\";}','2016-03-20 15:54:00'),(14,'05315030','O:8:\"stdClass\":5:{s:3:\"rua\";s:9:\"Rua Nagel\";s:6:\"bairro\";s:15:\"Vila Leopoldina\";s:6:\"cidade\";s:16:\"S&atilde;o Paulo\";s:6:\"estado\";s:2:\"SP\";s:3:\"cep\";s:9:\"05315-030\";}','2016-06-14 11:01:40');

/*Table structure for table `cache_precos` */

DROP TABLE IF EXISTS `cache_precos`;

CREATE TABLE `cache_precos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dias` int(11) DEFAULT NULL,
  `valor` decimal(15,7) DEFAULT NULL,
  `valor_idoso` decimal(15,7) DEFAULT NULL,
  `plano_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `plano_id` (`plano_id`),
  CONSTRAINT `cache_precos_ibfk_1` FOREIGN KEY (`plano_id`) REFERENCES `planos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cache_precos` */

/*Table structure for table `cambio` */

DROP TABLE IF EXISTS `cambio`;

CREATE TABLE `cambio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `valor` float DEFAULT NULL,
  `tipo` varchar(10) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `data_cadastro` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

/*Data for the table `cambio` */

insert  into `cambio`(`id`,`valor`,`tipo`,`data`,`data_cadastro`) values (14,3.348,'dolar','2016-11-22','2016-11-22 13:50:24'),(15,3.557,'euro','2016-11-22','2016-11-22 13:50:32'),(16,3.557,'euro','2016-11-22','2016-11-22 17:17:40'),(17,3.393,'dolar','2016-11-24','2016-11-24 10:56:33'),(18,3.582,'euro','2016-11-24','2016-11-24 10:56:37'),(19,3.428,'dolar','2016-11-25','2016-11-25 17:33:21'),(20,3.428,'dolar','2016-11-25','2016-11-25 17:44:36'),(21,3.637,'euro','2016-11-25','2016-11-25 18:02:35'),(22,3.428,'dolar','2016-11-28','2016-11-28 14:26:01'),(23,3.637,'euro','2016-11-28','2016-11-28 14:26:08'),(24,3.599,'euro','2016-11-29','2016-11-29 12:30:59'),(25,3.4,'dolar','2016-11-29','2016-11-29 12:31:13'),(26,3.406,'dolar','2016-11-29','2016-11-29 17:24:26'),(27,3.611,'euro','2016-11-29','2016-11-29 17:24:47'),(28,3.611,'euro','2016-11-29','2016-11-29 17:25:05'),(29,3.611,'euro','2016-11-29','2016-11-29 17:26:09'),(30,3.406,'dolar','2016-11-29','2016-11-29 17:26:44'),(31,3.406,'dolar','2016-11-29','2016-11-29 17:37:27'),(32,3.397,'dolar','2016-11-30','2016-11-30 13:19:00'),(33,3.6,'euro','2016-11-30','2016-11-30 13:19:17'),(34,3.6,'euro','2016-11-30','2016-11-30 15:44:47'),(35,3.397,'dolar','2016-11-30','2016-11-30 15:45:04');

/*Table structure for table `clientes` */

DROP TABLE IF EXISTS `clientes`;

CREATE TABLE `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `documento` varchar(30) DEFAULT NULL,
  `nascimento` date DEFAULT NULL,
  `sexo` int(1) DEFAULT '1' COMMENT '1=masculino  2=feminino',
  `venda_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `venda_id` (`venda_id`),
  CONSTRAINT `clientes_ibfk_1` FOREIGN KEY (`venda_id`) REFERENCES `vendas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `clientes` */

insert  into `clientes`(`id`,`nome`,`documento`,`nascimento`,`sexo`,`venda_id`) values (1,'Ivo Santos','390.814.758-19','1990-07-29',1,3),(5,'lucicleide balbino dos santos','249.937.158-70','1972-12-12',2,3),(6,'Ivo Santos','390.814.758-19','1990-07-29',1,4),(7,'lucicleide balbino dos santos','249.937.158-70','1972-12-12',2,4),(8,'Fulano de tal','817.226.276-03','1945-12-12',1,4),(9,'Ivo Santos','390.814.758-19','1990-07-29',1,5),(10,'Ivo Santos','390.814.758-19','1990-07-29',1,6),(11,'Ivo Santos','390.814.758-19','1990-07-29',1,7),(12,'Ivo Santos','390.814.758-19','1990-07-29',1,8),(13,'Ivo Santos','390.814.758-19','1990-07-29',1,9),(14,'Ivo Santos','390.814.758-19','1990-07-29',1,10),(15,'Ivo Santos','390.814.758-19','1990-07-29',1,11);

/*Table structure for table `cms` */

DROP TABLE IF EXISTS `cms`;

CREATE TABLE `cms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(70) DEFAULT NULL,
  `descricao` varchar(160) DEFAULT NULL,
  `h1` varchar(255) DEFAULT NULL,
  `conteudo` text,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `data_cadastro` datetime DEFAULT NULL,
  `data_modificacao` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cms` */

/*Table structure for table `coberturas` */

DROP TABLE IF EXISTS `coberturas`;

CREATE TABLE `coberturas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `descricao` varchar(1000) DEFAULT NULL,
  `seguradora_codigo_id` int(11) DEFAULT NULL,
  `tipo` varchar(20) DEFAULT NULL,
  `obrigatorio` tinyint(1) NOT NULL DEFAULT '0',
  `ordem` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

/*Data for the table `coberturas` */

insert  into `coberturas`(`id`,`nome`,`descricao`,`seguradora_codigo_id`,`tipo`,`obrigatorio`,`ordem`) values (1,'MORTE ACIDENTAL EM VIAGEM','Em caso de Morte Acidental do segurado durante a viagem, os beneficiários serão indenizados. ',412,'Coberturas',0,NULL),(2,'INVALIDEZ PERMANENTE TOTAL OU PARCIAL POR ACIDENTE EM VIAGEM','Em caso de invalidez permanente total ou parcial causada por acidente durante a viagem, o segurado será indenizado.',413,'Coberturas',0,NULL),(3,'EXTRAVIO DE BAGAGEM','Em caso de extravio, roubo ou furto de bagagem em viagens aéreas ou marítimas será feito o pagamento de uma indenização ao segurado. ',122,'Coberturas',0,NULL),(4,'EXTRAVIO DE BAGAGEM EM VIAGENS AEREAS','Em caso de extravio, roubo, furto ou desaparecimento total de bagagem em viagens aéreas será feito o pagamento de uma indenização ao segurado.',424,'Coberturas',0,NULL),(5,'CANCELAMENTO DE VIAGEM','No caso de cancelamento de viagem por evento coberto que impeça o segurado a iniciá-la, a SulAmérica garantirá o reembolso (até o limite de capital segurado) relativo às despesas com multas e diferenças tarifárias. ',145,'Coberturas',0,NULL),(6,'INTERRUPÇÃO DE VIAGEM','Em caso de interrupção da viagem por evento coberto, a SulAmérica garantirá o reembolso (até o limite de capital segurado) relativo às despesas com multas e diferenças tarifárias. ',414,'Coberturas',0,NULL),(7,'DESPESAS MEDICAS HOSPITALARES E ODONTOLÓGICAS EM VIAGEM AO EXTERIOR','Em caso de acidente pessoal ou doença, a cobertura garantirá a realização dos serviços médicos. De modo alternativo, o segurado poderá arcar com as despesas dos serviços prestados e ao término da viagem solicitar o reembolso (limitado ao capital segurado contratado). ',140,'Coberturas',1,NULL),(8,'DESPESAS FARMACÊUTICAS','Garante o reembolso (até o limite de capital segurado) de despesas com medicamentos necessários para tratamento médico/ odontológico emergencial prescritos por um médico.',421,'Coberturas',0,NULL),(9,'PRORROGAÇÃO DE ESTADA','No caso de doença súbita ou acidente durante a viagem onde a equipe médica determine que o segurado deva prolongar sua estadia, a SulAmérica fará o pagamento ou reembolso de diárias de hotel para o segurado. ',416,'Coberturas',0,NULL),(10,'ACOMPANHANTE EM CASO DE HOSPITALIZAÇÃO PROLONGADA','Em caso de hospitalização do segurado, decorrente de doença súbita ou acidente, a SulAmérica garantirá o pagamento ou reembolso de despesas de diárias de hotel para o acompanhante, até o limite máximo de 10 (dez) diárias. ',417,'Coberturas',0,NULL),(11,'HOSPEDAGEM DE ACOMPANHANTE','Desde que contratada, consiste no fornecimento de passagem aérea ida/volta ou o reembolso de despesas até o limite do capital segurado, para uma pessoa indicada em caso de acidente coberto ou doença súbita ocorrida com o segurado durante a viagem. ',418,'Coberturas',0,NULL),(12,'RETORNO DE MENORES','No caso de hospitalização do segurado, a SulAmérica garantirá a compra de uma passagem aérea, ida e volta, para que uma pessoa adulta possa acompanhar os menores de 14 anos que venham a ficar desacompanhado durante a viagem. ',420,'Coberturas',0,NULL),(13,'RETORNO DO SEGURADO','Em caso de doença súbita ou acidente e com indicação de médicos, a SulAmérica garantirá o pagamento de diferenças tarifárias entre o valor da passagem paga e o valor de passagem em classe econômica para o voô remarcado. ',415,'Coberturas',0,NULL),(14,'RETORNO DE ACOMPANHANTE ','Com o retorno do segurado (remoção médica ou funerária), será garantida a volta do acompanhante com o reembolso de diferenças entre o valor da passagem paga e o valor de passagem em classe econômica para o voô remarcado.',419,'Coberturas',0,NULL),(15,'DESPESAS JURÍDICAS PARA VIAGEM INTERNACIONAL','Garante o reembolso (até o limite de capital segurado) de despesas relacionadas a honorários advocatícios no caso do segurado sofrer qualquer acidente durante a viagem que necessite de assistência jurídica. ',422,'Coberturas',0,NULL),(16,'TRASLADO MÉDICO','Em caso de doença ou acidente, e que após os primeiros socorros indique a necessidade de retorno do segurado para seu domicílo ou remoção para um hospital melhor equipado, a SulAmérica garantirá os procedimentos necessários para a remoção médica.',143,'Coberturas',1,NULL),(17,'REGRESSO SANITÁRIO','Em caso de doença ou acidente, e que após a alta médica indique a necessidade de retorno do segurado para seu domicílo, a SulAmérica garantirá os procedimentos necessários para o regresso ao domicílio do segurado. ',142,'Coberturas',1,NULL),(18,'TRASLADO DE CORPO','Em caso de óbito do segurado durante a viagem, a SulAmérica garantirá o retorno do corpo até o seu domicílio/ local de sepultamento.',141,'Coberturas',1,NULL),(19,'FUNERAL','Em caso de morte natural ou acidental ocorrida durante a viagem, será realizada a prestação de serviço de funeral ou reembolsos de despesa (até o limite de capital segurado). ',42,'Coberturas',0,NULL),(20,'EXTRAVIO DE BAGAGEM EM VIAGENS MARITIMAS','Em caso de extravio, roubo ou furto de bagagem em viagens marítimas será feito o pagamento de uma indenização ao segurado.',425,'Coberturas',0,NULL),(21,'DESPESAS MEDICAS HOSPITALARES E ODONTOLÓGICAS EM VIAGEM NACIONAL','Em caso de acidente pessoal ou doença, a cobertura garantirá a realização dos serviços médicos. De modo alternativo, o segurado poderá arcar com as despesas dos serviços prestados e ao término da viagem solicitar o reembolso (limitado ao capital segurado contratado).',139,'Coberturas',0,NULL),(22,'CONCIERGE','O serviço de Concierge contempla apenas a indicação e/ou informação via telefone de Assistência para viagens, Informações de Eventos, Indicação de Locadoras, Indicação para Compra e envio de presentes e Welcome Home. Importante: o serviço de Concierge não compreende no fornecimento do serviço ou reservas.',4,'Serviços',0,NULL),(23,'TRANSMISSÃO DE MENSAGENS URGENTES','Caso o Segurado esteja impossibilitado de entrar em contato diretamente com sua família por motivo de acidente ou doença e tenha de transmitir mensagem de caráter urgente, a empresa de ASSISTENCIA encarregar-se- á desta pelo meio mais adequado. Os custos desta transmissão serão de responsabilidade da empresa de ASSISTÊNCIA.',2,'Serviços',0,NULL),(24,'ORIENTAÇÃO EM CASO DE PERDA DE DOCUMENTOS','No caso de perda ou roubo de documentos indispensáveis do segurado ao prosseguimento da viagem, a empresa de ASSISTÊNCIA prestará toda a orientação e ajuda necessária, junto às embaixadas ou órgãos competentes, para obtenção de passaporte ou outras medidas a serem tomadas.',3,'Serviços',0,NULL),(25,'LOCALIZAÇÃO DE BAGAGEM','No caso de extravio de bagagem do Segurado, regularmente despachada em vôos internacionais regulares, a empresa de ASSISTENCIA poderá assisti-lo na localização junto à Companhia Aérea responsável pelo transporte. Para tanto, é necessário que o Segurado, assim que tomar ciência do extravio de sua bagagem e antes de deixar o aeroporto, comunique o extravio à Companhia Aérea responsável, oficializando sua reclamação através de formulário próprio (P.I.R. – Property Irregularity Report).',1,'Serviços',0,NULL);

/*Table structure for table `coberturas_limites` */

DROP TABLE IF EXISTS `coberturas_limites`;

CREATE TABLE `coberturas_limites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cobertura_id` int(11) DEFAULT NULL,
  `plano_id` int(11) DEFAULT NULL,
  `moeda_limite` varchar(5) DEFAULT NULL,
  `limite` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `plano_id` (`plano_id`),
  KEY `cobertura_id` (`cobertura_id`),
  CONSTRAINT `coberturas_limites_ibfk_1` FOREIGN KEY (`plano_id`) REFERENCES `planos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `coberturas_limites_ibfk_2` FOREIGN KEY (`cobertura_id`) REFERENCES `coberturas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=423 DEFAULT CHARSET=latin1;

/*Data for the table `coberturas_limites` */

insert  into `coberturas_limites`(`id`,`cobertura_id`,`plano_id`,`moeda_limite`,`limite`) values (1,1,1,'R$','200000.0'),(2,2,1,'R$','200000.0'),(3,3,1,'R$','4000.0'),(4,4,1,'R$','4000.0'),(5,5,1,'R$','4000.0'),(6,6,1,'R$','4000.0'),(7,7,1,'US$','80000.0'),(8,8,1,'US$','400.0'),(9,9,1,'US$','1000.0'),(10,10,1,'US$','4000.0'),(11,11,1,'US$','1000.0'),(12,12,1,'US$','4000.0'),(13,13,1,'US$','2000.0'),(14,14,1,'US$','8000.0'),(15,15,1,'US$','6000.0'),(16,16,1,'US$','80000.0'),(17,17,1,'US$','80000.0'),(18,18,1,'US$','80000.0'),(19,19,1,'R$','7000.0'),(20,20,1,'R$','4000.0'),(21,7,2,'US$','50000.0'),(22,8,2,'US$','300.0'),(23,9,2,'US$','500.0'),(24,10,2,'US$','2000.0'),(25,11,2,'US$','500.0'),(26,12,2,'US$','2000.0'),(27,13,2,'US$','1000.0'),(28,14,2,'US$','4000.0'),(29,15,2,'US$','2000.0'),(30,16,2,'US$','50000.0'),(31,17,2,'US$','50000.0'),(32,18,2,'US$','50000.0'),(33,19,2,'R$','5000.0'),(34,1,2,'R$','100000.0'),(35,2,2,'R$','100000.0'),(36,3,2,'R$','3000.0'),(37,4,2,'R$','3000.0'),(38,5,2,'R$','2000.0'),(39,6,2,'R$','2000.0'),(40,7,3,'US$','15000.0'),(41,8,3,'US$','300.0'),(42,9,3,'US$','500.0'),(43,10,3,'US$','2000.0'),(44,11,3,'US$','500.0'),(45,12,3,'US$','2000.0'),(46,13,3,'US$','1000.0'),(47,14,3,'US$','4000.0'),(48,15,3,'US$','2000.0'),(49,16,3,'US$','50000.0'),(50,17,3,'US$','50000.0'),(51,18,3,'US$','50000.0'),(52,1,3,'R$','30000.0'),(53,2,3,'R$','30000.0'),(54,3,3,'R$','1500.0'),(55,4,3,'R$','1500.0'),(56,20,3,'R$','1500.0'),(57,7,6,'€','30000.0'),(58,8,6,'€','300.0'),(59,9,6,'€','500.0'),(60,10,6,'€','300.0'),(61,11,6,'€','500.0'),(62,12,6,'€','700.0'),(63,13,6,'€','300.0'),(64,14,6,'€','1400.0'),(65,15,6,'€','1000.0'),(66,16,6,'€','30000.0'),(67,17,6,'€','30000.0'),(68,18,6,'€','30000.0'),(69,1,6,'R$','30000.0'),(70,2,6,'R$','30000.0'),(71,3,6,'R$','1500.0'),(72,4,6,'R$','1500.0'),(73,20,6,'R$','1500.0'),(74,7,5,'€','30000.0'),(75,8,5,'€','300.0'),(76,9,5,'€','500.0'),(77,10,5,'€','700.0'),(78,11,5,'€','500.0'),(79,12,5,'€','1400.0'),(80,13,5,'€','700.0'),(81,14,5,'€','2800.0'),(82,15,5,'€','1000.0'),(83,16,5,'€','30000.0'),(84,17,5,'€','30000.0'),(85,18,5,'€','30000.0'),(86,19,5,'R$','7000.0'),(87,1,5,'R$','100000.0'),(88,2,5,'R$','100000.0'),(89,3,5,'R$','3000.0'),(90,4,5,'R$','3000.0'),(91,20,5,'R$','3000.0'),(92,5,5,'R$','2000.0'),(93,6,5,'R$','2000.0'),(94,7,15,'US$','50000.0'),(95,8,15,'US$','500.0'),(96,9,15,'US$','50.0'),(97,10,15,'US$','1000.0'),(98,11,15,'US$','50.0'),(99,12,15,'US$','2000.0'),(100,13,15,'US$','1000.0'),(101,14,15,'US$','4000.0'),(102,15,15,'US$','2000.0'),(103,16,15,'US$','25000.0'),(104,17,15,'US$','25000.0'),(105,18,15,'US$','25000.0'),(106,1,15,'R$','40000.0'),(107,2,15,'R$','40000.0'),(108,3,15,'R$','1000.0'),(109,4,15,'R$','1000.0'),(110,20,15,'R$','1000.0'),(111,5,15,'R$','1000.0'),(112,6,15,'R$','1000.0'),(113,7,16,'US$','7000.0'),(114,8,16,'US$','500.0'),(115,9,16,'US$','50.0'),(116,10,16,'US$','1000.0'),(117,11,16,'US$','50.0'),(118,12,16,'US$','2000.0'),(119,13,16,'US$','1000.0'),(120,14,16,'US$','4000.0'),(121,15,16,'US$','1000.0'),(122,16,16,'US$','7000.0'),(123,17,16,'US$','7000.0'),(124,18,16,'US$','7000.0'),(125,1,16,'R$','10000.0'),(126,2,16,'R$','10000.0'),(127,3,16,'R$','750.0'),(128,4,16,'R$','750.0'),(129,20,16,'R$','750.0'),(130,7,14,'US$','80000.0'),(131,8,14,'US$','400.0'),(132,9,14,'US$','50.0'),(133,10,14,'US$','2000.0'),(134,11,14,'US$','50.0'),(135,12,14,'US$','4000.0'),(136,13,14,'US$','2000.0'),(137,14,14,'US$','8000.0'),(138,15,14,'US$','2000.0'),(139,16,14,'US$','50000.0'),(140,17,14,'US$','50000.0'),(141,18,14,'US$','50000.0'),(142,1,14,'R$','60000.0'),(143,2,14,'R$','60000.0'),(144,3,14,'R$','2000.0'),(145,4,14,'R$','2000.0'),(146,20,14,'R$','2000.0'),(147,5,14,'R$','2000.0'),(148,6,14,'R$','2000.0'),(149,7,4,'US$','15000.0'),(150,8,4,'US$','300.0'),(151,9,4,'US$','300.0'),(152,10,4,'US$','300.0'),(153,11,4,'US$','500.0'),(154,12,4,'US$','2000.0'),(155,13,4,'US$','1000.0'),(156,14,4,'US$','4000.0'),(157,15,4,'US$','2000.0'),(158,16,4,'US$','50000.0'),(159,17,4,'US$','50000.0'),(160,18,4,'US$','50000.0'),(161,1,4,'R$','30000.0'),(162,2,4,'R$','30000.0'),(163,3,4,'R$','2000.0'),(164,20,4,'R$','2000.0'),(165,5,4,'R$','1500.0'),(166,6,4,'R$','1500.0'),(167,7,7,'€','30000.0'),(168,8,7,'€','300.0'),(169,9,7,'€','200.0'),(170,10,7,'€','300.0'),(171,11,7,'€','200.0'),(172,12,7,'€','700.0'),(173,13,7,'€','300.0'),(174,14,7,'€','1200.0'),(175,15,7,'€','1000.0'),(176,16,7,'€','30000.0'),(177,17,7,'€','30000.0'),(178,18,7,'€','30000.0'),(179,1,7,'R$','30000.0'),(180,2,7,'R$','30000.0'),(181,3,7,'R$','2000.0'),(182,20,7,'R$','2000.0'),(183,5,7,'R$','1500.0'),(184,6,7,'R$','1500.0'),(185,7,17,'€','30000.0'),(186,8,17,'€','200.0'),(187,9,17,'€','100.0'),(188,10,17,'€','200.0'),(189,11,17,'€','100.0'),(190,12,17,'€','700.0'),(191,13,17,'€','300.0'),(192,14,17,'€','1200.0'),(193,15,17,'€','1000.0'),(194,16,17,'€','30000.0'),(195,17,17,'€','30000.0'),(196,18,17,'€','30000.0'),(197,1,17,'R$','30000.0'),(198,2,17,'R$','30000.0'),(199,3,17,'R$','1500.0'),(200,20,17,'R$','1500.0'),(201,5,17,'R$','1500.0'),(202,6,17,'R$','1500.0'),(203,7,18,'€','30000.0'),(204,8,18,'€','400.0'),(205,9,18,'€','500.0'),(206,10,18,'€','400.0'),(207,11,18,'€','500.0'),(208,12,18,'€','700.0'),(209,13,18,'€','500.0'),(210,14,18,'€','1200.0'),(211,15,18,'€','1000.0'),(212,16,18,'€','30000.0'),(213,17,18,'€','30000.0'),(214,18,18,'€','30000.0'),(215,1,18,'R$','100000.0'),(216,2,18,'R$','100000.0'),(217,3,18,'R$','3000.0'),(218,20,18,'R$','3000.0'),(219,5,18,'R$','2000.0'),(220,6,18,'R$','2000.0'),(221,7,19,'US$','10000.0'),(222,8,19,'US$','200.0'),(223,9,19,'US$','200.0'),(224,10,19,'US$','200.0'),(225,11,19,'US$','300.0'),(226,12,19,'US$','2000.0'),(227,13,19,'US$','1000.0'),(228,14,19,'US$','4000.0'),(229,15,19,'US$','2000.0'),(230,16,19,'US$','30000.0'),(231,17,19,'US$','30000.0'),(232,18,19,'US$','30000.0'),(233,1,19,'R$','30000.0'),(234,2,19,'R$','30000.0'),(235,3,19,'R$','2000.0'),(236,20,19,'R$','2000.0'),(237,5,19,'R$','1000.0'),(238,6,19,'R$','1000.0'),(239,7,20,'US$','20000.0'),(240,8,20,'US$','400.0'),(241,9,20,'US$','500.0'),(242,10,20,'US$','400.0'),(243,11,20,'US$','700.0'),(244,12,20,'US$','2000.0'),(245,13,20,'US$','1000.0'),(246,14,20,'US$','4000.0'),(247,15,20,'US$','2000.0'),(248,16,20,'US$','70000.0'),(249,17,20,'US$','70000.0'),(250,18,20,'US$','70000.0'),(251,1,20,'R$','100000.0'),(252,2,20,'R$','100000.0'),(253,3,20,'R$','3000.0'),(254,20,20,'R$','3000.0'),(255,5,20,'R$','2000.0'),(256,6,20,'R$','2000.0'),(257,14,8,'US$','8000.0'),(258,15,8,'US$','1000.0'),(259,16,8,'US$','10000.0'),(260,17,8,'US$','10000.0'),(261,18,8,'US$','10000.0'),(262,19,8,'R$','5000.0'),(263,1,8,'R$','100000.0'),(264,2,8,'R$','100000.0'),(265,3,8,'R$','3000.0'),(266,4,8,'R$','3000.0'),(267,20,8,'R$','3000.0'),(268,5,8,'R$','2000.0'),(269,6,8,'R$','2000.0'),(270,7,8,'US$','15000.0'),(271,8,8,'US$','300.0'),(272,9,8,'US$','500.0'),(273,10,8,'US$','2000.0'),(274,11,8,'US$','300.0'),(275,12,8,'US$','4000.0'),(276,13,8,'US$','2000.0'),(277,7,9,'US$','10000.0'),(278,8,9,'US$','300.0'),(279,9,9,'US$','500.0'),(280,10,9,'US$','1000.0'),(281,11,9,'US$','300.0'),(282,12,9,'US$','2000.0'),(283,13,9,'US$','1000.0'),(284,14,9,'US$','4000.0'),(285,15,9,'US$','1000.0'),(286,16,9,'US$','10000.0'),(287,17,9,'US$','10000.0'),(288,18,9,'US$','10000.0'),(289,1,9,'R$','30000.0'),(290,2,9,'R$','30000.0'),(291,3,9,'R$','1500.0'),(292,4,9,'R$','1500.0'),(293,20,9,'R$','1500.0'),(294,7,10,'US$','15000.0'),(295,8,10,'US$','300.0'),(296,9,10,'US$','300.0'),(297,10,10,'US$','300.0'),(298,11,10,'US$','500.0'),(299,12,10,'US$','2000.0'),(300,13,10,'US$','1000.0'),(301,14,10,'US$','4000.0'),(302,15,10,'US$','1000.0'),(303,16,10,'US$','10000.0'),(304,17,10,'US$','10000.0'),(305,18,10,'US$','10000.0'),(306,1,10,'R$','30000.0'),(307,2,10,'R$','30000.0'),(308,3,10,'R$','2000.0'),(309,20,10,'R$','2000.0'),(310,5,10,'R$','1500.0'),(311,6,10,'R$','1500.0'),(312,7,21,'US$','10000.0'),(313,21,21,'US$','10000.0'),(314,8,21,'US$','200.0'),(315,9,21,'US$','200.0'),(316,10,21,'US$','200.0'),(317,11,21,'US$','300.0'),(318,12,21,'US$','2000.0'),(319,13,21,'US$','1000.0'),(320,14,21,'US$','4000.0'),(321,15,21,'US$','700.0'),(322,16,21,'US$','7000.0'),(323,17,21,'US$','7000.0'),(324,18,21,'US$','7000.0'),(325,1,21,'R$','30000.0'),(326,2,21,'R$','30000.0'),(327,3,21,'R$','2000.0'),(328,20,21,'R$','2000.0'),(329,5,21,'R$','1000.0'),(330,6,21,'R$','1000.0'),(331,7,22,'US$','20000.0'),(332,21,22,'US$','20000.0'),(333,8,22,'US$','400.0'),(334,9,22,'US$','500.0'),(335,10,22,'US$','400.0'),(336,11,22,'US$','700.0'),(337,12,22,'US$','2000.0'),(338,13,22,'US$','1000.0'),(339,14,22,'US$','4000.0'),(340,15,22,'US$','2000.0'),(341,16,22,'US$','20000.0'),(342,17,22,'US$','20000.0'),(343,18,22,'US$','20000.0'),(344,1,22,'R$','100000.0'),(345,2,22,'R$','100000.0'),(346,3,22,'R$','3000.0'),(347,20,22,'R$','3000.0'),(348,5,22,'R$','2000.0'),(349,6,22,'R$','2000.0'),(350,21,10,'US$','15000.0'),(351,22,1,NULL,'SIM'),(352,23,1,NULL,'SIM'),(353,24,1,NULL,'SIM'),(354,25,1,NULL,'SIM'),(355,22,2,NULL,'SIM'),(356,23,2,NULL,'SIM'),(357,24,2,NULL,'SIM'),(358,25,2,NULL,'SIM'),(359,23,3,NULL,'SIM'),(360,24,3,NULL,'SIM'),(361,25,3,NULL,'SIM'),(362,23,6,NULL,'SIM'),(363,24,6,NULL,'SIM'),(364,25,6,NULL,'SIM'),(365,22,5,NULL,'SIM'),(366,23,5,NULL,'SIM'),(367,24,5,NULL,'SIM'),(368,25,5,NULL,'SIM'),(369,23,4,NULL,'SIM'),(370,24,4,NULL,'SIM'),(371,25,4,NULL,'SIM'),(372,23,7,NULL,'SIM'),(373,24,7,NULL,'SIM'),(374,25,7,NULL,'SIM'),(375,22,8,NULL,'SIM'),(376,23,8,NULL,'SIM'),(377,24,8,NULL,'SIM'),(378,25,8,NULL,'SIM'),(379,23,9,NULL,'SIM'),(380,24,9,NULL,'SIM'),(381,25,9,NULL,'SIM'),(382,23,10,NULL,'SIM'),(383,24,10,NULL,'SIM'),(384,25,10,NULL,'SIM'),(385,22,11,NULL,'SIM'),(386,23,11,NULL,'SIM'),(387,24,11,NULL,'SIM'),(388,25,11,NULL,'SIM'),(389,22,12,NULL,'SIM'),(390,23,12,NULL,'SIM'),(391,24,12,NULL,'SIM'),(392,25,12,NULL,'SIM'),(393,23,13,NULL,'SIM'),(394,24,13,NULL,'SIM'),(395,25,13,NULL,'SIM'),(396,23,14,NULL,'SIM'),(397,24,14,NULL,'SIM'),(398,25,14,NULL,'SIM'),(399,23,15,NULL,'SIM'),(400,24,15,NULL,'SIM'),(401,25,15,NULL,'SIM'),(402,23,16,NULL,'SIM'),(403,24,16,NULL,'SIM'),(404,25,16,NULL,'SIM'),(405,23,17,NULL,'SIM'),(406,24,17,NULL,'SIM'),(407,25,17,NULL,'SIM'),(408,23,18,NULL,'SIM'),(409,24,18,NULL,'SIM'),(410,25,18,NULL,'SIM'),(411,23,19,NULL,'SIM'),(412,24,19,NULL,'SIM'),(413,24,19,NULL,'SIM'),(414,23,20,NULL,'SIM'),(415,24,20,NULL,'SIM'),(416,25,20,NULL,'SIM'),(417,23,21,NULL,'SIM'),(418,24,21,NULL,'SIM'),(419,25,21,NULL,'SIM'),(420,23,22,NULL,'SIM'),(421,24,22,NULL,'SIM'),(422,25,22,NULL,'SIM');

/*Table structure for table `cotacao_por_email` */

DROP TABLE IF EXISTS `cotacao_por_email`;

CREATE TABLE `cotacao_por_email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `nome_amigo` varchar(100) DEFAULT NULL,
  `email_amigo` varchar(100) DEFAULT NULL,
  `cotacao_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cotacao_id` (`cotacao_id`),
  CONSTRAINT `cotacao_por_email_ibfk_1` FOREIGN KEY (`cotacao_id`) REFERENCES `cotacoes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cotacao_por_email` */

/*Table structure for table `cotacoes` */

DROP TABLE IF EXISTS `cotacoes`;

CREATE TABLE `cotacoes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destino_id` int(3) DEFAULT NULL,
  `data_inicio` date DEFAULT NULL,
  `data_final` date DEFAULT NULL,
  `num_passageiros` int(3) DEFAULT NULL,
  `num_passageiros_maiores` int(3) DEFAULT NULL,
  `motivo_id` int(2) DEFAULT NULL,
  `tipo_id` int(2) DEFAULT NULL,
  `nome` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `data_cadastro` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `destino_id` (`destino_id`),
  KEY `motivo_id` (`motivo_id`),
  KEY `tipo_id` (`tipo_id`),
  CONSTRAINT `cotacoes_ibfk_1` FOREIGN KEY (`destino_id`) REFERENCES `destinos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `cotacoes_ibfk_2` FOREIGN KEY (`motivo_id`) REFERENCES `motivos_viagem` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `cotacoes_ibfk_3` FOREIGN KEY (`tipo_id`) REFERENCES `tipos_viagem` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=229 DEFAULT CHARSET=latin1;

/*Data for the table `cotacoes` */

insert  into `cotacoes`(`id`,`destino_id`,`data_inicio`,`data_final`,`num_passageiros`,`num_passageiros_maiores`,`motivo_id`,`tipo_id`,`nome`,`email`,`data_cadastro`) values (168,27,'2016-12-14','2016-12-14',1,0,1,1,'','','2016-12-02 15:43:10'),(169,27,'2016-12-14','2016-12-14',1,0,1,3,'','','2016-12-02 16:18:43'),(170,27,'2016-12-14','2016-12-14',1,0,1,1,'','','2016-12-02 16:55:16'),(171,27,'2016-12-14','2016-12-14',1,0,1,2,'','','2016-12-02 16:55:59'),(172,27,'2016-12-14','2016-12-14',1,0,1,3,'','','2016-12-02 16:58:36'),(173,27,'2016-12-21','2016-12-21',1,0,1,1,'','','2016-12-02 17:01:54'),(174,55,'2016-12-14','2016-12-14',1,0,1,1,'','','2016-12-02 19:43:34'),(175,3,'2016-12-14','2016-12-14',1,0,1,1,'','','2016-12-02 19:45:17'),(176,94,'2016-12-14','2016-12-14',1,0,1,1,'','','2016-12-02 19:55:55'),(177,94,'2016-12-14','2016-12-14',1,0,1,1,'','','2016-12-02 20:05:49'),(178,27,'2016-12-22','2016-12-22',1,0,1,1,'','','2016-12-03 13:54:43'),(179,27,'2016-12-21','2016-12-21',1,0,2,1,'','','2016-12-03 14:04:17'),(180,27,'2016-12-21','2016-12-21',1,0,1,1,'','','2016-12-03 16:44:18'),(181,27,'2016-12-14','2016-12-14',1,0,1,1,'','','2016-12-03 17:28:05'),(182,27,'2016-12-14','2016-12-14',1,0,2,1,'','','2016-12-03 17:28:46'),(183,27,'2016-12-14','2016-12-14',1,0,3,1,'','','2016-12-03 17:29:29'),(184,27,'2016-12-14','2016-12-14',1,0,1,2,'','','2016-12-03 17:30:07'),(185,27,'2016-12-21','2016-12-21',1,0,1,3,'','','2016-12-03 17:30:54'),(186,27,'2016-12-21','2016-12-21',1,0,3,1,'','','2016-12-03 17:31:23'),(187,27,'2017-01-11','2017-01-11',1,0,3,3,'','','2016-12-03 17:32:05'),(188,3,'2016-12-14','2016-12-14',1,0,1,1,'','','2016-12-03 17:32:44'),(189,3,'2016-12-21','2016-12-21',1,0,1,2,'','','2016-12-03 17:33:23'),(190,3,'2016-12-14','2016-12-14',1,0,1,3,'','','2016-12-03 17:34:08'),(191,3,'2016-12-14','2016-12-14',1,0,2,1,'','','2016-12-03 17:34:58'),(192,3,'2016-12-14','2016-12-14',1,0,2,2,'','','2016-12-03 17:35:27'),(193,3,'2016-12-21','2016-12-21',1,0,2,3,'','','2016-12-03 17:36:10'),(194,3,'2016-12-14','2016-12-14',1,0,3,1,'','','2016-12-03 17:36:47'),(195,3,'2016-12-21','2016-12-21',1,0,3,2,'','','2016-12-03 17:37:19'),(196,3,'2016-12-21','2016-12-21',1,0,3,3,'','','2016-12-03 17:38:24'),(197,57,'2016-12-14','2016-12-14',1,0,1,1,'','','2016-12-03 17:39:24'),(198,57,'2016-12-14','2016-12-14',1,0,2,1,'','','2016-12-03 17:40:03'),(200,49,'2016-12-14','2016-12-14',1,0,3,1,'','','2016-12-03 17:40:53'),(201,49,'2016-12-14','2016-12-14',1,0,3,1,'','','2016-12-03 17:41:40'),(202,57,'2016-12-21','2016-12-21',1,0,3,2,'','','2016-12-03 17:42:19'),(203,3,'2016-12-14','2016-12-14',1,0,1,1,'','','2016-12-03 19:21:22'),(204,3,'2016-12-14','2016-12-14',1,0,1,2,'','','2016-12-03 22:24:36'),(205,3,'2016-12-14','2016-12-14',1,0,1,1,'','','2016-12-03 22:27:57'),(206,27,'2016-12-14','2016-12-14',1,0,1,1,'','','2016-12-03 22:34:22'),(207,27,'2016-12-14','2016-12-14',1,0,3,1,'','','2016-12-03 22:42:01'),(208,3,'2016-12-14','2016-12-14',1,0,3,1,'','','2016-12-03 22:43:05'),(209,27,'2016-12-21','2016-12-21',1,0,1,1,'','','2016-12-04 10:51:02'),(210,27,'2016-12-28','2016-12-28',2,0,1,1,'','','2016-12-04 11:01:46'),(211,27,'2016-12-29','2016-12-29',3,1,1,1,'','','2016-12-04 14:48:57'),(212,3,'2016-12-21','2016-12-21',1,0,1,1,'','','2016-12-04 16:06:38'),(213,94,'2016-12-22','2016-12-22',1,0,1,1,'','','2016-12-04 17:51:41'),(214,168,'2016-12-21','2016-12-21',1,0,1,1,'','','2016-12-04 18:42:06'),(215,27,'2016-12-28','2016-12-28',1,0,1,1,'','','2016-12-05 10:29:09'),(216,94,'2016-12-29','2016-12-29',1,0,1,1,'','','2016-12-05 10:37:55'),(217,94,'2016-12-29','2016-12-30',1,0,1,1,'','','2016-12-05 10:54:46'),(218,204,'2016-12-29','2016-12-29',1,0,1,1,'','','2016-12-05 11:30:23'),(219,27,'2017-01-20','2017-01-20',1,0,1,1,'','','2016-12-05 11:54:31'),(220,27,'2017-01-25','2017-01-25',1,0,1,1,'','','2016-12-05 12:41:32'),(221,27,'2016-12-28','2016-12-28',1,0,1,1,'','','2016-12-05 15:04:18'),(222,3,'2017-01-18','2017-01-18',1,0,1,1,'','','2016-12-05 17:16:34'),(223,1,'2016-12-29','2016-12-29',3,1,3,1,'','','2016-12-05 17:20:12'),(224,1,'2016-12-29','2017-01-09',3,1,3,1,'','','2016-12-05 17:23:14'),(225,27,'2016-12-21','2016-12-21',1,0,1,1,'','','2016-12-05 20:36:35'),(226,27,'2017-01-11','2017-01-11',1,0,1,1,'','','2016-12-08 15:57:28'),(227,27,'2016-12-21','2016-12-21',1,0,1,1,'','','2016-12-08 16:05:28'),(228,27,'2017-01-11','2017-01-11',1,0,1,1,'','','2016-12-08 16:07:28');

/*Table structure for table `cupons` */

DROP TABLE IF EXISTS `cupons`;

CREATE TABLE `cupons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `codigo` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `data_inicio` date DEFAULT NULL,
  `data_final` date DEFAULT NULL,
  `quantidade` int(11) DEFAULT '1' COMMENT 'Quantidade maxima de vendas com esse cupom ',
  `quantidade_usado` int(11) DEFAULT '0' COMMENT 'Quantidade de vendas que já usam esse cupom',
  `desconto` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT '1' COMMENT '0=inativo 1=ativo',
  `funcionario_id` int(11) DEFAULT '0',
  `data_cadastro` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK` (`codigo`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `cupons` */

insert  into `cupons`(`id`,`nome`,`codigo`,`data_inicio`,`data_final`,`quantidade`,`quantidade_usado`,`desconto`,`status`,`funcionario_id`,`data_cadastro`) values (1,'facebook 10','facebook10','2016-11-28','2016-12-07',100,3,15,1,0,'2016-11-30 13:51:22');

/*Table structure for table `destinos` */

DROP TABLE IF EXISTS `destinos`;

CREATE TABLE `destinos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seguradora_destino_id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=229 DEFAULT CHARSET=latin1;

/*Data for the table `destinos` */

insert  into `destinos`(`id`,`seguradora_destino_id`,`nome`) values (1,1,'Afeganistão'),(2,3,'Albânia'),(3,14,'Argentina'),(4,23,'Barbados'),(5,26,'Benin'),(6,33,'Brunei'),(7,35,'Burkina Faso'),(8,41,'Canadá'),(9,69,'Fiji'),(10,83,'Guatemala'),(11,90,'Holanda'),(12,99,'Ilhas Cayman'),(13,102,'Ilhas Faeroes'),(14,125,'Jamaica'),(15,127,'Jordânia'),(16,128,'Kiribati'),(17,140,'Macedonia'),(18,159,'Namíbia'),(19,161,'Nepal'),(20,169,'Omã'),(21,173,'Paquistão'),(22,187,'Romênia'),(23,200,'Singapura'),(24,207,'Tailândia'),(25,208,'Taiwan'),(26,210,'Timor Leste'),(27,5,'Alemanha'),(28,7,'Angola'),(29,8,'Anguilla'),(30,11,'Antilhas Holandesas'),(31,29,'Bolívia'),(32,31,'Botswana'),(33,36,'Burundi'),(34,37,'Butão'),(35,39,'Camarões'),(36,43,'Chade'),(37,53,'Costa Rica'),(38,55,'Cuba'),(39,59,'Egito'),(40,60,'Emirados Árabes Unidos'),(41,68,'Etiópia'),(42,71,'Finlândia'),(43,78,'Granada'),(44,84,'Guiana'),(45,93,'Hungria'),(46,96,'Ilha Guernsey'),(47,97,'Ilha Jersey'),(48,113,'Ilhas Virgens'),(49,120,'Irlanda'),(50,178,'Porto Rico'),(51,192,'Santa Helena'),(52,198,'Serra Leoa'),(53,201,'Sri Lanka'),(54,205,'Suriname'),(55,32,'Brasil'),(56,13,'Argélia'),(57,17,'Austrália'),(58,24,'Bélgica'),(59,27,'Bermuda'),(60,34,'Bulgária'),(61,38,'Cabo Verde'),(62,46,'Chipre'),(63,57,'Djibouti'),(64,58,'Dominica'),(65,70,'Filipinas'),(66,79,'Grécia'),(67,91,'Honduras'),(68,95,'Ilha de Man'),(69,101,'Ilhas Cook'),(70,110,'Ilhas Svalbard'),(71,114,'Ilhas Virgens (US)'),(72,124,'Itália'),(73,126,'Japão'),(74,136,'Liechtenstein'),(75,142,'Malásia'),(76,145,'Mali'),(77,152,'Micronésia'),(78,154,'Moldávia'),(79,157,'Montserrat'),(80,162,'Nicarágua'),(81,166,'Noruega'),(82,181,'Quênia'),(83,190,'Samoa'),(84,206,'Tadjiquistão'),(85,209,'Tanzânia'),(86,217,'Tuvalu'),(87,9,'Antártida'),(88,10,'Antígua e Barbuda'),(89,18,'Áustria'),(90,20,'Bahamas'),(91,40,'Camboja'),(92,42,'Cazaquistão'),(93,51,'Coreia do Sul'),(94,66,'Estados Unidos'),(95,74,'Gâmbia'),(96,80,'Groelândia'),(97,85,'Guiana Francesa'),(98,94,'Iêmen'),(99,100,'Ilhas Comores'),(100,104,'Ilhas Malvinas'),(101,131,'Lesoto'),(102,138,'Luxemburgo'),(103,139,'Macau'),(104,144,'Maldivas'),(105,149,'Maurício'),(106,155,'Mônaco'),(107,158,'Myanmar '),(108,160,'Nauru'),(109,164,'Nigéria'),(110,165,'Niue'),(111,172,'Papua-Nova Guiné'),(112,175,'Peru'),(113,179,'Portugal'),(114,180,'Qatar'),(115,199,'Servia e Montenegro'),(116,216,'Turquia'),(117,219,'Uganda'),(118,15,'Armênia'),(119,16,'Aruba'),(120,25,'Belize'),(121,30,'Bósnia e Herzegovina'),(122,49,'Congo'),(123,52,'Costa do Marfim'),(124,56,'Dinamarca'),(125,67,'Estônia'),(126,77,'Gibraltar'),(127,87,'Guiné Equatorial'),(128,88,'Guiné-Bissau'),(129,103,'Ilhas Faroé'),(130,107,'Ilhas Natal'),(131,118,'Inglaterra'),(132,123,'Israel'),(133,132,'Letônia'),(134,133,'Líbano'),(135,141,'Madagascar'),(136,150,'Mauritânia'),(137,156,'Mongólia'),(138,168,'Nova Zelândia'),(139,176,'Polinésia Francesa'),(140,183,'República Centro-Africana'),(141,193,'Santa Lúcia'),(142,195,'São Tomé e Príncipe'),(143,213,'Trinidad e Tobago'),(144,218,'Ucrânia'),(145,228,'Zimbábue'),(146,19,'Azerbaijão'),(147,45,'China'),(148,76,'Geórgia'),(149,98,'Ilha Norfolk'),(150,106,'Ilhas Marshall'),(151,115,'Ilhas Wallis e Futuna'),(152,130,'Laos'),(153,134,'Libéria'),(154,146,'Malta'),(155,147,'Marrocos'),(156,148,'Martinica'),(157,151,'México'),(158,163,'Níger'),(159,185,'República Tcheca'),(160,197,'Senegal'),(161,204,'Suíça'),(162,214,'Tunísia'),(163,220,'Uruguai'),(164,222,'Vanuatu'),(165,224,'Venezuela'),(166,227,'Zâmbia'),(167,2,'África do Sul'),(168,4,'Alderney'),(169,21,'Bahrein'),(170,28,'Bielorússia'),(171,47,'Colômbia'),(172,50,'Coreia do Norte'),(173,54,'Croácia'),(174,61,'Equador'),(175,62,'Escócia'),(176,72,'França'),(177,73,'Gabão'),(178,75,'Gana'),(179,86,'Guiné'),(180,105,'Ilhas Marianas do Norte'),(181,108,'Ilhas Salomão'),(182,109,'Ilhas Seychelles'),(183,112,'Ilhas Turks e Caicos'),(184,116,'Índia'),(185,119,'Iraque'),(186,121,'Irlanda do Norte'),(187,135,'Líbia'),(188,137,'Lituânia'),(189,143,'Malavi'),(190,171,'Panamá'),(191,174,'Paraguai'),(192,188,'Ruanda'),(193,212,'Tonga'),(194,221,'Uzbequistão'),(195,223,'Vaticano'),(196,226,'Zaire'),(197,6,'Andorra'),(198,12,'Arábia Saudita'),(199,22,'Bangladesh'),(200,44,'Chile'),(201,48,'Comoros '),(202,63,'Eslováquia'),(203,64,'Eslovênia'),(204,65,'Espanha'),(205,81,'Guadalupe (FR)'),(206,82,'Guam'),(207,89,'Haiti'),(208,92,'Hong Kong'),(209,111,'Ilhas Tokelau'),(210,117,'Indonésia'),(211,122,'Islândia'),(212,129,'Kuwait'),(213,153,'Moçambique'),(214,167,'Nova Caledônia'),(215,170,'País de Gales'),(216,177,'Polônia'),(217,182,'Quirguistão'),(218,184,'República Dominicana'),(219,186,'Reunion'),(220,189,'Russia'),(221,191,'San Marino'),(222,194,'São Cristóvão e Nevis'),(223,196,'São Vincente e Granadinas'),(224,202,'Suazilândia'),(225,203,'Suécia'),(226,211,'Togo'),(227,215,'Turcomenistão'),(228,225,'Vietnã');

/*Table structure for table `destinos_planos` */

DROP TABLE IF EXISTS `destinos_planos`;

CREATE TABLE `destinos_planos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo_zona` int(11) DEFAULT NULL,
  `planos` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `destinos_planos` */

/*Table structure for table `empresas` */

DROP TABLE IF EXISTS `empresas`;

CREATE TABLE `empresas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa` varchar(80) NOT NULL,
  `logo` varchar(50) DEFAULT NULL,
  `cnpj` varchar(20) DEFAULT NULL,
  `cep` varchar(9) DEFAULT NULL,
  `endereco` varchar(50) DEFAULT NULL,
  `numero` varchar(5) DEFAULT NULL,
  `complemento` varchar(20) DEFAULT NULL,
  `bairro` varchar(40) DEFAULT NULL,
  `cidade` varchar(50) DEFAULT NULL,
  `estado` char(2) DEFAULT NULL,
  `contato` varchar(80) DEFAULT NULL,
  `contato_email` varchar(100) DEFAULT NULL,
  `telefone` varchar(15) DEFAULT NULL,
  `banco` varchar(25) DEFAULT NULL,
  `agencia` varchar(15) DEFAULT NULL,
  `conta` varchar(15) DEFAULT NULL,
  `data_alteracao` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `empresas` */

insert  into `empresas`(`id`,`empresa`,`logo`,`cnpj`,`cep`,`endereco`,`numero`,`complemento`,`bairro`,`cidade`,`estado`,`contato`,`contato_email`,`telefone`,`banco`,`agencia`,`conta`,`data_alteracao`) values (1,'FAN VIAGENS E TURISMO LTDA',NULL,'13.225.630/0001-06','05422-030','Rua Cláudio Soares','72','cj 1406','Pinheiros','São Paulo','SP','DOUGLAS DE FREITAS NOGUEIRA','contato@tripguard.com.br','(11) 97137-1516','Bradesco','0200','0107584-5','2014-11-08 21:42:24');

/*Table structure for table `enderecos` */

DROP TABLE IF EXISTS `enderecos`;

CREATE TABLE `enderecos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cep` varchar(9) DEFAULT NULL,
  `logradouro` varchar(100) DEFAULT NULL,
  `numero` varchar(20) DEFAULT NULL,
  `complemento` varchar(100) DEFAULT NULL,
  `bairro` varchar(50) DEFAULT NULL,
  `cidade` varchar(50) DEFAULT NULL,
  `estado` varchar(2) DEFAULT NULL,
  `venda_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `venda_id` (`venda_id`),
  CONSTRAINT `enderecos_ibfk_1` FOREIGN KEY (`venda_id`) REFERENCES `vendas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `enderecos` */

insert  into `enderecos`(`id`,`cep`,`logradouro`,`numero`,`complemento`,`bairro`,`cidade`,`estado`,`venda_id`) values (1,'05422-030','Rua Cláudio Soares','72','1406','Pinheiros','São Paulo','SP',3),(3,'05422-030','Rua Cláudio Soares','72','1406','Pinheiros','São Paulo','SP',4),(4,'05422-030','Rua Cláudio Soares','72','1406','Pinheiros','São Paulo','SP',5),(5,'05422-030','Rua Cláudio Soares','72','1406','Pinheiros','São Paulo','SP',6),(6,'05422-030','Rua Cláudio Soares','72','1406','Pinheiros','São Paulo','SP',7),(7,'05422-030','Rua Cláudio Soares','72','1406','Pinheiros','São Paulo','SP',8),(8,'05422-030','Rua Cláudio Soares','72','1406','Pinheiros','São Paulo','SP',9),(9,'05422-030','Rua Cláudio Soares','','1406','Pinheiros','São Paulo','SP',10),(10,'05422-030','Rua Cláudio Soares','','1406','Pinheiros','São Paulo','SP',11);

/*Table structure for table `faixas_idade` */

DROP TABLE IF EXISTS `faixas_idade`;

CREATE TABLE `faixas_idade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `limite_inferior` int(11) DEFAULT NULL,
  `limite_superior` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `faixas_idade` */

insert  into `faixas_idade`(`id`,`limite_inferior`,`limite_superior`) values (1,0,64),(2,65,90);

/*Table structure for table `formas_pagamento` */

DROP TABLE IF EXISTS `formas_pagamento`;

CREATE TABLE `formas_pagamento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `forma_pagamento` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `formas_pagamento` */

insert  into `formas_pagamento`(`id`,`forma_pagamento`) values (1,'Boleto'),(2,'Cartão de crédito'),(3,'Débito'),(4,'Depósito');

/*Table structure for table `logs_cielo` */

DROP TABLE IF EXISTS `logs_cielo`;

CREATE TABLE `logs_cielo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `venda_id` int(11) DEFAULT NULL,
  `origem` varchar(255) DEFAULT NULL,
  `msg` text,
  `data_log` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `venda_id` (`venda_id`),
  CONSTRAINT `logs_cielo_ibfk_1` FOREIGN KEY (`venda_id`) REFERENCES `vendas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `logs_cielo` */

/*Table structure for table `metas_mensais` */

DROP TABLE IF EXISTS `metas_mensais`;

CREATE TABLE `metas_mensais` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mes` int(2) DEFAULT NULL,
  `ano` int(4) DEFAULT NULL,
  `valor` float DEFAULT NULL,
  `data_cadastro` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `metas_mensais` */

/*Table structure for table `motivos_viagem` */

DROP TABLE IF EXISTS `motivos_viagem`;

CREATE TABLE `motivos_viagem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seguradora_motivo_id` int(11) NOT NULL,
  `nome` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `motivos_viagem` */

insert  into `motivos_viagem`(`id`,`seguradora_motivo_id`,`nome`) values (1,1,'Lazer'),(2,2,'Estudo'),(3,3,'Negócios');

/*Table structure for table `planos` */

DROP TABLE IF EXISTS `planos`;

CREATE TABLE `planos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `seguradora_plano_id` int(11) DEFAULT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `dias_min` int(4) DEFAULT NULL,
  `dias_max` int(4) DEFAULT NULL,
  `idade_min` int(2) DEFAULT '0',
  `idade_max` int(2) DEFAULT NULL,
  `idade_min_acrescimo` int(2) DEFAULT NULL,
  `idade_max_acrescimo` int(2) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

/*Data for the table `planos` */

insert  into `planos`(`id`,`nome`,`seguradora_plano_id`,`descricao`,`dias_min`,`dias_max`,`idade_min`,`idade_max`,`idade_min_acrescimo`,`idade_max_acrescimo`,`status`) values (1,'Mundo Prestige',1,NULL,NULL,NULL,0,64,65,90,1),(2,'Mundo',2,NULL,NULL,NULL,0,64,65,90,1),(3,'Mundo Compacto',3,NULL,NULL,NULL,0,64,65,90,1),(4,'Mundo Marítimo',4,NULL,NULL,NULL,0,64,65,90,1),(5,'Europa',5,NULL,NULL,NULL,0,64,65,90,1),(6,'Europa Compacto',6,NULL,NULL,NULL,0,64,65,90,1),(7,'Europa Marítimo',7,NULL,NULL,NULL,0,64,65,90,1),(8,'América Latina',8,NULL,NULL,NULL,0,64,65,90,1),(9,'América Latina Compacto',9,NULL,NULL,NULL,0,64,65,90,1),(10,'América Latina Marítimo',10,NULL,NULL,NULL,0,64,65,90,1),(11,'BRASIL',11,NULL,NULL,NULL,0,64,65,90,1),(12,'BRASIL PRESTIGE',12,NULL,NULL,NULL,0,64,65,90,1),(13,'BRASIL COMPACTO',13,NULL,NULL,NULL,0,64,65,90,1),(14,'Estudante Prestige',14,NULL,NULL,NULL,0,64,65,90,1),(15,'Estudante',15,NULL,NULL,NULL,0,64,65,90,1),(16,'Estudante Compacto',16,NULL,NULL,NULL,0,64,65,90,1),(17,'Europa Marítimo Compacto',17,NULL,NULL,NULL,0,64,65,90,1),(18,'Europa Marítimo Prestige',18,NULL,NULL,NULL,0,64,65,90,1),(19,'Mundo Marítimo Compacto',19,NULL,NULL,NULL,0,64,65,90,1),(20,'Mundo Marítimo Prestige',20,NULL,NULL,NULL,0,64,65,90,1),(21,'América Latina Marítimo Compacto',21,NULL,NULL,NULL,0,64,65,90,1),(22,'América Latina Marítimo Prestige',22,NULL,NULL,NULL,0,64,65,90,1);

/*Table structure for table `tipos_viagem` */

DROP TABLE IF EXISTS `tipos_viagem`;

CREATE TABLE `tipos_viagem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seguradora_tipo_id` int(11) NOT NULL,
  `nome` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `tipos_viagem` */

insert  into `tipos_viagem`(`id`,`seguradora_tipo_id`,`nome`) values (1,1,'Aéreo'),(2,2,'Marítima'),(3,3,'Aéreo + Marítima');

/*Table structure for table `vendas` */

DROP TABLE IF EXISTS `vendas`;

CREATE TABLE `vendas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `telefone` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `cotacao_id` int(11) DEFAULT NULL,
  `plano_id` int(11) DEFAULT NULL,
  `coberturas_info` text,
  `servicos_info` text,
  `tipo` int(1) NOT NULL DEFAULT '2' COMMENT '1=venda 2=pre-venda',
  `valor` float DEFAULT NULL,
  `valor_idoso` float DEFAULT NULL,
  `valor_total` float DEFAULT NULL,
  `valor_desconto` float DEFAULT NULL,
  `salt` varchar(20) DEFAULT NULL,
  `cupom_id` int(11) DEFAULT NULL,
  `forma_pagamento_id` int(1) DEFAULT NULL,
  `tid` varchar(100) DEFAULT NULL,
  `bandeira` varchar(20) DEFAULT NULL,
  `parcelas` int(1) DEFAULT NULL,
  `data_cadastro` datetime DEFAULT NULL,
  `data_modificacao` datetime DEFAULT NULL,
  `funcionario_id` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT NULL COMMENT '1= aprovado 2=pendente 3=cancelado',
  `data_pagamento` date DEFAULT NULL,
  `comentario` varchar(255) DEFAULT NULL,
  `ip` varchar(50) DEFAULT NULL,
  `viu_voucher` int(1) NOT NULL DEFAULT '0',
  `viu_voucher_em` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cotacao_id` (`cotacao_id`),
  KEY `plano_id` (`plano_id`),
  CONSTRAINT `vendas_ibfk_1` FOREIGN KEY (`cotacao_id`) REFERENCES `cotacoes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `vendas_ibfk_2` FOREIGN KEY (`plano_id`) REFERENCES `planos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `vendas` */

insert  into `vendas`(`id`,`nome`,`telefone`,`email`,`cotacao_id`,`plano_id`,`coberturas_info`,`servicos_info`,`tipo`,`valor`,`valor_idoso`,`valor_total`,`valor_desconto`,`salt`,`cupom_id`,`forma_pagamento_id`,`tid`,`bandeira`,`parcelas`,`data_cadastro`,`data_modificacao`,`funcionario_id`,`status`,`data_pagamento`,`comentario`,`ip`,`viu_voucher`,`viu_voucher_em`) values (1,'',NULL,'',208,1,NULL,NULL,2,48.04,72.14,NULL,NULL,'e9ad31',NULL,NULL,NULL,NULL,NULL,'2016-12-04 00:05:55','2016-12-04 00:05:55',NULL,NULL,NULL,NULL,'127.0.0.1',0,NULL),(2,'',NULL,'',209,1,NULL,NULL,2,48.04,72.14,NULL,NULL,'cfbfe5',NULL,NULL,NULL,NULL,NULL,'2016-12-04 10:51:37','2016-12-04 10:51:37',NULL,NULL,NULL,NULL,'127.0.0.1',0,NULL),(3,'Ivo Santos','(11) 22112211','is.ivosantos@gmail.com',210,1,NULL,NULL,2,48.04,72.14,96.08,NULL,'208587',NULL,NULL,NULL,NULL,NULL,'2016-12-04 11:06:40','2016-12-04 14:46:16',NULL,NULL,NULL,NULL,'127.0.0.1',0,NULL),(4,'Ivo Santos','(11) 22112211','is.ivosantos@gmail.com',211,5,NULL,NULL,1,19.74,29.62,69.1,3.46,'2bdaa2',NULL,1,NULL,NULL,NULL,'2016-12-04 14:49:16','2016-12-04 18:18:41',1,3,NULL,NULL,'127.0.0.1',0,NULL),(5,'Ivo Santos','(11) 22112211','is.ivosantos@gmail.com',212,8,NULL,NULL,1,14.54,21.88,14.54,1.02,'fa2296',NULL,4,NULL,NULL,NULL,'2016-12-04 16:06:59','2016-12-05 12:16:52',1,3,NULL,NULL,'127.0.0.1',0,NULL),(6,'Ivo Santos','(11) 22112211','is.ivosantos@gmail.com',213,3,NULL,NULL,1,15.48,23.23,15.48,0.77,'a879cc',NULL,1,NULL,NULL,NULL,'2016-12-04 17:51:57','2016-12-05 12:16:45',1,3,NULL,NULL,'127.0.0.1',0,NULL),(7,'Ivo Santos','(11) 22112211','is.ivosantos@gmail.com',214,6,NULL,NULL,2,14.92,22.39,14.92,NULL,'93bddc',NULL,NULL,NULL,NULL,NULL,'2016-12-04 18:42:26','2016-12-04 18:42:41',NULL,NULL,NULL,NULL,'127.0.0.1',0,NULL),(8,'Ivo Santos','(11) 22112211','is.ivosantos@gmail.com',219,1,'a:36:{i:0;a:3:{s:15:\"codigoCobertura\";i:1;s:7:\"capital\";s:8:\"200000.0\";s:6:\"premio\";s:4:\"2.37\";}i:1;a:3:{s:15:\"codigoCobertura\";i:1;s:7:\"capital\";s:8:\"200000.0\";s:6:\"premio\";s:4:\"3.55\";}i:2;a:3:{s:15:\"codigoCobertura\";i:1;s:7:\"capital\";s:8:\"200000.0\";s:6:\"premio\";s:4:\"0.59\";}i:3;a:3:{s:15:\"codigoCobertura\";i:1;s:7:\"capital\";s:8:\"200000.0\";s:6:\"premio\";s:4:\"0.88\";}i:4;a:3:{s:15:\"codigoCobertura\";i:1;s:7:\"capital\";s:6:\"4000.0\";s:6:\"premio\";s:4:\"0.05\";}i:5;a:3:{s:15:\"codigoCobertura\";i:1;s:7:\"capital\";s:6:\"4000.0\";s:6:\"premio\";s:4:\"0.07\";}i:6;a:3:{s:15:\"codigoCobertura\";i:1;s:7:\"capital\";s:6:\"4000.0\";s:6:\"premio\";s:4:\"0.38\";}i:7;a:3:{s:15:\"codigoCobertura\";i:1;s:7:\"capital\";s:6:\"4000.0\";s:6:\"premio\";s:4:\"0.58\";}i:8;a:3:{s:15:\"codigoCobertura\";i:1;s:7:\"capital\";s:6:\"4000.0\";s:6:\"premio\";s:4:\"4.92\";}i:9;a:3:{s:15:\"codigoCobertura\";i:1;s:7:\"capital\";s:6:\"4000.0\";s:6:\"premio\";s:4:\"7.38\";}i:10;a:3:{s:15:\"codigoCobertura\";i:1;s:7:\"capital\";s:7:\"80000.0\";s:6:\"premio\";s:5:\"18.36\";}i:11;a:3:{s:15:\"codigoCobertura\";i:1;s:7:\"capital\";s:7:\"80000.0\";s:6:\"premio\";s:5:\"27.53\";}i:12;a:3:{s:15:\"codigoCobertura\";i:1;s:7:\"capital\";s:5:\"400.0\";s:6:\"premio\";s:4:\"0.09\";}i:13;a:3:{s:15:\"codigoCobertura\";i:1;s:7:\"capital\";s:5:\"400.0\";s:6:\"premio\";s:4:\"0.14\";}i:14;a:3:{s:15:\"codigoCobertura\";i:1;s:7:\"capital\";s:6:\"1000.0\";s:6:\"premio\";s:4:\"0.19\";}i:15;a:3:{s:15:\"codigoCobertura\";i:1;s:7:\"capital\";s:6:\"1000.0\";s:6:\"premio\";s:4:\"0.28\";}i:16;a:3:{s:15:\"codigoCobertura\";i:1;s:7:\"capital\";s:6:\"4000.0\";s:6:\"premio\";s:4:\"0.23\";}i:17;a:3:{s:15:\"codigoCobertura\";i:1;s:7:\"capital\";s:6:\"4000.0\";s:6:\"premio\";s:4:\"0.34\";}i:18;a:3:{s:15:\"codigoCobertura\";i:1;s:7:\"capital\";s:6:\"1000.0\";s:6:\"premio\";s:4:\"0.06\";}i:19;a:3:{s:15:\"codigoCobertura\";i:1;s:7:\"capital\";s:6:\"1000.0\";s:6:\"premio\";s:4:\"0.09\";}i:20;a:3:{s:15:\"codigoCobertura\";i:1;s:7:\"capital\";s:6:\"4000.0\";s:6:\"premio\";s:4:\"1.07\";}i:21;a:3:{s:15:\"codigoCobertura\";i:1;s:7:\"capital\";s:6:\"4000.0\";s:6:\"premio\";s:4:\"1.62\";}i:22;a:3:{s:15:\"codigoCobertura\";i:1;s:7:\"capital\";s:6:\"2000.0\";s:6:\"premio\";s:4:\"1.95\";}i:23;a:3:{s:15:\"codigoCobertura\";i:1;s:7:\"capital\";s:6:\"2000.0\";s:6:\"premio\";s:4:\"2.93\";}i:24;a:3:{s:15:\"codigoCobertura\";i:1;s:7:\"capital\";s:6:\"8000.0\";s:6:\"premio\";s:4:\"0.95\";}i:25;a:3:{s:15:\"codigoCobertura\";i:1;s:7:\"capital\";s:6:\"8000.0\";s:6:\"premio\";s:4:\"1.44\";}i:26;a:3:{s:15:\"codigoCobertura\";i:1;s:7:\"capital\";s:6:\"6000.0\";s:6:\"premio\";s:4:\"1.51\";}i:27;a:3:{s:15:\"codigoCobertura\";i:1;s:7:\"capital\";s:6:\"6000.0\";s:6:\"premio\";s:4:\"2.25\";}i:28;a:3:{s:15:\"codigoCobertura\";i:1;s:7:\"capital\";s:7:\"80000.0\";s:6:\"premio\";s:4:\"0.97\";}i:29;a:3:{s:15:\"codigoCobertura\";i:1;s:7:\"capital\";s:7:\"80000.0\";s:6:\"premio\";s:4:\"1.47\";}i:30;a:3:{s:15:\"codigoCobertura\";i:1;s:7:\"capital\";s:7:\"80000.0\";s:6:\"premio\";s:4:\"3.67\";}i:31;a:3:{s:15:\"codigoCobertura\";i:1;s:7:\"capital\";s:7:\"80000.0\";s:6:\"premio\";s:4:\"5.52\";}i:32;a:3:{s:15:\"codigoCobertura\";i:1;s:7:\"capital\";s:7:\"80000.0\";s:6:\"premio\";s:4:\"7.79\";}i:33;a:3:{s:15:\"codigoCobertura\";i:1;s:7:\"capital\";s:7:\"80000.0\";s:6:\"premio\";s:5:\"11.68\";}i:34;a:3:{s:15:\"codigoCobertura\";i:1;s:7:\"capital\";s:6:\"7000.0\";s:6:\"premio\";s:4:\"0.21\";}i:35;a:3:{s:15:\"codigoCobertura\";i:1;s:7:\"capital\";s:6:\"7000.0\";s:6:\"premio\";s:4:\"0.32\";}}','a:4:{i:0;a:2:{s:13:\"codigoServico\";i:4;s:5:\"custo\";s:3:\"0.0\";}i:1;a:2:{s:13:\"codigoServico\";i:2;s:5:\"custo\";s:3:\"0.0\";}i:2;a:2:{s:13:\"codigoServico\";i:3;s:5:\"custo\";s:3:\"0.0\";}i:3;a:2:{s:13:\"codigoServico\";i:1;s:5:\"custo\";s:3:\"0.0\";}}',1,45.36,68.07,45.36,2.27,'7a8583',NULL,1,NULL,NULL,NULL,'2016-12-05 11:54:47','2016-12-05 12:41:06',1,3,'2016-12-05','','127.0.0.1',0,NULL),(9,'Ivo Santos','(11) 22112211','is.ivosantos@gmail.com',220,5,'a:40:{i:0;a:3:{s:15:\"codigoCobertura\";i:140;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:4:\"7.65\";}i:1;a:3:{s:15:\"codigoCobertura\";i:140;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:5:\"11.47\";}i:2;a:3:{s:15:\"codigoCobertura\";i:421;s:7:\"capital\";s:5:\"300.0\";s:6:\"premio\";s:4:\"0.08\";}i:3;a:3:{s:15:\"codigoCobertura\";i:421;s:7:\"capital\";s:5:\"300.0\";s:6:\"premio\";s:4:\"0.11\";}i:4;a:3:{s:15:\"codigoCobertura\";i:416;s:7:\"capital\";s:5:\"500.0\";s:6:\"premio\";s:3:\"0.1\";}i:5;a:3:{s:15:\"codigoCobertura\";i:416;s:7:\"capital\";s:5:\"500.0\";s:6:\"premio\";s:4:\"0.15\";}i:6;a:3:{s:15:\"codigoCobertura\";i:417;s:7:\"capital\";s:5:\"700.0\";s:6:\"premio\";s:4:\"0.04\";}i:7;a:3:{s:15:\"codigoCobertura\";i:417;s:7:\"capital\";s:5:\"700.0\";s:6:\"premio\";s:4:\"0.07\";}i:8;a:3:{s:15:\"codigoCobertura\";i:418;s:7:\"capital\";s:5:\"500.0\";s:6:\"premio\";s:4:\"0.03\";}i:9;a:3:{s:15:\"codigoCobertura\";i:418;s:7:\"capital\";s:5:\"500.0\";s:6:\"premio\";s:4:\"0.05\";}i:10;a:3:{s:15:\"codigoCobertura\";i:420;s:7:\"capital\";s:6:\"1400.0\";s:6:\"premio\";s:4:\"0.42\";}i:11;a:3:{s:15:\"codigoCobertura\";i:420;s:7:\"capital\";s:6:\"1400.0\";s:6:\"premio\";s:4:\"0.63\";}i:12;a:3:{s:15:\"codigoCobertura\";i:415;s:7:\"capital\";s:5:\"700.0\";s:6:\"premio\";s:4:\"0.76\";}i:13;a:3:{s:15:\"codigoCobertura\";i:415;s:7:\"capital\";s:5:\"700.0\";s:6:\"premio\";s:4:\"1.13\";}i:14;a:3:{s:15:\"codigoCobertura\";i:419;s:7:\"capital\";s:6:\"2800.0\";s:6:\"premio\";s:4:\"0.37\";}i:15;a:3:{s:15:\"codigoCobertura\";i:419;s:7:\"capital\";s:6:\"2800.0\";s:6:\"premio\";s:4:\"0.55\";}i:16;a:3:{s:15:\"codigoCobertura\";i:422;s:7:\"capital\";s:6:\"1000.0\";s:6:\"premio\";s:4:\"0.28\";}i:17;a:3:{s:15:\"codigoCobertura\";i:422;s:7:\"capital\";s:6:\"1000.0\";s:6:\"premio\";s:4:\"0.42\";}i:18;a:3:{s:15:\"codigoCobertura\";i:143;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:4:\"0.41\";}i:19;a:3:{s:15:\"codigoCobertura\";i:143;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:4:\"0.61\";}i:20;a:3:{s:15:\"codigoCobertura\";i:142;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:4:\"1.53\";}i:21;a:3:{s:15:\"codigoCobertura\";i:142;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:3:\"2.3\";}i:22;a:3:{s:15:\"codigoCobertura\";i:141;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:4:\"3.24\";}i:23;a:3:{s:15:\"codigoCobertura\";i:141;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:4:\"4.87\";}i:24;a:3:{s:15:\"codigoCobertura\";i:42;s:7:\"capital\";s:6:\"7000.0\";s:6:\"premio\";s:4:\"0.21\";}i:25;a:3:{s:15:\"codigoCobertura\";i:42;s:7:\"capital\";s:6:\"7000.0\";s:6:\"premio\";s:4:\"0.32\";}i:26;a:3:{s:15:\"codigoCobertura\";i:412;s:7:\"capital\";s:8:\"100000.0\";s:6:\"premio\";s:4:\"1.18\";}i:27;a:3:{s:15:\"codigoCobertura\";i:412;s:7:\"capital\";s:8:\"100000.0\";s:6:\"premio\";s:4:\"1.78\";}i:28;a:3:{s:15:\"codigoCobertura\";i:413;s:7:\"capital\";s:8:\"100000.0\";s:6:\"premio\";s:4:\"0.29\";}i:29;a:3:{s:15:\"codigoCobertura\";i:413;s:7:\"capital\";s:8:\"100000.0\";s:6:\"premio\";s:4:\"0.44\";}i:30;a:3:{s:15:\"codigoCobertura\";i:122;s:7:\"capital\";s:6:\"3000.0\";s:6:\"premio\";s:4:\"0.04\";}i:31;a:3:{s:15:\"codigoCobertura\";i:122;s:7:\"capital\";s:6:\"3000.0\";s:6:\"premio\";s:4:\"0.06\";}i:32;a:3:{s:15:\"codigoCobertura\";i:424;s:7:\"capital\";s:6:\"3000.0\";s:6:\"premio\";s:4:\"0.02\";}i:33;a:3:{s:15:\"codigoCobertura\";i:424;s:7:\"capital\";s:6:\"3000.0\";s:6:\"premio\";s:4:\"0.03\";}i:34;a:3:{s:15:\"codigoCobertura\";i:425;s:7:\"capital\";s:6:\"3000.0\";s:6:\"premio\";s:4:\"0.02\";}i:35;a:3:{s:15:\"codigoCobertura\";i:425;s:7:\"capital\";s:6:\"3000.0\";s:6:\"premio\";s:4:\"0.03\";}i:36;a:3:{s:15:\"codigoCobertura\";i:145;s:7:\"capital\";s:6:\"2000.0\";s:6:\"premio\";s:4:\"0.19\";}i:37;a:3:{s:15:\"codigoCobertura\";i:145;s:7:\"capital\";s:6:\"2000.0\";s:6:\"premio\";s:4:\"0.29\";}i:38;a:3:{s:15:\"codigoCobertura\";i:414;s:7:\"capital\";s:6:\"2000.0\";s:6:\"premio\";s:4:\"2.46\";}i:39;a:3:{s:15:\"codigoCobertura\";i:414;s:7:\"capital\";s:6:\"2000.0\";s:6:\"premio\";s:4:\"3.68\";}}','a:4:{i:0;a:2:{s:13:\"codigoServico\";i:4;s:5:\"custo\";s:3:\"0.0\";}i:1;a:2:{s:13:\"codigoServico\";i:2;s:5:\"custo\";s:3:\"0.0\";}i:2;a:2:{s:13:\"codigoServico\";i:3;s:5:\"custo\";s:3:\"0.0\";}i:3;a:2:{s:13:\"codigoServico\";i:1;s:5:\"custo\";s:3:\"0.0\";}}',1,19.32,28.99,19.32,0.97,'6f49a9',NULL,1,NULL,NULL,NULL,'2016-12-05 12:43:16','2016-12-05 20:40:52',1,3,'2016-12-05','','127.0.0.1',0,NULL),(10,'Ivo Santos','(11) 22112211','is.ivosantos@gmail.com',225,6,'a:34:{i:0;a:3:{s:15:\"codigoCobertura\";i:140;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:4:\"7.86\";}i:1;a:3:{s:15:\"codigoCobertura\";i:140;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:5:\"11.79\";}i:2;a:3:{s:15:\"codigoCobertura\";i:421;s:7:\"capital\";s:5:\"300.0\";s:6:\"premio\";s:4:\"0.08\";}i:3;a:3:{s:15:\"codigoCobertura\";i:421;s:7:\"capital\";s:5:\"300.0\";s:6:\"premio\";s:4:\"0.12\";}i:4;a:3:{s:15:\"codigoCobertura\";i:416;s:7:\"capital\";s:5:\"500.0\";s:6:\"premio\";s:4:\"0.11\";}i:5;a:3:{s:15:\"codigoCobertura\";i:416;s:7:\"capital\";s:5:\"500.0\";s:6:\"premio\";s:4:\"0.16\";}i:6;a:3:{s:15:\"codigoCobertura\";i:417;s:7:\"capital\";s:5:\"300.0\";s:6:\"premio\";s:4:\"0.02\";}i:7;a:3:{s:15:\"codigoCobertura\";i:417;s:7:\"capital\";s:5:\"300.0\";s:6:\"premio\";s:4:\"0.03\";}i:8;a:3:{s:15:\"codigoCobertura\";i:418;s:7:\"capital\";s:5:\"500.0\";s:6:\"premio\";s:4:\"0.03\";}i:9;a:3:{s:15:\"codigoCobertura\";i:418;s:7:\"capital\";s:5:\"500.0\";s:6:\"premio\";s:4:\"0.05\";}i:10;a:3:{s:15:\"codigoCobertura\";i:420;s:7:\"capital\";s:5:\"700.0\";s:6:\"premio\";s:4:\"0.21\";}i:11;a:3:{s:15:\"codigoCobertura\";i:420;s:7:\"capital\";s:5:\"700.0\";s:6:\"premio\";s:4:\"0.32\";}i:12;a:3:{s:15:\"codigoCobertura\";i:415;s:7:\"capital\";s:5:\"300.0\";s:6:\"premio\";s:4:\"0.33\";}i:13;a:3:{s:15:\"codigoCobertura\";i:415;s:7:\"capital\";s:5:\"300.0\";s:6:\"premio\";s:3:\"0.5\";}i:14;a:3:{s:15:\"codigoCobertura\";i:419;s:7:\"capital\";s:6:\"1400.0\";s:6:\"premio\";s:4:\"0.19\";}i:15;a:3:{s:15:\"codigoCobertura\";i:419;s:7:\"capital\";s:6:\"1400.0\";s:6:\"premio\";s:4:\"0.29\";}i:16;a:3:{s:15:\"codigoCobertura\";i:422;s:7:\"capital\";s:6:\"1000.0\";s:6:\"premio\";s:4:\"0.28\";}i:17;a:3:{s:15:\"codigoCobertura\";i:422;s:7:\"capital\";s:6:\"1000.0\";s:6:\"premio\";s:4:\"0.43\";}i:18;a:3:{s:15:\"codigoCobertura\";i:143;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:4:\"0.42\";}i:19;a:3:{s:15:\"codigoCobertura\";i:143;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:4:\"0.62\";}i:20;a:3:{s:15:\"codigoCobertura\";i:142;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:4:\"1.58\";}i:21;a:3:{s:15:\"codigoCobertura\";i:142;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:4:\"2.36\";}i:22;a:3:{s:15:\"codigoCobertura\";i:141;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:4:\"3.33\";}i:23;a:3:{s:15:\"codigoCobertura\";i:141;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:4:\"5.01\";}i:24;a:3:{s:15:\"codigoCobertura\";i:412;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:4:\"0.35\";}i:25;a:3:{s:15:\"codigoCobertura\";i:412;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:4:\"0.53\";}i:26;a:3:{s:15:\"codigoCobertura\";i:413;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:4:\"0.09\";}i:27;a:3:{s:15:\"codigoCobertura\";i:413;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:4:\"0.13\";}i:28;a:3:{s:15:\"codigoCobertura\";i:122;s:7:\"capital\";s:6:\"1500.0\";s:6:\"premio\";s:4:\"0.02\";}i:29;a:3:{s:15:\"codigoCobertura\";i:122;s:7:\"capital\";s:6:\"1500.0\";s:6:\"premio\";s:4:\"0.03\";}i:30;a:3:{s:15:\"codigoCobertura\";i:424;s:7:\"capital\";s:6:\"1500.0\";s:6:\"premio\";s:4:\"0.01\";}i:31;a:3:{s:15:\"codigoCobertura\";i:424;s:7:\"capital\";s:6:\"1500.0\";s:6:\"premio\";s:4:\"0.01\";}i:32;a:3:{s:15:\"codigoCobertura\";i:425;s:7:\"capital\";s:6:\"1500.0\";s:6:\"premio\";s:4:\"0.01\";}i:33;a:3:{s:15:\"codigoCobertura\";i:425;s:7:\"capital\";s:6:\"1500.0\";s:6:\"premio\";s:4:\"0.01\";}}','a:3:{i:0;a:2:{s:13:\"codigoServico\";i:2;s:5:\"custo\";s:3:\"0.0\";}i:1;a:2:{s:13:\"codigoServico\";i:3;s:5:\"custo\";s:3:\"0.0\";}i:2;a:2:{s:13:\"codigoServico\";i:1;s:5:\"custo\";s:3:\"0.0\";}}',1,14.92,22.39,14.92,0.75,'c476fe',NULL,1,NULL,NULL,NULL,'2016-12-05 20:38:52','2016-12-05 20:40:45',1,3,NULL,NULL,'127.0.0.1',0,NULL),(11,'Ivo Santos','(11) 22112211','is.ivosantos@gmail.com',226,6,'a:30:{i:0;a:3:{s:15:\"codigoCobertura\";i:140;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:4:\"7.65\";}i:1;a:3:{s:15:\"codigoCobertura\";i:140;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:5:\"11.47\";}i:2;a:3:{s:15:\"codigoCobertura\";i:421;s:7:\"capital\";s:5:\"300.0\";s:6:\"premio\";s:4:\"0.08\";}i:3;a:3:{s:15:\"codigoCobertura\";i:421;s:7:\"capital\";s:5:\"300.0\";s:6:\"premio\";s:4:\"0.11\";}i:4;a:3:{s:15:\"codigoCobertura\";i:416;s:7:\"capital\";s:5:\"500.0\";s:6:\"premio\";s:3:\"0.1\";}i:5;a:3:{s:15:\"codigoCobertura\";i:416;s:7:\"capital\";s:5:\"500.0\";s:6:\"premio\";s:4:\"0.15\";}i:6;a:3:{s:15:\"codigoCobertura\";i:417;s:7:\"capital\";s:5:\"300.0\";s:6:\"premio\";s:4:\"0.02\";}i:7;a:3:{s:15:\"codigoCobertura\";i:417;s:7:\"capital\";s:5:\"300.0\";s:6:\"premio\";s:4:\"0.03\";}i:8;a:3:{s:15:\"codigoCobertura\";i:418;s:7:\"capital\";s:5:\"500.0\";s:6:\"premio\";s:4:\"0.03\";}i:9;a:3:{s:15:\"codigoCobertura\";i:418;s:7:\"capital\";s:5:\"500.0\";s:6:\"premio\";s:4:\"0.05\";}i:10;a:3:{s:15:\"codigoCobertura\";i:420;s:7:\"capital\";s:5:\"700.0\";s:6:\"premio\";s:4:\"0.21\";}i:11;a:3:{s:15:\"codigoCobertura\";i:420;s:7:\"capital\";s:5:\"700.0\";s:6:\"premio\";s:4:\"0.31\";}i:12;a:3:{s:15:\"codigoCobertura\";i:415;s:7:\"capital\";s:5:\"300.0\";s:6:\"premio\";s:4:\"0.32\";}i:13;a:3:{s:15:\"codigoCobertura\";i:415;s:7:\"capital\";s:5:\"300.0\";s:6:\"premio\";s:4:\"0.49\";}i:14;a:3:{s:15:\"codigoCobertura\";i:419;s:7:\"capital\";s:6:\"1400.0\";s:6:\"premio\";s:4:\"0.18\";}i:15;a:3:{s:15:\"codigoCobertura\";i:419;s:7:\"capital\";s:6:\"1400.0\";s:6:\"premio\";s:4:\"0.28\";}i:16;a:3:{s:15:\"codigoCobertura\";i:422;s:7:\"capital\";s:6:\"1000.0\";s:6:\"premio\";s:4:\"0.28\";}i:17;a:3:{s:15:\"codigoCobertura\";i:422;s:7:\"capital\";s:6:\"1000.0\";s:6:\"premio\";s:4:\"0.42\";}i:18;a:3:{s:15:\"codigoCobertura\";i:143;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:4:\"0.41\";}i:19;a:3:{s:15:\"codigoCobertura\";i:143;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:4:\"0.61\";}i:20;a:3:{s:15:\"codigoCobertura\";i:142;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:4:\"1.53\";}i:21;a:3:{s:15:\"codigoCobertura\";i:142;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:3:\"2.3\";}i:22;a:3:{s:15:\"codigoCobertura\";i:141;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:4:\"3.24\";}i:23;a:3:{s:15:\"codigoCobertura\";i:141;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:4:\"4.87\";}i:24;a:3:{s:15:\"codigoCobertura\";i:412;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:4:\"0.35\";}i:25;a:3:{s:15:\"codigoCobertura\";i:412;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:4:\"0.53\";}i:26;a:3:{s:15:\"codigoCobertura\";i:413;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:4:\"0.09\";}i:27;a:3:{s:15:\"codigoCobertura\";i:413;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:4:\"0.13\";}i:28;a:3:{s:15:\"codigoCobertura\";i:122;s:7:\"capital\";s:6:\"1500.0\";s:6:\"premio\";s:4:\"0.02\";}i:29;a:3:{s:15:\"codigoCobertura\";i:122;s:7:\"capital\";s:6:\"1500.0\";s:6:\"premio\";s:4:\"0.03\";}}','a:3:{i:0;a:2:{s:13:\"codigoServico\";i:2;s:5:\"custo\";s:3:\"0.0\";}i:1;a:2:{s:13:\"codigoServico\";i:3;s:5:\"custo\";s:3:\"0.0\";}i:2;a:2:{s:13:\"codigoServico\";i:1;s:5:\"custo\";s:3:\"0.0\";}}',1,14.51,21.78,14.51,0.73,'44ffeb',NULL,1,NULL,NULL,NULL,'2016-12-08 15:58:35','2016-12-08 15:59:12',NULL,2,NULL,NULL,'127.0.0.1',0,NULL),(12,'',NULL,'',228,5,'a:40:{i:0;a:3:{s:15:\"codigoCobertura\";i:140;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:5:\"11.47\";}i:1;a:3:{s:15:\"codigoCobertura\";i:140;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:4:\"7.65\";}i:2;a:3:{s:15:\"codigoCobertura\";i:421;s:7:\"capital\";s:5:\"300.0\";s:6:\"premio\";s:4:\"0.11\";}i:3;a:3:{s:15:\"codigoCobertura\";i:421;s:7:\"capital\";s:5:\"300.0\";s:6:\"premio\";s:4:\"0.08\";}i:4;a:3:{s:15:\"codigoCobertura\";i:416;s:7:\"capital\";s:5:\"500.0\";s:6:\"premio\";s:4:\"0.15\";}i:5;a:3:{s:15:\"codigoCobertura\";i:416;s:7:\"capital\";s:5:\"500.0\";s:6:\"premio\";s:3:\"0.1\";}i:6;a:3:{s:15:\"codigoCobertura\";i:417;s:7:\"capital\";s:5:\"700.0\";s:6:\"premio\";s:4:\"0.07\";}i:7;a:3:{s:15:\"codigoCobertura\";i:417;s:7:\"capital\";s:5:\"700.0\";s:6:\"premio\";s:4:\"0.04\";}i:8;a:3:{s:15:\"codigoCobertura\";i:418;s:7:\"capital\";s:5:\"500.0\";s:6:\"premio\";s:4:\"0.05\";}i:9;a:3:{s:15:\"codigoCobertura\";i:418;s:7:\"capital\";s:5:\"500.0\";s:6:\"premio\";s:4:\"0.03\";}i:10;a:3:{s:15:\"codigoCobertura\";i:420;s:7:\"capital\";s:6:\"1400.0\";s:6:\"premio\";s:4:\"0.63\";}i:11;a:3:{s:15:\"codigoCobertura\";i:420;s:7:\"capital\";s:6:\"1400.0\";s:6:\"premio\";s:4:\"0.42\";}i:12;a:3:{s:15:\"codigoCobertura\";i:415;s:7:\"capital\";s:5:\"700.0\";s:6:\"premio\";s:4:\"1.13\";}i:13;a:3:{s:15:\"codigoCobertura\";i:415;s:7:\"capital\";s:5:\"700.0\";s:6:\"premio\";s:4:\"0.76\";}i:14;a:3:{s:15:\"codigoCobertura\";i:419;s:7:\"capital\";s:6:\"2800.0\";s:6:\"premio\";s:4:\"0.55\";}i:15;a:3:{s:15:\"codigoCobertura\";i:419;s:7:\"capital\";s:6:\"2800.0\";s:6:\"premio\";s:4:\"0.37\";}i:16;a:3:{s:15:\"codigoCobertura\";i:422;s:7:\"capital\";s:6:\"1000.0\";s:6:\"premio\";s:4:\"0.42\";}i:17;a:3:{s:15:\"codigoCobertura\";i:422;s:7:\"capital\";s:6:\"1000.0\";s:6:\"premio\";s:4:\"0.28\";}i:18;a:3:{s:15:\"codigoCobertura\";i:143;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:4:\"0.61\";}i:19;a:3:{s:15:\"codigoCobertura\";i:143;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:4:\"0.41\";}i:20;a:3:{s:15:\"codigoCobertura\";i:142;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:3:\"2.3\";}i:21;a:3:{s:15:\"codigoCobertura\";i:142;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:4:\"1.53\";}i:22;a:3:{s:15:\"codigoCobertura\";i:141;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:4:\"4.87\";}i:23;a:3:{s:15:\"codigoCobertura\";i:141;s:7:\"capital\";s:7:\"30000.0\";s:6:\"premio\";s:4:\"3.24\";}i:24;a:3:{s:15:\"codigoCobertura\";i:42;s:7:\"capital\";s:6:\"7000.0\";s:6:\"premio\";s:4:\"0.32\";}i:25;a:3:{s:15:\"codigoCobertura\";i:42;s:7:\"capital\";s:6:\"7000.0\";s:6:\"premio\";s:4:\"0.21\";}i:26;a:3:{s:15:\"codigoCobertura\";i:412;s:7:\"capital\";s:8:\"100000.0\";s:6:\"premio\";s:4:\"1.78\";}i:27;a:3:{s:15:\"codigoCobertura\";i:412;s:7:\"capital\";s:8:\"100000.0\";s:6:\"premio\";s:4:\"1.18\";}i:28;a:3:{s:15:\"codigoCobertura\";i:413;s:7:\"capital\";s:8:\"100000.0\";s:6:\"premio\";s:4:\"0.44\";}i:29;a:3:{s:15:\"codigoCobertura\";i:413;s:7:\"capital\";s:8:\"100000.0\";s:6:\"premio\";s:4:\"0.29\";}i:30;a:3:{s:15:\"codigoCobertura\";i:122;s:7:\"capital\";s:6:\"3000.0\";s:6:\"premio\";s:4:\"0.06\";}i:31;a:3:{s:15:\"codigoCobertura\";i:122;s:7:\"capital\";s:6:\"3000.0\";s:6:\"premio\";s:4:\"0.04\";}i:32;a:3:{s:15:\"codigoCobertura\";i:424;s:7:\"capital\";s:6:\"3000.0\";s:6:\"premio\";s:4:\"0.03\";}i:33;a:3:{s:15:\"codigoCobertura\";i:424;s:7:\"capital\";s:6:\"3000.0\";s:6:\"premio\";s:4:\"0.02\";}i:34;a:3:{s:15:\"codigoCobertura\";i:425;s:7:\"capital\";s:6:\"3000.0\";s:6:\"premio\";s:4:\"0.03\";}i:35;a:3:{s:15:\"codigoCobertura\";i:425;s:7:\"capital\";s:6:\"3000.0\";s:6:\"premio\";s:4:\"0.02\";}i:36;a:3:{s:15:\"codigoCobertura\";i:145;s:7:\"capital\";s:6:\"2000.0\";s:6:\"premio\";s:4:\"0.29\";}i:37;a:3:{s:15:\"codigoCobertura\";i:145;s:7:\"capital\";s:6:\"2000.0\";s:6:\"premio\";s:4:\"0.19\";}i:38;a:3:{s:15:\"codigoCobertura\";i:414;s:7:\"capital\";s:6:\"2000.0\";s:6:\"premio\";s:4:\"3.68\";}i:39;a:3:{s:15:\"codigoCobertura\";i:414;s:7:\"capital\";s:6:\"2000.0\";s:6:\"premio\";s:4:\"2.46\";}}','a:4:{i:0;a:2:{s:13:\"codigoServico\";i:4;s:5:\"custo\";s:3:\"0.0\";}i:1;a:2:{s:13:\"codigoServico\";i:2;s:5:\"custo\";s:3:\"0.0\";}i:2;a:2:{s:13:\"codigoServico\";i:3;s:5:\"custo\";s:3:\"0.0\";}i:3;a:2:{s:13:\"codigoServico\";i:1;s:5:\"custo\";s:3:\"0.0\";}}',2,19.32,28.99,NULL,NULL,'fc8023',NULL,NULL,NULL,NULL,NULL,'2016-12-08 16:07:59','2016-12-08 16:07:59',NULL,NULL,NULL,NULL,'127.0.0.1',0,NULL);

/*Table structure for table `vendas_boletos` */

DROP TABLE IF EXISTS `vendas_boletos`;

CREATE TABLE `vendas_boletos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `venda_id` int(11) DEFAULT NULL,
  `empresa_id` int(11) DEFAULT '1',
  `cliente_nome` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `cliente_email` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `campo_livre_sacado` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `descricao` text COLLATE latin1_general_ci,
  `data_vencimento` date DEFAULT NULL,
  `valor` float DEFAULT NULL,
  `status` int(1) DEFAULT '2' COMMENT '1= pago 2 = aguardando pagamento 3=cancelado',
  `funcionario_id` int(11) DEFAULT NULL,
  `data_pagamento` date DEFAULT NULL,
  `data_cadastro` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `venda_id` (`venda_id`),
  CONSTRAINT `vendas_boletos_ibfk_1` FOREIGN KEY (`venda_id`) REFERENCES `vendas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

/*Data for the table `vendas_boletos` */

insert  into `vendas_boletos`(`id`,`venda_id`,`empresa_id`,`cliente_nome`,`cliente_email`,`campo_livre_sacado`,`descricao`,`data_vencimento`,`valor`,`status`,`funcionario_id`,`data_pagamento`,`data_cadastro`) values (1,4,1,'Ivo Santos','is.ivosantos@gmail.com','','','2016-12-07',65.64,2,NULL,NULL,'2016-12-04 16:05:19'),(2,6,1,'Ivo Santos','is.ivosantos@gmail.com','','','2016-12-07',14.71,2,NULL,NULL,'2016-12-04 17:52:24'),(3,8,1,'Ivo Santos','is.ivosantos@gmail.com','','','2016-12-08',43.09,1,1,'2016-12-05','2016-12-05 12:05:32'),(4,9,1,'Ivo Santos','is.ivosantos@gmail.com','','','2016-12-08',18.35,1,1,'2016-12-05','2016-12-05 12:43:32'),(5,10,1,'Ivo Santos','is.ivosantos@gmail.com','','','2016-12-08',14.17,2,NULL,NULL,'2016-12-05 20:40:00'),(6,11,1,'Ivo Santos','is.ivosantos@gmail.com','','','2016-12-11',13.78,2,NULL,NULL,'2016-12-08 15:59:16');

/*Table structure for table `vendas_cron` */

DROP TABLE IF EXISTS `vendas_cron`;

CREATE TABLE `vendas_cron` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `venda_id` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT '2' COMMENT '1 = gerado 2 = disponivel 3 = erro 4 = rodando',
  `data_cadastro` datetime DEFAULT NULL,
  `data_modificacao` datetime DEFAULT NULL,
  `comentario` text,
  PRIMARY KEY (`id`),
  KEY `venda_id` (`venda_id`),
  CONSTRAINT `vendas_cron_ibfk_1` FOREIGN KEY (`venda_id`) REFERENCES `vendas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `vendas_cron` */

/*Table structure for table `vouchers` */

DROP TABLE IF EXISTS `vouchers`;

CREATE TABLE `vouchers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `venda_id` int(11) DEFAULT NULL,
  `cliente_id` int(11) DEFAULT NULL,
  `seguradora_numero_apolice` int(11) DEFAULT NULL,
  `seguradora_numero_sorte` int(11) DEFAULT NULL,
  `seguradora_numero_compra` int(11) DEFAULT NULL,
  `data_cadastro` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `venda_id` (`venda_id`),
  KEY `cliente_id` (`cliente_id`),
  CONSTRAINT `vouchers_ibfk_1` FOREIGN KEY (`venda_id`) REFERENCES `vendas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `vouchers_ibfk_2` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `vouchers` */

insert  into `vouchers`(`id`,`venda_id`,`cliente_id`,`seguradora_numero_apolice`,`seguradora_numero_sorte`,`seguradora_numero_compra`,`data_cadastro`) values (1,9,13,260872,20877,11509,NULL);

/*Table structure for table `ws_users` */

DROP TABLE IF EXISTS `ws_users`;

CREATE TABLE `ws_users` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `user` varchar(20) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `ws_users` */

insert  into `ws_users`(`id`,`user`,`password`) values (1,'ws_segurocerto_user','957b0e0fc8997db58df90b87a17be424806d6d8d');

/*Table structure for table `xmls_cielo` */

DROP TABLE IF EXISTS `xmls_cielo`;

CREATE TABLE `xmls_cielo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `request` text,
  `response` text,
  `operacao` varchar(255) DEFAULT NULL,
  `origem` varchar(255) DEFAULT NULL,
  `data_pagamento` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `xmls_cielo` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
